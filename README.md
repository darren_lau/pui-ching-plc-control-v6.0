﻿# README (`PooiToIoT_V1` v1_180924) #

- This readme will guide you to run the `PooiToIoT_V1` along with required system settings.

### What is this repository for? ###

- Smart School as an IoT project in Pui Ching / Pooi To.
- Note that `PuiChingV4` = `PooiToV1`.

### How do I get set up? ###
* Clone the directory
* Extract any archived/compressed files into directory with the same file name. Here is the most common example (Read `./File_Description.md` for details):
```
cd ./ProjectName
tar -xzvf resources_pun.tar.gz
```
* Read available documents before action:
	* `./doc/API_description.md`: Description of avaliable HTTP APIs.
	* `./doc/change_logs.md`: Change Logs of this project.
	* `./doc/cmd_list.md`: Useful command list for this project.
	* `./doc/dev_notes.md`: Any development related topics. Usually contains high level discussions.
	* `./doc/File_Description.md`: Brief summary of *EACH FILE*. Very long but very useful.
	* `./doc/pm2_notes.md`: Useful commands to let PM2 monitor all the running process.
	* `./doc/Site's note.txt`: Some on-site notes on configuration.
	* `./doc/scheduled_tasks.md`: Scheduled tasks to (run by `crontab -e`).
	* `./README.md`: This file.

### How to start the modules ###

* Install NodeJS. Make sure the version is `8.0` or higher. (LTS version is recommended)
* Install Dependencies through `npm install` in directory `./ProjectName`.
* Make sure the contents are listed correctly in `./ProjectName/config/*` and `./ProjectName/mode/src/server/modules/server_config.js`.
* Make sure the destination of data feed has been set into this host. (PLC / Cool Master / Delight controls)
* Make directory `./ProjectName/logs` for system logs.
* Preparation before starting the servers:
	- Install `GruntJS` and then `grunt-replace` (use option `--save-dev`)
	- i.e. `npm install grunt --save-dev` and then `npm install grunt-replace --save-dev`
	- run `Gruntfile.js` as `grunt build --mode=dev` , or `...=prod`, or directly modify file `./config/server/server_config.xml`
	- `npm install` to install missing modules.
	- Check if all files in `mode/src` are correctly compiled. `@@` prefix should not exist in all files.
	- Open DB server
	- Run the scripts to see the effect
	- In sequence: `mosca_custom` > `device_control` > `express_host`
* How to make soft links pointing to the actual directories:
	- Window: `mklink /J dest source`
	- Linux: `ln -s source/* dest`
* Follow `./pm2_notes.md` to start the following processes:
	* `node ./PooiToIoT_V1/prod/src/server/express_host.js` (Web console with telnet control page)
	* `node ./PooiToIoT_V1/prod/src/server/device_control.js [client_id]` (Device controller)
	* `node ./PooiToIoT_V1/prod/src/server/mosca_custom.js` (MQTT broker with embedded logic)
	* `node ./PooiToIoT_V1/prod/src/server/mqtt_slack.js` (Alarm monitor)
	* `node ./PooiToIoT_V1/prod/src/server/mqtt_spambot.js` (Sensor emulator)
	* `node ./PooiToIoT_V1/prod/src/server/slack_bot.js` (Watch for abnormal state)

### How to access the web console ###
- Goto http://host:9997/
- Login as `system_admin`/`password` (others may have been changed in site. Default is `user`/`1234` and `admin`/`password`).
- See if the logs are rotating without errors.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin (michael_soong)
* Other community or team contact (darren_lau)