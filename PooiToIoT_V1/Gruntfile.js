var util = require('util');
var fs = require('fs');
var xml2js = require('xml2js');
var mkdirp = require('mkdirp');

var XML_File = __dirname + "/config/server/";

var server_config = {};

var XML_parser = new xml2js.Parser();

var convert_to_map = function (xml_obj) {
    // Before: { name: [ 'Super' ], Surname: [ 'Man' ], age: [ '23' ] }
    // After: { name: 'Super', Surname: 'Man', age: '23' }
    var result = {};
    for (var key in xml_obj) {
        if (xml_obj.hasOwnProperty(key)) {
            //console.log(key + " -> " + p[key]);
            result[key] = xml_obj[key][0];
        } else {
            //For potential use
        }
    }
    return result;
};

module.exports = function (grunt) {
    grunt.loadNpmTasks('grunt-replace');
    grunt.initConfig({
        replace: {
            dist: {
                options: {
                    patterns: [
                    { json: function (done) { done(server_config); } }
                    ]
                },
                files: [
                {
                    expand: true,
                    flatten: false,
                    src: ['src/**/*.js', 'src/**/*.html', 'src/**/*.css'],
                    dest: __dirname + '/' + grunt.option('mode')
                }
                ]
            }
        }
    });
    grunt.registerTask('loadfile', 'Load config', function (mode) {
        var done = this.async();
        //Kept switch instead of if-then-else statement for potential use
        //mode must not be src itself or any restricted name
        switch (mode) {
            case "dev":
                XML_File += "server_config.xml";
                break;
            case "prod":
                XML_File += "server_config.xml";
                break;
            default:
                grunt.fail.warn("mode not exist nor supported");
        }
        fs.readFile(XML_File, function (err, data) {
            if (err) { grunt.fail.warn(err); }
            else {
                XML_parser.parseString(data, function (err, result) {
                    if (err) { grunt.fail.warn(err); }
                    else {
                        //console.log(result);
                        server_config = convert_to_map(result['server_config'][mode][0]);
                        //console.log(server_config);
                        mkdirp(__dirname + '/' + mode + '/logs', function (err) {
                            if (err) { grunt.fail.warn(err); }
                            else {
                                mkdirp(__dirname + '/' + mode + '/src', function (err) {
                                    if (err) { grunt.fail.warn(err); }
                                    else { done(); }
                                });
                            }
                        });
                    }
                });
            }
        });
    });
    var mode = grunt.option('mode');
    grunt.registerTask('build', ['loadfile:' + mode, 'replace']);
    grunt.registerTask('default', function () {
        grunt.fail.warn('Please provide task and parameters e.g. "grunt build --mode=dev" ');
    });
};