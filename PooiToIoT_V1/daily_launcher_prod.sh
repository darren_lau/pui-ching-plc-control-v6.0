#!/bin/bash
export NODE_PATH=/usr/lib/node_modules

LOG_PATH="/home/safeuser/plc_server/prod/logs/daily_launcher.log"

echo "Running scripts at `date`" >> $LOG_PATH

/usr/local/bin/node /home/safeuser/plc_server/prod/src/task/refresh_plc.js
#/usr/local/bin/node /home/safeuser/plc_server_5.0/dev/src/task/refresh_plc.js
echo "PLC Refreshed" >> $LOG_PATH

/usr/local/bin/node /home/safeuser/plc_server/prod/src/task/reset_error.js
#/usr/local/bin/node /home/safeuser/plc_server_5.0/dev/src/task/reset_error.js
echo "DB error flushed" >> $LOG_PATH

#/usr/local/bin/pm2 flush
#echo "PM2 logs flushed"

/usr/bin/mongodump -d punDB -o /home/safeuser/plc_server/punDBdump >> $LOG_PATH 1> /dev/null
#/usr/bin/mongodump -d punDB -o /home/safeuser/plc_server_5.0/punDBdump >> $LOG_PATH 1> /dev/null
echo "DB Backuped" >> $LOG_PATH

echo "Done." >> $LOG_PATH
cd