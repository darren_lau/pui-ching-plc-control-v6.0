var Sequelize = require("sequelize");

var exports = module.exports = {

    //Would import more fields from Device_Status. If it is being tracked.
    schema: function (sequelize) {
        return sequelize.define('Device_History', {
            _id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
            device_id: { type: Sequelize.STRING },
            power_usage: { type: Sequelize.FLOAT },
            timestamp: { type: Sequelize.DATE }
        });
    },

    /**
        schema.virtual('device_ref', {
            ref: 'Device',
            localField: 'device_id',
            foreignField: 'device_id',
            justOne: true
        });
    **/

    query_obj: function (body) {
        return {
            _id: body._id
        };
    },

    update_field: function (body) {
        return {
            device_id: body.device_id,
            power_usage: body.power_usage,
            timestamp: body.timestamp
        };
    }
};

