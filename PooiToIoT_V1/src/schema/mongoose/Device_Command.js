var mongoose = require('mongoose');

var exports = module.exports = function (db) {

    var ReadingSchema = new mongoose.Schema(
        {
            //Assume id is in String
            siteId: String,
            client_id: String,
            modbus_fc: Number,
            fc_content: {
                address: Number,
                value: [Number]
            },
            last_issued: Date,
            last_completed: Date
        },
        {
            versionKey: false
        }
    );

    var collectionName = 'Device_Command';
    var exportModelName = 'Device_Command';
    return mongoose.model(exportModelName, ReadingSchema, collectionName);
};