var mongoose = require('mongoose');

var exports = module.exports = function (db) {

    var ScheduleDefSchema = new mongoose.Schema(
        {
            client_id: String,
            raise_time: Date,
            fixed_time: Date,
            errno: Number,
            errdesc: String
        },
        {
            versionKey: false
        }
    );

    var collectionName = 'error_issue';
    var exportModelName = 'error_issue';
    return mongoose.model(exportModelName, ScheduleDefSchema, collectionName);
};