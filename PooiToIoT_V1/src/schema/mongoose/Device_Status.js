var mongoose = require('mongoose');

var exports = module.exports = function (db) {

    var ReadingSchema = new mongoose.Schema(
        {
            //Assume id is in String
            siteId: String,
            client_id: String,
            connect_state: String,  //"connected", " disconnected", "undefined"
            coils: [{
                address: Number,
                value: Number
            }],
            register: [{
                address: Number,
                value: Number
            }],
            last_updated: Date,
            client_pid: Number
        },
        {
            toObject: { virtuals: true },
            toJSON: { virtuals: true },
            versionKey: false
        }
    );

    ReadingSchema.virtual('device_ref', {
        ref: 'Device',
        localField: 'client_id',
        foreignField: 'client_id',
        justOne: true
    });

    var collectionName = 'Device_Status';
    var exportModelName = 'Device_Status';
    return mongoose.model(exportModelName, ReadingSchema, collectionName);
};