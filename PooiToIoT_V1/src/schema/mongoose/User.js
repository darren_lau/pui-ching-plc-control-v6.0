var mongoose = require('mongoose');
var crypto = require('crypto');

var exports = module.exports = function (db) {

    var UserSchema = new mongoose.Schema(
        {
            //Basic info for auth
            username: { type: String, lowercase: true, unique: true },
            hash: String,
            usertype: String,
            salt: String,
            //Optional info, not for auth
            user_inf_name: String
        },
        {
            versionKey: false
        }
    );

    //Undocumented default "sha1" before Node V6
    //https://github.com/ericelliott/credential/issues/70
    UserSchema.methods.setPassword = function (password) {
        //Since it's not private key, it's better to not be same for everyone
        this.salt = "<code>ETAG</code><!--" + crypto.randomBytes(4).toString('hex') + "-->";
        this.hash = crypto.pbkdf2Sync(password, this.salt, 1024, 64, "sha1").toString('hex');
    };

    UserSchema.methods.validPassword = function (password) {
        var hash = crypto.pbkdf2Sync(password, this.salt, 1024, 64, "sha1").toString('hex');
        //console.log([hash, this.hash]);		
        return this.hash === hash;
    };

    var collectionName = 'User';
    var exportModelName = 'User';
    return mongoose.model(exportModelName, UserSchema, collectionName);

};