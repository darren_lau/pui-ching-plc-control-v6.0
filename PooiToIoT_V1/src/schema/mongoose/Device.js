var mongoose = require('mongoose');

var exports = module.exports = function (db) {

    var DeviceSchema = new mongoose.Schema(
        {
            //Assume id is in String
            siteId: String,
            mqtt_tag: String,
            //PLC
            client_id: String,
            client_name: String,
            client_type: String,
            ip_addr: String,
            mac: String,
            //Zone
            zoneId: String,
            tower: String,
            floor: String,
            desc: String,
            //Schedule
            default_id: String,
            //Appl
            appliances: [{
                appl_name: String,
                appl_type: String,
                appl_index: Number
            }]
        },
        {
            versionKey: false
        }
    );

    var collectionName = 'Device';
    var exportModelName = 'Device';
    return mongoose.model(exportModelName, DeviceSchema, collectionName);

};
