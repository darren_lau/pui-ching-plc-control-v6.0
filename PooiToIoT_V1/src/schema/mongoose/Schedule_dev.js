var mongoose = require('mongoose');

var exports = module.exports = function (db) {

    var ScheduleDevSchema = new mongoose.Schema(
        {
            siteId: String,
            zoneId: String,
            date_index: Number,
            dev_start_addr: Number,
            slot_length: Number,
            mod_date_value: String,
            time_slots: [{
                slotId: Number,
                desc: String,
                //mod_flag: Boolean,
                slot_value: Number,
                remark: String
            }]
        },
        {
            versionKey: false
        }
    );

    var collectionName = 'Schedule_dev';
    var exportModelName = 'Schedule_dev';
    return mongoose.model(exportModelName, ScheduleDevSchema, collectionName);
};