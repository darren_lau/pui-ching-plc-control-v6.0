var mongoose = require('mongoose');


var exports = module.exports = function (db) {

    var ScheduleDefSchema = new mongoose.Schema(
        {
            default_id: String,
            default_name: String,
            //XML_name
            xml_weekday: String,
            xml_weekend: String,
            xml_publicholiday: String
        },
        {
            versionKey: false
        }
    );

    var collectionName = 'Schedule_def';
    var exportModelName = 'Schedule_def';
    return mongoose.model(exportModelName, ScheduleDefSchema, collectionName);
};