var mongoose = require('mongoose');

var exports = module.exports = function (db) {

    var ReadingSchema = new mongoose.Schema(
        {
            //Assume id is in String
            siteId: String,
            zoneId: String,
            client_type: String,
            client_id: String,
            Appliance: [{}] //Messy, dynamic, depends on almost everything
        },
        {
            versionKey: false
        }
    );

    var collectionName = 'Device_Status';
    var exportModelName = 'Device_Status';
    return mongoose.model(exportModelName, ReadingSchema, collectionName);
};