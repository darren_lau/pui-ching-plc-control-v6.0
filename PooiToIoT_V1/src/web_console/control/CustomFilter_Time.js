var app = angular.module('homeScript');

app.factory('CustomFilter_Time', [
    '$http', '$log', '$window', '$q', '$timeout', 'global',
    function (
        $http, $log, $window, $q, $timeout, global
    ) {
        var o = {
            foo: "bar",
            check_range_fail: "Data are kept for 5 days only.",
            check_range: function (start) {
                var now = new Date();
                return true;
                //return start.getTime() >= global.last_reset_point(new Date(now.getTime() - global.past_limit)).getTime();
            }
        };

        o.Clear_Filter_Basic = function (param_map) {
            if (!param_map.lock_kDDL) {
                for (let i in param_map.kDDL) {
                    if (param_map.kDDL.hasOwnProperty(i)) {
                        if ($(param_map.kDDL[i]).data(global._kDDL)) {
                            $(param_map.kDDL[i]).data(global._kDDL).value(null);
                        }
                    }
                }
            }

            var fStart = [];
            var fEnd = [];

            for (let i in param_map.kDPS) {
                if (param_map.kDPS.hasOwnProperty(i)) {
                    $(param_map.kDPS[i]).data(global._kDP).value(param_map.clear_from);
                    if (fStart.indexOf(i) < 0) { fStart.push(i); }
                }
            }
            for (let i in param_map.kTPS) {
                if (param_map.kTPS.hasOwnProperty(i)) {
                    $(param_map.kTPS[i]).data(global._kTP).value(param_map.clear_from);
                    if (fStart.indexOf(i) < 0) { fStart.push(i); }
                }
            }
            for (let i in param_map.kDPE) {
                if (param_map.kDPE.hasOwnProperty(i)) {
                    $(param_map.kDPE[i]).data(global._kDP).value(param_map.clear_to);
                    if (fEnd.indexOf(i) < 0) { fEnd.push(i); }
                }
            }
            for (let i in param_map.kTPE) {
                if (param_map.kTPE.hasOwnProperty(i)) {
                    $(param_map.kTPE[i]).data(global._kTP).value(param_map.clear_to);
                    if (fEnd.indexOf(i) < 0) { fEnd.push(i); }
                }
            }

            console.log([fStart, fEnd]);

            var f_param = { logic: "and", filters: [] };

            for (let i = 0; i < fStart.length; i++) {
                f_param.filters.push({ field: fStart[i], operator: "gte", value: param_map.clear_from });
            }

            for (let i = 0; i < fEnd.length; i++) {
                f_param.filters.push({ field: fEnd[i], operator: "lte", value: param_map.clear_to });
            }

            for (let i in param_map.kDDL) {
                if (param_map.kDDL.hasOwnProperty(i)) {
                    if ($(param_map.kDDL[i]) && $(param_map.kDDL[i]).data(global._kDDL) && $(param_map.kDDL[i]).data(global._kDDL).value().length > 0) {
                        f_param.filters.push({
                            field: i, operator: "eq", value: $(param_map.kDDL[i]).data(global._kDDL).value()
                        });
                    }
                }
            }

            //if (param_map.flag_bin) {
            //    f_param.filters.push({ field: "flag_bin", operator: "eq", value: true });
            //}

            console.log([param_map, f_param]);
            if ($(param_map.kDS)) {
                try {
                    param_map.kDS.filter(f_param);
                } catch (e) {
                    console.log(e);
                }
            }

            //console.log(param_map.filter);
            //console.log(param_map.kDS.filter(param_map.filter));
        };

        o.Set_Filter_Basic = function (param_map) {
            var f_param = { logic: "and", filters: [] };
            /**
             * 
            var kDP_S = $(param_map.kDPS).data(global._kDP).value();
            var kTP_S = $(param_map.kTPS).data(global._kTP).value();
            var kDP_E = $(param_map.kDPE).data(global._kDP).value();
            var kTP_E = $(param_map.kTPE).data(global._kTP).value();

            var start = new Date();
            var end = new Date();
            ///console.log(device_id);
            if (kDP_S && kTP_S && kDP_E && kTP_E && param_map.fStart && param_map.fEnd) {
                start.setFullYear(kDP_S.getFullYear(), kDP_S.getMonth(), kDP_S.getDate());
                start.setHours(kTP_S.getHours(), kTP_S.getMinutes(), kTP_S.getSeconds(), 0);
                end.setFullYear(kDP_E.getFullYear(), kDP_E.getMonth(), kDP_E.getDate());
                end.setHours(kTP_E.getHours(), kTP_E.getMinutes(), kTP_E.getSeconds(), 999);
                f_param.filters.push({ field: param_map.fStart, operator: "gte", value: start });
                f_param.filters.push({ field: param_map.fEnd, operator: "lte", value: end });
                //console.log(start);
                //console.log(end);
            }
            */

            var fStart = {};
            var fEnd = {};

            var dummy_date = new Date();

            for (let i in param_map.kDPS) {
                if (param_map.kDPS.hasOwnProperty(i)) {
                    //console.log(param_map.kDPS);
                    dummy_date = $(param_map.kDPS[i]).data(global._kDP).value();
                    if (!fStart[i]) {
                        fStart[i] = new Date();
                        fStart[i].setHours(0, 0, 0, 0);
                    }
                    fStart[i].setFullYear(dummy_date.getFullYear(), dummy_date.getMonth(), dummy_date.getDate());
                }
            }

            for (let i in param_map.kTPS) {
                if (param_map.kTPS.hasOwnProperty(i)) {
                    dummy_date = $(param_map.kTPS[i]).data(global._kTP).value();
                    if (!fStart[i]) {
                        fStart[i] = new Date();
                    }
                    fStart[i].setHours(dummy_date.getHours(), dummy_date.getMinutes(), 0, 0);
                }
            }

            for (let i in param_map.kDPE) {
                if (param_map.kDPE.hasOwnProperty(i)) {
                    dummy_date = $(param_map.kDPE[i]).data(global._kDP).value();
                    if (!fEnd[i]) {
                        fEnd[i] = new Date();
                        fEnd[i].setHours(23, 59, 59, 999);
                    }
                    fEnd[i].setFullYear(dummy_date.getFullYear(), dummy_date.getMonth(), dummy_date.getDate());
                }
            }

            for (let i in param_map.kTPE) {
                if (param_map.kTPE.hasOwnProperty(i)) {
                    dummy_date = $(param_map.kTPE[i]).data(global._kTP).value();
                    if (!fEnd[i]) {
                        fEnd[i] = new Date();
                    }
                    fEnd[i].setHours(dummy_date.getHours(), dummy_date.getMinutes(), 0, 0);
                }
            }

            for (let i in fStart) {
                if (fStart.hasOwnProperty(i)) {
                    if (!o.check_range(fStart[i])) {
                        alert(o.check_range_fail);
                        return;
                    }
                    f_param.filters.push({ field: i, operator: "gte", value: fStart[i] });
                }
            }

            for (let i in fEnd) {
                if (fEnd.hasOwnProperty(i)) {
                    if (!o.check_range(fStart[i])) {
                        alert(o.check_range_fail);
                        return;
                    }
                    f_param.filters.push({ field: i, operator: "lte", value: fEnd[i] });
                }
            }

            for (let i in param_map.kDDL) {
                if (param_map.kDDL.hasOwnProperty(i)) {
                    if ($(param_map.kDDL[i]) && $(param_map.kDDL[i]).data(global._kDDL) && $(param_map.kDDL[i]).data(global._kDDL).value().length > 0) {
                        f_param.filters.push({
                            field: i, operator: "eq", value: $(param_map.kDDL[i]).data(global._kDDL).value()
                        });
                    }
                }
            }

            //if (param_map.flag_bin) {
            //    f_param.filters.push({ field: "flag_bin", operator: "eq", value: true });
            //}

            //if (param_map.device_ref) {
            //    f_param.filters.push({ field: "device_ref", operator: "isnotnull" });
            //}

            console.log([param_map, f_param]);

            if ($(param_map.kDS)) {
                try {
                    param_map.kDS.filter(f_param);
                } catch (e) {
                    console.log(e);
                }
            }
        };

        o.Get_Filter_Basic = function (param_map) {
            var f_param = { logic: "and", filters: [] };
            var fStart = {};
            var fEnd = {};

            var dummy_date = new Date();

            for (let i in param_map.kDPS) {
                if (param_map.kDPS.hasOwnProperty(i)) {
                    dummy_date = $(param_map.kDPS[i]).data(global._kDP).value();
                    if (!fStart[i]) {
                        fStart[i] = new Date();
                        fStart[i].setHours(0, 0, 0, 0);
                    }
                    fStart[i].setFullYear(dummy_date.getFullYear(), dummy_date.getMonth(), dummy_date.getDate());
                }
            }

            for (let i in param_map.kTPS) {
                if (param_map.kTPS.hasOwnProperty(i)) {
                    dummy_date = $(param_map.kTPS[i]).data(global._kTP).value();
                    if (!fStart[i]) {
                        fStart[i] = new Date();
                    }
                    fStart[i].setHours(dummy_date.getHours(), dummy_date.getMinutes(), 0, 0);
                }
            }

            for (let i in param_map.kDPE) {
                if (param_map.kDPE.hasOwnProperty(i)) {
                    dummy_date = $(param_map.kDPE[i]).data(global._kDP).value();
                    if (!fEnd[i]) {
                        fEnd[i] = new Date();
                        fEnd[i].setHours(23, 59, 59, 999);
                    }
                    fEnd[i].setFullYear(dummy_date.getFullYear(), dummy_date.getMonth(), dummy_date.getDate());
                }
            }

            for (let i in param_map.kTPE) {
                if (param_map.kTPE.hasOwnProperty(i)) {
                    dummy_date = $(param_map.kTPE[i]).data(global._kTP).value();
                    if (!fEnd[i]) {
                        fEnd[i] = new Date();
                    }
                    fEnd[i].setHours(dummy_date.getHours(), dummy_date.getMinutes(), 0, 0);
                }
            }

            for (let i in fStart) {
                if (fStart.hasOwnProperty(i)) {
                    if (!o.check_range(fStart[i])) {
                        alert(o.check_range_fail);
                        return;
                    }
                    f_param.filters.push({ field: i, operator: "gte", value: fStart[i] });
                }
            }

            for (let i in fEnd) {
                if (fEnd.hasOwnProperty(i)) {
                    if (!o.check_range(fStart[i])) {
                        alert(o.check_range_fail);
                        return;
                    }
                    f_param.filters.push({ field: i, operator: "lte", value: fEnd[i] });
                }
            }

            for (let i in param_map.kDDL) {
                if (param_map.kDDL.hasOwnProperty(i)) {
                    if ($(param_map.kDDL[i]) && $(param_map.kDDL[i]).data(global._kDDL)) {
                        f_param.filters.push({
                            field: i, operator: "eq", value: $(param_map.kDDL[i]).data(global._kDDL).value()
                        });
                    }
                }
            }

            //console.log([param_map, f_param]);
            return f_param;
        };

        //Return service object
        return o;
    }]);