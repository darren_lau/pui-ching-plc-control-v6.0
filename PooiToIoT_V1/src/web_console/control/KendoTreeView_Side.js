var app = angular.module('homeScript');

app.factory('KendoTreeView_Side', ['$http', '$log', '$window', '$q', '$timeout', 'global', 'server_access',
    function ($http, $log, $window, $q, $timeout, global, server_access) {
        var o = {
            kName: "#sideTree",
            ItemTemplate: "#ItemTemplate_KendoTreeView_Side",
            APIRoute: server_access.http_link
        };

        o.read_onSuccess = function (data, e, dI) {
            var raw_list;
            var tree_data_list = [];

            tree_data_list.push({ text: "Device Status" });

            try {
                raw_list = angular.fromJson(data);
            } catch (err) {
                e.error(o.oNot_json_obj(kName, op), 0, "");
                return;
            }
            //console.log(raw_list);

            //Sort array
            raw_list = raw_list.sort(function (a, b) {
                var firstsort = a.tower.localeCompare(b.tower);
                var secondsort = parseInt(a.floor) - parseInt(b.floor);
                var thirdsort = a.zoneId.localeCompare(b.zoneId);
                if (firstsort !== 0) { return firstsort; }
                else if (secondsort !== 0) { return secondsort; }
                else if (thirdsort !== 0) { return thirdsort; }
                else { return 0; }
            });

            for (var i0 = 0; i0 < raw_list.length; i0++) {
                //Assumed in format "rm(floor), block"
                var block_name = "Block " + raw_list[i0].tower;//raw_list[i0].desc.split(',')[2].trim();
                if (global.skip_block_name.indexOf(raw_list[i0].tower) >= 0) { continue; }
                var floor_name = raw_list[i0].floor + "/F";//raw_list[i0].desc.split(',')[1].trim();
                //var floor_name_tmp = raw_list.zones[i0].desc.split(',')[0].split('(')[1];
                //var floor_name = floor_name_tmp.slice(0, floor_name_tmp.length - 1);
                var zoneId = raw_list[i0].zoneId;
                var zone_name = zoneId + ": " + raw_list[i0].desc;//.split(',')[0].trim();
                var found_block = false;
                var found_floor = false;
                var found_zone = false;

                //Search in layer "Block"
                for (var i1 = 0; i1 < tree_data_list.length; i1++) {
                    if (tree_data_list[i1].text === block_name) {
                        found_block = true;
                        for (var i2 = 0; i2 < tree_data_list[i1].items.length; i2++) {
                            if (tree_data_list[i1].items[i2].text === floor_name) {
                                found_floor = true;
                                for (var i3 = 0; i3 < tree_data_list[i1].items[i2].items.length; i3++) {
                                    if (tree_data_list[i1].items[i2].items[i3].text === zone_name) {
                                        found_zone = true;
                                        //It's strange to have duplicate zones in this structure. Ignore then.
                                    }
                                }
                                if (!found_zone) { tree_data_list[i1].items[i2].items.push({ text: zone_name }); }
                            }
                        }
                        if (!found_floor) { tree_data_list[i1].items.push({ text: floor_name, items: [{ text: zone_name }] }); }
                    }
                }
                if (!found_block) { tree_data_list.push({ text: block_name, items: [{ text: floor_name, items: [{ text: zone_name }] }] }); }
            }
            if (global.sUserType === "admin" || global.sUserType === "system_admin") {
                if (!dI.mini) {
                    tree_data_list.push({ text: "Add Zone" });
                    tree_data_list.push({ text: "Power Usage" });
                }
                tree_data_list.push({ text: "Edit User" });
            }

            for (var i0 = 0; i0 < raw_list.length; i0++) {
                //Assumed in format "rm(floor), block"
                var block_name = "Block " + raw_list[i0].tower;//raw_list[i0].desc.split(',')[2].trim();

                //Move special floor
                var blk_k_index = tree_data_list.map(d => d.text).indexOf(block_name);
                var g_f_index = -1;
                var tmp_obj = null;
                if (blk_k_index >= 0) {
                    g_f_index = tree_data_list[blk_k_index].items.map(d => d.text).indexOf("G/F");
                    if (g_f_index >= 0) {
                        tmp_obj = tree_data_list[blk_k_index].items[g_f_index];
                        tree_data_list[blk_k_index].items.splice(g_f_index, 1);
                        tree_data_list[blk_k_index].items.unshift(tmp_obj);
                    }
                }
            }

            tree_data_list.push({ text: "BMS" });
            tree_data_list.push({ text: "錢涵洲紀念樓" });
            //tree_data_list.push({ text: "About" });
            tree_data_list.push({ text: "Logout" });
            //console.log(tree_data_list);
            e.success(tree_data_list);
        };

        o.call_server = function (op, e, dI) {
            var API_setting_map = {
                route1: o.APIRoute,
                //"c": { route2: "/create", method: "POST", obj: e.data },
                "r": { route2: "/getDevices?siteId=" + global.siteId, method: "GET", obj: null }
                //"r_post": { route2: "/read", method: "POST", obj: e.data },
                //"u": { route2: "/update", method: "POST", obj: e.data },
                //"d": { route2: "/destroy", method: "POST", obj: e.data }
            };

            var http_link = "";
            try {
                http_link = API_setting_map.route1 + API_setting_map[op].route2;
            } catch (e) {
                e.error(server_access.oNot_supported_obj(o.kName, op), 0, "");
                return;
            }
            var http_method = API_setting_map[op].method;
            var post_obj = API_setting_map[op].obj;

            server_access.init(o.kName, op, http_link, http_method, post_obj, function (data) { o.read_onSuccess(data, e, dI); }, e.error);
        };

        //sideTree
        o.dataSource_online = function () {
            return new kendo.data.HierarchicalDataSource({
                transport: {
                    read: function (e) { o.call_server("r", e, { mini: global.sUserType === "system_admin" ? false : true }); }
                },
                schema: {
                    model: {
                        hasChildren: "items",
                        children: "items"
                    }
                }
            });
        };

        o.Options = {
            //autoBind: false,
            autoScroll: true,
            dataSource: o.dataSource_online(),
            dataTextField: "text",
            template: kendo.template($(o.ItemTemplate).html()), //o.ItemTemplate,
            //select: function (e) {}
            error: function (e) {
                global.customKendoDataSourceErrorHandler(e);
                //this.cancelChanges();
            }
        };

        o.reload_online = function () {
            if ($(o.kName) && $(o.kName).data(global._kTV) && $(o.kName).data(global._kTV).dataSource) {
                $(o.kName).data(global._kTV).dataSource.read();
            }
        };

        o.onUserType = function (sUserType) {
            o.reload_online();
        };

        o.expand_item = function (e) {
            var sideTree = $(o.kName).data(global._kTV);
            var target = $(e.target);
            var toggleIcon = target.closest(".k-icon");
            if (!toggleIcon.length) {
                sideTree.toggle(target.closest(".k-item"));
            }
            /**
            var sideTree = $(o.kName).data(global._kTV);
            //console.log(newValue);
            if (sideTree) {
                //alert("OK")
                //console.log(sideTree.findByText(newValue));
                sideTree.expand(sideTree.findByText(newValue));
                //sideTree.expand(".k-item");
            } else {
                alert("OOPS");
                console.log(newValue);
            }
            return;
            **/
        };

        //Return service object
        return o;
    }]);