var app = angular.module('homeScript');

app.factory('KendoDropDownList_UserType', [
	'$http', '$log', '$window', '$q', '$timeout',
    function (
		$http, $log, $window, $q, $timeout
	) {
        var o = {
			sUserType_list: [{ text: 'admin' }, { text: 'user' }]
		};
		
		//User Grid's dataSource, k-options
        o.DataSource_local = new kendo.data.DataSource({ data: o.sUserType_list });

        o.Options = {
            dataSource: o.DataSource_local,
            optionLabel: "--User type--",
            dataTextField: "text",
            dataValueField: "text"
        };
		return o;
	}]);