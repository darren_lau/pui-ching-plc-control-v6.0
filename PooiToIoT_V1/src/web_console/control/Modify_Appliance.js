var app = angular.module('homeScript');

app.factory('Modify_Appliance', ['$http', '$log', '$window', '$q', '$timeout', 'global', 'server_access', 'setZone_inf',
    function ($http, $log, $window, $q, $timeout, global, server_access, setZone_inf) {
        var o = {
            kName: "Modify_Appliance",
            APIRoute: server_access.http_link,
        };

        o.call_server = function (not_grid, op, zone_inf, zone_ref, appl_inf, appl_ref) {
            //Goal: call o.server_access with proper parameters
            //o.server_access = function (kName, op, e, dI) {...};
            console.log("o.not_dataSource_crud(" + o.kName + "," + op + ")");

            //Simple confirmation window 
            if (op == "d") {
                //Note: Although it's return type is boolean, it cannot be assigned to any variable
                if (!confirm("Are you sure you're going to delete the item?")) { return; }
            }

            var dataItem = {
                zone_inf: zone_inf,
                zone_ref: zone_ref,
                appl_inf: appl_inf,
                appl_ref: appl_ref
            };

            var success = function (data) {
                console.log("o.not_dataSource_crud: Operation success.");
                if ((zone_inf) && (zone_inf.zoneId)) {
                    //Refresh the page
                    //The param of the function is based from the sideTree, which is modified
                    setZone_inf.init(zone_inf.zoneId + ": desc");
                }
            }

            var success_read_outer = function (data) {
                console.log("o.not_dataSource_crud: Operation success.");
                if ((zone_inf) && (zone_inf.zoneId)) {
                    setZone_inf.init(zone_inf.zoneId + ": desc");
                }
            }

            var success_read_inner = function (data) {
                //console.log(dI);
                var http_read_device = o.APIRoute + "/getDevices?client_id=" + dI.client_id;

                var status_arr = [];
                try { var objs = angular.fromJson(data)[0].Appliance; }
                catch (err) { global.customKendoDataSourceErrorHandler(global.ooNot_json_obj(o.kName, "r"), 0, ""); return; }
                //console.log(objs);
                for (var i0 = 0; i0 < objs.length; i0++) {
                    if (objs[i0].appl_type == dI.appl_type) {
                        for (var i1 = 0; i1 < objs[i0].devices.length; i1++) {
                            //console.log(objs[i0].devices[i1]);
                            var status_obj = angular.copy(dI);
                            status_obj.appl_name = objs[i0].devices[i1].appl_name;
                            status_obj.state = objs[i0].devices[i1].state;
                            status_arr.push(status_obj);
                        }
                    }
                }

                var success_read_Intermediate = function (data) {
                    try { var objs2 = angular.fromJson(data)[0]; }
                    catch (err) { e.error(o.oNot_json_obj(kName, op), 0, ""); }
                    console.log(status_obj);
                    for (var i3 = 0; i3 < status_arr.length; i3++) {
                        for (var i2 = 0; i2 < objs2.appliances.length; i2++) {
                            if (objs2.appliances[i2].appl_name == status_arr[i3].appl_name) {
                                status_arr[i3].appl_index = objs2.appliances[i2].appl_index;
                                break;
                            }
                        }
                    }
                    o.success_read_outer(status_arr);
                }
 
                console.log("o.server_access: Reading appl_index");
                //Get appl_index from Device
                server_access.not_dataSource_crud(o.kName, "r, config", http_read_device, "GET", null, success_read_Intermediate);
            }

            if (op === "r") {
                success = success_read_inner;
            }

            var API_setting_map = {
                route1: o.APIRoute,
                "c": { route2: "/addAppl", method: "POST", obj: null },
                "r": { route2: "/getStatus?zoneId=" + zone_inf.zoneId, method: "GET", obj: null },
                //"r_post": { route2: "/read", method: "POST", obj: e.data },
                "u": { route2: "/editAppl", method: "POST", obj: null },
                "d": { route2: "/deleteAppl", method: "POST", obj: null }
            };

            var http_link = API_setting_map.route1 + API_setting_map[op].route2;
            var http_method = API_setting_map[op].method;
            var post_obj = o.make_post_obj[op](dataItem);

            if (post_obj) {
                //Make server_access
                server_access.not_dataSource_crud(o.kName, op, http_link, http_method, post_obj, success);
            } else {
                global.customKendoDataSourceErrorHandler(global.oNot_complete_obj(o.kName, op));
            }
        }

        o.make_post_obj = {
            "r": true, //HTTP GET
            //dI: { zone_inf, zone_ref, appl_inf, appl_ref }
            //console.log(dI);
            //Step 0: Input validation
            "c": function (dI) {
                if (!(dI && dI.zone_ref && dI.appl_inf)) {
                    return false;
                } else if (!(dI.appl_inf.appl_name && dI.appl_inf.appl_type && (dI.appl_inf.appl_index >= 0)
                    && dI.zone_ref.zoneId && dI.zone_ref.client_id)) {
                    return false;
                } else {
                    var hit_something = false;
                    console.log(dI.zone_ref);
                    console.log(dI.appl_inf);
                    for (var i0 = 0; i0 < dI.zone_ref.appliances.length; i0++) {
                        if ((dI.zone_ref.appliances[i0].appl_index == dI.appl_inf.appl_index) ||
                            (dI.zone_ref.appliances[i0].appl_name == dI.appl_inf.appl_name)) {
                            hit_something = true;
                            global.customKendoDataSourceErrorHandler(global.oInf_used_obj(o.kName, "c", dI.zone_ref), 0, "");
                            return false;
                        }
                    }
                    if (!hit_something) {
                        dI.appl_inf.appl_index = Math.floor(dI.appl_inf.appl_index);
                        return dI;
                    }
                }
            },
            "u": function (dI) {
                //u = d + c
                if (!(dI && dI.zone_ref && dI.appl_inf)) {
                    return false;
                } else if (!(dI.appl_inf.appl_name && dI.appl_inf.appl_type && (dI.appl_inf.appl_index >= 0)
                    && dI.zone_ref.zoneId && dI.zone_ref.client_id)) {
                    return false;
                } else if (!(dI.appl_ref.appl_name && dI.zone_ref.client_id)) {
                    return false;
                } else {
                    dI.appl_inf.appl_index = Math.floor(dI.appl_inf.appl_index);
                    return dI;
                }
            },
            "d": function (dI) {
                if (!(dI && dI.zone_ref && dI.appl_ref)) {
                    return false;
                } else if (!(dI.appl_ref.appl_name && dI.zone_ref.client_id)) {
                    return false;
                } else {
                    return dI;
                }
            }
        };
        
        //Return service object
        return o;
    }]);