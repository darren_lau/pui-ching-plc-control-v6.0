var app = angular.module('homeScript');

app.factory('KendoValidator_Zone', ['$http', '$log', '$window', '$q', '$timeout', 'global', 'server_access',
    function ($http, $log, $window, $q, $timeout, global, server_access) {
        var o = {
            kName_add: "#addZoneValidator",
            kName_edit: "#editZoneValidator"
        };

        o.oZone_DBinf_validate = function (event, op, oZone_DBinf) {
            //event.preventDefault();
            var validator;
            //Binding target Kendo Validator
            if (op == 'c') { validator = $(o.kName_add).kendoValidator().data(global._kV); }
            else if (op == 'u') { validator = $(o.kName_edit).kendoValidator().data(global._kV); }
            else { console.log('$scope.oZone_DBinf_validate: error in op: ' + op); return false; }

            //Validation 0: All fields are required. Written in HTML
            if (validator.validate()) {
                //Validation 1: Check IP address format
                var ip_valid = true;
                if (oZone_DBinf.ip_addr.split('.').length != 4) {
                    ip_valid = false;
                } else {
                    var ip_arr = oZone_DBinf.ip_addr.split('.');
                    for (var i0 = 0; i0 < 4; i0++) {
                        var ip_int = parseInt(ip_arr[i0]);
                        if (!(ip_int >= 0) && (ip_int <= 255)) {
                            ip_valid = false;
                        }
                    }
                }
                //Validation 2: Check MAC address format
                var mac_valid = true;
                if (oZone_DBinf.mac.split(':').length != 6) {
                    mac_valid = false;
                } else {
                    var mac_arr = oZone_DBinf.mac.split(':');
                    for (var i0 = 0; i0 < 6; i0++) {
                        var mac_str = mac_arr[i0];
                        if (mac_str.length != 2) {
                            mac_valid = false;
                        }
                    }
                }
                if (!ip_valid) {
                    //console.log("invalid");
                    alert("validator: IP should be in format '192.168.0.1'.");
                    return false;
                }
                if (!mac_valid) {
                    //console.log("invalid");
                    alert("validator: MAC should be in format 'ff:ff:ff:ff:ff:ff'.");
                    return false;
                }
                //console.log("valid");
                //Do the operation if valid
                //$scope.not_dataSource_crud('applGrid', op);
                return true;
            } else {
                //console.log("invalid");
                alert("validator: Please fill in all the fields. All of them are required fields.");
                return false;
            }
        }


        //Return service object
        return o;
    }]);