var app = angular.module('homeScript');

app.factory('mqtt_controller', ['$http', '$log', '$window', '$q', '$timeout', 'global', 'server_access',
    function ($http, $log, $window, $q, $timeout, global, server_access) {
        var o = {
            http_link_mqtt: server_access.http_link + "/fireMqttMessage",
            kName: "mqtt_controller"
        };

        //Fire MQTT message by o.server_access
        o.fireMqttMessage = function (mqttClient, dI) {
            //console.log(dI);
            if (!dI) { console.log("dataItem is not found!"); return; }
            var target_state = dI.state ? 0 : 1;
            var post_obj = {
                topic: dI.mqtt_tag + "/setStatus",
                message: angular.toJson({
                    appl_type: dI.appl_type,
                    appl_name: dI.appl_name,
                    command: "state",
                    value: target_state
                })
            };

            if (mqttClient) {
                //Send via web socket
                var mqtt_message = new Messaging.Message(post_obj.message);
                mqtt_message.destinationName = post_obj.topic;
                mqttClient.send(mqtt_message);
            } else {
                //Send as HTTP request
                //"mqtt" for firing mqtt message
                var success = function (data) {
                    //dataItem.state = dataItem.state ? 0 : 1;
                    console.log("o.fireMqttMessage: Comment has been sent successfully.");
                };
                server_access.not_dataSource_crud(o.kName, "mqtt", o.http_link_mqtt, "POST", post_obj, success);
            }
        };

        //Fire MQTT message by o.server_access
        o.fireMqttMessageForAC = function (dI) {
            //"mqtt" for firing mqtt message
            var post_obj = {
                topic: dI.mqtt_tag + "/setStatus",
                message: angular.toJson({
                    appl_type: dI.appl_type,
                    appl_name: dI.appl_name,
                    command: dI.key,
                    value: dI[dI.key]
                })
            };
            console.log(post_obj);

            var success = function (data) {
                //dataItem.state = dataItem.state ? 0 : 1;
                console.log("o.fireMqttMessageForAC: Comment has been sent successfully.");
            };

            server_access.not_dataSource_crud(o.kName, "mqtt4ac", o.http_link_mqtt, "POST", post_obj, success);
        };

        //Return service object
        return o;
    }]);