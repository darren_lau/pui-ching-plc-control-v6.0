var app = angular.module('homeScript', ["kendo.directives", "amChartsDirective"]);
//var app = angular.module('homeScript');

app.config(['$compileProvider',
    function ($compileProvider) {
        $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|tel|file|blob):/);
    }]);

app.config(['$httpProvider',
    function ($httpProvider) {
        $httpProvider.useLegacyPromiseExtensions(false);
    }]);

//Factory part has been skipped.
//The following functions will be mega main() functions of control scripts.

app.controller('Asset1Ctrl', [
    '$scope', '$http', '$interval', '$log', '$timeout',
    'global', 'check_login',
    'mqtt_controller', 'AC_controller', 'CustomFilter_Time',
    'Modify_Appliance', 'Modify_Zone', 'setZone_inf', 'KendoTreeView_Side',
    'KendoDropDownList_UserType', 'KendoGrid_User', 'KendoGrid_Device_Status',
    'KendoDropDownList_ScheduleDefault',
    'KendoLineChart_PowerUsage',
    'KendoMisc_Appliance', 'KendoScheduler_Appliance', 'KendoTabStrip_Appliance',
    'KendoValidator_Zone', 'KendoWindow_about',
    'amChart',
    function (
        $scope, $http, $interval, $log, $timeout,
        global, check_login,
        mqtt_controller, AC_controller, CustomFilter_Time,
        Modify_Appliance, Modify_Zone, setZone_inf, KendoTreeView_Side,
        KendoDropDownList_UserType, KendoGrid_User, KendoGrid_Device_Status,
        KendoDropDownList_ScheduleDefault,
        KendoLineChart_PowerUsage,
        KendoMisc_Appliance, KendoScheduler_Appliance, KendoTabStrip_Appliance,
        KendoValidator_Zone, KendoWindow_about,
        amChart
    ) {
        //Check login for every 60 seconds
        //clearInterval($scope.login_interval);
        //$scope.login_interval = setInterval(function () { global.isloggedin(); }, 1000 * 60);
        //Check user type
        //global.sUserType();

        //Check login for every 60 seconds
        if (angular.isDefined($scope.login_interval)) {
            $interval.cancel($scope.login_interval);
        }
        $scope.login_interval = $interval(function () {
            check_login.isloggedin(function () {
                //Kept for potential use
                KendoGrid_User.reload_online();
                KendoScheduler_Appliance.reload_online();
                console.clear(); //Argh. Really Too many logs.
            });
        }, global._iCheckLoginPeriod);

        //Check user type
        check_login.sUserType(function (sUserType) {
            console.log("User type: " + sUserType);
            global.sUserType = sUserType;
            KendoTreeView_Side.onUserType(sUserType);
            KendoGrid_User.onUserType(sUserType);
            KendoTabStrip_Appliance.onUserType(sUserType);
            if (sUserType === "user") {
                //Keep for potential use ($scope only)
            } else if (sUserType === "admin") {
                //Keep for potential use ($scope only)
            }
            $scope.show_system_admin = sUserType === "system_admin";
        });

        //Special operation for Kendo Scheudler
        kendo.culture().calendar.firstDay = new Date().getDay();

        //Dummy DataSource
        $scope.nothing = [];

        //Kendo Grid
        $scope.userGridOptions = KendoGrid_User.Options;
        $scope.Options_KendoGrid_Device_Status = KendoGrid_Device_Status.Options;
        //Kendo Window
        $scope.aboutWindowOptions = KendoWindow_about.Options;
        //Kendo TreeView
        $scope.sideTreeOptions = KendoTreeView_Side.Options;
        //$scope.sideTreeOptions_mini = KendoTreeView_Side.Options_mini;
        //Kendo Numberic Text Box
        $scope.applindexNumericTextBoxOptions = KendoMisc_Appliance.applindexNumericTextBoxOptions;
        $scope.applSchedulerNumericTextBoxOptions = KendoMisc_Appliance.applSchedulerNumericTextBoxOptions;
        //Kendo Drop Down List
        $scope.UserTypeDropDownListOptions = KendoDropDownList_UserType.Options;
        $scope.ScheduleDefaultDropDownListOptions = KendoDropDownList_ScheduleDefault.Options;
        //Kendo Scheduler
        $scope.applSchedulerOptions = KendoScheduler_Appliance.Options;
        //Kendo Time Picker
        $scope.applSchedulerStartTimePickerOptions = KendoScheduler_Appliance.applSchedulerTimePickerOptions;
        $scope.applSchedulerEndTimePickerOptions = KendoScheduler_Appliance.applSchedulerTimePickerOptions;
        //Kendo Linear Gauge
        $scope.temperatureLinearGaugeOptions = KendoMisc_Appliance.temperatureLinearGaugeOptions;
        //Kendo Radial Gauge
        $scope.humidityRadialGaugeOptions = KendoMisc_Appliance.humidityRadialGaugeOptions;
        $scope.gasRadialGaugeOptions = KendoMisc_Appliance.gasRadialGaugeOptions;
        $scope.co2RadialGaugeOptions = KendoMisc_Appliance.co2RadialGaugeOptions;


        //Initialization of some "dirty" variables 
        $scope.dataItem = {};

        //To make log in service/ factory visiable
        //In controller, just type console.log is OK
        $scope.$log = global.$log;

        //Instead of $apply, since the value is changed by Kendo and then service objects, it's not in Angular $scope.
        //Therefore, if you want to watch it, use $scope.$watch(fReturn($scope),fChange(new))
        $scope.sideTreeSelectedItem = "Device Status";
        $scope.$watch(
            function ($scope) { return $scope.sideTreeSelectedItem ? $scope.sideTreeSelectedItem.text : null; },
            function (newValue) {
                console.log("o.sideTreeOptions: Selecting ", newValue);
                //Initialization of variables
                $scope.Show_KendoGrid_Device_Status = false;
                $scope.show_applGrid = false;
                $scope.show_addZone = false;
                $scope.show_userGrid = false;
                $scope.show_nullGrid = false;
                $scope.show_powerUsage = false;
                $scope.oZone_DBinf = { siteId: global.siteId };
                $scope.selected_appl = {};
                $scope.selected_appl_ref = {};
                if (newValue === "About") { $scope.aboutWindow.open().center(); }
                else if (newValue === "Device Status") {
                    $scope.Show_KendoGrid_Device_Status = true;
                } else if (newValue === "Add Zone") {
                    $scope.show_addZone = true;
                    //console.log($(KendoDropDownList_ScheduleDefault.kName_Add).data(global._kDDL));
                } else if (newValue === "Power Usage") {
                    $scope.show_powerUsage = true;
                } else if (newValue === "Edit User") {
                    $scope.show_userGrid = true;
                } else if (newValue === "BMS") {
                    global.fGotoLink("http://10.0.223.1");
                } else if (newValue === "錢涵洲紀念樓") {
                    global.fGotoLink("http://223.197.2.137/school");
                } else if (newValue === "Logout") {
                    global.fGotoLink_sameWin('/login');
                } else if (newValue) {
                    //console.log([newValue, newValue === "Power Usage"]);
                    if (newValue.indexOf("/F") < 0 && newValue.indexOf("Block") < 0) {
                        $scope.show_applGrid = true;
                        console.log("setZone_inf.init(" + newValue + ")");
                        setZone_inf.init(newValue);
                    } else {
                        $scope.show_applGrid = false;
                        $scope.show_nullGrid = true;
                        KendoTreeView_Side.expand_item(newValue);
                    }
                }
            }
        );

        $scope.KendoTreeView_Side_onClick = KendoTreeView_Side.expand_item;
        $scope.KendoTreeView_Side_onChange = function (dataItem) {
            //This may be a unfinished feature about Kendo + AngularJS.
            //Previously k-on-change can apply angular expression (a.k.a sideTreeSelectedItem = dataItem)
            //However when it is in ng-include instead of same *.html, this expression is not eval-ed well.
            //Somehow preforming the assignment here is effective.
            //console.log(dataItem);
            $scope.sideTreeSelectedItem = dataItem;
        }

        //Scope-Service function binding
        $scope.fGotoLink = global.fGotoLink;

        //Watching variables, binded functions of section "Appliance Control" and "Appliance information"
        $scope.selected_appl = [];
        $scope.selected_appl_ref = []; //Reference to the target object

        $scope.show_info = function (dataItem) {
            //console.log(dataItem);
            $scope.selected_appl = dataItem;
            $scope.selected_appl_ref = angular.copy(dataItem);
        };

        //Calling service with binded $scope objects
        $scope.Modify_Appliance = function (not_grid, op) {
            Modify_Appliance.call_server(not_grid, op, $scope.oZone_DBinf, $scope.oZone_DBinf_ref, $scope.selected_appl, $scope.selected_appl_ref);
        };

        $scope.applScheduler_pushtoPLC = function () {
            KendoScheduler_Appliance.pushtoPLC($scope.oZone_DBinf, $scope.oZone_DBinf_ref, $scope.selected_appl, $scope.selected_appl_ref);
        };

        //Watching variables, binded functions of section "Zone/ Client Information" and "Add Zone"
        $scope.oZone_inf = ""; //Selected zone informaion, binded with appliances
        $scope.oZone_DBinf = ""; //Selected zone informaion, no binding
        $scope.oZone_infNew = ""; //Specially for addZone


        $scope.$watch(
            function ($scope) { return global.oZone_inf; },
            function (newValue) { $scope.oZone_inf = newValue; }
        );
        $scope.$watch(
            function ($scope) { return global.oZone_DBinf; },
            function (newValue) {
                $scope.oZone_DBinf = newValue;
                $scope.oZone_DBinf_ref = angular.copy(newValue);
                KendoLineChart_PowerUsage.reload_online();
            }
        );

        //Watching variables, binded functions of section "Scheduler"

        //Variables for MQTT client (codes not in this script)
        //MQTT Client Object from other script: Will be defined when HTML is loaded
        $scope.mqttClient;
        global.mqttClient = $scope.mqttClient;
        $scope._sMqtt_server_addr = global._sMqtt_server_addr;
        //MQTT Client Status object. For UI purpose only.
        $scope.mqtt_status = global.oMqtt_notDefined;
        //Setter of $scope.mqtt_status. Triggered in the MQTT Client script.
        $scope.set_mqtt_status = function (obj) { $scope.mqtt_status = { css: obj.css, msg: obj.msg }; };
        //Get selected zoneId (done in global.server_access)
        $scope.sCurrentMqttTopic = "";
        $scope.always_subscribe = ["/TowerX/Floor99/X99-1/PLC/Status"]; //Hardcode since it is always on
        $scope.$watch(
            function ($scope) { return setZone_inf.sCurrentMqttTopic; },
            function (newValue) {
                var oldValue = $scope.sCurrentMqttTopic;
                console.log(oldValue);
                $scope.sCurrentMqttTopic = newValue;
                //This scope do not know if the mqttclient is connected to mosca
                //Can only assume it is connected              
                if ($scope.mqttClient) {
                    //"Tower/Block/zoneId/cmd"
                    //cmd = {"Status/setStatus/Sensors"}
                    console.log([oldValue, $scope.always_subscribe.indexOf(oldValue)]);
                    if (oldValue && $scope.always_subscribe.indexOf(oldValue) < 0) { $scope.mqttClient.unsubscribe(oldValue + "/#"); console.log("unsubscribe: " + oldValue + "/#"); }
                    if (newValue) { $scope.mqttClient.subscribe(newValue + "/#"); console.log("subscribe: " + newValue + "/#"); }
                    //$scope.set_mqtt_status(global.oMqtt_disconnected);
                } else {
                    $scope.set_mqtt_status(global.oMqtt_notDefined);
                }
            }
        );

        $scope.subscribe_mqtt_topics = function () {
            let topic_arr = [];
            angular.copy($scope.always_subscribe, topic_arr); //Must use copy or the array will be shared!.
            if ($scope.sCurrentMqttTopic && $scope.sCurrentMqttTopic.length > 0) {
                topic_arr.push($scope.sCurrentMqttTopic);
            }
            if ($scope.mqttClient) {
                topic_arr.forEach((d, i, a) => {
                    $scope.mqttClient.subscribe(`${d}/#`);
                    console.log(`subscribe: ${d}/#`);
                    $scope.sCurrentMqttTopic = d;
                });
            }
        };

        //Fire MQTT Message with API
        $scope.fireMqttMessage = function (dataItem) {
            //console.log(dataItem);
            $scope.show_loading_bar(global.kLoadingBar["Appliance_Control"]);
            //mqtt_controller.fireMqttMessage($scope.mqttClient, dataItem);
            //v6.0: Use HTTP POST for User Type Checking
            mqtt_controller.fireMqttMessage(null, dataItem);
            $timeout($scope.remove_loading_bar, global._iModbus_poll_interval * 2, true, global.kLoadingBar["Appliance_Control"]); //Worse case will be 2 polling period
            //Instead of waiting for polling, change the UI instantly
            //dataItem.state = dataItem.state ? 0 : 1;
        };

        //Validation of oZone_DBinf (involves Kendo Validator)
        //It could be put in service section, but translation would be complicated
        $scope.oZone_DBinf_validate = function (e, op) {
            if (KendoValidator_Zone.oZone_DBinf_validate(e, op, $scope.oZone_DBinf)) {
                Modify_Zone.call_server('applGrid', op, $scope.oZone_DBinf, $scope.oZone_DBinf_ref, $scope.selected_appl, $scope.selected_appl_ref);
            }
        };

        $scope.setZone_status = function (topic, msg) {
            if ($scope.always_subscribe.indexOf(topic) < 0) {
                setZone_inf.setZone_status(topic, msg); //always_subscribe
            } else {
                //console.log("setZone_status: Skipped topic " + topic);
            }
        };

        $scope.Alarm_Status = false;
        $scope.setAlarm_status = function (topic, msg) {
            if ($scope.always_subscribe.indexOf(topic) >= 0) {
                $scope.Alarm_Status = setZone_inf.setAlarm_status($scope.Alarm_Status, topic, msg);
            } else {
                //console.log("setAlarm_status: Skipped topic " + topic);
            }
        };

        //AC Controller stuffs
        $scope.OperationModeDropDownListOptions = AC_controller.OperationModeDropDownListOptions;
        $scope.FanSpeedDropDownListOptions = AC_controller.FanSpeedDropDownListOptions;
        $scope.SetTemperatureSliderOptions = AC_controller.SetTemperatureSliderOptions;
        $scope.SwingDropDownListOptions = AC_controller.SwingDropDownListOptions;
        $scope.RoomTemperatureSliderOptions = AC_controller.RoomTemperatureSliderOptions;
        $scope.temperature_font = AC_controller.temperature_font;
        $scope.ACControlOnChange = function (dI, key, kD, set_val) {
            $scope.show_loading_bar(global.kLoadingBar["VRV_Control"]);
            AC_controller.ACControlOnChange(dI, key, kD, set_val); //V6.0: Loading bar was forgotten
            $timeout($scope.remove_loading_bar, global._iModbus_poll_interval * 2, true, global.kLoadingBar["VRV_Control"]); //Worse case will be 2 polling period
        }

        $scope.SetDelightSliderOptions = AC_controller.SetDelightSliderOptions;
        $scope.DelightOnChange = function (dI, key, kD, set_val) {
            $scope.show_loading_bar(global.kLoadingBar["Appliance_Control"]);
            AC_controller.DelightOnChange(dI, key, kD, set_val);
            $timeout($scope.remove_loading_bar, global._iModbus_poll_interval * 2, true, global.kLoadingBar["Appliance_Control"]);
        }

        $scope.inc_local_progress_alt = AC_controller.inc_local_progress_alt;
        $scope.inc_local_progress = AC_controller.inc_local_progress;
        $scope.expand_col = AC_controller.expand_col;
        $scope.close_col = AC_controller.close_col;
        $scope.map_text_from_value = AC_controller.map_text_from_value;
        $scope.selected_css = AC_controller.selected_css;

        $scope.show_loading_bar = function (el) {
            //console.log([el, $(el)]);
            kendo.ui.progress($(el), true);
            $(el).show();
        };

        $scope.remove_loading_bar = function (el) {
            // Clear up the loading indicator for this chart
            //var loading = $(o.kLoadingBar, e.sender.element.parent())
            var loading = $(el);
            kendo.ui.progress(loading, false);
            $(el).hide();
        };

        //Power Usage
        //There will be a KendoChart and a KendoGrid contains a similar time filter.
        $scope.show_map_nav_bar_PU = true;

        $scope.Selected_KDDL_KC_PU_Zone = "PLC_2a";
        $scope.Options_KDDL_KC_PU_Zone = KendoLineChart_PowerUsage.KDDL_Zone.Options;
        $scope.Options_KDDL_KC_PU_Interval = KendoLineChart_PowerUsage.KDDL_Interval.Options;
        $scope.Selected_KDDL_KC_PU_Interval = "FIFTEEN_MINUTES";
        $scope.Options_KendoLineChart_PowerUsage = KendoLineChart_PowerUsage.LineChart.Options;

        $scope.time_filter_params = [
            KendoLineChart_PowerUsage.time_filter_param,
        ];

        $scope.Options_KendoDatePicker = function (op, se) {
            var option = [$scope.time_filter_params[op]().clear_from, $scope.time_filter_params[op]().clear_to];
            return { format: "yyyy/MM/dd", value: option[se] };
        };
        $scope.Options_KendoTimePicker = function (op, se) {
            var option = [$scope.time_filter_params[op]().clear_from, $scope.time_filter_params[op]().clear_to];
            return { format: "HH:mm", interval: 15, value: option[se] };
        };

        $scope.Set_Filter_Time = function (op) { CustomFilter_Time.Set_Filter_Basic($scope.time_filter_params[op]()); $scope.RedrawScale(op); };

        $scope.Clear_Filter_Time = function (op) { CustomFilter_Time.Clear_Filter_Basic($scope.time_filter_params[op]()); $scope.RedrawScale(op); };

        $scope.Export_To_IMG = function (op) {
            var dummy = { toIMG: function () { return false; } };
            var a = [
                KendoLineChart_PowerUsage
            ];
            return a[op].toIMG();
        };
        $scope.Export_To_XLSX = function (op) {
            var dummy = { toXLSX: function () { return false; } };
            var a = [
                KendoLineChart_PowerUsage
            ];

            var b = CustomFilter_Time.Get_Filter_Basic(a[op].time_filter_param());
            var c = [
                [KendoLineChart_PowerUsage, global._kLC],
            ];

            return a[op].toXLSX(b, c[op] ? global._kDS(c[op][0].kName, c[op][1]) : false);
        };

        $scope.RedrawScale = function (op) {
            var dummy = { RedrawScale: function () { return false; } };
            var a = [
                KendoLineChart_PowerUsage,
            ];
            var b = CustomFilter_Time.Get_Filter_Basic(a[op].time_filter_param());

            return a[op].RedrawScale(b);
        };

        $scope.amChartOptions = amChart.Options;
        $scope.DataGen = amChart.DataGen;


    }]);

app.controller('LoginCtrl', [
    '$scope',
    '$window',
    function ($scope, $window) {
        $scope.username = "";
        $scope.password = "";
        $scope.message = "";
        $scope.onKeyPressed = function (event) {
            console.log(event);
        };
        $scope.login = function (username, password) {
            /**
            var url = "http://" + $window.location.host + "/web_console/asset4_asdDB.html";
            console.log(angular.toJson([username, password]));
            if (username == "admin") { $scope.gotoConsole(url); }
            **/
        };
        $scope.gotoConsole = function (url) {
            //console.log(url);
            //$window.location.href = url;
        };
        if (angular.isDefined($scope.notf2))
            $scope.notf2.show("Login failed.\nWrong username or password.", "error");
    }]);