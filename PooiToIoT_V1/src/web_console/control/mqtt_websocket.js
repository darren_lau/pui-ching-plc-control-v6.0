//Control of the mqttClient (subscribe/ unsubscribe) will be done in angular scope.
//It should be loaded after the angularJS script to get the scope successfully.
//Now the parameter is determined by the angularJS.

//Global variables
var mqttClient;
var mqttInterval;
var angularScope;
var status_ok = { css: "mqtt_status_ok", msg: "Connected and running" };
var status_undefined = { css: "mqtt_status_err", msg: "Error: Client is not defined" };
var status_disconnected = { css: "mqtt_status_err", msg: "Error: Client is disconnected" };

//Setting up mqttClient
function set_mqtt() {
    //Set client object
    mqttClient = null;

    //Bind to anglular $scope
    angularScope = angular.element(document.getElementById('Asset1Body')).scope();
    angularScope.$apply(function () {

        try {
            //http://localhost:8080/server
            var addr = angularScope._sMqtt_server_addr + Math.round(Math.random() * 65536);
            console.log("mqttclient: Try setting mqttClient (" + addr + ")");
            //mqttClient = new Messaging.Client("localhost", 8080, "server");
            //mqttClient = new Messaging.Client("128.199.182.171", 8080, "server");
            mqttClient = new Messaging.Client(
                addr.split('/')[2].split(':')[0],
                parseInt(addr.split('/')[2].split(':')[1]),
                addr.split('/')[3]
            );

            //Set event handlers
            mqttClient.onConnectionLost = onConnectionLost;
            mqttClient.onMessageArrived = onMessageArrived;
            //Connect to server
            mqttClient.connect({ onSuccess: onConnect });
            console.log("mqttclient: mqttClient is set.");
        } catch (e) {
            console.log(e);
            mqttClient = null;
        }

        angularScope.mqttClient = mqttClient;
        console.log("mqttclient: Found angularScope");
    });

}

//Called when the connection is made
function onConnect() {
    angularScope.$apply(function () {
        angularScope.set_mqtt_status(status_ok);
        angularScope.subscribe_mqtt_topics();
    });
}

//Called when the client loses its connection
function onConnectionLost(responseObject) {
    console.log("mqttclient: onConnectionLost");
    console.log(responseObject);
    angularScope.$apply(function () {
        angularScope.set_mqtt_status(status_disconnected);
        mqttClient = null;
    });
}

//Called when a message arrives
function onMessageArrived(message) {
    console.debug("mqttclient.js: onMessageArrived(): " + message.destinationName);
    //console.log("Topic: " + message.destinationName);
    //console.log(message.payloadString);
    try {
        var topic = message.destinationName;
        var resp = JSON.parse(message.payloadString);
        angularScope.$apply(function () {
            angularScope.setZone_status(topic, resp);
            angularScope.setAlarm_status(topic, resp);
        });
    } catch (e) {
        console.log("mqttclient.js: resp not JSON.");
    }
}

//Check MQTT Client status for every 5 seconds
function check_mqtt() {
    mqttClient = null;
    clearInterval(mqttInterval);
    //set_mqtt();
    mqttInterval = setInterval(function () { if (!mqttClient) { set_mqtt(); } }, parseInt('@@_iMQTT_CHECK_INTERVAL'));
    console.log("mqttclient.js loaded");
}

//Main process. Called when the HTML file is loaded.
$(document).ready(check_mqtt());

