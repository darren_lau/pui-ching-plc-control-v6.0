var app = angular.module('homeScript');

app.factory('KendoDropDownList_ScheduleDefault', [
    'global', 'server_access',
	'$http', '$log', '$window', '$q', '$timeout',
    function (
        global, server_access,
		$http, $log, $window, $q, $timeout
	) {
        var o = {
            kName: "#ScheduleDefaultDropDownList", //Warning: More than one ID (_Sch, _Add, _Edit)
            parent: "#KendoScheduler_Appliance", //But your son cannot be your parent
            APIRoute: server_access.http_link,
            http_link_edit: server_access.http_link + "/editZone?Sch_def=true",
            sSchedule_default_list: [{ default_name: 'Regular/ Holday', default_id: 'ruleA' }, { default_name: 'Need card all day', default_id: 'ruleB' }],
            Schema: {
                //data: "items", //See server API
                //total: "itemCount", //See server API
                model: {
                    id: 'default_id',
                    fields: {
                        _id: { from: '_id', nullable: true }, //From MongoDB
                        default_id: { from: 'default_id', defaultValue: "ruleA", validation: { required: true } },
                        default_name: { from: 'default_name', defaultValue: "", validation: { required: true } },
                        xml_list: { from: 'xml_list', nullable: true }
                    }
                }
            }
        };

        o.kName_Sch = o.kName + "_Sch";
        o.kName_Add = o.kName + "_Add";
        o.kName_Edit = o.kName + "_Edit";

        o.call_server = function (op, e, dI) {
            var API_setting_map = {
                route1: o.APIRoute,
                //"c": { route2: "/create", method: "POST", obj: e.data },
                "r": { route2: "/getSchedule_def", method: "GET", obj: null }
                //"r_post": { route2: "/read", method: "POST", obj: e.data },
                //"u": { route2: "/update", method: "POST", obj: e.data },
                //"d": { route2: "/destroy", method: "POST", obj: e.data }
            };

            var http_link = "";
            try {
                http_link = API_setting_map.route1 + API_setting_map[op].route2;
            } catch (e) {
                e.error(server_access.oNot_supported_obj(o.kName, op), 0, "");
                return;
            }
            var http_method = API_setting_map[op].method;
            var post_obj = API_setting_map[op].obj;

            server_access.init(o.kName, op, http_link, http_method, post_obj, e.success, e.error);
        };

        //Change schedule rules (change only, no custom rules supported yet)
        o.DataSource_online = function (dataItem) {
            return new kendo.data.DataSource({
                transport: {
                    // "jsonp" is required for cross-domain requests; use "json" for same-domain requests
                    create: function (e) { o.call_server("c", e, dataItem); },
                    read: function (e) { o.call_server("r", e, dataItem); },
                    update: function (e) { o.call_server("u", e, dataItem); },
                    destroy: function (e) { o.call_server("d", e, dataItem); },
                    parameterMap: function (options, operation) {
                        if (operation !== "read") { return angular.toJson(options); }
                    }
                },
                error: function (e) {
                    o.customKendoDataSourceErrorHandler(e);
                    this.cancelChanges();
                },
                //filter: o.assetGridDataSource_filter(dataItem),
                //sort: [{ field: "usertype", dir: "asc" }, { field: "username", dir: "asc" }],
                pageSize: 15,
                schema: o.Schema
            });
        };

        o.DataSource_local = new kendo.data.DataSource({ data: o.sSchedule_default_list });

        o.SelectOnChange = function (oZone_DBinf) {

            var op = "sch_def";

            var dataItem = {
                zone_inf: oZone_DBinf
                //zone_ref: global.zone_ref,
                //appl_inf: global.appl_inf,
                //appl_ref: global.appl_ref
            };

            console.log(dataItem);

            var success = function () {
                console.log("success()");
                //Refresh the Scheduler if the server data is changed
                var sch = $(o.parent).data(global._kS);
                if (sch) { sch.dataSource.read(); sch.refresh(); }
            };

            server_access.not_dataSource_crud(o.kName, op, o.http_link_edit, "POST", dataItem, success);
        };


        o.Options = {
            dataSource: o.DataSource_online(),
            //dataSource: o.DataSource_local(),
            //optionLabel: "--Select--",
            dataTextField: "default_name",
            dataValueField: "default_id",
            change: function (e) {
                //console.log("Changed to: " + this.value());
                global.oZone_DBinf.default_id = this.value();
                //console.log(o.oZone_DBinf);
                o.SelectOnChange(global.oZone_DBinf);
            }
        };


        //Return service object
        return o;
    }]);