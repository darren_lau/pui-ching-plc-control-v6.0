var app = angular.module('homeScript');

app.factory('KendoScheduler_Appliance', [
    'global', 'server_access',
	'$http', '$log', '$window', '$q', '$timeout',
    'KendoDropDownList_ScheduleDefault',
    function (
        global, server_access,
		$http, $log, $window, $q, $timeout,
        KendoDropDownList_ScheduleDefault
	) {
        var o = {
            //Constant section (for Scheduler)
            _iSchedulingInterval: global._iSchedulingInterval,
            dCurrentSelectedDate: new Date(), //Currently selected date for applScheudler
            oTodayDate: function () { return new Date(new Date().setHours(0, 0, 0, 0)); },
            oOneMonthLaterDate: function () {
                var newMonth = new Date(new Date(new Date().setMonth(new Date().getMonth() + 1)).setHours(0, 0, 0, 0));
                //console.log("Call newMonth = " + newMonth);
                return newMonth;
            },

            kName: "#applScheduler",
            pushtoPLC_top: "#applScheduler_toPLC_top",
            startTP: "#applSchedulerStartTimePicker",
            endTP: "#applSchedulerEndTimePicker",
            eventTemplate: "#applScheduler_eventTemplate",
            editTemplate: "#applScheduler_editor",

            http_link_save: server_access.http_link + "/editOneDaySchedule",
            http_link_push: server_access.http_link + "/refresh_plc_console"
        };

        o.oNextTimeSlotInDate = function () {
            //var now = new Date(new Date().getTime() + 1000 * 60 * 30); //30 mins later
            var now = new Date(new Date().getTime() + 1000 * 60 * o._iSchedulingInterval); //15 mins later
            //now.setHours(now.getHours(), Math.floor((now.getMinutes() / 30)) * 30, 0, 0); //Floor function
            now.setHours(now.getHours(), Math.floor((now.getMinutes() / o._iSchedulingInterval)) * o._iSchedulingInterval, 0, 0); //Floor function
            return new Date(now);
        };

        o.oNextNextTimeSlotInDate = function () {
            //var now = new Date(new Date().getTime() + 1000 * 60 * 30); //30 mins later
            var now = new Date(new Date().getTime() + 1000 * 60 * 2 * o._iSchedulingInterval); //15 mins later
            //now.setHours(now.getHours(), Math.floor((now.getMinutes() / 30)) * 30, 0, 0); //Floor function
            now.setHours(now.getHours(), Math.floor((now.getMinutes() / o._iSchedulingInterval)) * o._iSchedulingInterval, 0, 0); //Floor function
            return new Date(now);
        };

        o.oStartOfDate = function () {
            //var now = new Date(new Date().getTime() + 1000 * 60 * 30); //30 mins later
            var now = new Date(); //15 mins later
            //now.setHours(now.getHours(), Math.floor((now.getMinutes() / 30)) * 30, 0, 0); //Floor function
            now.setHours(0, 0, 0, 0); //Floor function
            return new Date(now);
        };

        o.DateToStr = function (d) {
            var h = d.getHours();
            var m = d.getMinutes();
            var apm = "AM";
            var ph = "";
            var pm = "";
            if (h > 12) { h -= 12; apm = "PM"; }
            else if (h == 12) { apm = "PM"; }
            else if (h < 10) { ph = "0"; }
            if (m < 10) { pm = "0"; }
            else if (m == 0) { pm = "00"; }
            return ph + h + ":" + pm + m + " " + apm;
        }

        o.fConvertDateToSlots = function (start, end) {
            //Param start and end are day objects. 
            //Return {startX, endX} for time_slots index.
            if ((start instanceof Date) && (end instanceof Date)) {
                //return {
                //    startX: (start.getHours() - 8) * (60 / 30) + Math.floor(start.getMinutes() / 30),
                //    endX: (end.getHours() - 8) * (60 / 30) + Math.floor(end.getMinutes() / 30) - 1
                //};
                return {
                    startX: (start.getHours() - 7) * (60 / o._iSchedulingInterval) + Math.floor(start.getMinutes() / o._iSchedulingInterval),
                    endX: (end.getHours() - 7) * (60 / o._iSchedulingInterval) + Math.floor(end.getMinutes() / o._iSchedulingInterval) - 1
                };
            } else {
                return null;
            }
        };

        //_itime_slot_length: (23 - 7) * (60 / 30), //08:00 - 23:00 with 30-minute interval
        o._itime_slot_length = (23 - 7) * (60 / o._iSchedulingInterval); //08:00 - 23:00 with 15-minute interval

        o.make_http_link_read = function () {
            //Step 1: Make the corrosponding http read request
            var curZone = global.oZone_DBinf ? global.oZone_DBinf.zoneId : "null";
            var curDate = o.dCurrentSelectedDate;
            var dateIndex = curDate.getDate();
            var year = curDate.getFullYear();
            var month = curDate.getMonth() + 1;
            var day = curDate.getDate();
            if (month < 10) { month = "0" + month; }
            if (day < 10) { day = "0" + day; }
            var dateString1 = year + "-" + month + "-" + day;
            return server_access.http_link
                + '/getSchedule_scheduler_month?siteId=' + global.siteId
                + '&zoneId=' + curZone
                //+ '&date_index=' + dateIndex
                + '&mod_date_value=' + dateString1;
            //console.log(o.dCurrentSelectedDate);
            //e.error(o.oNot_supported_obj(o.kName, op), 0, "");
        }

        o.on_read_success = function (raw_data, e) {
            var curZone = global.oZone_DBinf ? global.oZone_DBinf.zoneId : "null";
            var curDate = o.dCurrentSelectedDate;
            var dateIndex = curDate.getDate();
            var year = curDate.getFullYear();
            var month = curDate.getMonth() + 1;
            var day = curDate.getDate();
            if (month < 10) { month = "0" + month; }
            if (day < 10) { day = "0" + day; }
            var dateString1 = year + "-" + month + "-" + day;

            //console.log(raw_data);
            try {
                data = angular.fromJson(raw_data);
            } catch (err) {
                e.error(o.oNot_json_obj(o.kName, op), 0, "");
                return;
            }
            //Step 2: Modify the list to fit the schema
            var final_arr = [];
            //console.log(new Date() + "data.length " + data.length);
            //if (data.length > 0) {
            for (var i0 = 0; i0 < data.length; i0++) {
                //Remake the dayString
                var itr_day = i0 + 1;
                if (itr_day < 10) { itr_day = "0" + itr_day; }
                var str_month = parseInt(month) < 10 ? "0" + parseInt(month) : "" + parseInt(month);
                var dateString = year + "-" + str_month + "-" + itr_day;
                //console.log("dateString = " + dateString + " dateString1 = " + dateString1);
                if (new Date(dateString) < new Date(dateString1)) {
                    //console.log("str_month = " + str_month);

                    var int_month = parseInt(str_month) + 1;
                    //console.log("int_month = " + int_month);
                    str_month = int_month < 10 ? "0" + int_month : "" + int_month;
                    dateString = year + "-" + str_month + "-" + itr_day;
                }
                //console.log("newDateString = " + dateString);
                /**
                if (i0 == 20) {
                    console.log(new Date(dateString).getMonth() + 1);
                    console.log(parseInt(dateString.split('-')[1]));
                }
                **/

                //console.log(dateString + 'ZZZZZZZZ' + dateString1);
                if (new Date(dateString).getMonth() + 1 != parseInt(dateString.split('-')[1])) {
                    //console.log("Skipped: " + dateString);
                    continue;
                }

                var timeSlots = data[i0].time_slots;
                for (var x = 0; x < timeSlots.length; x++) {
                    //Stupid Date object parsing
                    //console.log(timeSlots[x].desc.substring(0, 5));
                    //console.log(timeSlots[x].desc.substr(timeSlots[x].desc.length - 5));
                    var sh = parseInt(timeSlots[x].desc.substring(0, 5).split(':')[0]);
                    var sm = parseInt(timeSlots[x].desc.substring(0, 5).split(':')[1]);
                    var eh = parseInt(timeSlots[x].desc.substr(timeSlots[x].desc.length - 5).split(':')[0]);
                    var em = parseInt(timeSlots[x].desc.substr(timeSlots[x].desc.length - 5).split(':')[1]);
                    var sd = new Date(dateString); sd.setHours(sh, sm, 0, 0);
                    var ed = new Date(dateString); ed.setHours(eh, em, 0, 0);
                    //console.log(sd);
                    //console.log(ed);
                    if (parseInt(timeSlots[x].slot_value) > 0) {
                        if (sd.getTime() < o.oNextTimeSlotInDate().getTime()) {
                            //console.log("skipped: " + sd);
                            continue;
                        }
                        else if ((final_arr.length > 0)
                            && (final_arr[final_arr.length - 1].end.getTime() == sd.getTime())
                            && (final_arr[final_arr.length - 1].remark == timeSlots[x].remark)) {
                            final_arr[final_arr.length - 1].end = ed;
                        } else {
                            /**
                            if (x == 10) {
                                console.log(.length - 1].end);
                                console.log(sd);
                                console.log(angular.equals(final_arr[final_arr.length - 1].end == sd));
                            }
                            **/
                            final_arr.push({
                                id: data[i0]._id,
                                remark: timeSlots[x].remark,
                                start: sd,
                                end: ed,
                                siteId: data[i0].siteId,
                                zoneId: data[i0].zoneId,
                                date_index: data[i0].date_index,
                                mod_date_value: data[i0].mod_date_value.substring(0, 10),
                                slotId: timeSlots[x].slotId,
                                slot_value: timeSlots[x].slot_value
                            });
                        }
                    }
                }
            }

            //Special operation: Change background colors of the Scheudlar
            //o.SchedulerChangeBG(o.kName);

            //console.log(final_arr);
            e.success(final_arr);
        }

        o.make_save_post_obj = function (e, dI) {
            //If it is called by Kendo with an Object kendo.dataSource (yes it is a specified object),
            //dataSource.data() contains the processed data already.
            //This function is to convert the dataSource.data() to meet the server's schema.
            //If it is called by anything else, insert the stuffs into dI.not_dataSource and dI.target_obj before here, or it fails.

            //Meanwhile, the post_obj count valid ONLY from the o.oNextTimeSlotInDate().
            //Server must handle this case correctly.
            //Limitation (or not): Only 1 day will be effected. Original function: saveOneDaySchedule

            //console.log($(o.kName).data(global._kS).dataSource.data());
            //console.log("NOPE"); return; 

            //Step 0: Check data present
            var events;
            if ($(o.kName)
                && $(o.kName).data(global._kS)
                && $(o.kName).data(global._kS).dataSource
                && $(o.kName).data(global._kS).dataSource.data()) {
                events = $(o.kName).data(global._kS).dataSource.data();
            } else if (dI && dI.not_dataSource) {
                events = dI.not_dataSource;
            } else {
                e.error(o.oRequest_fail_obj(o.kName, op + "(client)"), 0, "");
                return;
            }
            var target;
            if (e && e.data) {
                target = e.data;
            } else if (dI && dI.target_obj) {
                target = dI.target_obj;
            } else {
                e.error(o.oRequest_fail_obj(o.kName, op + "(client)"), 0, "");
                return;
            }
            if (!events || !target || !target.start) {
                e.error(o.oRequest_fail_obj(o.kName, op + "(client)"), 0, "");
                return;
            }

            //Step 1: Make dateString from target, initialize Date objects and time_slots
            //Assume there are no cross-day event and dateString can be constructed by target.start
            var tempYear = target.start.getFullYear();
            var tempMonth = target.start.getMonth() + 1;
            var tempDay = target.start.getDate();
            if (tempMonth < 10) { tempMonth = "0" + tempMonth; }
            if (tempDay < 10) { tempDay = "0" + tempDay; }
            var dateString1 = tempYear + "-" + tempMonth + "-" + tempDay;

            var targetDayObject = angular.copy(target.start);
            targetDayObject.setHours(0, 0, 0, 0);
            var nextDateObject = angular.copy(targetDayObject);
            nextDateObject.setDate(target.start.getDate() + 1);

            var time_slots = [];
            for (var x = 0; x < o._itime_slot_length; x++) {
                time_slots.push({ slot_value: "", remark: "" });
            }
            //console.log(dateString1);
            //console.log(targetDayObject);
            //console.log(nextDayObject);
            //clearTimeout(o.oRequest_timeout_obj);
            //e.success(e.data); return;

            //Step 3: Loop through the events, get the events from the target day, and then convert them into time_slots
            console.log("events:");
            console.log(events);
            for (var x = 0; x < events.length; x++) {
                //Step 4: Validation and ignore invalid case
                if (!(events[x] && events[x].start && events[x].end)) { continue; }
                if (events[x].start < targetDayObject || events[x].start > nextDateObject) { continue; }

                //Step 5: Make time_slots
                var event = events[x];
                var timeDiff = event.end.getTime() / 60000 - event.start.getTime() / 60000; //60 * 1000ms 
                console.log("timeDiff = " + timeDiff);
                var tempObj = o.fConvertDateToSlots(event.start, event.end); //return { startX, endX }
                if (!tempObj) { console.log("ERROR: fConvertDateToSlots return null"); continue; } else { console.log(tempObj); }
                var startX = tempObj.startX;
                var endX = tempObj.endX;
                //if (timeDiff > 30) {
                if (timeDiff > o._iSchedulingInterval) {
                    if (startX < endX) {
                        for (var y = startX; y <= endX; y++) {
                            if (startX == y)
                                time_slots[y].slot_value = "1";
                            else if (y == endX)
                                time_slots[y].slot_value = "3";
                            else
                                time_slots[y].slot_value = "2";
                            time_slots[y].remark = event.remark;
                        }
                    }
                } else if ((startX != null) && (startX >= 0)) {
                    //console.log(startX);
                    time_slots[startX].slot_value = (event.slot_value == "1") ? "1" : "0";
                    time_slots[startX].remark = event.remark;
                }
            }

            //Step 6: Clean up events with slot_value = 0
            for (var x = 0; x < o._itime_slot_length; x++) {
                if ((time_slots[x].slot_value != "1") && (time_slots[x].slot_value != "2") && (time_slots[x].slot_value != "3")) {
                    time_slots[x].slot_value = "0";
                    time_slots[x].remark = "";
                }
            }

            //Step 7: slot_value done. Fire POST request
            return {
                siteId: global.siteId,
                zoneId: global.oZone_DBinf.zoneId,
                date_index: parseInt(tempDay),
                mod_date_value: dateString1,
                time_slots: angular.toJson(time_slots)
            };
        }

        //This is just an adaper. In general KendoGrid case, you don't even need this!
        //Map of functions with k = op, v = function (kName, op, e, dI)
        //kName: name of involved component in HTML (convension)
        //op: operation (c/r/u/d) (or mqtt)
        //e: Kendo event object (leave it null if not proided)
        //dataItem: dataItem provided by Kendo (or built by user manually)
        o.server_access = {
            //Since the saving mechianism is moved to the distinct button,
            //Unless the "read" is real $http request, others are local transport.
            //Keeping the code at this structure is for "neat coding style"

            //Special case: c = u = d = save, therefore redirect it.
            //Note that no need to stop the timer here
            "c": function (kName, op, e, dI) { o.server_access["save"](o.kName, "save", e, dI); },
            "u": function (kName, op, e, dI) { o.server_access["save"](o.kName, "save", e, dI); },
            "d": function (kName, op, e, dI) { o.server_access["save"](o.kName, "save", e, dI); },

            //$http.get(http_link_read).success(o.on_read_success).error(function (data, status, headers, config) {
            //    e.error(o.oErr_from_server(kName, op, data, status), 0, "");
            //});
            //e.error(o.oNot_supported_obj(kName, op), 0, "");
            "r": function (kName, op, e, dI) {
                server_access.init(o.kName, op, o.make_http_link_read(), "GET", null,
                    function (data) { o.on_read_success(data, e); },
                    e.error);
            },
            "save": function (kName, op, e, dI) {
                server_access.init(o.kName, op, o.http_link_save, "POST", o.make_save_post_obj(e, dI),
                    function (data) { console.log(data); e.success(true); },
                    e.error);
            },
        };

        o.pushtoPLC = function (zone_inf, zone_ref, appl_inf, appl_ref) {
            var op = "push";

            var dI = {
                zone_inf: zone_inf,
                zone_ref: zone_ref,
                appl_inf: appl_inf,
                appl_ref: appl_ref
            };

            if (!(dI && dI.zone_inf && dI.zone_inf.zoneId)) {
                console.log(o.kName + ", " + op + ": " + "Zone ID Not found!");
                return;
            }

            var success = function (data) {
                alert("PLC updated successfully!");
                console.log(data);
            }

            var post_obj = { just: "doit", zoneId: dI.zone_inf.zoneId };

            server_access.not_dataSource_crud(o.kName, op, o.http_link_push, "POST", post_obj, success);
        }

        o.DataSource_online = function (dataItem) {
            return new kendo.data.SchedulerDataSource({
                //batch: true,
                transport: {
                    create: function (e) { o.server_access['c'](o.kName, "c", e, dataItem); },
                    read: function (e) { o.server_access['r'](o.kName, "r", e, dataItem); },
                    update: function (e) { o.server_access['u'](o.kName, "u", e, dataItem); },
                    destroy: function (e) { o.server_access['d'](o.kName, "d", e, dataItem); },
                    parameterMap: function (options, operation) {
                        if (operation !== "read" && options.models) {
                            return { models: kendo.stringify(options.models) };
                        }
                    }
                },
                requestEnd: function (e) {
                    if (e.type != "read") {
                        e.sender.read();
                        if ($(o.kName).data(global._kS)) {
                            $(o.kName).data(global._kS).refresh();
                        }
                    }
                    //alert("HELLO");
                    //console.log(e);
                },
                schema: {
                    model: {
                        id: "id",
                        fields: {
                            id: { from: 'id', defaultValue: "" },
                            remark: { from: 'remark', defaultValue: "" },
                            start: { from: 'start', defaultValue: new Date(), validation: { required: true } },
                            end: { from: 'end', defaultValue: new Date(), validation: { required: true } },
                            siteId: { from: 'siteId', defaultValue: "", validation: { required: true } },
                            zoneId: { from: 'zoneId', defaultValue: "", validation: { required: true } },
                            date_index: { from: 'date_index', defaultValue: 0 },
                            mod_date_value: { from: 'mod_date_value', defaultValue: "" },
                            slotId: { from: 'slotId', defaultValue: 0 },
                            slot_value: { from: 'slot_value', defaultValue: 1, validation: { required: true } }
                        }
                    }
                },
                /**
                filter: {
                    logic: "or",
                    filters: [
                        { field: "ownerId", operator: "eq", value: 1 },
                        { field: "ownerId", operator: "eq", value: 2 }
                    ]
                }
                **/
            });
        };

        o.Options = function (dataItem) {
            return {
                dataSource: o.DataSource_online(dataItem),
                date: new Date(),
                min: new Date(),
                max: new Date(o.oOneMonthLaterDate().getTime() - 1), //Since here don't support ">=" symbol
                currentTimeMarker: false,
                majorTick: o._iSchedulingInterval, //30,
                minorTickCount: 1,
                mobile: true,
                editable: {
                    template: kendo.template($(o.editTemplate).html()),
                    move: true,
                    resize: false,
                    //width: 1000,
                    confirmation: "Are you sure you want to cancel/delete this slot?"
                },
                messages: {
                    allDay: "Whole Day",
                    event: "Require Card Input",
                    editor: {
                        editorTitle: "Edit event"
                    }
                },
                eventTemplate: kendo.template($(o.eventTemplate).html()),
                footer: false,
                allDaySlot: false,
                startTime: new Date(new Date().setHours(7, 0, 0, 0)),//o.oNextTimeSlotInDate(), //Don't show past event of the current day
                endTime: new Date(new Date().setHours(23, 0, 0, 0)),
                //height: 800,
                //width: 800,
                views: ["day", "week", "month"],
                /**
                [{
                    type: "month",
                    startTime: new Date(),
                    endTime: new Date(new Date().setDate(new Date().getDate() + 31))
                }, "day"],
                **/
                timezone: "Etc/UTC",

                cancel: function (e) {
                    e.preventDefault();
                    e.container.data("kendoWindow").close();
                    e.sender.dataSource.read();
                    e.sender.refresh();
                },


                dataBound: function (e) {
                    o.SchedulerChangeBG("applScheduler", false, e);
                },

                edit: function (e) {
                    //console.log(e.event.start);
                    //Set window size
                    $(".k-edit-form-container").width("680px");
                    $(".k-edit-form-container").height("auto");

                    //Special case: Month view selecting today
                    if (e.sender.element.context.textContent.indexOf("AM") < 0) {
                        e.event.start = o.oNextTimeSlotInDate();
                        e.event.end = o.oNextNextTimeSlotInDate();
                        //$(o.startTP).data(global._kTP).value("08:00 AM");
                        //$(o.endTP).data(global._kTP).value("08:" + o._iSchedulingInterval + " AM");
                        $(o.startTP).data(global._kTP).value(o.DateToStr(e.event.start));
                        $(o.endTP).data(global._kTP).value(o.DateToStr(e.event.end));
                    }

                    //Block out range selection
                    //var selectedDate = (e.sender.element.context.textContent.indexOf("AM") < 0) ? o.oNextTimeSlotInDate() : e.event.start;
                    //console.log(selectedDate);
                    var selectedDate = e.event.start;
                    if ((selectedDate < o.oNextTimeSlotInDate()) || (selectedDate >= o.oOneMonthLaterDate())) {
                        alert("Outside scheduler range");
                        e.preventDefault();
                        return;
                    }

                    //Recorrect default time
                    if (e.event.start.getHours() < 7) {
                        e.event.start.setHours(7, 0, 0, 0);
                        $(o.startTP).data(global._kTP).value("07:00 AM");
                    }
                    //if (e.event.end.getHours() < 8) {
                    //    e.event.end.setHours(8, o._iSchedulingInterval, 0, 0);
                    //    $(o.endTP).data(global._kTP).value("08:30 AM");
                    //}
                    //Change for 15-minute-interval
                    if (e.event.end.getHours() < 7) {
                        e.event.end.setHours(7, o._iSchedulingInterval, 0, 0);
                        $(o.endTP).data(global._kTP).value("07:" + o._iSchedulingInterval + " AM");
                    }

                    //console.log(e.event.start);

                    /**
                    //If it's create event, change the text "Cancel" to "Delete"
                    if (e.event.isNew()) {
                        e.container.find(".k-scheduler-cancel").html('Delete');
                    }
                    **/
                },
                navigate: function (e) {
                    //Must set to 00:00 since pressing "Today" in Scheduler are returning new Date()
                    var selectedDate = new Date(e.date.setHours(0, 0, 0, 0));
                    //Block out range selection
                    if (selectedDate < o.oTodayDate() || selectedDate >= o.oOneMonthLaterDate()) {
                        alert("Outside scheduler range");
                        e.preventDefault();
                        return;
                    }
                    var dropdownlist = $(KendoDropDownList_ScheduleDefault.kName_Sch).data(global._kDDL);

                    //Disable button if not showing today events
                    if ((e.view == "day") && (selectedDate.getTime() == o.oTodayDate().getTime())) {
                        //console.log(e.date);
                        $(o.pushtoPLC_top).prop("disabled", false).removeClass("k-state-disabled");
                        dropdownlist.enable(false);
                    } else {
                        $(o.pushtoPLC_top).prop("disabled", true).addClass("k-state-disabled");
                        dropdownlist.enable();
                    }

                    //o.SchedulerChangeBG("applScheduler", true, e);
                },
                add: function (e) {
                    if (!o.checkAvailability(e.event.start, e.event.end, e.event)) {
                        e.preventDefault();
                    }
                },
                save: function (e) {
                    //console.log(e.event.end);
                    //Correct time in auto or deny it
                    if (e.event.end < o.oNextTimeSlotInDate()) { e.preventDefault(); }
                    else if (e.event.start < o.oNextTimeSlotInDate()) { e.event.start = o.oNextTimeSlotInDate(); }
                    else if (!o.checkAvailability(e.event.start, e.event.end, e.event)) { e.preventDefault(); }
                },
                resize: function (e) {
                    if (o.checkClash(e.start, e.end, e.event)) {
                        this.wrapper.find(".k-marquee-color").addClass("invalid-slot");
                        e.preventDefault();
                    }
                },
                resizeEnd: function (e) {
                    if (!o.checkAvailability(e.start, e.end, e.events)) {
                        e.preventDefault();
                    }
                },
                remove: function (e) {
                    //alert("Removing", e.event.siteId);
                }
            }
        };

        //The following checking function are made for Kendo Scheudler
        //They are called frequently by the event handlers of Kendo Scheudler
        o.occurrencesInRangeByResource = function (start, end, resourceFieldName, event) {
            var scheduler = $(o.kName).getKendoScheduler();

            var occurrences = scheduler.occurrencesInRange(start, end);

            var idx = occurrences.indexOf(event);
            if (idx > -1) {
                occurrences.splice(idx, 1);
            }

            event = $.extend({}, event);

            return o.filterByResource(occurrences, resourceFieldName, event[resourceFieldName]);
        }

        o.filterByResource = function (occurrences, resourceFieldName, value) {
            var result = [];
            var occurrence;

            for (var idx = 0, length = occurrences.length; idx < length; idx++) {
                occurrence = occurrences[idx];
                if (occurrence[resourceFieldName] === value) {
                    result.push(occurrence);
                }
            }
            return result;
        }

        o.checkClash = function (start, end, event) {
            var occurrences = o.occurrencesInRangeByResource(start, end, "id", event);
            if (occurrences.length > 0) {
                return true;
            }
            return false;
        }

        o.checkAvailability = function (start, end, event) {
            if (o.checkClash(start, end, event)) {
                setTimeout(function () {
                    alert("This slot is not available in this time period.");
                }, 0);
                return false;
            }
            return true;
        }

        o.applSchedulerTimePickerOptions = {
            min: "07:00",
            max: "23:00",
            interval: o._iSchedulingInterval //30
        };

        //Change color of specified Kendo Scheduler with custom rules
        //Warning: Big mess with jQuery, HTML and CSS, not MVC style
        o.SchedulerChangeBG = function (kName, onNavigate, e) {
            //console.log(e.sender);
            //First check exist of the object
            if (!$(o.kName) && $(o.kName).data(global._kS)) {
                console.log("SchedulerChangeBG: element not found from " + kName); return;
            } else {
                var scheduler = $(o.kName).data(global._kS);
                var view = scheduler.view();
                view.table.find("td[role='gridcell']").each(function () {
                    if ($(this) != null) {
                        var element = $(this);
                        if (element != null) {
                            var slot = scheduler.slotByElement(element);
                            //console.log(element);
                            //console.log(slot.isDaySlot);
                            if (slot != null) {
                                //element.addClass("invalid-slot");
                                var dateSlot = slot.startDate;
                                var cmpDate = (!slot) ? o.oNextTimeSlotInDate() : (slot.isDaySlot) ? o.oStartOfDate() : o.oNextTimeSlotInDate();
                                if ((dateSlot < cmpDate) || (dateSlot >= o.oOneMonthLaterDate())) {
                                    //console.log(dateSlot);
                                    //console.log(element);
                                    element.addClass("invalid-slot");
                                    //scheduler.refresh();
                                } else if ((dateSlot.getDay() == 0) || (dateSlot.getDay() == 6)) {
                                    element.addClass("holiday-slot");
                                }
                            }
                        }
                    }
                });
            }
        };

        o.reload_online = function () {
            if ($(o.kName)  && $(o.kName).data(global._kS) && $(o.kName).data(global._kS).dataSource) {
                $(o.kName).data(global._kS).dataSource.read();
                $(o.kName).data(global._kS).refresh();
            }
        }

        return o;
    }]);