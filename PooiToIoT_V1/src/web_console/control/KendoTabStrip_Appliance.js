var app = angular.module('homeScript');

app.factory('KendoTabStrip_Appliance', ['$http', '$log', '$window', '$q', '$timeout', 'global',
    function ($http, $log, $window, $q, $timeout, global) {
        var o = {
            kName: "#applGrid_tab",
        };

        o.onUserType = function (sUserType) {

            var tab = $(o.kName).data(global._kTS);

            //Hide or disable elements which don't match the sUserType (initially show all)
            if (sUserType == "system_admin") {
                //Kept for potential use
            } else if (sUserType == "admin") {
                //Kept for potential use
                if (tab) {
                    //Remove is irreversible process
                    tab.remove(6);
                    tab.remove(4);
                    tab.remove(3);
                }
            } else {
                //If it's not effective here (usually involve AJAX contents),
                //Do the operation in their settings
                //e.g. sideTree
                /**
                var sideTree = $("#sideTree").data("kendoTreeView");
                sideTree.remove(sideTree.findByText("Add Zone"));
                **/

                //To endable or disable tabs in Kendo Tab Strip
                if (tab) {
                    //Remove is irreversible process
                    tab.remove(6);
                    tab.remove(4);
                    tab.remove(3);
                    tab.enable(tab.tabGroup.children().eq(1), false);
                }
            }
        }
        //Return service object
        return o;
    }]);