var app = angular.module('homeScript');


app.factory('setZone_inf', ['$http', '$log', '$window', '$q', '$timeout', 'global', 'server_access', 'KendoScheduler_Appliance',
    function ($http, $log, $window, $q, $timeout, global, server_access, KendoScheduler_Appliance) {
        var o = {
            kName: "setZone_inf",
            APIRoute: server_access.http_link,
            sCurrentMqttTopic: "",
            sSensorArr: ["Humidity", "Temperature", "PM2.5", "PM10", "VOC"],
            audio: new Audio('resources/se/AlertEffect.mp3')
        };

        //Set oZone_inf by o.server_access
        o.init = function (location) {
            console.log("o.init()");

            global.oZone_inf.length = 0;

            //location = "zoneId:desc"
            //If zoneId if absent, return directly.
            var zoneId;
            try {
                zoneId = location.split(':')[0];
            } catch (e) {
                console.log(o.kName + ": " + "zoneId is missing!");
                zoneId = false;
                return;
            }

            var http_link_read_devices = o.APIRoute + "/getDevices?zoneId=" + zoneId;
            var op = "r, device";

            var success_getZone = function (data) {
                //console.log("success_getZone()");
                var http_link_sen = o.APIRoute + "/getStatus?client_type=Sensor&zoneId=" + zoneId;
                var op = "sensor";

                var Zone_arr = {}; //Final data map
                var device_arr;
                try { device_arr = angular.fromJson(data); }
                catch (err) { global.customKendoDataSourceErrorHandler(global.oNot_json_obj(o.kName, op), 0, ""); return; }
                for (var i2 = 0; i2 < device_arr.length; i2++) {
                    o.sCurrentMqttTopic = device_arr[i2].mqtt_tag;
                    global.oZone_DBinf = device_arr[i2];
                    var device_obj = device_arr[i2];
                    //Step 2: Combine the 2 arrays into a big data map
                    for (var i1 = 0; i1 < device_obj.appliances.length; i1++) {
                        if (!Zone_arr[device_obj.appliances[i1].appl_type]) { Zone_arr[device_obj.appliances[i1].appl_type] = []; }
                        Zone_arr[device_obj.appliances[i1].appl_type].push({
                            siteId: device_obj.siteId,
                            mqttclient_id: device_obj.mqttclient_id,
                            client_id: device_obj.client_id,
                            client_name: device_obj.client_name,
                            client_type: device_obj.client_type,
                            ip_addr: device_obj.ip_addr,
                            mac: device_obj.mac,
                            zoneId: device_obj.zoneId,
                            tower: device_obj.tower,
                            floor: device_obj.floor,
                            mqtt_tag: device_obj.mqtt_tag,
                            desc: device_obj.desc,
                            appl_name: device_obj.appliances[i1].appl_name,
                            appl_type: device_obj.appliances[i1].appl_type,
                            appl_index: device_obj.appliances[i1].appl_index
                        });
                    }
                    //console.log(Zone_arr);
                    //Ouput the final array
                    // $timeout.cancel(o.request_timeout_obj);
                }

                //Sort by device name
                Object.keys(Zone_arr).forEach((appl_type) => {
                    Zone_arr[appl_type] = Zone_arr[appl_type].sort((a,b) => {
                        return a.appl_name.localeCompare(b.appl_name);
                    });
                });           

                global.oZone_inf = Zone_arr;
                KendoScheduler_Appliance.reload_online();
                server_access.not_dataSource_crud(o.kName, op, http_link_sen, "GET", null, success_getSensor);
            };

            var success_getSensor = function (data) {
                //Double try without fail message
                if (data && data.length > 0) {
                    o.setZone_status("Sensor", data);
                    $timeout(function () { o.setZone_status("Sensor", data); }, 3000);
                } else {
                    console.log("Error: Device_Status not found in DB. location: " + location);
                }
            };

            //Return empty array if no query param
            //if (!dI.location) { clearTimeout(o.oRequest_timeout_obj); e.success([]); return; }
            //console.log(http_link_read_devices);
            server_access.not_dataSource_crud(o.kName, op, http_link_read_devices, "GET", null, success_getZone);
        };

        o.makeBits = function (number) {
            var s = number.toString(2);
            var l = 8 - s.length;
            if (l > 0) {
                for (var i = 0; i < l; i++) {
                    s = "0" + s;
                }
            }
            return s;
        };

        //Set oZone_status and then modify oZone_inf
        o.setZone_status = function (topic, newVal) {
            var oZone_inf = global.oZone_inf;
            //console.log([topic, newVal]);

            //"Tower/Block/zoneId/cmd"
            //cmd = {"Status/setStatus/Sensor"}
            if (!oZone_inf) { return; }
            if (topic.indexOf("setStatus") >= 0) { return; }

            if (topic.indexOf("Status") >= 0 && topic.indexOf("Sensor") < 0) {
                //console.log("o.setZone_status()");
                for (var i0 = 0; i0 < newVal.Appliance.length; i0++) {
                    for (var i1 = 0; i1 < newVal.Appliance[i0].devices.length; i1++) {
                        if (oZone_inf[newVal.Appliance[i0].appl_type]) {
                            for (var i2 = 0; i2 < oZone_inf[newVal.Appliance[i0].appl_type].length; i2++) {
                                if (newVal.Appliance[i0].devices[i1].appl_name === oZone_inf[newVal.Appliance[i0].appl_type][i2].appl_name) {
                                    //oZone_inf[newVal.Appliance[i0].appl_type][i2].state = newVal.Appliance[i0].devices[i1].state;
                                    //oZone_inf[newVal.Appliance[i0].appl_type][i2].on_off = newVal.Appliance[i0].devices[i1].on_off;
                                    var p = newVal.Appliance[i0].devices[i1];
                                    for (var key in p) {
                                        if (p.hasOwnProperty(key)) {
                                            //Append the status
                                            oZone_inf[newVal.Appliance[i0].appl_type][i2][key] = p[key];
                                            if (key === 'lock_bits') {
                                                //oZone_inf[newVal.Appliance[i0].appl_type][i2][key] = o.makeBits(p[key]);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } else if (topic.indexOf("Sensor") >= 0) {
                console.log("setZone_inf: Loading sensor...");
                var lock;
                if (!newVal.Appliance) { return; }
                for (let i0 = 0; i0 < newVal.Appliance.length; i0++) {
                    if (o.sSensorArr.indexOf(newVal.Appliance[i0].appl_type) < 0) { continue; }
                    for (let i1 = 0; i1 < newVal.Appliance[i0].devices.length; i1++) {
                        if (!oZone_inf.sensor) { oZone_inf.sensor = {}; }

                        if (!oZone_inf.sensor[newVal.Appliance[i0].appl_type]) {
                            oZone_inf.sensor[newVal.Appliance[i0].appl_type] = [];
                        }

                        lock = false;

                        for (let i2 = 0; i2 < oZone_inf.sensor[newVal.Appliance[i0].appl_type].length; i2++) {
                            //console.log(oZone_inf.sensor[newVal.Appliance[i0].appl_type]);
                            var a = oZone_inf.sensor[newVal.Appliance[i0].appl_type][i2].appl_name; //console.log(a);
                            var b = newVal.Appliance[i0].devices[i1].appl_name; //console.log(b);
                            if (a === b) {
                                oZone_inf.sensor[newVal.Appliance[i0].appl_type][i2].value = parseInt(newVal.Appliance[i0].devices[i1].value);
                                //console.log('lock');
                                lock = true;
                            }
                        }

                        if (!lock) {
                            oZone_inf.sensor[newVal.Appliance[i0].appl_type].push({
                                appl_name: newVal.Appliance[i0].devices[i1].appl_name,
                                appl_type: newVal.Appliance[i0].appl_type,
                                value: parseInt(newVal.Appliance[i0].devices[i1].value)
                            });
                        }

                    }
                }
            }
        };

        o.setAlarm_status = function (Alarm_Status, topic, msg) {
            //console.log([topic, msg]);
            if (topic.indexOf("Status") >= 0) {
                //console.log(msg);

                var alarm_arr = msg.Appliance.map((appliance) => {
                    return appliance.devices.filter((device) => {
                        return device.state;
                    }).map((device) => {
                        return device.appl_name;
                    });
                });

                var out_arr = [];

                alarm_arr.forEach((alarm_subarr) => {
                    alarm_subarr.forEach((alarm_obj) => {
                        out_arr.push(alarm_obj);
                        o.audio.play();
                    });
                });

                //console.log(alarm_arr);

                return out_arr.length > 0 ? out_arr.reduce((a, c) => { return a + ", " + c; }) : "";
            } else {
                return Alarm_Status;
            }
        };

        //Return service object
        return o;
    }]);