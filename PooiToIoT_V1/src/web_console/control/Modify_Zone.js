var app = angular.module('homeScript');

app.factory('Modify_Zone', ['$http', '$log', '$window', '$q', '$timeout', 'global', 'server_access', 'KendoTreeView_Side',
    function ($http, $log, $window, $q, $timeout, global, server_access, KendoTreeView_Side) {
        var o = {
            kName: "Modify_Zone",
            APIRoute: server_access.http_link,
        };

        o.call_server = function (not_grid, op, zone_inf, zone_ref, appl_inf, appl_ref) {
            //Goal: call o.server_access with proper parameters
            //o.server_access = function (kName, op, e, dI) {...};
            console.log("o.not_dataSource_crud(" + o.kName + "," + op + ")");

            //Simple confirmation window 
            if (op == "d") {
                //Note: Although it's return type is boolean, it cannot be assigned to any variable
                if (!confirm("Are you sure you're going to delete the item?")) { return; }
            }

            var dataItem = {
                zone_inf: zone_inf,
                zone_ref: zone_ref,
                appl_inf: appl_inf,
                appl_ref: appl_ref
            };

            var success = function (data) {
                console.log("o.not_dataSource_crud: Operation success.");
                alert("Zone Modified.");
                if ((zone_inf) && (zone_inf.zoneId)) {
                    //Refresh the page
                    //The param of the function is based from the sideTree, which is modified
                    KendoTreeView_Side.reload_online();
                }
            }

            var API_setting_map = {
                route1: o.APIRoute,
                "c": { route2: "/addZone", method: "POST", obj: dataItem },
                //"r": { route2: "/getStatus?zoneId=" + dI.zoneId, method: "GET", obj: null },
                //"r_post": { route2: "/read", method: "POST", obj: e.data },
                "u": { route2: "/editZone", method: "POST", obj: dataItem },
                "d": { route2: "/deleteZone", method: "POST", obj: dataItem }
            };

            var http_link = API_setting_map.route1 + API_setting_map[op].route2;
            var http_method = API_setting_map[op].method;
            var post_obj = API_setting_map[op].obj;

            if (post_obj) {
                //Make server_access
                server_access.not_dataSource_crud(o.kName, op, http_link, http_method, post_obj, success);
            } else {
                global.customKendoDataSourceErrorHandler(global.oNot_complete_obj(o.kName, op));
            }
        }


        //Return service object
        return o;
    }]);