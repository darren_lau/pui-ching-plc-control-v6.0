var app = angular.module('homeScript');

app.factory('KendoLineChart_PowerUsage', [
    '$http', '$log', '$window', '$q', '$timeout',
    'global', 'server_access',
    //'ExportToXLSX_X_LC',
    //'KendoDropDownList_Zone', 'KendoDropDownList_Interval',
    function (
        $http, $log, $window, $q, $timeout,
        global, server_access,
        //ExportToXLSX_X_LC,
        //KendoDropDownList_Zone, KendoDropDownList_Interval
    ) {

        var o = {
            kName: "#KendoLineChart_PowerUsage",
            kLoadingBar: "#LoadingBar_PowerUsage",
            kNavBar: "#Graph_NavBar_KC_PowerUsage",
            APIRoute: global.URL_PREFIX + "/Device_History",

            BaseUnit: 15,//X minutes
            chartName: "Exported_LineCount.png",

            //Used to sum up the data
            group_arr: { "IN count": ["fw"], "OUT count": ["bw"] },
            field_ts: "timestamp",
            field_te: "timestamp",
            DataTypeName: function (c) { return ["Line", "Count"].join(c); }
        };

        o.show_loading_bar = function () {
            kendo.ui.progress($(o.kLoadingBar), true);
            $(o.kLoadingBar).show();
        };

        o.remove_loading_bar = function (e) {
            // Clear up the loading indicator for this chart
            //var loading = $(o.kLoadingBar, e.sender.element.parent())
            var loading = $(o.kLoadingBar);
            kendo.ui.progress(loading, false);
            $(o.kLoadingBar).hide();
        };


        o.Series = function () {
            var t = [
                //{ field: "created_in", name: "Created In" },
                //{ field: "deleted_in", name: "Deleted In" },
                //{ field: "created_deleted_in", name: "Created Deleted In" }
                //{ field: "fw", name: "IN count", color: "#428bca", visible: true, type: "column", stack: false, gap: 0, spacing: 0 }, //Forward
                //{ field: "bw", name: "OUT count", color: "#5cb85c", visible: true, type: "column", stack: false, gap: 0, spacing: 0 } //Backward
                { field: "power_usage", name: "kWh", type: "line", visible: true },
                //{ field: "fbw_diff", name: "DIFF", visible: true, type: "column", stack: false, gap: 0 },
            ];

            //console.log(t);
            return t;
        };

        o.SampleData = function () {
            var g = function () { return Math.floor(Math.random() * 100) + 100; }
            var h = function () {
                var p = {
                    created_in: g(),
                    deleted_in: g(),
                    created_deleted_in: g(),
                    fw: g(),
                    bw: g(),
                    fbw_net: g(),
                    fbw_diff: g()
                };
                p.timestamp = new Date();
                p.t = p.timestamp.getTime();
                p.timestamp_str = global.Date2Str(p.timestamp, "YYYY/MM/DD HH:mm");
                return p;
            }
            var a = [];
            for (var i = 0; i < 100; i++) { a.push(h()); }
            return a;
        }

        o.FilterStart = function () {
            return o.time_filter_param().clear_from;
        };

        o.FilterEnd = function () {
            return o.time_filter_param().clear_to;
        };

        o.default_zone = function () {
            return "PLC_2a"; //KendoDropDownList_Zone.sSchema_list[KendoDropDownList_Zone.default_index];
        };

        o.default_interval = function () {
            return "FIFTEEN_MINUTES"; //KendoDropDownList_Interval.sSchema_list[KendoDropDownList_Interval.default_index].value;
        };

        o.time_filter_param = function () {
            var now = new Date();
            var clear_to = new Date(now.getTime() + 86400 * 1000); clear_to.setHours(0, 0, 0, 0);
            var clear_from = new Date(now.getTime()); clear_from.setHours(0, 0, 0, 0);
            var a = {
                //kDDL: { "device_id": "#KDDL_KC_X_LC_Zone", "resolution": "#KDDL_KC_X_LC_Interval" },
                kDDL_interval: { "resolution": "#KDDL_KC_PU_Interval" }, //Used by Excel only
                kDDL: { "device_id": "#KDDL_KC_PU_Zone" },
                kDPS: { "timestamp": "#KDP_KC_PowerUsage_S" },
                kDPE: { "timestamp": "#KDP_KC_PowerUsage_E" },
                kTPS: { "timestamp": "#KTP_KC_PowerUsage_S" },
                kTPE: { "timestamp": "#KTP_KC_PowerUsage_E" },
                kDS: global._kDS(o.kName, global._kC),
                clear_to: clear_to,
                clear_from: clear_from,
                lock_kDDL: true
            };

            return a;
        };

        //r: response from Kendo's XHR request
        //post_processing_param: { t_low, t_high, t_int }
        //return the modified data array
        o.DataModInit = function (r, post_processing_param) {
            //console.log(r);
            //console.log([r.itemCount, r.items.length]);
            var inner_data = []; //r.items;

            //Required: Data should be arleady sorted

            //Hard copy another set of data array
            for (var i = 0; i < r.items.length; i++) {
                inner_data.push(r.items[i]);
            }

            //Show linecount only
            inner_data = inner_data.filter(function (d, i, a) {
                return d.fw && d.bw;
            });

            //Sum up the number for scaling
            inner_data = global.sum_from_base_arr(inner_data, global.xovis_report_interval_fixed, o.group_arr, o.field_ts, o.field_te, post_processing_param);

            //Fill timestamp
            inner_data = inner_data.map(function (d, i, a) {
                try {
                    //t_dif = (new Date(r.items[i].timeFrom)).getTime();
                    //t_dit = (new Date(r.items[i].timeTo)).getTime();
                    //t_dic = new Date((t_dif + t_dit) / 2);

                    //if ((t_dic < t_low) || (t_dic > t_high)) { continue; }
                    //o_temp.timestamp = t_dic;
                    //o_temp.t = (t_dif + t_dit) / 2;
                    d.timestamp = new Date(d[o.field_ts]);
                    d.t = d.timestamp.getTime();
                    d.timestamp_str = global.Date2Str(d.timestamp, "YYYY/MM/DD HH:mm");

                    return d;
                } catch (e) {
                    console.log(e);
                    return d;
                }
            });

            //Fill start and end time data if not exist
            var start_obj = inner_data.filter(function (d, i, a) { return d.t === post_processing_param.t_low; });
            var end_obj = inner_data.filter(function (d, i, a) {
                //console.log([new Date(d.timeTo).getTime(), post_processing_param.t_high ])
                return new Date(d.timeTo).getTime() === post_processing_param.t_high;
            });
            //console.log([start_obj, end_obj, inner_data.length]);

            if (start_obj.length === 0) {
                //console.log("Appending start data");
                inner_data.unshift({
                    timestamp: new Date(post_processing_param.t_low),
                    t: post_processing_param.t_low,
                    fw: 0,
                    bw: 0,
                    timestamp_str: global.Date2Str(new Date(post_processing_param.t_low), "YYYY/MM/DD HH:mm")
                });
            }
            if (end_obj.length === 0) {
                //console.log("Appending end data");
                var t_offset = global.v_interval[post_processing_param.t_int] * (1000 * 60);
                inner_data.push({
                    timestamp: new Date(post_processing_param.t_high - t_offset),
                    t: post_processing_param.t_high - t_offset,
                    fw: 0,
                    bw: 0,
                    timestamp_str: global.Date2Str(new Date(post_processing_param.t_low), "YYYY/MM/DD HH:mm")
                });
            }

            //Show Accumlate Difference (# of people in toilet)
            if (inner_data.length > 0) {
                var tmp_f = new Date(inner_data[0].timeFrom);
                //console.log([tmp_f.getHours() ,tmp_f.getMinutes()] );
                if (tmp_f.getHours() === 3 && tmp_f.getMinutes() === 0) {
                    console.log("[Xovis_LineCount] Hidden feature 'XOVIS net value' triggered!");
                    var tmp_t = 0;
                    var fbw = {};
                    inner_data = inner_data.map(function (d, i, a) {
                        if (d.t < tmp_t) {
                            console.log("ERROR: need sort!");
                        } else {
                            tmp_t = d.t;
                        }
                        if (d.fw && d.bw) {
                            if (fbw[d.device_id]) {
                                fbw[d.device_id] += d.fw;
                                fbw[d.device_id] -= d.bw;
                            } else {
                                fbw[d.device_id] = d.fw - d.bw;
                            }
                            d.fbw_net = fbw[d.device_id];
                        }
                        return d;
                    });
                }
            }

            inner_data = inner_data.map(function (d, i, a) {
                d.fbw_diff = d.fw - d.bw;
                return d;
            });

            //console.log(inner_data);
            //o.count_queue_sum(inner_data);

            return inner_data;
        };

        o.fill_timestamp = function (inner_data) {
            //Fill timestamp
            inner_data.items = inner_data.items.map(function (d, i, a) {
                try {
                    d.timestamp = new Date(d[o.field_ts]);
                    d.t = d.timestamp.getTime();
                    d.timestamp_str = global.Date2Str(d.timestamp, "YYYY/MM/DD HH:mm");
                    return d;
                } catch (e) {
                    console.log(e);
                    return d;
                }
            });
            return inner_data.items;
        };

        o.call_server = function (op, e, dI) {
            var API_setting_map = {
                route1: o.APIRoute,
                "r_post": { route2: "/read", method: "POST", obj: e.data }
                //"r_post": { route2: "/read?flag_bin=true", method: "POST", obj: e.data },
            };

            var http_link = API_setting_map.route1 + API_setting_map[op].route2;
            var http_method = API_setting_map[op].method;
            var post_obj = API_setting_map[op].obj;

            //console.log(post_obj);

            //TODO: Put logic to server
            //TODO: Modify filter and pass to server with default setting
            //Insert pre-processing for client-side filtering
            //console.log(post_obj.filter.filters[0].value.getTime());
            //console.log(post_obj.filter.filters[1].value.getTime());

            server_access.init(o.kName, op, http_link, http_method, post_obj, function (r) {
                //e.success(o.DataModInit(r, global.make_post_processing_param(post_obj, o.time_filter_param(), e.error)));
                //console.log(r);
                e.success(o.fill_timestamp(r));
            }, e.error);
        };

        o.count_queue_sum = function (data) {
            var sum_q = 0;
            var sum_cin = 0;
            var sum_din = 0;
            var sum_cdin = 0;
            data.map(function (d, index, a) {
                for (var i = 0; i < 12; i++) {
                    var s = i < 1 ? "00" : i < 10 ? "0" + i : i;
                    if (d["bin_" + s] && d["bin_" + s].value && !isNaN(d["bin_" + s].value)) {
                        //if (d["bin_" + s].value > 0) { console.log(d["bin_" + s]);}
                        sum_q += d["bin_" + s].value;
                    }
                }
                sum_cin += d.created_in.value;
                sum_din += d.deleted_in.value;
                sum_cdin += d.created_deleted_in.value;
            });
            console.log([sum_q, sum_cin, sum_din, sum_cdin]);
        };


        o.LineChart = {};
        o.KDDL_Zone = {};
        o.KDDL_Interval = {};

        o.KDDL_Interval.sSchema_list = [
            { text: "15 minutes", value: "FIFTEEN_MINUTES", int_value: 15 },
            { text: "30 minutes", value: "THIRTY_MINUTES", int_value: 30 },
            { text: "1 hour", value: "ONE_HOUR", int_value: 60 }
        ];
        o.KDDL_Interval.DataSource_local = new kendo.data.DataSource({ data: o.KDDL_Interval.sSchema_list });
        o.KDDL_Interval.Options = {
            dataSource: o.KDDL_Interval.DataSource_local,
            index: 0, //o.default_index,
            //value: o.default_choice,
            //optionLabel: "--Schema--",
            dataTextField: "text",
            dataValueField: "value"
        };

        o.KDDL_Zone.sSchema_list = [
            //{ text: global.zoneId2Name("T2-3F01F"), value: "aa_female1" },
            //{ text: global.zoneId2Name("T2-3F01M"), value: "aa_male1" }
            //{ text: "Female Toilet (7.2)", value: "aa_female1" },
            //{ text: "Male Toilet (7.2)", value: "aa_male1" }
        ];

        //TODO: Like Sidebar, Read getDevice, then get all the client_id a.k.a device_id
        o.KDDL_Zone.sSchema_list = ["PLC_2a", "PLC_2b", "PLC_3", "PLC_13L", "PLC_13E", "PLC_13AC", "PLC_14L", "PLC_14E", "PLC_14AC", "PLC_15AC", "PLC_15LA", "PLC_15LB", "PLC_ALARM", "PLC_9LC"].map((d, i, a) => { return { text: d, value: d }});

        o.KDDL_Zone.DataSource_local = new kendo.data.DataSource({ data: o.KDDL_Zone.sSchema_list });

        o.KDDL_Zone.Options = {
            dataSource: o.KDDL_Zone.DataSource_local,
            index: 0, //o.default_index,
            //value: o.default_choice,
            //optionLabel: "--Schema--",
            dataTextField: "text",
            dataValueField: "value"
        };

        o.LineChart.DataSouce = function (dataItem) {
            return new kendo.data.DataSource({

                transport: {
                    read: function (e) { o.call_server("r_post", e, dataItem); },
                    parameterMap: function (options, operation) {
                        if (operation !== "read") { return angular.toJson(options); }
                    }
                },

                //data: o.SampleData(),

                filter: {
                    logic: "and", filters: [
                        {
                            field: "timestamp",
                            operator: "gte",
                            value: o.FilterStart()
                        }, {
                            field: "timestamp",
                            operator: "lte",
                            value: o.FilterEnd()
                        },
                        {
                            field: "device_id",
                            operator: "eq",
                            value: dataItem.device_id //o.default_zone().value
                        }
                    ]
                },

                sort: [{ field: "timestamp", dir: "asc" }],
                error: function (e) {
                    global.customKendoDataSourceErrorHandler(e);
                    this.cancelChanges();
                },

                serverSorting: true,
                serverFiltering: true
            });
        };

        o.LineChart.Options = function (dataItem) {
            //console.log(dataItem);
            return {
                theme: "bootstrap",
                dataSource: o.LineChart.DataSouce(dataItem),

                title: {
                    text: global.export_make_header({ ToUI: "Chart", DataTypeName: o.DataTypeName(' '), ts: o.FilterStart(), te: o.FilterEnd(), interval: o.default_interval(), zone_name: o.default_zone().text }), //"Please Apply Filter To Generate Title",
                    font: "1.5em Arial,Helvetica,sans-serif",
                    border: { width: 0 }
                },

                //pdf: true,
                legend: {
                    position: "top",
                    border: { width: 0 },
                    labels: { font: "1.0em Arial,Helvetica,sans-serif", padding: 2 },
                    inactiveItems: { labels: { font: "1.0em Arial,Helvetica,sans-serif" } },
                    //labels: { template: "#: text #", }
                    orientation: "horizontal"
                },
                //seriesDefaults: { type: "bar", stack: true },
                seriesDefaults: { type: "line" },
                chartArea: { height: 640 },
                axisDefaults: { labels: { font: "0.7em Arial,Helvetica,sans-serif" } },
                categoryAxis: {
                    //series: [{ field: "t" }],
                    //field: "timestamp",
                    field: "t",
                    type: "date",
                    labels: {
                        dateFormat: {
                            hours: "HH mm"
                        },
                        rotation: -60,
                        step: Math.floor(60 / o.BaseUnit),
                        format: "{0:MM-dd HH:mm}"
                    },
                    baseUnit: "minutes",
                    crosshair: { visible: true },
                    baseUnitStep: o.BaseUnit,
                    minorGridLines: { visible: true, step: Math.floor(30 / o.BaseUnit) },
                    majorGridLines: { visible: true, step: Math.floor(60 / o.BaseUnit) },
                    majorTicks: { size: 24, width: 2, step: Math.floor(60 / o.BaseUnit) },
                    minorTicks: { visible: true, size: 12, width: 1, step: Math.floor(30 / o.BaseUnit) },
                    title: "Timestamp"
                },
                valueAxis: {
                    labels: { format: "N0" },
                    //majorUnit: Math.floor(o.BaseUnit / 3),
                    //minorUnit: Math.floor(o.BaseUnit / 15),
                    minorGridLines: { visible: true },
                    title: "Net Count"
                },
                tooltip: {
                    visible: true,
                    //shared: true,
                    template: "#= series.name #: #= value # <br/> #= dataItem.timestamp_str  #",
                    font: "0.8em Arial,Helvetica,sans-serif"
                },
                zoomable: false, //{ mousewheel: { lock: "y" } },
                series: o.Series(),
                render: function (e) {
                    // Clear up the loading indicator for this chart
                    //var loading = $(".chart-loading", e.sender.element.parent());
                    //console.log("render()");
                    //kendo.ui.progress(loading, false);
                },
                dataBound: function (e) {
                    o.remove_loading_bar(e);
                }
            };
        };

        o.reload_online = function () {
            if ($(o.kName) && $(o.kName).data(global._kC) && $(o.kName).data(global._kC).dataSource) {
                $(o.kName).data(global._kC).dataSource.read();
                $(o.kName).data(global._kC).refresh();
            }
        };

        o.resize = function () {
            if ($(o.kName) && $(o.kName).data(global._kC)) {
                $(o.kName).data(global._kC).resize();
            }
        };

        o.redraw = function () {
            if ($(o.kName) && $(o.kName).data(global._kC)) {
                $(o.kName).data(global._kC).redraw();
            }
        };

        o.toIMG = function () {
            if ($(o.kName) && $(o.kName).data(global._kC)) {
                $(o.kName).data(global._kC).exportImage().done(function (data) {
                    kendo.saveAs({
                        dataURI: data,
                        fileName: o.chartName
                    });
                });
            }
            return true;
        };

        o.toXLSX_Paged = function (filter_in, KendoDataSource) {


            var q_view = $("#Queue_Excel_KC_X_LC");

            function doTask(taskNum, next) {
                var time = Math.floor(Math.random() * 3000);

                setTimeout(function () {
                    console.log(taskNum);
                    next();
                }, time);
            }

            function createTask(taskNum) {
                return function (next) {
                    doTask(taskNum, next);
                };
            }

            var tasks = [1, 2, 3];

            for (var i = 0; i < tasks.length; i++) {
                q_view.queue('tasks', createTask(tasks[i]));
            }

            $(document).queue('tasks', function () {
                console.log("all done");
            });

            $(document).dequeue('tasks');

            /**
            var opt_arr = [];



            var processSchema = function (data) {
                return $.when.apply($, $.map(opt_arr, function (d,i) {
                    var def = new $.Deferred();
                    db.executeSql(sql, data, function (tx, results) {
                        def.resolve(results);
                    });
                    return def;
                })).promise();
            };
            **/
            //console.log([filter_in, KendoDataSource]);
            console.log([KendoDataSource.total(), KendoDataSource.totalPages()]);
            /**
            KendoDataSource.query({
                filter: filter_in,
                sort: [{ field: "timeFrom", dir: "asc" }], //This may be a technical debt
            }).then(function () {
                var data = KendoDataSource.data();

                console.log([KendoDataSource.total(), KendoDataSource.totalPages()]);
                console.log(data);
            }).fail(global.customKendoDataSourceErrorHandler);
            **/
            return true; //ExportToXLSX_X_LC.genXLSX();
        };

        //o.toXLSX = o.toXLSX_Paged;


        o.toXLSX = function (filter_in, KendoDataSource) {
            o.show_loading_bar();

            var zone_name = false;
            var z_kDDL = o.time_filter_param().kDDL["device_id"];
            if ($(z_kDDL) && $(z_kDDL).data(global._kDDL)) {
                zone_name = $(z_kDDL).data(global._kDDL).text();
            }

            KendoDataSource.query({
                filter: filter_in,
                sort: [{ field: "timestamp", dir: "asc" }] //This may be a technical debt
            }).then(function () {
                //var data = KendoDataSource.data();
                //console.log([KendoDataSource.total(), KendoDataSource.totalPages()]);
                //console.log(data);
                //console.log(filter_in);
                console.log([$.makeArray(KendoDataSource.data()), zone_name, global.make_post_processing_param({ filter: filter_in }, o.time_filter_param(), global.customKendoDataSourceErrorHandler)]);
                return true;//ExportToXLSX_X_LC.genXLSX($.makeArray(KendoDataSource.data()), zone_name, global.make_post_processing_param({ filter: filter_in }, o.time_filter_param(), global.customKendoDataSourceErrorHandler));
            }).fail(global.customKendoDataSourceErrorHandler).always(o.remove_loading_bar);
        };

        o.RedrawScale = function (filter_in) {
            //console.log({ filter_in });
            //return;


            //console.log("o.RedrawScale");
            var interval = false;
            var i_kDDL = o.time_filter_param().kDDL_interval["resolution"];
            if ($(i_kDDL) && $(i_kDDL).data(global._kDDL)) {
                interval = $(i_kDDL).data(global._kDDL).value();
            }

            //V6: Challenge: Return selected zone in SideTree or in the DropDownList (Currently UI will have conflict) 
            var zone_name = false;
            var z_kDDL = o.time_filter_param().kDDL["device_id"];
            if ($(z_kDDL) && $(z_kDDL).data(global._kDDL)) {
                zone_name = $(z_kDDL).data(global._kDDL).text();
            }

            //console.log(filter_in);
            var ts = new Date();
            var te = new Date();
            for (var i = 0; i < filter_in.filters.length; i++) {
                if (filter_in.filters[i].field === o.field_ts) { ts = filter_in.filters[i].value; }
                if (filter_in.filters[i].field === o.field_te) { te = filter_in.filters[i].value; }

            }

            var new_title = false;
            var title_param = {
                ToUI: "Chart",
                DataTypeName: o.DataTypeName(' '),
                interval: interval,
                zone_name: zone_name,
                ts: ts,
                te: te
            };
            try {
                new_title = global.export_make_header(title_param);
                //console.log(make_header(title_param));
            } catch (e) {
                console.log(e);
            }

            if (interval && $(o.kName) && $(o.kName).data(global._kC)) {
                o.BaseUnit = global.v_interval[interval];
                var new_options = o.LineChart.Options({ "device_id": zone_name });
                if (new_title) { new_options.title.text = new_title; }
                $(o.kName).data(global._kC).setOptions({ categoryAxis: new_options.categoryAxis, valueAxis: new_options.valueAxis, title: new_options.title });
                $(o.kName).data(global._kC).redraw();
            }

        };


        //Return service object
        return o;
    }]);
