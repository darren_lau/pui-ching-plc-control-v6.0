var app = angular.module('homeScript');

//Instead of making a batch of files, move tiny stuffs to here.

app.factory('KendoMisc_Appliance', ['$http', '$log', '$window', '$q', '$timeout', 'global',
    function ($http, $log, $window, $q, $timeout, global) {
        var o = {
            foo: "bar"
        };

        //Kendo Numeric Text Box
        o.applindexNumericTextBoxOptions = {
            format: "n0",
            decimals: 0,
            min: 0,
            placeholder: "Coil Index",
            step: 1
        };

        o.applSchedulerNumericTextBoxOptions = {
            format: "n0",
            decimals: 0,
            min: 0,
            max: 1,
            placeholder: "Slot Value",
            step: 1
        };

        //Temperature and humidity gauges
        o.temperatureLinearGaugeOptions = {
            /**
            pointer: {
                value: 28
            },
            **/
            scale: {
                majorUnit: 10,
                minorUnit: 1,
                min: 0,
                max: 40,
                vertical: true
                /**
                ranges: [
                    {
                        from: 0,
                        to: 10,
                        color: "#2798df"
                    }, {
                        from: 30,
                        to: 35,
                        color: "#ffc700"
                    }, {
                        from: 35,
                        to: 40,
                        color: "#c20000"
                    }
                ]
                **/
            }
        };

        o.humidityRadialGaugeOptions = {
            /**
            pointer: {
                value: 50
            },
            **/
            scale: {
                minorUnit: 2,
                startAngle: -30,
                endAngle: 210,
                min: 20,
                max: 100,
                labels: {
                    position: "outside",
                    template: "#= value #%"
                },
                ranges: [
                    {
                        from: 70,
                        to: 80,
                        color: "#ffc700"
                    }, {
                        from: 80,
                        to: 90,
                        color: "#ff7a00"
                    }, {
                        from: 90,
                        to: 100,
                        color: "#c20000"
                    }
                ]
            }
        };
        
        o.gasRadialGaugeOptions = {
            /**
            pointer: {
                value: 50
            },
            **/
            scale: {
                minorUnit: 50,
                startAngle: 0,
                endAngle: 180,
                min: 0,
                max: 1000,
                labels: {
                    position: "outside",
                    template: "#= value #"
                },
                ranges: [
                    {
                        from: 0,
                        to: 120,
                        color: "#99ff99"
                    }, {
                        from: 120,
                        to: 240,
                        color: "#4dff4d"
                    }, {
                        from: 240,
                        to: 360,
                        color: "#00e600"
                    }, {
                        from: 360,
                        to: 420,
                        color: "#ffff66"
                    }, {
                        from: 420,
                        to: 480,
                        color: "#ffcc00"
                    }, {
                        from: 480,
                        to: 540,
                        color: "#ff6600"
                    }, {
                        from: 540,
                        to: 585,
                        color: "#ff6666"
                    }, {
                        from: 585,
                        to: 640,
                        color: "#cc0000"
                    }, {
                        from: 640,
                        to: 700,
                        color: "#800000"
                    }, {
                        from: 700,
                        to: 1000,
                        color: "#ff00ff"
                    }
                ]
            }
        };
        
        o.co2RadialGaugeOptions = {
            /**
            pointer: {
                value: 50
            },
            **/
            scale: {
                minorUnit: 1,
                startAngle: 0,
                endAngle: 180,
                min: 0,
                max: 10,
                labels: {
                    position: "outside",
                    template: "#= value #"
                },
                ranges: [
                    {
                        from: 0,
                        to: 0.175,
                        color: "#99ff99"
                    }, {
                        from: 0.175,
                        to: 0.225,
                        color: "#4dff4d"
                    }, {
                        from: 0.225,
                        to: 0.35,
                        color: "#00e600"
                    }, {
                        from: 0.35,
                        to: 2,
                        color: "#ffcc00"
                    }, {
                        from: 2,
                        to: 5,
                        color: "#ff6600"
                    }, {
                        from: 5,
                        to: 10,
                        color: "#cc0000"
                    }
                ]
            }
        };

        //Return service object
        return o;
    }]);