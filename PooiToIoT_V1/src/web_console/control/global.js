//var app = angular.module('homeScript', ["kendo.directives"]);
var app = angular.module('homeScript');

app.factory('global', ['$http', '$log', '$window', '$q', '$timeout',
    function ($http, $log, $window, $q, $timeout) {
        var o = {
            //Constant section
            _sNode_server_addr: "@@ET_sEXPRESS_URL", //Server address
            _sMqtt_server_addr: "@@ET_sMOSCA_URL_HTTP", //Mqtt (Mosca) server address
            _iSchedulingInterval: parseInt("@@ET_iSCHEDULINGINTERVAL"), //30 or 15 minutes
            _iTimeout_val: parseInt('@@_iCONSOLE_REQUEST_TIMEOUT'), //Timeout value for server access (10 seconds)
            _host_root: "", //Assumed localhost
            _iCheckLoginPeriod: parseInt("@@_iCHECKLOGINPERIOD"), //1 minute
            _logout_url: "/logout",
            siteId: "@@ET_sSITE_ID", //siteId for global use
            sUserType: "", //User Type of current user
            sCurrentMqttTopic: "", //Currently subscribed MQTT topic (depends on zone selected)
            _iModbus_poll_interval: parseInt("@@_iMODBUS_POLL_INTERVAL"), //Poll period from modbus_control

            skip_block_name: ["X"], //KendoTreeView_Side

            URL_PREFIX: "", //_sNode_server_addr

            oRequest_timeout_obj: setTimeout(function () { console.log("TIMEOUT"); }, 1000 * 3600), //Timeout object (not $timeout)
            oMqtt_notDefined: { css: "mqtt_status_err", msg: "Error: Client is not defined" }, //MQTT Status object (used in HTML)
            oMqtt_disconnected: { css: "mqtt_status_err", msg: "Error: Client is disconnected" }, //MQTT Status object (used in HTML)

            //Global variable
            oZone_inf: {},
            oZone_DBinf: {}, //Zone information from DB

            //Kendo Glossary
            _kC: "kendoChart",
            _kG: "kendoCrid",
            _kS: "kendoScheduler",
            _kDDL: "kendoDropDownList",
            _kDP: "kendoDatePicker",
            _kTP: "kendoTimePicker",
            _kTV: "kendoTreeView",
            _kTS: "kendoTabStrip",
            _kW: "kendoWindow",
            _kV: "kendoValidator",
            kLoadingBar: {
                "Appliance_Control": "#kendoLoadingBar",
                "VRV_Control": "#kendoLoadingBar_VRV"
            },
            _kDS: function (id, k) { return $(id) && $(id).data(k) ? $(id).data(k).dataSource : false; },

            //Constant section (function, big objects)
            fGotoLink_sameWin: function (url) {
                //Redirect to url within same window
                $window.location.href = url;
            },

            fGotoLink: function (url) {
                //Open url with new window
                $window.open(url, '_blank');
            },

            //Error objects NOT included in server_access
            oNot_complete_obj: function (kName, op) {
                //This is a bit complicated since it is usually not system error. 
                //This should help user to make the correct input.
                var msg = kName + ": Information not complete. ";
                if (kName === "applDetailGrid") {
                    if (op === "c") {
                        msg += "Please make sure you have selected a zone and filled in all required fields.";
                    } else if (op === "d") {
                        msg += "Please make sure you have selected a appliance to delete.";
                    }
                }
                return {
                    status: 617,
                    statusText: msg,
                    responseText: angular.toJson({ kName: kName, op: op })
                };
            },

            oInf_used_obj: function (kName, op, zone_ref) {
                return {
                    status: 613,
                    statusText: kName + ": Output coil or Appliance name has been used by other appliances.",
                    responseText: angular.toJson({ kName: kName, op: op, zoneId: zone_ref.zoneId })
                };
            },

            $log: console.log,

            Date2Str: function (d, fs) {
                return moment(d).format(fs); //Foreign Library MomentJS!
            },

            zone_arr: [{
                zoneId: "T2-3F01F",
                zoneName: "Female Toilet (7.2)"
            }, {
                zoneId: "T2-3F01M",
                zoneName: "Male Toilet (7.2)"
            }],

            //ParseInt for resolution of Xovis reports
            v_interval: {
                "ONE_MINUTE": 1,
                "FIVE_MINUTES": 5,
                "FIFTEEN_MINUTES": 15,
                "THIRTY_MINUTES": 30,
                "ONE_HOUR": 60 * 1,
                "SIX_HOURS": 60 * 6,
                "TWELVE_HOURS": 60 * 12,
                "ONE_DAY": 60 * 24
            },

            //Change integer into Specified format
            verbose_i: function (v_i) {
                return [
                    v_i % 60 === 0 ? Math.floor(v_i / 60) : v_i,
                    v_i % 60 === 0 ? Math.floor(v_i / 60) === 1 ? "hr" : "hrs" : "mins"
                ];
            },

            timestr_padzero: function (n) {
                if (n < 0) { throw new Error("Input must not be negative number!"); }
                else {
                    return n < 1 ? "00" : n < 10 ? "0" + n : n;
                }
            },

            monthNames: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],


            //Dummy last item for copying convinence
            dummy: "dummy"
        };

        o.zoneId2Name = function (id) {
            let found_zoneInf = o.zone_arr.filter((z) => { return z.zoneId === id; });
            return found_zoneInf.length > 0 ? found_zoneInf[0].zoneName : id;
        };

        o.verbose_date_excel = function (t) {
            return [t.getDate(), o.monthNames[t.getMonth()], t.getFullYear(), [o.timestr_padzero(t.getHours()), o.timestr_padzero(t.getMinutes())].join('')].join(" ");
        };

        o.verbose_date_chart = function (t) {
            //console.log(t);
            return [[t.getDate(), o.monthNames[t.getMonth()].substring(0, 3), t.getFullYear()].join("-"), [o.timestr_padzero(t.getHours()), o.timestr_padzero(t.getMinutes())].join('')].join(" ");
        };

        //{interval, ts, te, zone_name}
        o.export_make_header = function (param) {
            var i_a = o.verbose_i(o.v_interval[param.interval]);

            //console.log(param);
            switch (param.ToUI) {
                case "Excel": return [param.DataTypeName, "-", param.zone_name, "by", i_a[0], i_a[1], "from", o.verbose_date_excel(param.ts), "-", o.verbose_date_excel(param.te)].join(' ');
                case "Chart": return [param.DataTypeName, "by", i_a[0], i_a[1], "interval", "-", param.zone_name, "\n", o.verbose_date_chart(param.ts), "to", o.verbose_date_chart(param.te)].join(' ');
                default: return "undefined";
            }
        };

        //Kendo genereal event handlers
        o.customKendoDataSourceErrorHandler = function (e) {
            // e: Kendo event
            clearTimeout(o.oRequest_timeout_obj);
            //if (angular.isDefined(o.oRequest_timeout_obj.cancel)) { o.oRequest_timeout_obj.cancel(); }
            if (!e.xhr) { e = { xhr: e }; }
            console.log("e = " + e);
            alert("Error code: " + e.xhr.status + " - " + e.xhr.statusText + "\n" +
                "Error message: " + e.xhr.responseText);
            if (parseInt(e.xhr.status) === 401) { o.fGotoLink_sameWin('/login'); }
        };

        return o;
    }]);