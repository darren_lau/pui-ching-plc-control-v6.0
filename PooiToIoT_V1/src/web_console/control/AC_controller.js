var app = angular.module('homeScript');

app.factory('AC_controller', ['$http', '$log', '$window', '$q', '$timeout', 'mqtt_controller',
    function ($http, $log, $window, $q, $timeout, mqtt_controller) {
        var o = {
            operation_mode_list: [
                //Daikin: 0, 1, 2, 3, 5 
                //Toshiba: 0, 1, 2, 3, 5
                { value: '0', text: 'Cool' },
                { value: '1', text: 'Heat' },
                { value: '2', text: 'Auto' },
                { value: '3', text: 'Dry' },
                { value: '4', text: 'HAUX' },
                { value: '5', text: 'Fan' },
                { value: '6', text: 'HH' },
                { value: '8', text: 'VAM Auto' },
                { value: '9', text: 'VAM Bypass' },
                { value: '10', text: 'VAM Heat Exch.' },
                { value: '11', text: 'VAM Normal' }
            ],
            fan_speed_list: [
                //Daikin: 0, 1, 2, 3
                //Toshiba: 0, 1, 2, 3
                { value: '0', text: 'Low' },
                { value: '1', text: 'Medium' },
                { value: '2', text: 'High' },
                { value: '3', text: 'Auto' },
                { value: '4', text: 'Top' },
                { value: '5', text: 'Very Low' },
                { value: '7', text: 'VAM Super High' },
                { value: '8', text: 'VAM Low Fresh Up' },
                { value: '9', text: 'VAM Fresh Up' }
            ],
            swing_list: [
                //Daikin: 0, 1, 2, 3, 4, 5, 6
                //Toshiba: 0, 1, 2, 3, 4, 5, 6
                { value: '0', text: 'Vertical' },
                { value: '1', text: '30 deg' },
                { value: '2', text: '45 deg' },
                { value: '3', text: '60 deg' },
                { value: '4', text: 'Horizontal' },
                { value: '5', text: 'Auto' },
                { value: '6', text: 'OFF' },
                { value: '7', text: '(Not docuemented)' }
            ]
        };

        o.OperationModeDropDownListDataSource = new kendo.data.DataSource({ data: o.operation_mode_list });

        o.OperationModeDropDownListOptions = {
            dataSource: o.OperationModeDropDownListDataSource,
            dataTextField: "text",
            dataValueField: "value",
            change: function (e) {
                //console.log("Operation Mode = " + this.value());
            }
        };

        o.FanSpeedDropDownListDataSource = new kendo.data.DataSource({ data: o.fan_speed_list });

        o.FanSpeedDropDownListOptions = {
            dataSource: o.FanSpeedDropDownListDataSource,
            dataTextField: "text",
            dataValueField: "value",
            change: function (e) {
                //console.log("Fan Speed = " + this.value());
            }
        };

        o.SetTemperatureSliderOptions = {
            min: 18,
            max: 28,
            smallStep: 0.5,
            largeStep: 1.0,
            change: function (e) {
                //console.log("Set temperature = " + e.value);
            }
        };

        o.SwingDropDownListDataSource = new kendo.data.DataSource({ data: o.swing_list });

        o.SwingDropDownListOptions = {
            dataSource: o.SwingDropDownListDataSource,
            dataTextField: "text",
            dataValueField: "value",
            change: function (e) {
                //console.log("Swing = " + this.value());
            }
        };

        o.RoomTemperatureSliderOptions = {
            min: 8,
            max: 32,
            smallStep: 0.1,
            largeStep: 1.0,
            change: function (e) {
                //console.log("Room temperature = " + e.value);
            }
        };

        //18 = #0000ff, 25.5 = #00ff00, 33 = #ff0000
        o.temperature_font = function (t) {
            //console.log(room_temperature);
            var h = 33;
            var m = 25.5;
            var l = 18;
            var r = t > h ? 255 : t < m ? 0 : (t - m) * (255 / (h - m));
            var g = t > h ? 0 : t < l ? 0 : (h - m - Math.abs(m - t)) * 255 / (h - m);
            var b = t < l ? 255 : t > m ? 0 : (m - t) * (255 / (m - l));
            var d = "rgb(" + Math.round(r) + "," + Math.round(g) + "," + Math.round(b) + ")";
            //console.log(d);
            return {
                'font-weight': 'bold',
                color: d//Math.random() < 0.5? 'red' : 'blue'
            };
        };

        o.selected_css = function (selected) {
            /**
            var option = {
                'margin-top': '10px',
                'padding': '5px',
                'border-style': 'solid',
                'border-width': '1px',
                'border-color': '#555555',
                'border-radius': '4px',
                'text-align': 'center',
            };
            option['border-color'] = selected? '#39ae07' : '#555555';
            option['box-shadow'] = selected? '0px 0px 9px #b6ff00' : '0px 0px 9px #888888';
            return option;
            **/
            return selected ? "option selected" : "option unselect";
        };

        o.map_text_from_value = function (value, key) {
            var list = {
                "operation_mode": o.operation_mode_list,
                "swing": o.swing_list,
                "fan_speed": o.fan_speed_list
            };
            if (!list[key]) { return ""; }
            for (var i = 0; i < list[key].length; i++) {
                if (list[key][i].value === value + "") {
                    return list[key][i].text;
                }
            }
            return "";
        };

        o.ACControlOnChange = function (dI, key, kD, set_val) {
            var valid = false;
            if (kD) {
                if ($("#" + dI.appl_name + "_" + key).data(kD)) {
                    valid = true;
                }
            } else {
                valid = true;
            }
            //console.log("A");
            //Add custom rules here
            if (key === "lock_bits") {
                //console.log("B");
                var arr = [0, 2, 4, 6, 8, 9, 10, 11, 12, 13, 14, 15];
                if (arr.indexOf(parseInt(dI[key])) < 0) {
                    //console.log("C");
                    valid = false;
                }
            }
            if (key === "on_off") {
                dI[key] = dI[key] ? 0 : 1;
            }
            if (key === "operation_mode" || key === "swing" || key === "fan_speed") {
                dI[key] = set_val;
            }
            if (key === "set_temperature" && !kD) {
                dI[key] = parseFloat(dI[key]) + set_val;
            }
            if (valid) {
                //console.log(appl_name + ": " + key + " = " + value);
                dI['key'] = key;
                mqtt_controller.fireMqttMessageForAC(dI);
            }
        };

        o.inc_local_progress_alt = function () {
            //console.log("NOT Local Progress++ @" + new Date());
        };

        o.inc_local_progress = function () {
            //console.log("Loading Progress++ @" + new Date().getMilliseconds());
        };

        o.expand_col = function (id, rel) {
            $("#" + id + "_option_Set" + rel).toggle();
            $("#" + id + "_t" + rel).show(function () {
                //console.log($(this).text());
                $(this).text($(this).text() === "\u25bc" ? "\u25b2" : "\u25bc");
            });
        };

        o.close_col = function (id, rel) {
            console.log($("#" + id + "_option_Set" + rel));
            $("#" + id + "_option_Set" + rel).hide();
        };

        //start of DelightController

        o.SetDelightSliderOptions = {
            min: 0,
            max: 100,
            smallStep: 20,
            largeStep: 100,
            change: function (e) {
            }
        };

        o.DelightOnChange = function (dI, key, kD, set_val) {
            var valid = false;
            if (kD) {
                if ($("#" + dI.appl_name + "_" + key).data(kD)) {
                    valid = true;
                }
            } else {
                valid = true;
            }
            
            if (key === "state" && !kD) {
                dI[key] = set_val;
            }

            if (valid) {
                //console.log(appl_name + ": " + key + " = " + value);
                dI['key'] = key;
                mqtt_controller.fireMqttMessageForAC(dI);
            }
        };

        //End



        return o;
    }]);