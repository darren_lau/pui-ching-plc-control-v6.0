"use strict";
var db_connection_sequelize = require('../server/modules/db_connection_sequelize');
var server_logger = require('../server/modules/server_logger');
var server_config = require('../server/modules/server_config');

const _bDebug_mode = process.argv.indexOf("debug") >= 0; //If true, spam the console with debug messages
let logger = server_logger.SERVER_LOGGING(`${server_config._sLOG_DIR}/punDB_V4_mysql_PuiChing.log`, { level: _bDebug_mode ? "debug" : 'warn' }); //Logger of this module

const ONE_DAY = 1000 * 60 * 60 * 24;
const ONE_MINUTE = 1000 * 60;
const ONE_HOUR = 1000 * 60 * 60;
const PowerIncrement = 300 * (ONE_HOUR / ONE_MINUTE); //300kWh in ONE_MINUTE

const now = new Date();
const t_start = new Date(now.getTime() - ONE_DAY);
const t_end = now;

let Create_Device_History = function () {
    let a = [];
    let pw = 0;
    for (let t_cur = t_start.getTime(); t_cur < t_end.getTime(); t_cur += ONE_MINUTE) {
        pw += PowerIncrement;
        a.push({
            device_id: "PLC_2a", // "PLC_1PWA",
            power_usage: Math.random() * PowerIncrement,
            timestamp: t_cur
        });
    }
    return a;
};

let remake_Device_History = async function (callback) {
    logger.log('info', "Device_History: Gathering information...");

    await Create_Device_History().reduce((p, bind_obj) => {

        return p.then(() => {
            logger.log('debug', `Adding Device ${bind_obj.device_id} in ${bind_obj.timestamp}`);
            let option_obj = {
                where: {
                    device_id: bind_obj.device_id,
                    timestamp: bind_obj.timestamp
                }
            };
            return db_connection_sequelize.Device_History.upsert(bind_obj, option_obj);
        }).then((created) => {
            logger.log(created ? 'info' : 'error', `Upserted Device ${bind_obj.device_id} in ${bind_obj.timestamp}, created: ${created}`);
        });

    }, Promise.resolve());
};

let db_init = async function () {
    return new Promise((t, f) => {
        db_connection_sequelize.init(function (err) {
            if (err) { f(err) }
            else { logger.info("mySQL connection is opened by Sequelize."); t(); }
        });
    });
};

let main = async function () {
    try {
        await db_init();
        await remake_Device_History();
    } catch (e) {
        logger.error(e);
        process.exit(1);
    }
};

main().then(() => {
    logger.info("info", "All tasks executed with no errors.");
    process.exit(0);
}).catch((e) => {
    logger.error(e)
    process.exit(1);
});