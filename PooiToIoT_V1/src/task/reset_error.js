"use strict";
let server_logger = require('../server/modules/server_logger.js');
let server_config = require('../server/modules/server_config.js');
let db_connection = require('../server/modules/db_connection.js');

let logger = server_logger.SERVER_LOGGING(`${server_config._sLOG_DIR}/reset_error.log`, { level: 'info' }); //Set logger

let db_update = function (callback) {
    logger.log('info', 'Updating error_issue in DB...');
    let query_obj = {
        fixed_time: null
    };
    let update_field = {
        fixed_time: new Date()
    };
    let option_obj = { multi: true };
    db_connection.error_issue.update(query_obj, update_field, option_obj, callback);
};

let init = function () {
    logger.log('info', "Start resetting errors.");
    db_connection.init(logger, (err) => {
        if (err) {
            logger.log('error', err);
            process.exit(1);
        } else {
            logger.log('info', "Database connected.");
            db_update((err, num) => {
                if (err) { logger.log('error', err); process.exit(1); }
                else { logger.log('info', 'Done. Message returned: ' + JSON.stringify(num)); process.exit(0); }
            });
        }
    });
};

logger.log('info', 'Process start with PID ' + process.pid);
setTimeout(init, 5 * 1000);