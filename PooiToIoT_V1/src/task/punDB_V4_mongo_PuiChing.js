"use strict";
var async = require('async');
var extend = require('util')._extend; //Shallow copy

var db_connection = require('../server/modules/db_connection');
var server_logger = require('../server/modules/server_logger');
var server_config = require('../server/modules/server_config');

var _bDebug_mode = process.argv.indexOf("debug") >= 0; //If true, spam the console with debug messages
var logger = server_logger.SERVER_LOGGING(server_config._sLOG_DIR + '/setPunDB_V6.log', { level: _bDebug_mode ? "debug" : "info", mute_console: false });

var make_appl_index_arr = function (base, itr, len) {
    var arr = [];
    for (var i = 0; i < itr; i++) {
        arr.push(base + i * len);
    }
    return arr;
};

var _siteId = "PuiChing_P"; //Site ID for global use

//Known information for setting up DB. Check correctness manually
//Note: For PLC coil index, PLC itself will skip Q8, Q9 but modbus didn't,
//Any Qx with x >= 10 will be reduced by 2. e.g. Q10 = 8

//All zone ID
var known_config_zone = ["A6-1", "A6-2", "K5-1", "K5-2", "K6-1", "K9-1", "K13-1", "K14-1", "K14-2", "K15-1", "X99-1", "KG-1"];
//All device ID
var known_device_id = ["PLC_2a", "PLC_2b", "PLC_3", "PLC_13L", "PLC_13E", "PLC_13AC", "PLC_14L", "PLC_14E", "PLC_14AC", "PLC_15AC", "PLC_15LA", "PLC_15LB", "PLC_ALARM", "PLC_9LC", "PLC_Door"];
//All zone description 
var known_config_desc = [
    "學生活動中心, 6/F, Block A",
    "樂器練習室/多功能室, 6/F, Block A",
    "體育館, 5/F, Block K",
    "體育館, 5/F, Block K",
    "乒乓球場, 6/F, Block K",
    "綜合多用途訓練中心, 9/F, Block K",
    "十三樓, 13/F, Block K",
    "演講廳, 14/F, Block K",
    "多功能室, 14/F, Block K",
    "十五樓, 15/F, Block K",
    "警鐘, 99/F, Block X",
    "地下, G/F, Block K"
];
//Applicances under the zone
var known_zone_appl = [
    [{ appl_type: "分體冷氣機", number: 5, index: 0 }, { appl_type: "窗口冷氣機", number: 3, index: 5 }, { appl_type: "燈光區", number: 5, index: 8 }],
    [{ appl_type: "分體冷氣機", number: 3, index_arr: [0, 1, 4] }, { appl_type: "窗口冷氣機", number: 2, index: 2 }, { appl_type: "燈光區", number: 7, index: 8 }],
    [{ appl_type: "分體冷氣機", number: 10, index: 0 }, { appl_type: "燈光區", number: 1, index: 15 }],
    [{ appl_type: "熱水爐", number: 2, index: 10 }],
    [{ appl_type: "分體冷氣機", number: 2, index: 12 }],
    [{ appl_type: "燈光區C", number: 45, index: 0 }],
    //{ appl_type: "門口感應器", number: 1, index: 200 }],
    [{ appl_type: "燈光區", number: 12, index: 0 }, { appl_type: "分體冷氣機", number: 10, index: 0 }],
    [{ appl_type: "燈光區", number: 4, index_arr: [0, 1, 4, 5] }, { appl_type: "分體冷氣機", number: 4, index_arr: [0, 1, 7, 8] }],
    [{ appl_type: "燈光區", number: 12, index_arr: [2, 3, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15] }, { appl_type: "分體冷氣機", number: 8, index_arr: [2, 3, 4, 5, 6, 9, 10, 11] }],
    [{ appl_type: "DaikinAC", number: 12, index_arr: make_appl_index_arr(16, 12, 16) },
    { appl_type: "ToshibaAC", number: 8, index_arr: make_appl_index_arr(208, 8, 16) },
    { appl_type: "燈光區A", number: 11, index: 4 },
    { appl_type: "燈光區B", number: 13, index: 0 }],
    [{ appl_type: "警鐘", number: 4, index: 0 }],
    [{ appl_type: "車閘", number: 1, index: 200 }],
    // [{ appl_type: "dummy", number: 0, index: 0 }]
];

// Name of each appliance. Default ApplianceTypeNumber (e.g. Air conditioner 0)
var appl_name_map = {

    "K9-1": {
        "燈光區C": [
            "大堂走廊1", "大堂走廊2", "男女廁門口", "廁所走廊", "大堂走廊3", "大堂走廊4", "大堂走廊5", "K907", "K908", "K909", //00-09
            "大堂走廊6", "大堂走廊7", "女廁門口", "大堂走廊8", "大堂走廊9", "大堂走廊10", "K916(1)", "大堂走廊11", "K916(2)", "大堂走廊12", //10-19
            "K917(1)", "K917(2)", "大堂走廊13", "K903", "K902", "K904", "K905", "K906", "K918(1)", "K918(2)", //20-29 
            "K918(3)", "K913(1)", "K913(2)", "K915", "大堂走廊14", "大堂走廊15", "K914(1)", "K914(2)", "大堂走廊16", "K912(1)", //30-39 
            "K910", "大堂走廊17", "K912(2)", "大堂走廊18", "K911" //40-44 
        ],
        //"門口感應器": ["門口感應器"] 
    },

    "K14-2": {
        "燈光區": ["會議室0", "會議室1", "健身室", "休息室0", "休息室1", "休息室2", "休息室3", "休息室4", "休息室5", "休息室6", "休息室7", "休息室8"],
        "分體冷氣機": ["會議室0", "會議室1", "休息室0", "休息室1", "健身室", "休息室2", "休息室3", "休息室4"]
    },
    "X99-1": {
        "警鐘": ["停車場水浸(B1)", "停車場水浸(B2)", "升降機警鐘", "CCTV警報", "火警警報"]
    }

};

var special_mqtt_map = {
    //180426: It is NOT USED as the Alarm route is already defined as zone X99-1
    //Effect: Will be read only
    //"X99-1":  "/Special/Alarm" 
};

//Zone under which PLC (By appliance type) ? Look for which register from which PLC? 
var known_zone_client_binding = [
    { index: 0, schreg: { index: 0, reg: 298 } },
    { index: 1, schreg: { index: 1, reg: 298 } },
    { index: 2, schreg: { index: 2, reg: 298 } },
    { index: 2, schreg: { index: 2, reg: 398 } },
    { index: 2, schreg: { index: 2, reg: 498 } },
    { index: 13, schreg: { index: 13, reg: 298 } },
    //{ index_arr: [13, 14], schreg: { index: 13, reg: 298 } },
    //{ index: 13, schreg: { index: 13, reg: 398 } },
    //{ index: 13, schreg: { index: 13, reg: 498 } },
    { index_arr: [3, 5], schreg: { index: 3, reg: 298 } },
    { index_arr: [6, 8], schreg: { index: 6, reg: 298 } },
    { index_arr: [6, 8], schreg: { index: 6, reg: 348 } },
    { index_arr: [9, 9, 10, 11], schreg: { index: 10, reg: 298 } },
    { index: 12, schreg: { index: 12, reg: 298 } },
    { index: 12, schreg: { index: 12, reg: 398 } }
    //       { index: 9, schreg: { index: 9, reg: 3700 } }
];
// Which type of PLC? Or it's coolmaster?
var known_device_type = ["PLC", "PLC", "PLC", "PLC", "PLC", "PLC", "PLC", "PLC", "PLC", "CoolMasterNet", "PLC", "PLC", "PLC", "DelightController", "DoorSensor"];
// Devices' IP
var known_device_ip = [
    "10.0.223.200", "10.0.223.201", "10.0.223.44",
    "10.0.223.203", "10.0.223.204", "10.0.223.205",
    "10.0.223.207", "10.0.223.208", "10.0.223.206",
    "10.0.223.210", "10.0.223.211", "10.0.223.212",
    "10.0.223.202", "10.0.223.45", "10.0.223.213"
    //"192.168.1.30", "10.0.223.45", "10.0.223.213"
];
// Devices' MAC
var known_device_mac = [
    "ff:ff:ff:ff:ff:ff", "ff:ff:ff:ff:ff:ff", "ff:ff:ff:ff:ff:ff",
    "ff:ff:ff:ff:ff:ff", "ff:ff:ff:ff:ff:ff", "ff:ff:ff:ff:ff:ff",
    "ff:ff:ff:ff:ff:ff", "ff:ff:ff:ff:ff:ff", "ff:ff:ff:ff:ff:ff",
    "ff:ff:ff:ff:ff:ff", "ff:ff:ff:ff:ff:ff", "ff:ff:ff:ff:ff:ff",
    "ff:ff:ff:ff:ff:ff", "ff:ff:ff:ff:ff:ff", "ff:ff:ff:ff:ff:ff"
];
// Devices' Name
var known_device_name = [
    "FT1A-B40RC", "FT1A-B40RC", "FT1A-B40RC",
    "FT1A-B40RC", "FT1A-B40RC", "FT1A-B40RC",
    "FT1A-B40RC", "FT1A-B40RC", "FT1A-B40RC",
    "CoolMasterNet_Modbus_IP_Server", "FT1A-B40RC", "FT1A-B40RC",
    "FT1A-B40RC", "FT1A-DLight", "FT1A-DoorSen" //DelightController, DoorSensor
];
// User stuff
var known_user_name = ['admin', 'user', 'system_admin'];
var knwon_user_inf_name = ['Admin', 'User', 'Sytem Admin'];
var knwon_user_password = ['password', '1234', 'password'];
var known_user_isadmin = ['admin', 'user', 'system_admin'];
// Schedule ruies
var known_sch_def_id = ['ruleA', 'ruleB', 'ruleC']; //ruleB = skip checking holidays 
var known_sch_def_name = ['正常 (學生)', '全日拍卡', '正常 (教職員)'];
var known_sch_def_xml = ['Regular', 'Holiday', 'NeedCardAllDay'];
var known_sch_def_key = [[0, 1, 1], [2, 2, 2], [0, 2, 2]]; //Weekday, Weekend, PublicHoliday
// Zone under which rule?
var known_zone_sch = ['ruleB', 'ruleB', 'ruleB', 'ruleB', 'ruleB', 'ruleC', 'ruleC', 'ruleC', 'ruleC', 'ruleB', 'ruleC', 'ruleC', 'ruleB'];

var correct_floor = function (floor, failed_string) {
    return isNaN(floor) ? failed_string.split('/')[0].trim() : floor; // e.g. G/F, B1/F -> G, B1
};

var gen_mqtt_tag = function (number) {
    var zoneId = known_config_zone[number];
    var desc = known_config_desc[number];
    var tower = desc.split(',')[2].split(' ')[2];
    var floor = parseInt(desc.split(',')[1]);
    floor = correct_floor(floor, desc.split(',')[1]);
    return "/Tower" + tower + "/Floor" + floor + "/" + zoneId;
};

var main = function () {
    logger.log('info', "Process start with pid = " + process.pid);
    db_connection.init(logger, function (err) {
        if (err) { logger.log('error', err); process.exit(1); }
        else {
            logger.log('info', "DB connected.");
            async.parallel([
                remake_device,
                //remake_schedule,
                //remake_device_status,
                //remake_schedule_default,
                //remake_user
            ], function (err, result) {
                if (err) { logger.log('error', err); process.exit(1); }
                else { logger.log('info', 'DB operation done.'); process.exit(0); }
            });
        }
    });
};

var add_plc_to_zone = function (ObjFrom, bind_index, sch_reg, zone_appl_index, appl_type_index) {
    var Device = extend({}, ObjFrom);
    logger.log('debug', "Device: Adding PLC " + known_device_id[bind_index] + " into Zone " + ObjFrom.zoneId + "...");
    Device.client_id = known_device_id[bind_index];
    Device.client_name = known_device_name[bind_index];
    Device.client_type = known_device_type[bind_index];
    Device.ip_addr = known_device_ip[bind_index];
    Device.mac = known_device_mac[bind_index];
    //Device.default_id = sch_reg;
    Device.appliances = [];

    //More stuff
    //Logic is still right
    var DeviceCompleted = extend({}, Device);
    if (appl_type_index < 0) {
        for (var i0 = 0; i0 < known_zone_appl[zone_appl_index].length; i0++) {
            DeviceCompleted = add_appl_to_plc(DeviceCompleted, zone_appl_index, i0);
        }
    } else {
        DeviceCompleted = add_appl_to_plc(Device, zone_appl_index, appl_type_index);
    }
    return DeviceCompleted;
};

var add_appl_to_plc = function (ObjFrom, zone_appl_index, appl_type_index) {
    var Device = extend({}, ObjFrom);
    //logger.log('warn', [zone_appl_index, appl_type_index]);
    var appl_obj = known_zone_appl[zone_appl_index][appl_type_index];
    logger.log('debug', "Device: Adding " + appl_obj.number + " " + appl_obj.appl_type
        + " into PLC " + ObjFrom.client_id
        + " in Zone " + ObjFrom.zoneId + "...");
    if (appl_obj.number > 0) {
        for (var i0 = 0; i0 < appl_obj.number; i0++) {
            var new_appl = {};
            if (!appl_obj.index_arr) {
                new_appl.appl_name = appl_name_map[ObjFrom.zoneId] ? appl_name_map[ObjFrom.zoneId][appl_obj.appl_type][i0] : appl_obj.appl_type + i0;
                new_appl.appl_type = appl_obj.appl_type;
                new_appl.appl_index = appl_obj.index + i0;
            } else {
                //logger.log("warn", [ObjFrom.zoneId, appl_obj.appl_type, i0]);
                new_appl.appl_name = appl_name_map[ObjFrom.zoneId] ? appl_name_map[ObjFrom.zoneId][appl_obj.appl_type][i0] : appl_obj.appl_type + i0;
                new_appl.appl_type = appl_obj.appl_type;
                new_appl.appl_index = appl_obj.index_arr[i0];
            }
            Device.appliances.push(new_appl);
        }
    }
    return Device;
};

var remake_device = function (callback) {
    logger.log('info', "Device: Gathering information...");
    var Devices = [];  //Zones information
    var sch_reg = -1;
    for (var i0 = 0; i0 < known_zone_client_binding.length; i0++) {
        logger.log('debug', "zoneId: " + known_config_zone[i0]);
        var Device = {
            //SiteID
            siteId: _siteId,
            //Zone information
            zoneId: known_config_zone[i0],
            tower: known_config_desc[i0].split(',')[2].split(' ')[2],
            floor: parseInt(known_config_desc[i0].split(',')[1]),
            desc: known_config_desc[i0].split(',')[0],
            mqtt_tag: special_mqtt_map[known_config_zone[i0]] ? special_mqtt_map[known_config_zone[i0]] : gen_mqtt_tag(i0),
            default_id: known_zone_sch[i0]
        };
        Device.floor = correct_floor(Device.floor, known_config_desc[i0].split(',')[1]);

        logger.log('debug', "Device: Expanding from bind information...");
        if (!known_zone_client_binding[i0].index_arr) {
            //Single PLC in a Zone
            sch_reg = known_zone_client_binding[i0].index === known_zone_client_binding[i0].schreg.index ?
                known_zone_client_binding[i0].schreg.reg : -1;
            Device = add_plc_to_zone(Device, known_zone_client_binding[i0].index, sch_reg, i0, -1);
            Devices.push(Device);
        } else {
            //Multiple PLC in a Zone
            for (var i1 = 0; i1 < known_zone_client_binding[i0].index_arr.length; i1++) {
                sch_reg = known_zone_client_binding[i0].index_arr[i1] === known_zone_client_binding[i0].schreg.index ?
                    known_zone_client_binding[i0].schreg.reg : -1;
                Device = add_plc_to_zone(Device, known_zone_client_binding[i0].index_arr[i1], sch_reg, i0, i1);
                Devices.push(Device);
            }
        }
    }
    //logger.log('info', Devices.length);
    //logger.log('info', Devices[15]);
    //logger.log('info', Devices[14]);

    //Merging appliances which have same client_id and zoneID
    var Devices_merged = [];
    var Devices_map = {};
    for (var i = 0; i < Devices.length; i++) {
        var zoneId = Devices[i].zoneId;
        var client_id = Devices[i].client_id;
        var cpObj = extend({}, Devices[i]);
        if (!Devices_map[zoneId]) {
            Devices_map[zoneId] = {};
            Devices_map[zoneId][client_id] = cpObj;
        } else if (!Devices_map[zoneId][client_id]) {
            Devices_map[zoneId][client_id] = cpObj;
        } else {
            logger.log('info', 'CHECK');
            Devices_map[zoneId][client_id].appliances = Devices_map[zoneId][client_id].appliances.concat(cpObj.appliances);
        }
    }

    for (var zone in Devices_map) {
        if (Devices_map.hasOwnProperty(zone)) {
            for (var client in Devices_map[zone]) {
                if (Devices_map[zone].hasOwnProperty(client)) {
                    logger.log('info', [zone, client]);
                    Devices_merged.push(Devices_map[zone][client]);
                }
            }
        }
    }

    logger.log('info', Devices_merged.length);

    async.each(Devices_merged, function (bind_obj, callback) {
        //logger.log('info', bind_obj);
        logger.log('debug', "Adding PLC " + bind_obj.client_id + " with Zone " + bind_obj.zoneId);
        var query_obj = {
            siteId: _siteId,
            zoneId: bind_obj.zoneId,
            client_id: bind_obj.client_id
        };
        var update_field = {
            $set: bind_obj
        };
        var option_obj = {
            upsert: true
        };
        db_connection.Device.update(query_obj, update_field, option_obj, function (err) {
            if (err) { return callback(err); }
            else {
                logger.log('info', "Device: PLC " + bind_obj.client_id + " with Zone " + bind_obj.zoneId + " is upserted.");
                return callback();
            }
        });
    }, function (err) {
        if (err) { return callback(err); }
        else {
            logger.log('info', "Devices: Done.");
            return callback();
        }
    });
};

var target_start_hour = 7;
var target_end_hour = 23;
var target_hour = 60;
var target_interval = server_config._iSchedulingInterval; //30;
var target_length = (target_end_hour - target_start_hour) * (target_hour / target_interval);
//The numbers support decimals e.g. 13.75 (13:45)
var need_card_map = {
    "Regular": { sch_id: "Regular", startX: 0, endX: (16 - target_start_hour) * (target_hour / target_interval) },
    "Holiday": { sch_id: "Holiday", startX: 0, endX: (13 - target_start_hour) * (target_hour / target_interval) },
    "NeedCardAllDay": { sch_id: "NeedCardAllDay", startX: 0, endX: (23 - target_start_hour) * (target_hour / target_interval) }
};

function hour(i0) { var hr = Math.floor(target_start_hour + i0 / (target_hour / target_interval)); if (hr < 10) { hr = "0" + hr; } return hr; }
function minute(i0) { var min = i0 % (target_hour / target_interval) * target_interval; if (min < 10) { min = "0" + min; } return min; }

var remake_schedule = function (callback) {
    logger.log('info', "Schedule: Gathering information...");
    var Schedules = [];  //Schedule information

    for (var number0 = 0; number0 < known_config_zone.length; number0++) {
        for (var number1 = 0; number1 < 31; number1++) {
            //Making time_slots
            var time_slots = [];
            for (var i0 = 0; i0 < target_length; i0++) {
                //var tmp = Math.random() >= 0.5 ? true : false;
                time_slots.push({
                    slotId: i0 + 1,
                    desc: hour(i0) + ":" + minute(i0) + "-" + hour(i0 + 1) + ":" + minute(i0 + 1),
                    //mod_flag: tmp,
                    //slot_value: Math.random() >= 0.5 ? 1 : 0,
                    slot_value: 0,
                    remark: ""
                });
            }

            /**
            time_slots = time_slots.concat(time_slots.slice(0, 2));
            time_slots.shift();
            time_slots.shift();
            for (var i = 0; i < time_slots.length; i++) {
                time_slots[i].slotId = (i + 1);
            }
            **/

            var schedule_dev = {
                siteId: _siteId,
                zoneId: known_config_zone[number0],
                date_index: number1 + 1,
                //client_id: known_device_id[known_zone_client_binding[number0].schreg.index],
                dev_start_addr: known_zone_client_binding[number0].schreg.reg,
                slot_length: target_length,
                mod_date_value: "2016-01-01", //moment().toISOString().substring(0, 10),
                time_slots: time_slots
            };
            Schedules.push(schedule_dev);
        }
    }

    async.each(Schedules, function (bind_obj, callback) {
        //logger.log('info', bind_obj);
        //if (bind_obj.zoneId != "K15-1") { return callback(); }
        //if ()
        //else {
        logger.log('debug', "Adding Schedule of Zone " + bind_obj.zoneId + " on date_index " + bind_obj.date_index);
        var query_obj = {
            zoneId: bind_obj.zoneId,
            date_index: bind_obj.date_index
        };
        var update_field = {
            $set: bind_obj
        };
        var option_obj = {
            upsert: true
        };
        db_connection.Schedule.update(query_obj, update_field, option_obj, function (err) {
            if (err) { return callback(err); }
            else {
                logger.log('info', "Schedule: Schedule of Zone" + bind_obj.zoneId + " on date_index " + bind_obj.date_index + " is upserted.");
                return callback();
            }
        });
        //}
    }, function (err) {
        if (err) { return callback(err); }
        else {
            logger.log('info', "Schedule: Done.");
            return callback();
        }
    });
};

var remake_device_status = function (callback) {
    logger.log('info', "Device Status: Gathering information...");

    //Gatehring zone status information
    var device_status = [];
    for (var number = 0; number < known_zone_appl.length; number++) {
        var appliances = [];
        var appl_obj = known_zone_appl[number];
        for (var i0 = 0; i0 < appl_obj.length; i0++) {
            appliances.push({
                appl_type: appl_obj[i0].appl_type,
                devices: []
            });
            for (var i1 = 0; i1 < appl_obj[i0].number; i1++) {
                appliances[i0].devices.push({ appl_name: appl_obj[i0].appl_type + i1, state: Math.round(Math.random()) });
            }
        }
        device_status.push({
            siteId: _siteId,
            zoneId: known_config_zone[number],
            Appliance: appliances
        });
    }

    async.each(device_status, function (bind_obj, callback) {
        //logger.log('info', bind_obj);
        logger.log('debug', "Adding Device Status of Zone" + bind_obj.zoneId);
        var query_obj = {
            siteId: bind_obj.siteId,
            zoneId: bind_obj.zoneId
        };
        var update_field = {
            $set: bind_obj
        };
        var option_obj = {
            upsert: true
        };
        db_connection.Device_Status.update(query_obj, update_field, option_obj, function (err) {
            if (err) { return callback(err); }
            else {
                logger.log('info', "Device Status: Device Status of Zone" + bind_obj.zoneId + " is upserted.");
                return callback();
            }
        });
    }, function (err) {
        if (err) { return callback(err); }
        else {
            logger.log('info', "Device Status: Done.");
            return callback();
        }
    });
};

var remake_schedule_default = function (callback) {
    logger.log('info', "Schedule default: Gathering information...");

    var sch_defs = [];
    for (var number = 0; number < known_sch_def_id.length; number++) {
        var sch_def = {
            default_id: known_sch_def_id[number],
            default_name: known_sch_def_name[number]
        };
        for (var i0 = 0; i0 < 3; i0++) {
            sch_def.xml_weekday = known_sch_def_xml[known_sch_def_key[number][0]];
            sch_def.xml_weekend = known_sch_def_xml[known_sch_def_key[number][1]];
            sch_def.xml_publicholiday = known_sch_def_xml[known_sch_def_key[number][2]];
        }
        sch_defs.push(sch_def);
    }

    async.each(sch_defs, function (bind_obj, callback) {
        //logger.log('info', bind_obj);
        logger.log('debug', "Adding Schedule default " + bind_obj.default_id);
        var query_obj = {
            default_id: bind_obj.default_id
        };
        var update_field = {
            $set: bind_obj
        };
        var option_obj = {
            upsert: true
        };
        db_connection.Schedule_def.update(query_obj, update_field, option_obj, function (err) {
            if (err) { return callback(err); }
            else {
                logger.log('info', "Schedule default: Schedule default " + bind_obj.default_id + " is upserted.");
                return callback();
            }
        });
    }, function (err) {
        if (err) { return callback(err); }
        else {
            logger.log('info', "Schedule default: Done.");
            return callback();
        }
    });
};

var remake_user = function (callback) {
    logger.log('info', "User: Gathering information...");

    var users = [];
    for (var number = 0; number < known_user_name.length; number++) {
        var user = new db_connection.User();
        user.username = known_user_name[number];
        user.usertype = known_user_isadmin[number];
        user.user_inf_name = knwon_user_inf_name[number];
        user.setPassword(knwon_user_password[number]);
        users.push(user);
    }

    async.each(users, function (bind_obj, callback) {
        //logger.log('info', bind_obj);
        logger.log('debug', "Adding User " + bind_obj.username);
        var query_obj = {
            username: bind_obj.username
        };
        var temp_obj = bind_obj.toObject();
        var update_field = {
            $set: {
                username: temp_obj.username,
                usertype: temp_obj.usertype,
                hash: temp_obj.hash,
                salt: temp_obj.salt,
                user_inf_name: temp_obj.user_inf_name
            }
        };
        var option_obj = {
            upsert: true
        };
        db_connection.User.update(query_obj, update_field, option_obj, function (err) {
            if (err) { return callback(err); }
            else {
                logger.log('info', "User: User " + bind_obj.username + " is upserted.");
                return callback();
            }
        });
    }, function (err) {
        if (err) { return callback(err); }
        else {
            logger.log('info', "User: Done.");
            return callback();
        }
    });
};

main();