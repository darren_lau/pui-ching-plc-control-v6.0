"use strict";
/**
 * Called externally from scheudled event (e.g. 11:55p.m.)
 * Goal: Check mod-date from DB, and then refresh the PLC's register
 * Given: All servers are up
 * Tactic: Call from the API of top layer i.e. Node server, to keep the code clean
 * Param: n/a 
 * Output: console.log 
**/

/**
 * External libraries (npm/node)
 */
let request = require('request'); //HTTP request to hosting ("node") server
let moment = require('moment'); //Logger
let crypto = require('crypto'); //Auth with another aspect

let server_logger = require('../server/modules/server_logger.js');
let server_config = require('../server/modules/server_config.js');
let db_connection = require('../server/modules/db_connection.js');

let logger = server_logger.SERVER_LOGGING(`${server_config._sLOG_DIR}/refresh_plc.log`, { level: 'info' }); //Set logger

/**
 * Constants
 */

const _sNode_server_addr = '@@ET_sEXPRESS_URL'; //Link of hosting ("node") server

/**
 * Global variables (for convinenece)
 */
let sZone_list = [];

/**
 * Exit route with potential logging
 */

//"Graceful" exiting procedure. Try to run this instead of closing abruptly.
function graceful_exit(code) {
    logger.log('debug', 'Refresh PLC closed with code = ' + code); //Never log for some reason
    process.exit(code !== undefined ? code : 1); //But will run this
}

/**
 * Functions for the process
 */

//Starting function. For easy route management.
function fStart_session() {
    //Get Deices from DB -> Get Schedule from DB for each zone -> Call Node server with exactly same data from DB
    //If mod-date mismatch, default XML will be read there. If match, no additional step processed
    //Finally fire MQTT message to push the values to PLC.
    //Since there's passport module in the server, a SPECIAL API is called.
    fCall_API();
}

function fCall_API() {
    //Confusion of use case. Added a hard-coded key for security.
    //next date = current date + 1 day = 86400 * 1000 ms
    let next_date = new Date((new Date()).getTime() + 1000 * 60 * 60 * 24);
    let date_str = moment(next_date).toISOString().substring(0, 10);
    let message = "Refreshing date: " + date_str;
    logger.log('info', message);
    let secret = "<code>ETAG</code><!--push_plc-->";
    let hash = crypto.pbkdf2Sync(message, secret, 1024, 64).toString('hex');
    let hash_again = crypto.pbkdf2Sync(hash, secret, 1024, 64).toString('hex');
    let http_link = _sNode_server_addr + "/refresh_plc";
    let post_obj = {
        message: message,
        date_str: date_str,
        key: hash_again
    };
    //logger.log('debug', post_obj);
    request.post(http_link, { form: post_obj }, (error, response, body) => {
        if (!error && response.statusCode === 200) {
            logger.log('info', "fCall_API: success");
            logger.log('info', body);
            graceful_exit(0);
        } else {
            logger.log('error', response.statusCode + ":" + response.body);
            logger.log('error', "fCall_API: session failed. Exiting with error.");
            graceful_exit(1);
        }
    });
}

/**
 * Main process session
 */

let main = function () {
    logger.log('info', "Refresh PLC on start with pid = " + process.pid);

    fStart_session();
};

main();