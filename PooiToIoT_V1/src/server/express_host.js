"use strict";
//Library used

//User Auth
var passport = require('passport');
var session = require('express-session');
//Express with middleware
var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var ejs = require('ejs');
var morgan = require('morgan');
//Built with Node
var http = require('http');
var crypto = require('crypto');

//Self defined libraties
var api_root = require('./routes/api_root');
var server_config = require('./modules/server_config'); //Custom settings
var server_logger = require('./modules/server_logger');
var user_auth = require('./modules/user_auth'); //User Authentication
var db_connection_mongoose = require('./modules/db_connection');
var db_connection_sequelize = require('./modules/db_connection_sequelize');
var api_General_sequelize = require(`./routes/api_General_sequelize`);
var api_General_mongoose = require(`./routes/api_General_mongoose`);

//DB Schema related
var Device_History = require(`../schema/sequelize/Device_History.js`)

//Calling library service
var app = express();

var logger = server_logger.SERVER_LOGGING(server_config._sLOG_DIR + '/express_host.log', { level: 'info' }); //using Winston instead of Morgan

logger.stream = {
    write: function (message, encoding) {
        logger.info(message);
    }
};

//Calling library with parameters
app.engine('html', ejs.renderFile);
app.set('views', server_config.ET_sWEBPAGE_DIR + '/view/');
app.set('view engine', 'html');

app.use(function (req, res, next) {
    if (/.*\.js/.test(req.path)) {
        res.charset = "utf-8";
    }
    next();
});

app.use('/resources', express.static(server_config.resource_dir)); //Hosting files - see Express Static
app.use('/web_console', express.static(server_config.ET_sWEBPAGE_DIR));
app.use(morgan("combined", { "stream": logger.stream }));
app.use(bodyParser.json(server_config.UPLOAD_LIMIT.JSON));
app.use(bodyParser.raw(server_config.UPLOAD_LIMIT.RAW));
app.use(bodyParser.text(server_config.UPLOAD_LIMIT.TEXT));
app.use(bodyParser.urlencoded(server_config.UPLOAD_LIMIT.URLENCODED));
app.use(cookieParser());
app.use(session(user_auth.LOGIN_SESSION));
app.use(passport.initialize());
app.use(passport.session());

//api_root.logger_init(logger);
app.use('/', api_root);
app.use('/Device_History', new api_General_sequelize.init(logger, db_connection_sequelize.Device_History, Device_History.query_obj, Device_History.update_field, []));

// Setting up Login Sessions
passport.serializeUser(user_auth.LOGIN_SERIALIZE);
passport.deserializeUser(user_auth.LOGIN_DESERIALIZE);
passport.use(user_auth.LOGIN_STRATEGY_DB());

/**
 * Main Process
 */
db_connection_mongoose.init(logger, function (err) {
    if (err) { logger.log('error', err); process.exit(1); }
    else { logger.log('info', "MongoDB connection is opened by Mongoose."); }
});

db_connection_sequelize.init( function (err) {
    if (err) { logger.log('error', err); process.exit(1); }
    else { logger.log('info', "mySQL connection is opened by Sequelize."); }
});

var server = http.createServer(app).listen(server_config._iHOST_PORT, function () {
    //var host = server.address().address;
    //var port = server.address().port;
    logger.log('info', 'Listening at port ', server.address().port);
});
