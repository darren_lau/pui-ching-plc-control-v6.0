"use strict";
// JavaScript source code
// 170920: Now the script will read for all devices within the given site. Binding logic will be groupped once again.

/**
 * External libraries (npm/node)
 */
let async = require('async'); //Flow control
let fs = require('fs'); //File stream
let mosca = require('mosca'); //Mosca server
let http = require('http'); //HTTP Bundle for Mosca
let express = require('express'); //Express module for Mosca
let child_process = require('child_process');

/**
 * Internal libraries
 */
let server_config = require('./modules/server_config.js');
let server_logger = require('./modules/server_logger.js');
let db_connection = require('./modules/db_connection.js');
let load_xml = require('./modules/load_xml.js');
let mapping_logic = require('./modules/mapping_logic.js');

/**
 * Objects form external source
 */
let oDeviceINF_XML = {}; //Device config in XML format { client_id, XML docuement content bounded with client_id }
let oDeviceINF_DB = {}; //Array of Device from DB. { client_id, DB.Device bounded with client_id }
let oDeviceINF_PARSED = {}; //Parsed oDeviceINF_DB for mapping_logic

/**
 * Constants
 */
const _bDebug_mode = process.argv.indexOf("debug") >= 0 ? true : false; //If true, spam the console with debug messages
const _bObserve_mode = process.argv.indexOf("observe") >= 0 ? true : false; //If true, spam the console with Device Status
const _bLogClint = process.argv.indexOf("log_client") >= 0 ? true : false; //If true, log connected client in {id, time, ip}
const _sConfig_dir = server_config._sCONFIG_DIR; //Config file's directory
const _oDeviceQuery = { siteId: server_config._sSiteId };
const _iMosca_read_interval = server_config._iMosca_read_interval;

//Settings for Mosca server
const _oMosca_settings = {
    port: server_config.ET_iMOSCA_PORT_MQTT, //MQTT port
    http: {
        port: server_config.ET_iMOSCA_PORT_HTTP, //HTTP port
        bundle: true, //HTTP bundle
        static: server_config.ET_sMOSCA_HTTP_HOST_DIR,
        stats: false // True for boardcasting $SYS messages 
    }
};

/**
 * Internal global objects
 */

let logger = server_logger.SERVER_LOGGING(`${server_config._sLOG_DIR}/mosca_custom.log`, { level: _bDebug_mode ? "debug" : "info", mute_console: false }); //Logger of this module
let rwloop_interval = null; //Interval object of reading modbus
let bRWLock = false; //IO lock 

//Mosca: for http bundle
let app = null; //express();
let httpServ = null; //http.createServer(app);
let oMosca_server = null; //Mosca server object
let _bSIGTERM = false;

let TCP_SOCKET_TIMEOUT = 1000 * 40;
let shutdown_table = {};

/**
 * General functions (Stage 1)
 */

let o_Err = function (msg) {
    let o = Object.assign({}, _oIDObj);
    o.err = msg;
    return o;
};

let bStatusTimeout = function (oDeviceStatus) {
    //logger.log("info", [oDeviceStatus.last_updated? (new Date()).getTime() - oDeviceStatus.last_updated.getTime() : 0, server_config._iDEVICE_STATUS_TIMEOUT]);
    return !(oDeviceStatus.last_updated && (new Date()).getTime() - oDeviceStatus.last_updated.getTime() <= server_config._iDEVICE_STATUS_TIMEOUT);
};

let bStatusNotOK = function (oDeviceStatus) {
    //logger.log("info", oDeviceStatus.connect_state);
    return oDeviceStatus.connect_state !== "connected";
};

let checkWeirdTCP = function (oDeviceStatus) {
    if (process.platform != "linux") {
        logger.log("debug", `Platform is ${process.platform}!`);
        return false; //Linux only
    }

    let found_ip = "";

    //logger.log("debug", JSON.stringify(oDeviceStatus));

    if (oDeviceStatus.device_ref && oDeviceStatus.device_ref.ip_addr) {
        found_ip = oDeviceStatus.device_ref.ip_addr;
        //if (oDeviceStatus.device_ref.client_type != "PLC") {
        //    logger.log("debug", "Device is not running in TCP!");
        //    return false;
        //}
    } else {
        return false;
    }

    //logger.log("debug", found_ip);

    let stdout = null;
    try {
        stdout = child_process.execSync(`netstat -A inet -p | grep ${found_ip}`, { stdio: ['pipe', 'pipe', "ignore"] }).toString();
        //logger.log("debug", `stdout: ${stdout}`);
        let a_str = stdout.split("\n");
        for (let i = 0; i < a_str.length; i++) {
            if ((a_str[i].indexOf("TIME_WAIT") >= 0) || (a_str[i].indexOf("TIME_SYNC") >= 0)) {
                //And SYNC_SENT
                logger.log("debug", `FOUND! ${found_ip}`);
                return shutdown_table[oDeviceStatus.client_id] ? (new Date().getTime() - shutdown_table[oDeviceStatus.client_id].getTime()) > TCP_SOCKET_TIMEOUT : true;
            }
        }
        return false;
    } catch (err) {
        logger.log("error", err.toString());
        return false;
    }
};

let CloseProcess = function (oDeviceStatus) {
    //Handle timeout
    try {
        shutdown_table[oDeviceStatus.client_id] = new Date();
        if (oDeviceStatus.client_pid) {
            logger.log("info", `Killing process ${oDeviceStatus.client_pid}...`);
            process.kill(oDeviceStatus.client_pid);
        } else {
            logger.log("error", "Process PID is not found!");
        }
    } catch (e) {
        if (e.code === "ESRCH") {
            logger.log("info", "The process is already gone.");
        } else {
            logger.log("error", e);
        }
    }
}

let fCheck_oDeviceStatus = function (oDeviceStatus) {
    if (bStatusTimeout(oDeviceStatus)) {
        logger.log("warn", `Device Status timeout. Last updated was ${oDeviceStatus.last_updated}. Trying to restart device ${oDeviceStatus.client_id}...`);
        //Handle timeout
        CloseProcess(oDeviceStatus);
        return false;
    } else if (bStatusNotOK(oDeviceStatus)) {
        logger.log("debug", `Device Status returned ${oDeviceStatus.state} for device ${oDeviceStatus.client_id}. Waiting Device online again...`);
        if (checkWeirdTCP(oDeviceStatus)) {
            logger.log("warn", `Device Status has weird connection. Trying to restart device ${oDeviceStatus.client_id}...`);
            CloseProcess(oDeviceStatus);
        }
        return false;
    } else {
        //if (checkWeirdTCP(oDeviceStatus)) {
        //    logger.log("warn", `Device Status has weird connection. Trying to restart device ${oDeviceStatus.client_id}...`);
        //    CloseProcess(oDeviceStatus);
        //    return false;
        //} else {
            return true;
        //}
    }
};

/**
 * Mosca related stuffs
 */
let mosca_setup = function () {

    //Create Mosca Server with settings
    app = app ? app : express();
    httpServ = httpServ ? httpServ : http.createServer(app);
    oMosca_server = oMosca_server ? oMosca_server : new mosca.Server(_oMosca_settings);

    /**
     * HTTP server event emitters
     */
    httpServ.on('clientError', (err, socket) => {
        socket.end('HTTP/1.1 400 Bad Request\r\n\r\n');
    });

    httpServ.on('close', () => {
        logger.log('info', "Mosca HTTP is closed.");
    });

    /**
     * Mosca server event emitters
     */
    //Set the server up when ready
    oMosca_server.on('ready', () => {
        //Fired when the mqtt server is ready
        logger.log('info', 'Mosca server is up and running...');
    });

    oMosca_server.on('clientConnected', (client) => {
        if (_bSIGTERM) { graceful_exit(0); }
        else if (_bLogClint) {
            let filepath = `${server_config._sLOG_DIR}/oConnectedClient.txt`;
            let client_inf = {
                id: client.id,
                remoteAddress: client.connection.stream.remoteAddress,
                localAddress: client.connection.stream.localAddress,
                AddressInf: client.connection.stream.address(),
                timestamp: new Date()
            };
            fs.appendFile(filepath, JSON.stringify(client_inf), 'utf-8', function (err) {
                logger.log(err ? "error" : "info", err ? err : `New client is logged to ${filepath}`);
            });
        } else {
            logger.log('info', `Client Connected (${client.id})`);
        }
    });

    oMosca_server.on('clientDisconnecting', (client) => {
        logger.log('debug', `Client Disconnecting(${client.id})`);
        if (_bSIGTERM) { graceful_exit(0); }
    });

    oMosca_server.on('clientDisconnected', (client) => {
        logger.log('debug', `Client Disconnected(${client.id})`);
        if (_bSIGTERM) { graceful_exit(0); }
    });

    oMosca_server.on('subscribed', (sTopic, client) => {
        logger.log('info', `Topic subscribed: ${sTopic} by client ${client.id}`);
        if (_bSIGTERM) { graceful_exit(0); }
    });

    oMosca_server.on('unsubscribed', (sTopic, client) => {
        logger.log('info', `Topic unsubscribed: ${sTopic} by client ${client.id}`);
        if (_bSIGTERM) { graceful_exit(0); }
    });

    //Fired when a message is published
    oMosca_server.on('published', (packet, client) => {
        if (_bSIGTERM) { graceful_exit(0); }
        //Decoding the packet...
        //logger.log('info', JSON.stringify(packet));
        let sCmd = packet.cmd; //string
        let bRetain = packet.retain; //boolean
        let iQos = packet.qos; //integer
        let bDup = packet.dup; //boolean
        let iLength = packet.payload ? packet.payload.length : undefined; //integer
        let sTopic = packet.topic; //string
        let sMessage = packet.payload.constructor.name === "Buffer" ? packet.payload.toString() : packet.payload;

        if (_bDebug_mode) {
            //logger.log('debug', `Published: topic=${sTopic}, message=${sMessage}`);
        } else if (sTopic.indexOf("$SYS") < 0) { //Ignore $SYS messages
            logger.log('verbose', `Published: topic=${sTopic}, length =${iLength}`);
        }

        let sTopic_arr = sTopic.split('/');
        if (sTopic_arr.indexOf("Status") >= 0) {
            logger.log('verbose', `TODO: Handle status.`);
        }
        if (sTopic_arr.indexOf("setStatus") >= 0) {
            //logger.log('info', "TODO: setStaus");
            logger.log('info', `Published: topic=${sTopic}, length =${iLength}`);
            mapping_logic.parse_mqtt_message(logger, sTopic, sMessage, oDeviceINF_XML, oDeviceINF_DB, oDeviceINF_PARSED, (err, parsed_cmd_list) => {
                logger.log(err ? "error" : "verbose", err ? err : "Device_Command is generated.");
                if (_bDebug_mode) {
                    let filepath = `${server_config._sLOG_DIR}/device_cmd_arr.json`;
                    fs.writeFile(filepath, JSON.stringify(parsed_cmd_list, null, 4), "utf-8", (err) => {
                        logger.log(err ? "error" : "debug", err ? err : `File dumped to ${filepath}`);
                    });
                }
                //logger.log('verbose', `Write to ${client_obj.client_id}: ${JSON.stringify(oWrite_obj1)}`);
                //{"sCommand":"write","target_register":66,"target_value":226,"tid":1506321008925}
                //{"sCommand":"write","target_register":0,"target_value":1,"tid":1506321153288,"option":"coil"}
                //{"sCommand":"write_sch","target_register":298,"target_value":[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0],"tid":1506333295776}
                async.each(parsed_cmd_list, (parsed_cmd, cb_in) => {
                    logger.log("info", JSON.stringify(parsed_cmd));
                    let fc_value = null;
                    let fc_num = null;
                    if (!parsed_cmd.sCommand) { return cb_in("sCommand is not present!"); }
                    else {
                        switch (parsed_cmd.sCommand) {
                            case "write":
                                fc_value = [parsed_cmd.target_value];
                                fc_num = parsed_cmd.option && parsed_cmd.option === "coil" ? 5 : 6;
                                break;
                            case "write_sch":
                                fc_value = parsed_cmd.target_value;
                                fc_num = parsed_cmd.option && parsed_cmd.option === "coil" ? 15 : 16;
                                break;
                            default: return cb_in(`sCommand ${parsed_cmd.sCommand} is not supported!`);
                        }
                    }
                    let device_cmd = {
                        //Assume id is in String
                        siteId: server_config._sSiteId,
                        client_id: parsed_cmd.client_id,
                        modbus_fc: fc_num,
                        fc_content: {
                            address: parsed_cmd.target_register,
                            value: fc_value
                        },
                        last_issued: new Date(),
                        last_completed: null
                    };

                    let _oIDObj = {
                        siteId: device_cmd.siteId,
                        client_id: device_cmd.client_id
                    };

                    db_connection.Device_Command.findOneAndUpdate(_oIDObj, { $set: device_cmd }, { upsert: true, new: true }, cb_in);
                }, (err) => {
                    logger.log(err ? "error" : "verbose", err ? err : "Device_Command is sent.");
                    logger.log("debug", "setStatus return.");
                });
            });
        }

        if (sTopic === "END_MOSCA" && sMessage === "END_MOSCA") {
            logger.log('info', "Received END_MOSCA package. exiting...");
            _bSIGTERM = true;
            graceful_exit(0);
        }
    });
};

/**
 * Process flow
 * May not be optimized since the process flow may expand
 */

let rwloop = function () {
    logger.log('debug', `rwloop(): RW in progress: ${bRWLock}`);
    if (!bRWLock) {
        bRWLock = true;
        db_connection.Device_Status.find(_oDeviceQuery).populate('device_ref').exec((err, oDeviceStatus) => {
            if (err) { logger.log('error', err); bRWLock = false; }
            else {
                mapping_logic.parse_device_status(logger, oDeviceStatus.filter(fCheck_oDeviceStatus), oDeviceINF_XML, oDeviceINF_DB, oDeviceINF_PARSED, (err, mqtt_msg_arr) => {
                    logger.log(err ? "error" : "verbose", err ? err : "MQTT message is generated.");
                    if (_bDebug_mode) {
                        let filepath = `${server_config._sLOG_DIR}/mqtt_msg_arr.json`;
                        fs.writeFile(filepath, JSON.stringify(mqtt_msg_arr, null, 4), "utf-8", (err) => {
                            logger.log(err ? "error" : "debug", err ? err : `File dumped to ${filepath}`);
                        });
                    }
                    try {
                        mqtt_msg_arr.forEach((mqtt_msg_device) => {
                            mqtt_msg_device.forEach((mqtt_msg_device_zone) => {
                                let mqtt_topic = mqtt_msg_device_zone.sTopic;
                                let mqtt_msg = mqtt_msg_device_zone.sMessage;
                                if (_bObserve_mode) {
                                    logger.log('info', mqtt_topic);
                                    let parsed_msg = JSON.parse(mqtt_msg);
                                    parsed_msg.Appliance.forEach((mqtt_msg_appl_type) => {
                                        mqtt_msg_appl_type.devices.forEach((mqtt_msg_appl) => {
                                            logger.log('info', JSON.stringify(mqtt_msg_appl));
                                        });
                                    });
                                }
                                if (oMosca_server) {
                                    oMosca_server.publish({
                                        topic: mqtt_topic,
                                        payload: mqtt_msg, // or a Buffer
                                        qos: 0, // 0, 1, or 2
                                        retain: false // or true
                                    }, (err) => {
                                        logger.log(err ? "error" : "debug", err ? err : "Message sent.");
                                    });
                                } else {
                                    throw new Excption("Mosca server is not up!");
                                }
                            });
                        });
                    } catch (e) {
                        logger.log('error', e);
                    }
                    //more_progress(mqtt_msg_arr);
                    logger.log('debug', 'rwloop return.');
                    bRWLock = false;
                });
            }
        });
    } else {
        logger.log('debug', 'Skipped: rwloop in progress.');
    }
};

let rwloop_refresh = function () {
    if (rwloop_interval) clearInterval(rwloop_interval);
    rwloop_interval = setInterval(rwloop, _iMosca_read_interval);
};

let fOnExternalDataLoaded = function () {
    //Insert any task you want. However the rwloop already does everything you want.
    if (_bDebug_mode) {
        logger.log('info', "The client is running in debug mode.");
    }
    rwloop_refresh();
};

//Bind oDeviceINF_DB with loaded oDevice_inf
let fApply_device_inf = function (err, oLoaded_obj) {
    if (err) {
        logger.log('error', err);
        process.exit(9);
    } else {
        //Exit process when there are nothing match to target device with the given information
        if (!oLoaded_obj) {
            logger.log('error', JSON.stringify(o_Err("Site information not found")));
            process.exit(9);
        } else {
            oDeviceINF_DB = oLoaded_obj;

            let client_arr = oDeviceINF_DB.map((d, i, a) => { return d.client_id; });
            logger.log('info', "[db_connection] Site information found.");
            logger.log('debug', "Site information found: " + JSON.stringify(client_arr));
            //process.exit(0);
            async.each(client_arr, (sClient_id, cb_in) => {
                load_xml.loadForMQTTControl(logger, sClient_id, (err, config_from_xml) => {
                    if (err) { logger.log('error', err); }
                    else { oDeviceINF_XML[sClient_id] = config_from_xml; }
                    return cb_in();
                });
            }, (err) => {
                if (err) { logger.log('error', err); } //Should not happen. Config file error should be skipped.
                else {
                    logger.log('info', "[load_xml] Device configs are loaded.");
                    if (_bDebug_mode) {
                        let filepath = `${server_config._sLOG_DIR}/client_config_${_oDeviceQuery.siteId}.json`;
                        fs.writeFile(filepath, JSON.stringify(oDeviceINF_XML, null, 4), "utf-8", (err) => {
                            logger.log(err ? "error" : "debug", err ? err : `File dumped to ${filepath}`);

                        });
                    }
                    mapping_logic.build_plc_based_inf(logger, oDeviceINF_DB, (err, wrapped_oDevice) => {
                        if (err) {
                            logger.log('error', err);
                            process.exit(9);
                        } else {
                            logger.log('info', "[plc_zone_appl] Device info are parsed.");
                            oDeviceINF_PARSED = wrapped_oDevice;
                            fOnExternalDataLoaded();
                        }
                    });
                }
            });
        }
    }
};

//"Graceful" exiting procedure. Try to run this instead of closing abruptly.
let graceful_exit = function (code) {
    //Close the stuffs if they exists
    let exit_db = function (cb) {
        if (db_connection) {
            db_connection.onExit();
            if (db_connection.mongoose.connection) {
                db_connection.mongoose.connection.close((err) => {
                    logger.log('debug', "Mongoose disconnected from mongoDB.");
                    return cb(err);
                });
            } else {
                return cb();
            }
        }
    };

    let exit_http = function (cb) {
        try {
            if (httpServ) {
                httpServ.close();
            }
            logger.log('info', "Mosca HTTP is being closed.");
        } catch (e) {
            logger.log('error', e);
        }
        return cb();
    };

    let exit_mosca = function (cb) {
        if (oMosca_server) {
            oMosca_server.close(() => {
                logger.log('info', "Mosca is exited.");
                return cb();
            });
        } else {
            logger.log('info', "Mosca is already exited.");
            return cb();
        }
    };

    async.parallel([exit_db, exit_http, exit_mosca], (err) => {
        if (err) logger.log('error', err);
        process.exit(code !== undefined ? code : 1);
    });
};

//Event emitters of this process (not server nor connection but process itself). See Node API for more infomration
let set_process_handlers = function () {
    process.on('uncaughtException', (err) => {
        //Hope logging with logger is still ok (e.g. listen EADDRINUSE)
        logger.log('error', 'Caught exception (exit instantly): ' + err, () => {
            _bSIGTERM = true;
            graceful_exit(1); //Redundancy exit route
        });
    });

    process.on('exit', (code) => {
        //Only sync process is allowed. Do all the closing works before reaching here
        logger.log('info', 'Process exited with code ' + code);
    });

    process.on('SIGINT', () => {
        //Triggered when user press Ctrl-C to "close" the program
        logger.log('info', "Process is shutting down by SIGINT...");
        _bSIGTERM = true;

        //Real exit - or no exit
        graceful_exit(0);
    });

    process.on('SIGTERM', () => {
        //Identical to SIGINT but usually by software (shell?)
        logger.log('info', "Process is shutting down by SIGTERM...");
        _bSIGTERM = true;

        //Real exit - or no exit
        graceful_exit(0);
    });
};

/**
 * Main process session
 */
let main = function () {
    //Fire the first message with logger
    logger.log('info', "Mosca Custom server on start with pid = " + process.pid);
    set_process_handlers(process);
    mosca_setup();

    db_connection.init(logger, (err) => {
        if (err) { return fApply_device_inf(err, null); }
        else {
            logger.log('info', "DB connection is opened by Mongoose.");
            return db_connection.Device.find(_oDeviceQuery, fApply_device_inf);
        }
    });
};

main();
//End of JavaScript source code