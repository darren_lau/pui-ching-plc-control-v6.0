﻿/**
{
            "sTopic": "/TowerX/Floor99/X99-1/PLC/Status",
            "sMessage": "{
                \"client_id\":\"PLC_ALARM\",
                \"client_name\":\"FT1A-B40RC\",
                \"client_type\":\"PLC\",
                \"Appliance\":[{
                    \"appl_type\":\"警鐘\",
                    \"devices\":[
                        {\"appl_name\":\"停車場水浸 (地庫 B1)\",\"state\":1 },
                        {\"appl_name\":\"停車場水浸 (地庫 B2)\",\"state\":0},
                        {\"appl_name\":\"升降機警鐘\",\"state\":0},
                        {\"appl_name\":\"CCTV警報\",\"state\":1}
                    ]
                }]
            }"
}
**/

"use strict";
let client_mqtt = require("./modules/mqtt_client.js");
let server_config = require('./modules/server_config.js');
let server_logger = require('./modules/server_logger.js');

const _bDebug_mode = process.argv.indexOf("debug") >= 0; //If true, spam the console with debug messages

let logger = server_logger.SERVER_LOGGING(server_config._sLOG_DIR + '/mqtt_spambot.log', { level: (_bDebug_mode ? 'debug' : 'warn') });

let mqttClient = null;

let poll_interval = null;

let gen_data = function (field, appl_name, num) {
    let gen_random_val = function (field) {
        switch (field) {
            case "Temperature": return Math.random() * 15 + 20; //°C
            case "Humidity": return Math.random() * 50 + 50; //%
            case "VOC": return Math.random() * 9.98 + 0.02; //PPM
            case "PM2.5": return Math.random() * 1000; //μg/m³
            case "PM10": return Math.random() * 1000; //μg/m³
            default: return 0;
        }
    };
    let o = { "appl_type": field, "devices": [] };
    for (let i = 0; i < num; i++) {
        o.devices.push({ "appl_name": `${appl_name} ${i + 1}`, "value": gen_random_val(field) });
    }
    return o;
}

//May be integrated with SetDB scripts.
let gen_mqtt_with_binding = function ({ tower, floor, zdiv }) {
    return {
        sTopic: `/Tower${tower}/Floor${floor}/${tower}${floor}-${zdiv}/PLC/Status/Sensor`,
        sMessage: JSON.stringify({
            //"client_id": "",
            //"client_name": "mqtt_spambot",
            //"client_type": "mqtt",
            "Appliance": [
                gen_data("Temperature", "溫度", 3),
                gen_data("Humidity", "濕度", 3),
                gen_data("PM2.5", "PM2.5", 3),
                gen_data("PM10", "PM10", 3),
                gen_data("VOC", "VOC", 3)
            ]
        })
    }
}

let zone_arr = ["A6-1", "A6-2", "K5-1", "K5-2", "K6-1", "K9-1", "K13-1", "K14-1", "K14-2", "K15-1"]
let split_and_gen_by_zoneId = function (d) {
    let a = d.split("-");
    return gen_mqtt_with_binding({
        tower: d[0],
        floor: a[0].slice(1),
        zdiv: a[1]
    });
};

let p_send_mqtt = async function (a, b, c) {
    return new Promise((t, f) => {
        logger.debug(JSON.stringify(b));
        mqttClient.publish(a, b, c, t);
    });
};

let send_mqtt = async function () {
    await zone_arr.map(split_and_gen_by_zoneId).reduce((p, m) => {
        return p.then(() => {
            return p_send_mqtt(m.sTopic, m.sMessage, { qos: 0 });
        });
    }, Promise.resolve());
};

let MQTTonConnected = function () {
    logger.log('info', "[main] MQTT client is connected to MQTT broker.");

    clearInterval(poll_interval);
    poll_interval = setInterval(() => {
        send_mqtt()
            .then(() => {
                logger.verbose("Batch send.");
            }).catch((err) => {
                logger.error(err);
            })
    }, 10000);
    /**
    mqttClient.subscribe("/TowerA/Floor6/A6-2/PLC/Status", { qos: 0 }, (err, grant) => {
        if (err) { logger.log('error', err); process.exit(1); }
        else { logger.log('info', "[main] MQTT client is subscribed to: " + JSON.stringify(grant)); }
    });
    **/
};

let MQTTonMessage = function (sTopic, bMessage) {
    logger.log('verbose', "[mqtt_message]" + JSON.stringify({ sTopic: sTopic, bMessage: bMessage.toString() }));

    //check_alarm(bMessage.toString());
};

let MQTTonDisconnected = function () {
    logger.log('error', "TODO: Handle MQTT disconection");

    clearInterval(poll_interval);
};

let init = function () {
    logger.log('info', "START_MQTT: Process start with pid " + process.pid);

    mqttClient = client_mqtt.boot_mqtt_basic(logger, server_config.msg_ext_mqtt_link, MQTTonConnected, MQTTonMessage, MQTTonDisconnected)

    if (mqttClient) {
        logger.log('info', "[main] MQTT client is booted.");
    } else {
        logger.log('error', "[main] MQTT client is NOT booted.");
        process.exit(1);
    }
};

init();     