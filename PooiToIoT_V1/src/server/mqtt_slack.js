﻿/**
{
            "sTopic": "/TowerX/Floor99/X99-1/PLC/Status",
            "sMessage": "{
                \"client_id\":\"PLC_ALARM\",
                \"client_name\":\"FT1A-B40RC\",
                \"client_type\":\"PLC\",
                \"Appliance\":[{
                    \"appl_type\":\"警鐘\",
                    \"devices\":[
                        {\"appl_name\":\"停車場水浸 (地庫 B1)\",\"state\":1 },
                        {\"appl_name\":\"停車場水浸 (地庫 B2)\",\"state\":0},
                        {\"appl_name\":\"升降機警鐘\",\"state\":0},
                        {\"appl_name\":\"CCTV警報\",\"state\":1}
                    ]
                }]
            }"
}
**/

"use strict";
let request = require("request");

let client_mqtt = require("./modules/mqtt_client.js");
let server_config = require('./modules/server_config.js');
let server_logger = require('./modules/server_logger.js');

const _bDebug_mode = process.argv.indexOf("debug") >= 0; //If true, spam the console with debug messages

let logger = server_logger.SERVER_LOGGING(server_config._sLOG_DIR + '/mqtt_slack.log', { level: (_bDebug_mode ? 'debug' : 'warn') });

const ALARM_MQTT = "/TowerX/Floor99/X99-1/PLC/Status/#";
//const SLACK_WEB_HOOK = "https://hooks.slack.com/services/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"; //Production
const SLACK_WEB_HOOK = "https://hooks.slack.com/services/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"; //Development

let mqttClient = null;

let alarm_map = {}

let desc_obj_alarm_attachment = function (o, d) {
    let corrected_device_name = d.appl_name.replace(/[()]/g, " ");
    return {
        "author_name": `實時警報器`,
        "title": d.state ? "警報正在嗚響" : "警報現正解除",
        "fallback": d.state ? `警報正在嗚響: ${corrected_device_name}` : `警報現正解除: ${corrected_device_name}`,
        "color": d.state ? "danger" : "good",
        "fields": [
            //{
            //    "title": "Client ID",
            //    "value": d.client_id,
            //    "short": true
            //},
            {
                "title": "警報",
                "value": corrected_device_name,
                "short": true
            },
            {
                "title": "狀況",
                "value": d.state ? "ON" : "OFF",
                "short": true
            },
            {
                "title": "時間",
                "value": new Date().toLocaleString(),
                "short": true
            }
        ],
        "footer": "DB Monitor",
        "ts": (new Date()).getTime() / 1000
    };
};

let post_alarm = function (o, d, cb) {
    //let post_text = desc_obj_error_str(op, d);
    let post_attachment = desc_obj_alarm_attachment(o, d);
    //logger.log('debug', JSON.stringify(d));
    request.post({
        url: SLACK_WEB_HOOK,
        form: JSON.stringify({
            //text: post_text
            attachments: [post_attachment]
        })
    }, (err, httpResponse, body) => {
        if (err) {
            return cb(err);
        } else {
            if (httpResponse.statusCode !== 200) {
                logger.log('error', httpResponse.statusCode + " " + body);
            }
            return cb();
        }
    });
};

let check_and_update_alarm = function (o) {
    if (!alarm_map[o.client_id]) {
        alarm_map[o.client_id] = {};
    }
    o.Appliance.forEach(aa => {
        aa.devices.forEach(d => {
            if (!(d.appl_name in alarm_map[o.client_id])) {
                alarm_map[o.client_id][d.appl_name] = false;
            }
            if (d.state != alarm_map[o.client_id][d.appl_name]) {
                alarm_map[o.client_id][d.appl_name] = d.state;
                //logger.log("info", JSON.stringify(desc_obj_alarm_attachment(o, d)));
                post_alarm(o, d, (err) => {
                    logger.log(err ? "error" : "verbose", err ? err : `${o.client_id}: Alarm state (${d.appl_name}) has been posted to slack.`);
                });
            } else {
                logger.log("verbose", `${o.client_id}: Alarm state (${d.appl_name}) is not changed.`);
            }
        });
    });
};

let check_alarm = function (mqtt_s) {
    var o = null;
    try {
        o = JSON.parse(mqtt_s);
        if (!(o.Appliance && o.Appliance.length)) { throw new Error(`Message does not contains Appliance information!`); }
        o.Appliance.forEach((aa) => {
            if (!(aa.devices && aa.devices.length)) { throw new Error(`Message does not contains Device status!`); }
        });
        check_and_update_alarm(o);
    } catch (e) {
        logger.log("error", e);
    }
};

let MQTTonConnected = function () {
    logger.log('info', "[main] MQTT client is connected to MQTT broker.");
    mqttClient.subscribe(ALARM_MQTT, { qos: 0 }, (err, grant) => {
        if (err) { logger.log('error', err); process.exit(1); }
        else { logger.log('info', "[main] MQTT client is subscribed to: " + JSON.stringify(grant)); }
    });
};

let MQTTonMessage = function (sTopic, bMessage) {
    logger.log('verbose', "[mqtt_message]" + JSON.stringify({ sTopic: sTopic, bMessage: bMessage.toString() }));

    check_alarm(bMessage.toString());
};

let MQTTonDisconnected = function () {
    logger.log('error', "TODO: Handle MQTT disconection");
};

let init = function () {
    logger.log('info', "START_MQTT: Process start with pid " + process.pid);

    mqttClient = client_mqtt.boot_mqtt_basic(logger, server_config.msg_ext_mqtt_link, MQTTonConnected, MQTTonMessage, MQTTonDisconnected)

    if (mqttClient) {
        logger.log('info', "[main] MQTT client is booted.");
    } else {
        logger.log('error', "[main] MQTT client is NOT booted.");
        process.exit(1);
    }
};

init();     