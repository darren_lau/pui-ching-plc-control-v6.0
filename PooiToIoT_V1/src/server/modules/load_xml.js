"use strict";
//NPM libraries
var xml2js = require('xml2js');
var async = require('async');

//Node libraries
var fs = require('fs');

//Global Config
var server_config = require('./server_config.js');
var plc_config_validation = require('./plc_config_validation.js');

//Set xml2js with correct_syntax
var XML_parser = new xml2js.Parser({
    attrNameProcessors: [fCorrect_syntax],
    tagNameProcessors: [fCorrect_syntax],
    valueProcessors: []
});

//Correct syntax which is OK in XML but not in NodeJS
function fCorrect_syntax(sName) {
    //Hyphen to Underscore
    sName = sName.replace('-', '_');
    //Lowercase to prevent variable confusion
    sName = sName.toLowerCase();
    return sName;
}

var logger;

var _sCONFIG_DIR = server_config._sCONFIG_DIR;
var OBJ_holidayDates = {};
var OBJ_defaultSchedules = {};
var OBJ_plcConfig = {};

//Read and parse a XML file. Validation is NOT included.
//err: redirected error
//callback: function (err, JSobj)
var loadXML = function (filepath, callback) { 
    fs.readFile(filepath, function (err, data) {
        if (err) { return callback(err, null); } 
        XML_parser.parseString(data, function (err, result) {
            if (err) { return callback(err, null); }
            else {
                return callback(null, result);
            }
        });
    });
}

var read_holidayDates = function (callback) {
    //server_config.XML_holidayDates
    var fileName = server_config.XML_holidayDates;
    logger.log('debug', "Reading Holiday Dates " + fileName + "...");
    loadXML(_sCONFIG_DIR + '/' + fileName, function (err, JSObj) {
        if (err) { return callback(err); }
        else {
            logger.log('info', "Holiday Dates are loaded.");
            OBJ_holidayDates = JSObj;
            return callback(null);
        }
    });
}

var read_defaultSchedules = function (callback) {
    //server_config.XML_defaultSchedules
    var fileName = server_config.XML_defaultSchedules;
    logger.log('debug', "Reading Default Schedules " + fileName + "...");
    loadXML(_sCONFIG_DIR + '/' + fileName, function (err, JSObj) {
        if (err) { return callback(err); }
        else {
            logger.log('info', "Default Schedules are loaded.");
            OBJ_defaultSchedules = JSObj;
            return callback(null);
        }
    });
}

var read_plcConfig = function (fileName, callback) {
    logger.log('debug', "Reading config file " + fileName + "...");
    loadXML(_sCONFIG_DIR + '/' + fileName, function (err, JSObj) {
        if (err) { return callback(err); }
        else {
            OBJ_plcConfig = JSObj.mqtt_client; //Root = mqtt_client
            logger.log('debug', "Applying config file " + fileName + "...");
            plc_config_validation.fApply_config(logger, OBJ_plcConfig, function (pass, err) {
                return callback(pass ? null : fileName + " not correct at value: mqtt_client." + err);
            });
        }
    });
}

//Called by "express_host"
var loadForExpressHost = function (logger_in, callback) {
    logger = logger_in;
    logger.log('info', 'Loading XML files...');
    async.parallel([
        read_holidayDates,
        read_defaultSchedules
    ], function (err) {
        return err? callback(err): callback(null, OBJ_holidayDates, OBJ_defaultSchedules); 
    });
}

//Called by "mqtt_control"
var loadForMQTTControl = function (logger_in, _sClient_id, callback) {
    logger = logger_in;
    //Read, parse, and apply config file. Validation is included.
    logger.log('verbose', "Reading PLC Config file...");
    read_plcConfig(server_config._sConfig_XML_filename(_sClient_id), function (err) {
        return err ? callback(err) : callback(null, OBJ_plcConfig);
    });
}

var exports = module.exports = {
    loadForExpressHost: loadForExpressHost,
    loadForMQTTControl: loadForMQTTControl
};

/**
var XML_holidayDates = '/holidayDates.xml';
var XML_defaultSchedules = '/defaultSchedules.xml';
var STR_holidayDates = fs.readFileSync(__dirname + XML_defaultSchedules);
XML_parser.parseString(data, function (err, result) {
    for (var i = 0; i < result.Schedule.index.length; i++) {
        var type = result.Schedule.index[i]['$'].id;
        if (type == dayType) {
            var time_slots_array = result.Schedule.index[i].time_slots[0].slot;
            //console.log(time_slots_array.length);
            for (var y = 0; y < time_slots_array.length; y++) {
                returnArray[y] = {
                    slot_value: time_slots_array[y]['$'].slot_value,
                    remark: (ret ? ret : time_slots_array[y]['$'].remark)
                };
            }
        }
    }
});
**/