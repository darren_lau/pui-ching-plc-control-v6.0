"use strict";

var server_config = require('./server_config.js');
var extend = require('util')._extend; //Shallow copy
var async = require('async');
var _iRead_register_from = server_config._iRead_register_from; //From document, for potential use

//PLC - Zone - Appliance manager
//This script is made from rule "do not change the schema!"
//This is to avoid putting pure logic codes in the controller scripts.

//Data source: Devices from DB
//Try not to make any global variables here.
//But define a unified data type is essential

/**
    Schema "Devices" from DB (no "key" is defined, only "siteId" - "client_id" - "zoneId" combination is unique):
    //Assume id is in String
    siteId: String,
    mqtt_tag: String,
    //PLC
    client_id: String,
    client_name: String,
    client_type: String,
    ip_addr: String,
    mac: String,
    //Zone
    zoneId: String,
    tower: String,
    floor: String,
    desc: String,
    //Schedule
    default_id: String,
    //Appl
    appliances: [{
        appl_name: String,
        appl_type: String,
        appl_index: Number
    }]
**/

//Schema "Device_Status" from DB (no "key" is defined, only "siteId" - "client_id" - "zoneId" combination is unique):
/**
	//Assume id is in String
  	siteId: String,
  	zoneId: String,
  	client_type: String,
  	client_id: String,
  	Appliance: [{
        appl_name: String,
        Field: Type // This is from XML
    }] //Messy, dynamic, depends on almost everything
**/

// Schema "oModbus_inf" in this script ( key = "siteId" THEN "client_id" THEN "zoneId"):
/**
"siteId": {
    "client_id" : {
        //PLC
        client_id: String,
        client_name: String,
        client_type: String,
        ip_addr: String,
        mac: String,

        zones: {
            zoneId: {
                //Zone
                    tower: String,
                    floor: String,
                    desc: String,
                //Schedule
                    default_id: String,
                //MQTT
                    mqtt_tag: String,
                //Appl
                    appliances: {
                        "appl_index": {
                            appl_name: String,
                            appl_type: String,
                            appl_index: Number
                        }
                    }   
            }
        }
    }
}
**/

//Schema "sZone_status" from npm jsmodbus
//This may be emulated by "modbus_control" also
/**
{
    fc: Number,
    byteCount: Number,
    register: [Number],
    coils: [Number]
}
**/

//Build an array of "oModbus_inf" from an array "Devices"
//Assumed "Zone" - "Appliance" must be unique and traceable from "PLC" - "Zone"
//Additive basis - "if not exist, add it"
//@Param
//  logger: logger from caller
//  oDevice: "Devices" from DB
//@Output callback
//  err: null if no error, or any error messages from this or other modules
//  oModbus_inf: "oModbus_inf" from this module
var build_plc_based_inf = function (logger, oDevice, callback) {
    logger.log('verbose', "Building PLC based information from DB...");
    var JSArr = mongoDoc_to_plain_JSObj(oDevice);
    if (!JSArr) { return callback("Coverting Mongo Docuement failed. Please check the retrieved object from DB."); }
    var oModbus_inf = {};
    for (var i0 = 0; i0 < JSArr.length; i0++) {
        if (!oModbus_inf[JSArr[i0].client_id]) {
            oModbus_inf[JSArr[i0].client_id] = {
                client_id: JSArr[i0].client_id,
                client_name: JSArr[i0].client_name,
                client_type: JSArr[i0].client_type,
                ip_addr: JSArr[i0].ip_addr,
                mac: JSArr[i0].mac,
                zones: {}
            };
        }
        if (!oModbus_inf[JSArr[i0].client_id].zones[JSArr[i0].zoneId]) {
            oModbus_inf[JSArr[i0].client_id].zones[JSArr[i0].zoneId] = {
                zoneId: JSArr[i0].zoneId,
                tower: JSArr[i0].tower,
                floor: JSArr[i0].floor,
                desc: JSArr[i0].desc,
                mqtt_tag: JSArr[i0].mqtt_tag,
                default_id: JSArr[i0].default_id,
                appliances: {}
            };
        }
        for (var i1 = 0; i1 < JSArr[i0].appliances.length; i1++) {
            var appl_index = JSArr[i0].appliances[i1].appl_index;
            if (!oModbus_inf[JSArr[i0].client_id].zones[JSArr[i0].zoneId].appliances[appl_index]) {
                oModbus_inf[JSArr[i0].client_id].zones[JSArr[i0].zoneId].appliances[appl_index] = {
                    appl_index: appl_index,
                    appl_name: JSArr[i0].appliances[i1].appl_name,
                    appl_type: JSArr[i0].appliances[i1].appl_type
                };
            }
        }
    }
    var oWrapped = {};
    oWrapped[JSArr[0].siteId] = oModbus_inf;
    return callback(null, oWrapped);
};

var mongoDoc_to_plain_JSObj = function (mongoDocs) {
    var JSArr = [];
    for (var i0 = 0; i0 < mongoDocs.length; i0++) {
        JSArr.push(JSON.parse(JSON.stringify(mongoDocs[i0].toObject())));
    }
    return JSArr;
};

//Return MQTT publish topic for specific zoneId
function sMqtt_publish_topic(oZone_obj) {
    if (!(oZone_obj && oZone_obj.mqtt_tag)) {
        logger.log('error', "mqtt_tag is missing for zoneId: " + oZone_obj.zoneId);
        return "ERROR";
    } else {
        return oZone_obj.mqtt_tag + '/PLC/Status';
    }
}

//@Param
//  device_arr: [oAppl_expanded] (see function "insert_status_from_xml")
//@Output callback
//  err: error message
//  arr: [
//      appl_type: oAppl_expanded.appl_type,
//      devices: [oAppl_expanded.appl_status]
//  ]
var rearrange_arr = function (device_arr, callback) {
    //array - map - array operation
    var map = {};
    var arr = [];
    async.each(device_arr, function (device_obj, callback) {
        if (!map[device_obj.appl_type]) {
            map[device_obj.appl_type] = {
                appl_type: device_obj.appl_type,
                devices: []
            };
        }
        map[device_obj.appl_type].devices.push(device_obj.appl_status);
        callback(null);
    }, function (err) {
        if (err) { return callback(err, null); }
        else {
            async.each(map, function (appl_obj, callback) {
                arr.push(appl_obj);
                callback(null);
            }, function (err) {
                return err ? callback(err, null) : callback(null, arr);
            });
        }
    });
};

//@param
//  oAppl_raw: {
//      appl_name: String,
//      appl_type: String,
//      appl_index: Number
//  }
//  iRegister_value: [Number]
//  iCoil_value: [Number]
//  oConfig: From xml. Passed from "mqtt_control"
//@Output callback
//  err: error message
//  oAppl_expanded: oAppl_raw + { appl_status: { field: value } }
var insert_status_from_xml = function (oAppl_raw_external, iRegister_value, iCoil_value, oConfig, callback) {
    //To make appending objects effective, initialize the appl_status first
    var oAppl_raw = extend({}, oAppl_raw_external);
    oAppl_raw.appl_status = {};
    var oAppl_status = {};
    var iFound_appl_type = -1;
    var iFound_command = -1;

    //Step 2.1: Search for appl_type in config XML file
    for (var i1 = 0; i1 < oConfig.type.length; i1++) {
        /**
        logger.log('debug', "config: " + oConfig.type[i1].$.id +
            ", oModbus_inf: " + oAppl_arr[i0].appl_type + '.');
            **/
        if (oConfig.type[i1].$.id === oAppl_raw.appl_type) {
            iFound_appl_type = i1;
            //Step 2.2: Search for command "read" under appl_type
            for (var i2 = 0; i2 < oConfig.type[i1].command.length; i2++) {
                //logger.log('debug', oConfig.type[i1].command[i2].$.type);
                if (oConfig.type[i1].command[i2].$.type === "read") {
                    iFound_command = i2;
                    //Step 2.3: Append the appl_status object with field entries
                    for (var i3 = 0; i3 < oConfig.type[i1].command[i2].field.length; i3++) {
                        //oField_obj = {id, type, offet, range}
                        //Validation of oField_obj has been done before
                        var oField_obj = oConfig.type[i1].command[i2].field[i3].$;
                        var iIndex_target = oAppl_raw.appl_index + parseInt(oField_obj.offset);
                        var iField_raw_value;
                        // logger.log('debug', "oField_obj=" + JSON.stringify(oField_obj));
                        try {
                            //logger.log('debug', iIndex_target);
                            //It's convinent to determine it is D (register) or Q (coil) since D >= 200 and Q < 128
                            if (iIndex_target >= 200) {
                                iField_raw_value = iRegister_value[iIndex_target];
                                //iField_raw_value = iRegister_value[iIndex_target - _iRead_register_from];
                            }
                            else if (iIndex_target < 128) { iField_raw_value = iCoil_value[iIndex_target]; }
                        } catch (e) { return callback("fAnalysis_zone_status: Target index is not supported: " + iIndex_target, null); }
                        if (isNaN(parseInt(iField_raw_value))) { return callback("fAnalysis_zone_status: iField_raw_value is invalid (" + JSON.stringify({ oAppl_raw_external, iRegister_value, iCoil_value }) + ")", null); }
                        //Convert raw value into desired field value
                        var nField_value;
                        if (oField_obj.type === "Integer") { nField_value = parseInt(iField_raw_value); }
                        if (oField_obj.type === "Integer/10") { nField_value = parseInt(iField_raw_value) / 10; }
                        if (oField_obj.type === "Integer/100") { nField_value = parseInt(iField_raw_value) / 100; }
                        //Finally append the appl_status (with appl_name for convinence)
                        oAppl_status[oField_obj.id] = nField_value;
                        oAppl_status['appl_name'] = oAppl_raw.appl_name;
                        //logger.log('info', JSON.stringify(oAppl_status));
                        //logger.log('info', "I was here");
                    }
                }
            }
            if (iFound_command < 0) {
                return callback('fAnalysis_zone_status: Command "read" not supported for the type "' + oType_obj.appl_type + '" yet', null);
            }
            //Append appl_status to oAppl_arr
            oAppl_raw.appl_status = extend({}, oAppl_status);
            //oAppl_arr.push(oAppl_obj);
            //logger.log('info', JSON.stringify(oAppl_arr[i0]));
            return callback(null, oAppl_raw);
        }
    }
    if (iFound_appl_type < 0) {
        return callback("fAnalysis_zone_status: appl_type " + oAppl_raw.appl_type + " not match", null);
    }
};

/**
 * Section of analysing Status/ SetStatus
 * fAnalysis_zone_status, cb_analysis_zone_status: Register address -> Message
 * fAnalysis_set_status: Message -> Register address
 */

//@Param (both schema a located above)
//  oModbus_inf_obj: oModbus_inf[siteId][client_id]
//  sZone_status: Register values in a PLC
//  oConfig: From config/plc/*.xml - see "./plc_config_validation" for details
//@Output callback
//  err: error messages
//  mqtt_arr:[{
//      sTopic: String,
//      sMessage: String
//  }]
function fAnalysis_zone_status(logger, oModbus_inf_obj, sZone_status, oConfig, callback) {
    //Step 1: Validation with initialization
    var iRegister_value; //Array of integer
    var iCoil_value; //Array of integer

    try {
        var JSObj = sZone_status; //JSON.parse(sZone_status);
        iRegister_value = JSObj.register;
        iCoil_value = JSObj.coils;
    } catch (e) {
        return callback("fAnalysis_zone_status: message is not JSON", null);
    }

    if (!oModbus_inf_obj.client_id) { return callback("fAnalysis_zone_status: client_id not exist", null); }
    var oTarget_modbus = extend({}, oModbus_inf_obj);

    var oMQTT_Messages = []; // [{sTopic, sMessage}]

    //logger.log('debug', "fAnalysis_zone_status()");
    //logger.log('debug', JSON.stringify(oModbus_inf_obj.zones));

    //Step 2: Loop through all zones all appliances
    async.each(oModbus_inf_obj.zones, function (oZone_inf, callback_2) {
        //logger.log('debug', JSON.stringify(oZone_inf));
        var oAppliances = {};
        //var i = 0;
        var device_arr = [];
        async.each(oZone_inf.appliances, function (oAppl_raw, callback_3) {
            //Step 3: Inject the values based from oConfig
            insert_status_from_xml(oAppl_raw, iRegister_value, iCoil_value, oConfig, function (err, oAppl_expanded) {
                //logger.log('debug', JSON.stringify(oAppl_expanded));
                //Inject failed. Pop error message, then proceed to the next appliance. Don't stop the process.
                if (err) { return callback_3(err); }
                else if (oAppl_expanded !== null && oAppl_expanded.appl_status) {
                    //Push if the expended appl_status exists
                    //Problem encountered: oAppliances cannot operate well here. Seems assigning values into an array in async mode is bad.
                    //Current solution: Make an array out of this loop, and call another function to operate them.
                    device_arr.push(oAppl_expanded);
                    return callback_3(null);
                } else {
                    return callback_3("uncaught suituation");
                }
                //callback();
            });
        }, function (err) {
            if (err) { return callback_2(err); }
            else {
                //logger.log('debug', device_arr);
                //process.exit(0);

                //Step 3.5: Group the status array into the preferred format. 
                rearrange_arr(device_arr, function (err, oAppliance_arr) {
                    if (err) { return callback_2(err); }
                    else {
                        //logger.log('debug', JSON.stringify(oAppliances));

                        //Step 4: Make Appliance Status based from desired format.
                        var oAppl_Status = {
                            client_id: oModbus_inf_obj.client_id,
                            client_name: oModbus_inf_obj.client_name,
                            client_type: oModbus_inf_obj.client_type,
                            Appliance: oAppliance_arr
                        };
                        if (oAppl_Status.Appliance === null) {
                            return callback_2("fAnalysis_zone_status: Error when transforming map to array: " + JSON.stringify(oAppliances));
                        }
                        //Step 5: Make MQTT Topics and Messages
                        oMQTT_Messages.push({ sTopic: sMqtt_publish_topic(oZone_inf), sMessage: JSON.stringify(oAppl_Status) });
                        return callback_2(null);
                    }
                });
            }
        });
    }, function (err) {
        //Step 6: return messages to fire out
        return err ? callback(err, null) : callback(null, oMQTT_Messages);
    });
}

//oModbus_inf -> oModbus_cp
//oModbus_inf2 -> device's starting register
//oConfig + oField_Obj -> offset of device's starting register
//oField_value -> actual nInput_value
//Return and fire it to modbus_childprocess
//Error message is the hint of the section
function fAnalysis_set_status(logger, oConfig, oModbus_inf_obj, sTopic, bMessage, callback) {
    //Setp 0: Convert them into string since MQTT message's type is uncertain
    var sTopic_str = sTopic;
    var sMessage_str = bMessage;
    if (sTopic_str.constructor.name !== "String") { sTopic_str = sTopic_str.toString(); }
    if (sMessage_str.constructor.name !== "String") { sMessage_str = sMessage_str.toString(); }

    logger.log('debug', 'Step 1');
    //Step 1: Check MQTT sTopic and bMessage,
    var sTopic_param = sTopic_str.split('/'); //sTopic_param = [siteId,zoneId,"setStatus"]; 
    var sTarget_zoneId = sTopic_param[sTopic_param.length - 2];
    var oWrite_param;
    try { oWrite_param = JSON.parse(sMessage_str); }
    catch (e) { return callback('fAnalysis_set_status: Message is not JSON: ' + sMessage_str); }
    logger.log('debug', 'Step 1.5');
    //Step 1.5: added in 150831: for scheudler, if the input is for scheduler use,
    //Route it to another function instead of continuing.
    if (oWrite_param && oWrite_param.command === "SET_SCHEDULER"
        && oWrite_param.value !== null && oWrite_param.sch_index !== null) {
        //Since its value is supposed to be built completey by other scripts, 
        //No database access will be made. This will directly pass the values.
        return fAnalysis_set_schedule(logger, oModbus_inf_obj.client_id, oWrite_param, callback);
    } else if (!(oWrite_param && oWrite_param.appl_type && oWrite_param.appl_name &&
        oWrite_param.command !== null && oWrite_param.value !== null)) {
        return callback('fAnalysis_set_status: message is not complete: ' + sMessage_str);
    }

    logger.log('debug', 'Step 2');
    //Step 2: Find oAppl_obj, the information of the target appliance
    if (!oModbus_inf_obj.zones[sTarget_zoneId]) {
        return callback('fAnalysis_set_status: Zone ' + sTarget_zoneId + ' not exist in oModbus_inf_obj');
    } else {
        async.filter(oModbus_inf_obj.zones[sTarget_zoneId].appliances, function (oAppl_obj, callback_1) {
            var found = oAppl_obj.appl_name === oWrite_param.appl_name && oAppl_obj.appl_type === oWrite_param.appl_type;
            logger.log('debug', [oAppl_obj.appl_name, oWrite_param.appl_name, oAppl_obj.appl_type, oWrite_param.appl_type, found]);
            return callback_1(null, found);
        }, function (err, results) {
            if (err) { return callback(err); }
            //logger.log('debug', JSON.stringify(results));
            //else if (results.length < 1) {
            else if (!results) {
                logger.log('debug', 'catch 0');
                return callback("fAnalysis_set_status: Appliance not found in this PLC. It is normal when multiple PLCs are assigned to a single zone.");
            } else if (results.length < 1) {
                logger.log('debug', 'catch 1');
                return callback("fAnalysis_set_status: Appliance not found in this PLC. It is normal when multiple PLCs are assigned to a single zone.");
            } else {
                //logger.log('info', '');
                //Suppose only 1 is found. So return the first found.
                var oAppl_obj = results[0];

                logger.log('debug', 'Step 3');

                //Step 3: Find oField_obj, the information of the command field
                var oField_obj;
                for (let i1 = 0; i1 < oConfig.type.length; i1++) {
                    if (oConfig.type[i1].$.id === oWrite_param.appl_type) {
                        for (let i2 = 0; i2 < oConfig.type[i1].command.length; i2++) {
                            if (oConfig.type[i1].command[i2].$.type === "write") {
                                for (let i3 = 0; i3 < oConfig.type[i1].command[i2].field.length; i3++) {
                                    if (oConfig.type[i1].command[i2].field[i3].$.id === oWrite_param.command) {
                                        oField_obj = oConfig.type[i1].command[i2].field[i3].$;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
                if (!oField_obj) {
                    return callback('fAnalysis_set_status: Command "' + oWrite_param.command +
                        '" not supported for the type "' + oWrite_param.appl_type + '" yet');
                }
                if (!oField_obj.offset) {
                    return callback('fAnalysis_set_status: oConfig.type[i1].command[i2].field[i3].$.offset is missing!');
                }
                //logger.log('info', JSON.stringify(oField_obj)); process.exit(0);

                logger.log('debug', 'Step 4');

                //Step 4: Modify the input value if required
                var nInput_value;
                if (oField_obj.type === "Integer") { nInput_value = parseInt(oWrite_param.value); }
                else if (oField_obj.type === "Integer/10") { nInput_value = parseInt(oWrite_param.value) / 10; }
                else if (oField_obj.type === "Integer/100") { nInput_value = parseInt(oWrite_param.value) / 100; }
                if (!(nInput_value >= 0)) {
                    return callback('fAnalysis_set_status: Input value not valid. type:"' + oField_obj.type +
                        '", value: "' + oWrite_param.value + '"');
                }

                logger.log('debug', 'Step 5');

                //Step 5: Preparing write objects to return (Coil)
                var oWrite_obj = null;
                var oWrite_obj2 = null;
                var iRegister_base = oAppl_obj.appl_index;
                var iRegister_offset = oField_obj.offset;
                var iTarget_register = parseInt(iRegister_base) + parseInt(iRegister_offset);
                oWrite_obj = {
                    client_id: oModbus_inf_obj.client_id,
                    sCommand: 'write',
                    target_register: iTarget_register,
                    target_value: nInput_value,
                    tid: new Date().getTime()
                };
                if (iTarget_register < 128) { oWrite_obj.option = "coil"; } //Exceptional case

                //Step 5.1: Preparing write objects to return (Register)
                if (oWrite_param.appl_type === 'Thermo') {
                    var iTarget_register2 = parseInt(iRegister_base) + parseInt(iRegister_offset) + 3;
                    oWrite_obj2 = {
                        client_id: oModbus_inf_obj.client_id,
                        sCommand: 'write',
                        target_register: iTarget_register2,
                        target_value: 1,
                        tid: new Date().getTime()
                    };
                }

                return callback(null, oWrite_obj, oWrite_obj2);
            }
        });
    }
}

//oWrite_param: from "mqtt_client"
//foundSchedule: DB Schema "Schedule_dev"
//oWrite_param: {
//  command: "SET_SCHEDULER",
//  client_id: foundSchedule.client_id,
//  sch_index: foundSchedule.dev_start_addr,
//  value: iSlot_val
//};
function fAnalysis_set_schedule(logger, client_id, oWrite_param, callback) {
    //Step 1.6: Check if PLC is still given
    if (!client_id) { return callback('fAnalysis_set_status: client_id not found from oModbus_inf.'); }
    //if (!oWrite_param.client_id) { return callback('fAnalysis_set_status: client_id not found in MQTT message. Please check message.'); }
    //Step 1.7: Check if this PLC handle the zone's schedule. Now PLC take cares of multiple zones.
    //if (client_id != oWrite_param.client_id) { return callback('fAnalysis_set_status: client_id not match. It is normal when multiple PLCs are assigned into a single zone.'); }

    //Step 1.9: Fire command
    var oWrite_obj = {
        sCommand: 'write_sch',
        client_id: client_id,
        target_register: oWrite_param.sch_index,
        target_value: oWrite_param.value,
        tid: new Date().getTime()
    };
    return callback(null, oWrite_obj);
}


var exports = module.exports = {
    build_plc_based_inf: build_plc_based_inf,
    fAnalysis_zone_status: fAnalysis_zone_status,
    fAnalysis_set_status: fAnalysis_set_status
};