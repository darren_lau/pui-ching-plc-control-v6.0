"use strict";
/**
 *  Database connection's mechanism goes here.
 *  Since we are using custom schemas based from noSQL server, connect the models here also.
 *  Author: Lau Tsz Hei Darren
 **/

// Server config
var server_config = require('./server_config.js'); //Custom settings

// Database connection
var mongoose = require('mongoose');

var model = module.exports;

var oMongoose_reconnect_inerval;
var _iMongoose_reconnect_timeout_amount = server_config._iMONGOOSE_RECONNECT_TIMEOUT;

var onExit = false;

model.mongoose = {};

model.init = function (logger, cb) {

    // Set up connection
    mongoose.connect(server_config.mongoURI, { useMongoClient: true });

    mongoose.Promise = global.Promise;

    var db = mongoose.connection;
    model.mongoose = mongoose; //To access this connection

    /**
     * Event emitters of mongoose connection
     */
    mongoose.connection.on('connecting', () => {
        logger.log('debug', "mongoose.on('connecting')");
    });

    mongoose.connection.on('connected', () => {
        logger.log('debug', "Mongoose is connected to DB server");
        if (oMongoose_reconnect_inerval) { clearTimeout(oMongoose_reconnect_inerval); }
        //logger.log('debug', mongoose.connection.readyState);
    });

    mongoose.connection.on('open', () => {
        logger.log('debug', "mongoose.on('open')");
        cb();
    });

    mongoose.connection.on('disconnecting', () => {
        logger.log('debug', "mongoose.on('disconnecting')");
    });

    mongoose.connection.on('disconnected', () => {
        logger.log('info', "Mongoose is disconnected from DB server");
    });

    mongoose.connection.on('close', () => {
        logger.log('debug', "mongoose.on('close')");
        if (!onExit) {
            cb_mongoose_connectWithRetry(logger);
        }
    });

    mongoose.connection.on('reconnected', () => {
        logger.log('debug', "mongoose.on('reconnected')");
        if (oMongoose_reconnect_inerval) { clearTimeout(oMongoose_reconnect_inerval); }
        //logger.log('debug', mongoose.connection.readyState);
    });

    mongoose.connection.on('error', (error) => {
        logger.log('error', "mongoose.on('error') : " + error);
        if (!onExit) {
            cb(error);
        }
        //mongoose.disconnect();
    });

    mongoose.connection.on('fullsetup', () => {
        logger.log('debug', "mongoose.on('fullsetup')");
    });

    // Refresh state 
    model.db = db;

    //Mongoose Schemas
    model.Device = require('../../schema/mongoose/Device.js')(db);
    model.Device_Status = require('../../schema/mongoose/Device_Status.js')(db);
    model.Device_Command = require('../../schema/mongoose/Device_Command.js')(db);
    model.Schedule = require('../../schema/mongoose/Schedule_dev.js')(db);
    model.Schedule_def = require('../../schema/mongoose/Schedule_default.js')(db);
    model.User = require('../../schema/mongoose/User.js')(db);
    model.error_issue = require('../../schema/mongoose/error_issue.js')(db);
    model.onExit = function () { onExit = true; };
    model.mongoose = mongoose;
};


var cb_mongoose_connectWithRetry = function (logger) {
    return mongoose.connect(server_config.mongoURI, (err) => {
        if (err) {
            logger.log('error', "Error when connecting MongoDB: " + err);
            logger.log('debug', 'Reconnecting MongoDB in ' + _iMongoose_reconnect_timeout_amount / 1000 + ' sec...');
            if (oMongoose_reconnect_inerval) { clearTimeout(oMongoose_reconnect_inerval); }
            oMongoose_reconnect_inerval = setTimeout(cb_mongoose_connectWithRetry, _iMongoose_reconnect_timeout_amount, logger);
        }
    });
};