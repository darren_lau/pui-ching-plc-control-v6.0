"use strict";
var moment = require('moment');
var async = require('async');
var crypto = require('crypto');

var oDataBase = require('./db_connection.js');
var loadXML = require('./load_xml.js');
var mqtt_client = require('./mqtt_client.js');
var server_config = require('./server_config.js');

//Global variables
var OneDayInLong = 1000 * 60 * 60 * 24;
var OBJ_holidayDates = {};
var OBJ_defaultSchedules = {};

var logger = {};

var init = function (logger_in) {
    logger = logger_in;
    loadXML.loadForExpressHost(logger, function (err, obj1, obj2) {
        if (err) { logger.log('error', err); }
        else {
            OBJ_holidayDates = obj1;
            OBJ_defaultSchedules = obj2;
            logger.log('info', 'XML files are loaded successfully.');
        }
    });
};

var getAllSchedule = function (req, res) {
    var siteId = req.query.siteId;
    var cb = req.query.callback;
    if (siteId === 'ETAG') {
        if (cb !== null) {
            oDataBase.Schedule.find({}, function (err, foundSchedules) {
                if (err) return res.status(404).send("Error: No schedules found at all.");
                if (foundSchedules.length === 0) return res.status(404).send("Error, schedules not found");
                res.status(200).send(cb + "(" + JSON.stringify(foundSchedules) + ");");
            });
        }
        else {
            oDataBase.Schedule.find({}, function (err, foundSchedules) {
                if (err) return res.status(404).send("Error: No schedules found at all.");
                if (foundSchedules.length === 0) return res.status(404).send("Error, schedules not found");
                res.status(200).send(foundSchedules);
            });
        }
    }
    else {
        res.send("Wrong site ID");
    }
};

var getTimeSlots = function (req, res) {
    var siteId = req.query.siteId;
    var tag_id = req.query.tag_id;
    var zoneId = req.query.zoneId;
    if (siteId === 'ETAG') {
        oDataBase.Schedule.find({ zoneId: zoneId }, function (err, foundSchedules) {
            if (err) return res.status(404).send("Error: No schedules found in this zone.");
            if (foundSchedules.length === 0) return res.status(404).send("Error, schedules not found for this zone");
            var finalResult = [];
            for (var x = 0; x < foundSchedules.length; x++) {
                var allTs = foundSchedules[x].time_slots;
                var activeTs = [];
                for (var y = 0; y < allTs.length; y++) {
                    if (allTs[y].slot_value === "1") {
                        activeTs.push(allTs[y]);
                    }
                }
                if (activeTs.length > 0) {
                    finalResult.push({
                        siteId: foundSchedules[x].siteId,
                        zoneId: foundSchedules[x].zoneId,
                        date_index: foundSchedules[x].date_index,
                        dev_start_addr: foundSchedules[x].dev_start_addr,
                        slot_length: foundSchedules[x].slot_length,
                        mod_date_value: foundSchedules[x].mod_date_value,
                        time_slots: activeTs
                    });
                }
            }
            res.status(200).send(finalResult);
        });
    }
    else {
        res.send("Wrong site ID");
    }
};

var getSchedule_console = function (req, res) {
    oDataBase.Schedule.find(req.query, function (err, docs) {
        if (err) { logger.log('error', err); res.status(500).send(err); return; }
        else if (docs.length === 0) { res.status(200).send("[]"); return; }
        else { res.status(200).send(docs); }
    });
};

function getDateType(dateToCompare, default_id) {
    //Set custom rule here. DB stores no rule but default_id
    //RuleA: Regular/ Holiday
    //RuleB: NeedCardAllDay
    var ret = null;
    var hols = OBJ_holidayDates.holidays.holiday;
    var date = new Date();
    var x = 0;
    //logger.log('debug', "getDateType(" + dateToCompare + "," + default_id + ")");
    //logger.log('debug', JSON.stringify(hols[0]));

    if (!default_id) {
        logger.log('error', "getDateType: default_id missing");
        return "NeedCardAllDay";
    }

    if (default_id === "ruleB") {
        //ruleB
        return "NeedCardAllDay";
    } else if (default_id === 'ruleA') {
        //ruleA
        for (x = 0; x < hols.length; x++) {
            //logger.log('debug', new Date(hols[x]._) + " vs " + new Date(dateToCompare));
            if (new Date(hols[x]._).getTime() === new Date(dateToCompare).getTime()) {
                return hols[x].$.remark; //ret = "Holiday";
            }
        }
        if (!ret) {
            date = new Date(dateToCompare);
            if (date.getDay() === 0 || date.getDay() === 6) {
                ret = "Holiday";
            } else {
                ret = "Regular";
            }
        }
        return ret;
    } else if (default_id === 'ruleC') {
        //ruleA
        for (x = 0; x < hols.length; x++) {
            //logger.log('debug', new Date(hols[x]._) + " vs " + new Date(dateToCompare));
            if (new Date(hols[x]._).getTime() === new Date(dateToCompare).getTime()) {
                return hols[x].$.remark; //ret = "Holiday";
            }
        }
        if (!ret) {
            date = new Date(dateToCompare);
            if (date.getDay() === 0) {
                ret = "NeedCardAllDay";
            } else {
                ret = "Regular";
            }
        }
        return ret;
    } else {
        logger.log('error', "getDateType: default_id not supported: " + default_id);
        return "NeedCardAllDay";
    }

}

function getTimeSlotArray(dayType) {
    //if (dayType != "Holiday" && dayType != "Regular") {
    //    return null;
    //}

    //Return null if dayType is not valid
    //logger.log('debug', dayType);
    if (!dayType) { return null; }
    //If it is a public holiday (!"Regular"), save the name, and change it to "Holiday" for binding  the XML values
    var ret = null;
    if (dayType !== "Regular" && dayType !== "Holiday" && dayType !== "NeedCardAllDay") {
        ret = dayType;
        dayType = "Holiday";
    }
    //var parser1 = new xml2js.Parser();
    var returnArray = [];
    let result = OBJ_defaultSchedules;
    for (var i = 0; i < result.schedule.index.length; i++) {
        var type = result.schedule.index[i]['$'].id;
        if (type === dayType) {
            var time_slots_array = result.schedule.index[i].time_slots[0].slot;
            time_slots_array.sort(function (a, b) {
                return a['$'].slotid - b['$'].slotid;
            });
            //console.log(time_slots_array.length);
            //logger.log('debug', JSON.stringify(time_slots_array));
            for (var y = 0; y < time_slots_array.length; y++) {
                returnArray[y] = {
                    slot_value: time_slots_array[y]['$'].slot_value,
                    remark: ret ? ret : time_slots_array[y]['$'].remar
                };
            }
            //if (i === 0)
            //    logger.log('debug', JSON.stringify(returnArray));
        }
    }
    return returnArray;
}

var getSchedule_scheduler = function (req, res) {
    var full_date1 = req.query.mod_date_value;
    var full_date = null;
    var qry = req.query;

    if (full_date1 !== null) {
        full_date = full_date1.substring(0, 10);
        delete qry.mod_date_value;
    }
    else {
        return res.send("Please provide a mod_date_value");
    }

    Device.find({ zoneId: qry.zoneId }, function (err, docs) {
        if (err) { logger.log('error', err); return res.status(500).send(err); }
        else if (docs.length === 0) {
            //logger.log('debug', "Device not found. zoneId: " + qry.zoneId);
            //return res.status(400).send("Device not found. zoneId: " + qry.zoneId);
            res.status(200).send("[]"); return;
        } else {
            if (!docs[0].default_id) {
                logger.log('error', "getDateType: default_id not found in Device " + qry.zoneId);
                return res.status(500).send("getDateType: default_id not found in Device " + qry.zoneId);
            } else {
                var default_id = docs[0].default_id.slice(0);
                oDataBase.Schedule.find(qry, function (err, docs) {
                    if (err) { logger.log('error', err); res.status(500).send(err); return; }
                    else if (docs.length === 0) { res.status(200).send("[]"); return; }
                    else {
                        var foundSchedules = docs;
                        if (foundSchedules.length > 0) {
                            var foundSchedule = foundSchedules[0];
                            var temp_mod_date = foundSchedule.mod_date_value.substring(0, 10);
                            if (full_date !== temp_mod_date) {
                                //logger.log('debug', "Getting from XML");
                                var resultObj = foundSchedules[0];
                                var typeOfDate = getDateType(full_date, default_id);
                                var timeSlots = getTimeSlotArray(typeOfDate);
                                if (timeSlots === null) { return res.status(500).send("Got nothing from XML"); }
                                if (timeSlots.length !== resultObj.time_slots.length) {
                                    return res.send("ERROR/something went wrong");
                                }

                                for (var y = 0; y < resultObj.time_slots.length; y++) {
                                    resultObj.time_slots[y].slot_value = timeSlots[y].slot_value;
                                    resultObj.time_slots[y].remark = timeSlots[y].remark;
                                }
                                var resultObj1 = [];
                                resultObj1.push(resultObj);
                                return res.send(resultObj1);
                            }
                            else {
                                //logger.log('debug', foundSchedule);
                                return res.status(200).send(foundSchedules);
                            }
                        }
                        else {
                            res.send("No schedule found");
                        }
                        //res.status(200).send(docs); 
                    }
                });
            }
        }
    });
};

var getSchedule_scheduler_month = function (req, res) {
    var full_date1 = req.query.mod_date_value;
    var full_date = null;
    var qry = req.query;

    if (full_date1 !== null) {
        full_date = full_date1.substring(0, 10);
        delete qry.mod_date_value;
    } else {
        return res.send("Please provide a mod_date_value");
    }

    oDataBase.Device.find({ zoneId: qry.zoneId }, function (err, docs) {
        if (err) { logger.log('error', err); return res.status(500).send(err); }
        else if (docs.length === 0) {
            //logger.log('debug', "Device not found. zoneId: " + qry.zoneId);
            //return res.status(400).send("Device not found. zoneId: " + qry.zoneId);
            res.status(200).send("[]"); return;
        } else {
            if (!docs[0].default_id) {
                logger.log('error', "getDateType: default_id not found in Device " + qry.zoneId);
                return res.status(500).send("getDateType: default_id not found in Device " + qry.zoneId);
            } else {
                var default_id = docs[0].default_id.slice(0);
                //Note that returning docs has been sorted
                oDataBase.Schedule.find(qry).sort('date_index').exec(function (err, docs) {
                    if (err) { logger.log('error', err); res.status(500).send(err); return; }
                    else if (docs.length === 0) { logger.log('debug', '200 NOTHING'); res.status(200).send("[]"); return; }
                    else {
                        var target_date = new Date();
                        var final_arr = [];

                        var foundSchedules = docs;

                        //logger.log('info', 'Schedule lengh = ' + foundSchedules.length);
                        for (var i0 = 0; i0 < foundSchedules.length; i0++) {
                            foundSchedules[i0].time_slots.sort(function (a, b) {
                                return a.slotId - b.slotId;
                            });

                            //Build str_new from full_date
                            //var target_date = new Date(now.getTime() + OneDayInLong * i0);
                            var str_year = target_date.getFullYear();
                            var str_month = target_date.getMonth() + 1;
                            var str_date_old = target_date.getDate();
                            var str_date_new = i0 + 1;
                            str_date_old = parseInt(str_date_old) < 10 ? "0" + str_date_old : "" + str_date_old;
                            str_date_new = parseInt(str_date_new) < 10 ? "0" + str_date_new : "" + str_date_new;
                            var str_old = str_year + "-" + str_month + "-" + str_date_old;
                            var str_new = str_year + "-" + str_month + "-" + str_date_new;
                            if (new Date(str_new) < new Date(str_old)) {
                                if (str_month + 1 >= 12) { str_month = 1; str_year++; } else str_month++;
                            }
                            str_month = parseInt(str_month) < 10 ? "0" + str_month : "" + str_month;
                            str_new = str_year + "-" + str_month + "-" + str_date_new;


                            if (new Date(str_new).getMonth() + 1 !== parseInt(str_new.split('-')[1])) {
                                //logger.log('debug', "Skipped: " + str_new);
                                continue;
                            }

                            //full_date = full_date.slice(0, 8) + str_date;
                            var foundSchedule = foundSchedules[i0];
                            var temp_mod_date = foundSchedule.mod_date_value.substring(0, 10);
                            //logger.log('info', 'str_new = ' + str_new + ' temp_mod_date = ' + temp_mod_date);
                            //logger.log('info', foundSchedule.date_index);
                            if (str_new !== temp_mod_date) {
                                //logger.log('debug', "Getting from XML " + str_new);
                                var resultObj = foundSchedules[i0];
                                resultObj.mod_date_value = str_new;
                                resultObj.time_slots.sort(function (a, b) {
                                    return a.slotId - b.slotId;
                                });
                                var typeOfDate = getDateType(str_new, default_id);
                                var timeSlots = getTimeSlotArray(typeOfDate);
                                //timeSlots.sort(function(a,b){
                                //    return a.slotId - b.slotId;
                                //});
                                if (timeSlots === null) { return res.status(500).send("Got nothing from XML"); }
                                if (timeSlots.length !== resultObj.time_slots.length) {
                                    logger.log('error', JSON.stringify(timeSlots.length));
                                    logger.log('error', JSON.stringify(resultObj.time_slots.length));
                                    return res.status(500).send("ERROR: something went wrong");
                                }

                                for (var y = 0; y < resultObj.time_slots.length; y++) {
                                    resultObj.time_slots[y].slot_value = timeSlots[y].slot_value;
                                    resultObj.time_slots[y].remark = timeSlots[y].remark;
                                }
                                final_arr.push(resultObj);
                            } else {
                                //logger.log('debug', "Getting from DB");
                                final_arr.push(foundSchedule);
                            }
                        }
                        return res.status(200).send(final_arr);
                    }
                });
            }
        }
    });
};

var oOneMonthLaterDate = function () {
    var newMonth = new Date(new Date(new Date().setMonth(new Date().getMonth() + 1)).setHours(0, 0, 0, 0));
    //$log.log("Call newMonth = " + newMonth);
    return newMonth;
};


//Same as in web-console. Checking is done in server side.
var oNextTimeSlotInDate = function () {
    var _iSI = server_config._iSchedulingInterval;

    //var now = new Date(new Date().getTime() + 1000 * 60 * 30); //30 mins later
    var now = new Date(new Date().getTime() + 1000 * 60 * _iSI); //30 mins later
    //now.setHours(now.getHours(), Math.floor((now.getMinutes() / 30)) * 30, 0, 0); //Floor function
    now.setHours(now.getHours(), Math.floor(now.getMinutes() / _iSI) * _iSI, 0, 0); //Floor function
    return new Date(now);
};

var fConvertDateToSlots = function (start, end) {
    var _iSI = server_config._iSchedulingInterval;

    //Param start and end are day objects. 
    //Return {startX, endX} for time_slots index.
    if (start instanceof Date && end instanceof Date) {
        //Change for 15 or 30 minutes interval
        //return {
        //    startX: (start.getHours() - 7) * (60 / 30) + Math.floor(start.getMinutes() / 30),
        //    endX: (end.getHours() - 7) * (60 / 30) + Math.floor(end.getMinutes() / 30) - 1
        //};
        return {
            startX: (start.getHours() - 7) * (60 / _iSI) + Math.floor(start.getMinutes() / _iSI),
            endX: (end.getHours() - 7) * (60 / _iSI) + Math.floor(end.getMinutes() / _iSI) - 1
        };
    } else {
        return null;
    }
};

//Check if the time_slot can be changed
var fCheckCanChangeForIndex = function (date_index, time_slot_index) {
    var nextSlot = oNextTimeSlotInDate();
    var nextIndex = fConvertDateToSlots(nextSlot, nextSlot).startX;
    //If not "Today" or later than the "nextSlot", return true (can change)
    return date_index !== nextSlot.getDate() || time_slot_index >= nextIndex;
};

//var _itime_slot_length = (23 - 7) * (60 / 30); //07:00 - 23:00 with 30-minute interval
var _itime_slot_length = (23 - 7) * (60 / server_config._iSchedulingInterval); //07:00 - 23:00 with 15-minute interval

var editOneDaySchedule = function (req, res) {
    var siteId = req.body['siteId'];
    var zoneId = req.body['zoneId'];
    var date_index = req.body['date_index'];
    var full_date = req.body['mod_date_value'];
    var ts = req.body['time_slots'];
    var ts_parsed;
    try { ts_parsed = JSON.parse(ts); } catch (e) { return res.status(400).send("Error: Post body is not JSON."); }

    //logger.log('debug', JSON.stringify(date_index));
    //logger.log('debug', JSON.stringify(full_date));
    //logger.log('debug', ts_parsed[ts_parsed.length - 1]);
    if (siteId !== 'PuiChing_P') { res.status(400).send("Wrong site ID"); return; }
    else {
        //logger.log('info', "find schedule");
        oDataBase.Schedule.find({ siteId: siteId, zoneId: zoneId, date_index: date_index }, function (err, foundSchedules) {
            if (err) { logger.log('error', err); res.status(500).send(err); return; }
            if (foundSchedules.length === 0) return res.status(404).send("Error, schedules not found");
            if (foundSchedules.length > 1) return res.status(404).send("Error, search parameter returns more than 1 schedules");
            //logger.log('debug', "ping_1");
            var foundSchedule = foundSchedules[0];
            if (_itime_slot_length !== ts_parsed.length) { logger.log('error', _itime_slot_length + " - " + ts_parsed.length); return res.status(500).send("Error, invalid number of time slots"); }
            //logger.log('debug', foundSchedule.mod_date_value + "-" + full_date);
            if (foundSchedule.mod_date_value !== full_date) {
                logger.log('debug', foundSchedule.mod_date_value + " is not equal to " + full_date + ", making them equal.");
                foundSchedule.mod_date_value = full_date;
            }
            foundSchedule.time_slots.sort(function (a, b) {
                return a.slotId - b.slotId;
            });
            for (var i = 0; i < ts_parsed.length; i++) {
                if (fCheckCanChangeForIndex(date_index, i)) {
                    //Trick: Making uue of i++
                    if (i >= foundSchedule.time_slots.length) {
                        foundSchedule.time_slots.push({
                            slot_value: ts_parsed[i].slot_value,
                            remark: ts_parsed[i].remark !== null ? ts_parsed[i].remark : ""
                        });
                        //logger.log('debug', "ADD: " + date_index + ', ' + i + ' value = ' + ts_parsed[i].slot_value);
                    } else {
                        foundSchedule.time_slots[i].slot_value = ts_parsed[i].slot_value;
                        foundSchedule.time_slots[i].remark = ts_parsed[i].remark !== null ? ts_parsed[i].remark : "";
                        //logger.log('debug', "EDIT: " + date_index + ', ' + i + ' value = ' + ts_parsed[i].slot_value);
                    }
                } else {
                    //logger.log('debug', "Locked: " + date_index + ', ' + i);
                }
            }
            foundSchedule.save(function (err) {
                if (err) { logger.log('error', err); res.status(500).send(err); return; }
                else {
                    //logger.log('debug', 'DB updated');
                    res.status(200).send("DB updated"); return;

                    //DB part is updated
                    //Update PLC secretly
                    //PushToPLC(foundSchedule, function (err) {
                    //    if (!err) {
                    //        logger.log('debug', "DB updated and pushed to PLC.");
                    //        res.status(200).send("DB updated and pushed to PLC."); return;
                    //    }
                    //    else {
                    //        logger.log('debug', "DB updated but cannot push to PLC.");
                    //        res.status(200).send("DB updated but cannot push to PLC."); return;
                    //    }
                    //});

                }
            });
        });
    }
};


//Suppoed the key is not made like that
var key_match = function (m, k) {
    var p = "<code>ETAG</code><!--push_plc-->";
    var u = crypto.pbkdf2Sync(m, p, 1024, 64).toString('hex');
    var n = crypto.pbkdf2Sync(u, p, 1024, 64).toString('hex');
    return k === n;
};

//Async hell
//Note that it only return callback(null) as it must loop through all the zones
var refresh_plc_step2 = function (oIntermediate_arr, callback) {
    var mod_data = oIntermediate_arr.mod_data;
    var date_str = oIntermediate_arr.date_str;
    if (mod_data.mod_date_value !== date_str) {
        logger.log('debug', "refresh_plc: Data outdated - Getting from XML");
        oDataBase.Device.find({ zoneId: mod_data.zoneId }, function (err, docs) {
            if (err) {
                logger.log('error', err); return callback(null);
            } else if (docs.length === 0) {
                logger.log('error', "Device not found. zoneId: " + mod_data.zoneId); return callback(null);
            } else if (!docs[0].default_id) {
                logger.log('error', "getDateType: default_id not found in Device " + mod_data.zoneId); return callback(null);
            } else {
                var typeOfDate = getDateType(date_str, docs[0].default_id);
                var timeSlots = getTimeSlotArray(typeOfDate);
                if (timeSlots === null) { return callback("Error: Got nothing from XML"); }
                if (timeSlots.length !== mod_data.time_slots.length) { return callback("Error: Something went wrong (" + timeSlots.length + "," + mod_data.time_slots.length + ")"); }
                for (var y = 0; y < mod_data.time_slots.length; y++) {
                    mod_data.time_slots[y].slot_value = parseInt(timeSlots[y].slot_value);
                    mod_data.time_slots[y].remark = parseInt(timeSlots[y].slot_value) ? timeSlots[y].remark : "";
                }
                mod_data.mod_date_value = date_str;

                var query_obj = {
                    zoneId: mod_data.zoneId,
                    date_index: mod_data.date_index
                };
                var update_field = {
                    time_slots: mod_data.time_slots,
                    mod_date_value: mod_data.mod_date_value
                };

                oDataBase.Schedule.update(query_obj, update_field, function (err) {
                    if (err) {
                        logger.log('error', err); return callback(null);
                    } else {
                        logger.log('debug', "Schedule replaced: " + mod_data.zoneId);
                        mqtt_client.PushToPLC(mod_data, function (err) {
                            if (err) { logger.log('error', err); }
                            return callback(null);
                        });
                    }
                });
            }
        });
    } else {
        logger.log('debug', "refresh_plc: Data updated already: " + mod_data.zoneId);
        mqtt_client.PushToPLC(mod_data, function (err) {
            if (err) { logger.log('error', err); }
            callback(null);
        });
    }
};

var refresh_plc = function (req, res) {
    //Step 0: Auth
    if (!(req.body && req.body.message && req.body.key && req.body.date_str)) {
        res.status(400).send("Request body not complete."); return;
    } else if (!key_match(req.body.message, req.body.key)) {
        res.status(400).send("Request body not correct."); return;
    } else {
        //Step 1: Schedule (with Zone)
        //res.status(200).send("KEY_PASS");
        //var hour = 1000 * 60 * 60; //Assume it's called before 04:00
        //var new_date = new Date((new Date()).getTime() + hour * 20);
        var date_index = parseInt(req.body.date_str.split('-')[2]);//new_date.getDate();
        logger.log('debug', '/refresh_plc({date_index:' + date_index + ', mod_date_value:' + req.body.date_str + '})');
        oDataBase.Schedule.find({ date_index: date_index }, function (err, foundSchedules) {
            if (err) { logger.log('error', err); res.status(500).send(err); return; }
            else {
                var oIntermediate_arr = [];
                for (var i = 0; i < foundSchedules.length; i++) {
                    oIntermediate_arr.push({
                        date_str: req.body.date_str,
                        mod_data: foundSchedules[i].toObject()
                    });
                }
                //Step 2: For each zone, get mqtt_tag, send it with Schedule
                async.each(oIntermediate_arr, refresh_plc_step2, function (err) {
                    if (err) { logger.log('error', err); res.status(500).send(err); return; }
                    else { res.status(200).send("Refreshing of all PLCs are completed."); return; }
                });
            }
        });
    }
};

var refresh_plc_console = function (req, res) {
    //Step 0: Auth
    if (!(req.body && req.body.zoneId && req.body.just && req.body.just === "doit")) {
        res.status(400).send("Request body not complete."); return;
    } else {
        //Step 1: Schedule (with Zone)
        var date_str = moment().toISOString().substring(0, 10);
        var date_index = parseInt(date_str.split('-')[2]);
        logger.log('debug', '/refresh_plc({date_index:' + date_index + ', mod_date_value:' + date_str + ', zoneId: ' + req.body.zoneId + '})');
        oDataBase.Schedule.find({ zoneId: req.body.zoneId, date_index: date_index }, function (err, foundSchedules) {
            if (err) { logger.log('error', err); res.status(500).send(err); return; }
            else {
                var oIntermediate_arr = [];
                for (var i = 0; i < foundSchedules.length; i++) {
                    oIntermediate_arr.push({
                        date_str: date_str,
                        mod_data: foundSchedules[i].toObject()
                    });
                }
                //Step 2: For each zone, get mqtt_tag, send it with Schedule
                async.each(oIntermediate_arr, refresh_plc_step2, function (err) {
                    if (err) { logger.log('error', err); res.status(500).send(err); return; }
                    else { res.status(200).send("Refreshing of all PLCs are completed."); return; }
                });
            }
        });
    }
};

var exports = module.exports = {
    getAllSchedule: getAllSchedule,
    getTimeSlots: getTimeSlots,
    getSchedule_console: getSchedule_console,
    getDateType: getDateType,
    getTimeSlotArray: getTimeSlotArray,
    getSchedule_scheduler: getSchedule_scheduler,
    getSchedule_scheduler_month: getSchedule_scheduler_month,
    oOneMonthLaterDate: oOneMonthLaterDate,
    oNextTimeSlotInDate: oNextTimeSlotInDate,
    fConvertDateToSlots: fConvertDateToSlots,
    fCheckCanChangeForIndex: fCheckCanChangeForIndex,
    editOneDaySchedule: editOneDaySchedule,
    refresh_plc: refresh_plc,
    refresh_plc_console: refresh_plc_console,
    init: init
};
