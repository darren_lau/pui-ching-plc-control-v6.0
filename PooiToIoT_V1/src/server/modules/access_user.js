"use strict";
var oDataBase = require('./db_connection');
var logger = {};

var init = function (l) { logger = l; };

var addUser = function (req, res) {
    if (!req.body.username || !req.body.hash || !req.body.usertype) {
        return res.status(400).send('Request body not complete.');
    }
    var obj_query = { username: req.body.username };
    oDataBase.User.find(obj_query, function (err, docs) {
        if (docs.length > 0) {
            res.status(400).send('User already exists. Please use other user name.'); return;
        } else {
            var user = new oDataBase.User();
            user.username = req.body.username;
            user.usertype = req.body.usertype;
            user.user_inf_name = req.body.user_inf_name;
            user.setPassword(req.body.hash);
            user.save(function (err) {
                if (err) { logger.log('error', err); res.status(500).send(err); return; }
                else { return res.status(200).send(user); }
            });
        }
    });
};

var getUser = function (req, res) {
    oDataBase.User.find(req.query, function (err, docs) {
        if (err) { logger.log('error', err); res.status(500).send(err); return; }
        else if (docs.length === 0) { res.status(200).send("[]"); return; }
        else { res.status(200).send(docs); }
    });
};

var editUser = function (req, res) {
    if (!req.body.username || !req.body.hash || !req.body.usertype) {
        return res.status(400).send('Request body not complete.');
    }
    var obj_query = { _id: req.body._id };
    oDataBase.User.find(obj_query, function (err, docs) {
        if (err) { logger.log('error', err); res.status(500).send(err); return; }
        else if (docs.length === 0) { return res.status(400).send('Request body not complete.'); }
        else {
            var user = docs[0];
            user.username = req.body.username;
            user.usertype = req.body.usertype;
            user.user_inf_name = req.body.user_inf_name;
            if (user.hash !== req.body.hash) { user.setPassword(req.body.hash); }
            user.save(function (err) {
                if (err) { logger.log('error', err); res.status(500).send(err); return; }
                else { return res.status(200).send(user); }
            });
        }
    });
};

var deleteUser = function (req, res) {
    if (!req.body.username) { return res.status(400).send('Request body not complete.'); }
    var obj_query = { _id: req.body._id };
    oDataBase.User.find(obj_query, function (err, docs) {
        if (err) { logger.log('error', err); res.status(500).send(err); return; }
        else if (docs.length === 0) { return res.status(400).send('User not found.'); }
        else {
            docs[0].remove(function (err) {
                if (err) { logger.log('error', err); res.status(500).send(err); return; }
                else { return res.status(200).send('User deleted.'); }
            });
        }
    });
};

var exports = module.exports = {
    addUser: addUser,
    getUser: getUser,
    editUser: editUser,
    deleteUser: deleteUser,
    init: init
};
