var exports = module.exports = {
    resolve: function (o) {
        //o = req.body
        let query_obj = {}; //WHERE
        let criteria = {}; //SELECT 
        let options = {}; //(remaining stuffs)

        options.skip = o.skip;
        options.limit = o.pageSize;
        if (o.sort) {
            options.sort = {};
            for (let i = 0; i < o.sort.length; i++) {
                options.sort[o.sort[i].field] = o.sort[i].dir;
            }
        }

        if (o.filter) {
            if (!o.filter.logic) {
                o.filter.logic = "and";
            }

            if (o.filter.logic === "and" || o.filter.logic === "or") {
                query_obj["$" + o.filter.logic] = [];
            } else {
                throw "[kGcmd2MongooseQuery] ERROR: " + o.filter.logic + " is not supported.";
            }

            for (let i = 0; i < o.filter.filters.length; i++) {
                let k = o.filter.filters[i].field;
                let v = o.filter.filters[i].value;
                let op = o.filter.filters[i].operator;
                let t_obj = {};

                if (!v) {
                    console.log(`[kGcmd2MongooseQuery] WARN: ${k} contains no value. Skipped.`); continue;
                }

                //Specially built for MongoDB < 3.0.0: Never use $eq as it is not supported
                let op_map = {
                    "eq": v,
                    "neq": { "$ne": v },
                    "contains": { "$in": [new RegExp(v)] },
                    "doesnotcontain": { "$nin": [new RegExp(v)] },
                    "startswith": { "$in": [new RegExp("^" + v)] },
                    "endswith": { "$in": [new RegExp(v + "$")] },
                    "isnull": null,
                    "isnotnull": { "$ne": null },
                    "isempty": { "$exists": false },
                    "isnotempty": { "$exists": true },
                    "gt": { "$gt": v },
                    "lt": { "$lt": v },
                    "gte": { "$gte": v },
                    "lte": { "$lte": v }
                };

                if (typeof op_map[op] === "undefined") throw `[kGcmd2MongooseQuery] ERROR: ${op} is not supported.`;
                t_obj[k] = op_map[op];

                query_obj["$" + o.filter.logic].push(t_obj);
            }
        }

        var result = { query_obj: query_obj, criteria: criteria, options: options };
        //console.log(JSON.stringify(result, null, 4));
        return result;
    }
};