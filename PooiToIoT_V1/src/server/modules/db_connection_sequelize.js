/**
 *  Database connection's mechanism goes here.
 *  Try to pass the MYSQL connection as a global object.
 *  EDIT: Now Sequeliz is used for a NodeJS - mySQL ORM.
 *  Author: Lau Tsz Hei Darren
 */

// Server config
var server_config = require('./server_config.js'); //Custom settings
var server_logger = require('./server_logger.js');
var Sequelize = require('sequelize');

var logger = server_logger.SERVER_LOGGING(server_config._sLOG_DIR + '/db_connection_sequelize.log', { level: 'verbose' }); //using Winston instead of Morgan
//var mysql = require('mysql');
//var fs = require('fs');

//var connection = mysql.createConnection(server_config.MYSQL_CONNECTION_PARAM);

/**
var standalone = function () {
    var query = process.argv[2];
    
    connection.query(query, function (err, rows, fields) {
        if (err) throw err;
        connection.end(function (err) {
            if (err) throw err;
            var out = {
                rows: rows,
                fields: fields
            };
            fs.writeFile('out.json', JSON.stringify(out, null, 4), function (err) {
                if (err) throw err;
                console.log('done.'); process.exit(0);
            });
        })
    });
    
    
    connection.on('error', function (err) { 
        console.log(err);
    });
     
}
**/

var model = module.exports;
var sequelize = null;

var make_sequelize = function () {
    //Set up DB Connection and middleware
    var p = server_config.MYSQL_CONNECTION_PARAM;
    logger.log('debug', JSON.stringify(p));
    //return mysql.createPool(p);
    sequelize = new Sequelize(p.database, p.user, p.password, {
        host: p.host,
        port: p.port,
        dialect: 'mysql',
        pool: {
            max: p.connectionLimit,
            min: 0,
            idle: 10000
        },
        dialectOptions: {
           socketPath: "/var/run/mysqld/mysqld.sock"
        },
        logging: function (str) {
            if (str.length > 2048) {
                str = str.substring(0, 2048);
            }
            logger.log('verbose', "[Sequelize]" + str);
        },
        define: {
            //Application wide model options
            timestamps: false, // true by default
            freezeTableName: true,
            charset: 'utf8',
            collate: 'utf8_general_ci'
        },
        timezone: '+08:00'
    });

    model.sequelize = sequelize;

    //Declare models based form the state
    model.Device_History = require('../../schema/sequelize/Device_History.js').schema(model.sequelize);

    //Link up Foreign tables
    //model.Device_Status.hasOne(model.Device, { as: "device_ref", foreignKey: 'device_id' });
    //model.Device_Status.belongsTo(model.Device, { foreignKey: 'device_id' });
    //model.Device.hasMany(model.Device_Status, { foreignKey: 'device_id' });
    
    //Filling linking parameters for APIs (This sructure is awful)
    //model.Include_Device_To_Status = [model.Device];
    //model.Include_Status_To_Device = []; //[model.Device_Status];

    //model.sequelize.sync();
};

//standalone();
model.init = function (cb) {
    //Test DB Connection
    sequelize.authenticate().then(cb).catch(cb);
};

make_sequelize();