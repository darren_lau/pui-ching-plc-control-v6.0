"use strict";
var oDataBase = require('./db_connection');
//var Device = oDataBase.Device;
var logger = {};

var init = function (l) { logger = l; };

var addZone = function (req, res) {
    //logger.log('debug', JSON.stringify(req.body));
    //Although some validation is done in UI, here will do it also
    //dI: { zone_inf, zone_ref, appl_inf, appl_ref }

    var zone_inf = req.body.zone_inf;
    if (!zone_inf) { res.status(400).send("Request body not complete."); return; }
    var new_zone = new Device({
        //Assume id is in String
        siteId: zone_inf.siteId,
        mqttclient_id: zone_inf.mqttclient_id,
        mqtt_tag: zone_inf.mqtt_tag,
        //PLC
        client_id: zone_inf.client_id,
        client_name: zone_inf.client_name,
        client_type: zone_inf.client_type,
        ip_addr: zone_inf.ip_addr,
        mac: zone_inf.mac,
        //Zone
        zoneId: zone_inf.zoneId,
        tower: zone_inf.tower,
        floor: zone_inf.floor,
        desc: zone_inf.desc,
        //Schedule
        default_id: zone_inf.default_id,
        //Appl
        appliances: []
    });

    new_zone.save(function (err) {
        if (err) { logger.log('error', err); res.status(500).send(err); return; }
        else { res.status(200).send('Zone (Device) created!'); }
    });
};

var editZone = function (req, res) {
    //logger.log('debug', JSON.stringify(req.body));
    //Although some validation is done in UI, here will do it also
    //dI: { zone_inf, zone_ref, appl_inf, appl_ref }
    //changeSchDef: Change default_id for all zones but nothing else.
    var zone_inf = req.body.zone_inf;
    var Sch_def = req.query.Sch_def;
    logger.log('info', Sch_def);
    if (!(zone_inf && zone_inf._id)) { res.status(400).send("Request body not complete."); return; }
    var zone_query = Sch_def ? { zoneId: zone_inf.zoneId } : { _id: zone_inf._id };
    var update_field = Sch_def ? {
        default_id: zone_inf.default_id
    } : {
        //Assume id is in String
        siteId: zone_inf.siteId,
        mqttclient_id: zone_inf.mqttclient_id,
        mqtt_tag: zone_inf.mqtt_tag,
        //PLC
        client_id: zone_inf.client_id,
        client_name: zone_inf.client_name,
        client_type: zone_inf.client_type,
        ip_addr: zone_inf.ip_addr,
        mac: zone_inf.mac,
        //Zone
        zoneId: zone_inf.zoneId,
        tower: zone_inf.tower,
        floor: zone_inf.floor,
        desc: zone_inf.desc,
        //Schedule
        default_id: zone_inf.default_id,
        //Appl
        appliances: zone_inf.appliances
    };
    oDataBase.Device.update(zone_query, update_field, { multi: true }, function (err) {
        if (err) { logger.log('error', err); res.status(500).send(err); return; }
        else { res.status(200).send("Zone (Device) edited!"); }
    });
};

var deleteZone = function (req, res) {
    //logger.log('debug', JSON.stringify(req.body));
    //Although some validation is done in UI, here will do it also
    //dI: { zone_inf, zone_ref, appl_inf, appl_ref }

    //Note: Not supposed to be called in normal routine
    //Only the first one in the doc list will be deleted - although it's expected to have 1 only
    var zone_ref = req.body.zone_ref;
    if (!(zone_ref && zone_ref._id)) { res.status(400).send("Request body not complete."); return; }
    var zone_query = { _id: zone_ref._id };
    oDataBase.Device.find(zone_query, function (err, docs) {
        if (err) { logger.log('error', err); res.status(500).send(err); return; }
        else if (docs.length === 0) { res.status(200).send("No such Device found, assumed completed."); }
        else {
            docs[0].remove(function (err) {
                if (err) { logger.log('error', err); res.status(500).send(err); return; }
                else { res.status(200).send("Zone(s) deleted successfully."); }
            });
        }
    });
};

var exports = module.exports = {
    addZone: addZone,
    editZone: editZone,
    deleteZone: deleteZone,
    init: init
};
