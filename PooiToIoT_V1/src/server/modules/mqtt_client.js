"use strict";
//MQTT to Mosca
var mqtt = require('mqtt');
var async = require('async');

var oDataBase = require('./db_connection.js');
var server_config = require('./server_config.js');
let server_logger = require('./server_logger.js');

let _bDebug_mode = true;
let logger = server_logger.SERVER_LOGGING(`${server_config._sLOG_DIR}/mqtt_client.log`, { level: _bDebug_mode ? "debug" : "info", mute_console: false }); //Logger of this module

var _iMQTT_RECONNECT_UNIT = server_config._iMQTT_RECONNECT_UNIT; //Attempt reconnect for every 4 seconds
var _iMQTT_RECONNECT_LIMIT = server_config._iMQTT_RECONNECT_LIMIT;  //Abondon to reconnect after 2 weeks

var mqttClient = {};
var iMqtt_retry_attempt = 0; //Reset on successful connection

//Return retry delay for reconnecting Mosca Server
var iMqtt_retry_delay = function (iMqtt_retry_attempt) {
    //Limitation: Not dynamic. Only initial value f(0) is recorded.
    //4 - 600 seconds
    /**
    iMqtt_retry_attempt++;
    var iMqtt_retry_amount = 4 * 1000 * iMqtt_retry_attempt;
    if (iMqtt_retry_amount > 600 * 1000) { iMqtt_retry_amount = 600 * 1000; }
    if (iMqtt_retry_amount <= 0) { iMqtt_retry_amount = 4000; }
    return iMqtt_retry_amount;
    **/
    return _iMQTT_RECONNECT_UNIT;
};

//MQTT Connect option
var connect_options = {
    reconnectPeriod: iMqtt_retry_delay(iMqtt_retry_attempt), //4 seconds
    //Never connect after iConnectTimeout
    connectTimeout: _iMQTT_RECONNECT_LIMIT //2 weeks
    //iConnectTimeout: 10000 //Mosca default?
};

var boot_mqtt_basic = function (merge_logger, URL, onConnected, onMessage, onDisconnected) {
    mqttClient = null; //Release client object
    let logger = merge_logger; //Merge logger into parent's logger
    logger.log('debug', "Connecting Mosca server: " + URL);

    /**
     * MQTT Client event handlers
     */
    mqttClient = mqtt.connect(URL, connect_options);

    mqttClient.on('connect', function () {
        logger.log('info', "Mosca server connected.");
        iMqtt_retry_attempt = 0;

        if (onConnected)
            onConnected();
    });

    mqttClient.on('reconnect', function () {
        iMqtt_retry_attempt++;
        logger.log('debug', "oMqttclient.on(reconnect) # " + iMqtt_retry_attempt);
        if (onDisconnected)
            onDisconnected();
    });

    mqttClient.on('close', function () {
        logger.log('debug', "oMqttclient.on(close)");
        if (onDisconnected)
            onDisconnected();
    });

    mqttClient.on('offline', function () {
        logger.log('error', "Mosca server disconnected.");
        if (onDisconnected)
            onDisconnected();
    });

    mqttClient.on('error', function (error) {
        logger.log('error', "oMqttclient.on(error): " + error);
        if (onDisconnected)
            onDisconnected();
        //process.exit(1);
    });

    mqttClient.on('message', function (sTopic, bMessage) {
        //bMessage is Buffer
        logger.log('info', "Received message: Topic - '" + sTopic + "', Message - '" + bMessage.toString() + "'");

        if (onMessage)
            onMessage(sTopic, bMessage);
    });

    return mqttClient;
};

var fireMqttMessage = function (req, res) {
    //logger.log("debug", JSON.stringify(req.user));
    if ((server_config._sSiteId == "PooiTo_PLC") && (!(req.user && req.user.usertype.indexOf("admin") >= 0))) {
        return res.status(400).send("Error, Operation is not permitted.");
    }

    var topic = req.body.topic;
    var message = req.body.message;
    if (topic && message) {
        try { JSON.parse(message); } catch (e) {
            return res.status(400).send("Error, message shuold be in JSON string.");
        }
        logger.log('debug', "fireMqttMessage: topic = " + topic + ", message = " + message);
        mqttClient.publish(topic, message, { qos: 1 });
        return res.status(200).send("Success");
    } else {
        return res.status(400).send("Error, post body not complete.");
    }
};

var PushToPLC = function (foundSchedule, callback) {
    //logger.log('debug', foundSchedule.zoneId); return callback(null);
    var Device_query = { zoneId: foundSchedule.zoneId };
    var sTopic;
    oDataBase.Device.find(Device_query, function (err, docs0) {
        if (err) { logger.log('error', err); return callback(err); }
        else if (docs0.length === 0) { return callback("PushToPLC: Device not found"); }
        else {
            sTopic = docs0[0].mqtt_tag + "/setStatus";
            var iSlot_val = [];
            for (var i0 = 0; i0 < foundSchedule.time_slots.length; i0++) {
                iSlot_val.push(foundSchedule.time_slots[i0].slot_value);
            }
            var param_sch = {
                command: "SET_SCHEDULER",
                client_id: foundSchedule.client_id,
                sch_index: foundSchedule.dev_start_addr,
                value: iSlot_val
            };
            var sMessage = JSON.stringify(param_sch);
            logger.log('debug', "PushToPLC: topic = " + sTopic + ", message = " + sMessage);
            mqttClient.publish(sTopic, sMessage, { qos: 2 });
            return callback(null);
        }
    });
};

var exports = module.exports = {
    mqttClient: mqttClient,
    boot_mqtt_basic: boot_mqtt_basic,
    fireMqttMessage: fireMqttMessage,
    PushToPLC: PushToPLC
};
