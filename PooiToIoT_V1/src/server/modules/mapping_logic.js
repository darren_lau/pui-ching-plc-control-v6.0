"use strict";
let async = require("async");

let plc_zone_appl = require('./plc_zone_appl.js');
let cool_master = require('./cool_master.js');
let server_config = require('./server_config.js');

const _sSiteId = server_config._sSiteId;
const _sCoolMaster_TypeName = server_config._sCoolMaster_TypeName; //From Device.client_type in DB 
const _sPLC_TypeName = server_config._sPLC_TypeName; //From Device.client_type in DB 
const _sDelightUdp_TypeName = server_config._sDelightUdp_TypeName; //From Device.client_type in DB 

let fs = require('fs'); //File stream
const _bDebug_mode = true;

//convert oDevice_Status [{address. value}] into [value] by address
let flatten_map = function (arr_map) {
    let max_addr = arr_map.reduce((a, b) => {
        return Math.max(a.address, b.address);
    });
    let arr = Array.from({ length: max_addr }, (v, i) => 0);

    arr_map.forEach((d, i, a) => {
        arr[d.address] = d.value;
    });
    return arr;
};

let flattened_oDevice_Status = function (d, client_type) {
    //d = DB.Device_Status
    //let unit = server_config._oModbus_read_unit;
    let unit = server_config._oModbus_read_range[client_type];
    for (var TypeName in unit) {
        if (unit.hasOwnProperty(TypeName)) {
            d[TypeName] = d[TypeName] && d[TypeName].length > 0 ? flatten_map(d[TypeName]) : d[TypeName];
        }
    }
    return d;
};

let grouped_oDevice_Status = function (oTarget_modbus, register_arr) {
    let oACReg = [];
    Object.keys(oTarget_modbus.zones).forEach(zone_id => {
        Object.keys(oTarget_modbus.zones[zone_id].appliances).forEach(appl_id => {
            let appl_obj = oTarget_modbus.zones[zone_id].appliances[appl_id];
            let arr_map = cool_master.va_to_arr(appl_obj.appl_index);
            if (!oACReg[arr_map[0]]) { oACReg[arr_map[0]] = []; }
            if (!oACReg[arr_map[0]][arr_map[1]]) { oACReg[arr_map[0]][arr_map[1]] = []; }
            for (let i = 0; i < cool_master.reg_length; i++) {
                oACReg[arr_map[0]][arr_map[1]][i] = register_arr[appl_obj.appl_index + i];
            }
        });
    });
    return oACReg;
};

/**
 * High level API
 */

/**
 * Parse DB.Device_Status with infromation from config.xml and DB.Device.
 * @logger: Logger of parent process
 * @oDevice_Status: Array of DB.Device_Status
 * @oConfig_XML: Map of parsed xml content. Mapped as { client_id: contents parsed by load_xml }
 * @oDevice:  Array of DB.Device
 * @return: {Array} of MQTT messages. Mapped as [{ siteId, zoneId, sTopic, sMessage }]. 
 */

let parse_device_status = function (logger, oDevice_Status, oConfig_XML, oDevice, wrapped_oDevice, cb) {
    //1. Parse Raw registers from each Device into Partial parsed message of Zone  
    //2. Group messages from each Zone
    //3. Fill in additional information (e.g. MQTT stuffs)
    //4. Return MQTT messages

    if (_bDebug_mode) {
        let filepath = `${server_config._sLOG_DIR}/wrapped_oDevice.json`;
        fs.writeFile(filepath, JSON.stringify(wrapped_oDevice, null, 4), "utf-8", (err) => {
            logger.log(err ? "error" : "debug", err ? err : `File dumped to ${filepath}`);
        });
    }

    let parsed_oDevice_Status = [];

    async.each(oDevice_Status, (d, cb_in) => {
        let client_type = wrapped_oDevice[_sSiteId][d.client_id].client_type;

        //logger.log('debug', JSON.stringify(wrapped_oDevice[_sSiteId][d.client_id]));

        let foDS = null; //Preprocess of oDevice_Status
        let parseFn = null;

        try {
            switch (client_type) {
                case _sPLC_TypeName:
                    foDS = flattened_oDevice_Status(d.toObject(), client_type); //Keep in mind that d is still Mongoose Docuement.
                    parseFn = plc_zone_appl.fAnalysis_zone_status;
                    break;
                case _sCoolMaster_TypeName:
                    foDS = flattened_oDevice_Status(d.toObject(), client_type);
                    foDS = grouped_oDevice_Status(wrapped_oDevice[_sSiteId][d.client_id], foDS.register);
                    parseFn = cool_master.fAnalysis_coolmaster_status;
                    break;
                case _sDelightUdp_TypeName :
                    foDS = flattened_oDevice_Status(d.toObject(), _sPLC_TypeName); //Keep in mind that d is still Mongoose Docuement.
                    parseFn = plc_zone_appl.fAnalysis_zone_status;
                    break;
                default:
                    logger.log("error", `parse_device_status: client_type (${client_type}) is not supported!`);
                    return cb_in();
            }
        } catch (e) {
            logger.log("error", `parse_device_status: Got error when parsing device_status ${client_type}!`);
            logger.log("error", e);
            return cb_in();
        }

        if (_bDebug_mode) {
            let filepath = `${server_config._sLOG_DIR}/preprocessed_oDevice_Status_${d.client_id}.json`;
            fs.writeFile(filepath, JSON.stringify(foDS, null, 4), "utf-8", (err) => {
                logger.log(err ? "error" : "debug", err ? err : `File dumped to ${filepath}`);
            });
        }

        parseFn(logger, wrapped_oDevice[_sSiteId][d.client_id], foDS, oConfig_XML[d.client_id], (error, oMQTT_Messages) => {
            //logger.log('info', oMQTT_Messages);
            if (error) { logger.log("error", error); }
            else { parsed_oDevice_Status.push(oMQTT_Messages); }
            return cb_in();
        });
    }, (err) => {
        if (err) { return cb(err, null); }
        else {
            if (_bDebug_mode) {
                let filepath = `${server_config._sLOG_DIR}/parse_device_status.json`;
                fs.writeFile(filepath, JSON.stringify(parsed_oDevice_Status, null, 4), "utf-8", (err) => {
                    logger.log(err ? "error" : "debug", err ? err : `File dumped to ${filepath}`);
                });
            }

            return cb(null, parsed_oDevice_Status);
        }
    });
};

/**
 * Parse MQTT message to Device_Command with infromation from config.xml and DB.Device.
 * @logger: Logger of parent process
 * @sTopic: MQTT topic in String
 * @sMessage: MQTT message in String
 * @oConfig_XML: Map of parsed xml content. Mapped as { client_id: contents parsed by load_xml }
 * @oDevice:  Array of DB.Device
 * @return: Callback function (err, msg). 
 */

let parse_mqtt_message = function (logger, sTopic, sMessage, oConfig_XML, oDevice, wrapped_oDevice, cb) {
    let zoneId = null;
    let cmd_arr = [];

    try {
        zoneId = sTopic.split('/')[3];
    } catch (e) {
        logger.log("error", `parse_device_status: Got error when parsing sTopic ${sTopic}!`);
        return cb(e);
    }

    let client_arr = oDevice.filter((d) => { return d.zoneId === zoneId; });
    let parseFn = null;

    async.each(client_arr, (client_obj, cb_in) => {

        switch (client_obj.client_type) {
            case _sPLC_TypeName:
                parseFn = plc_zone_appl.fAnalysis_set_status;
                break;
            case _sCoolMaster_TypeName:
                parseFn = cool_master.fAnalysis_set_coolmaster;
                break;
            case _sDelightUdp_TypeName: 
                parseFn = plc_zone_appl.fAnalysis_set_status;
                break;
            default:
                logger.log('error', `client_type (${client_obj.client_type}) is not supported under receiving MQTT message. Ignored.`);
                return cb_in();
        }

        parseFn(logger, oConfig_XML[client_obj.client_id], wrapped_oDevice[_sSiteId][client_obj.client_id], sTopic, sMessage, (err, oWrite_obj1, oWrite_obj2) => {
            if (err) { logger.log('error', err); }
            else {
                if (oWrite_obj1) cmd_arr.push(oWrite_obj1);
                if (oWrite_obj2) cmd_arr.push(oWrite_obj2);
            }
            return cb_in();
        });

    }, (err) => {
        if (err) { return cb(err); }
        else { return cb(null, cmd_arr); }
    });
};

var exports = module.exports = {
    build_plc_based_inf: plc_zone_appl.build_plc_based_inf,
    parse_device_status: parse_device_status,
    parse_mqtt_message: parse_mqtt_message
};