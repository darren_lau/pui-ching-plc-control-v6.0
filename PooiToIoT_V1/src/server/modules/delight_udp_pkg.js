"use strict";
//Get Subnet Info (11 ca 11 01)
//HEAD |TYPE |     |PKG# |Project ID                |CRC
//a5 5a 11 ca 11 01 01 07 00 16 27 00 00 0c 94 00 00 d2 aa
//a5 5a 11 ca 11 01 01 08 00 16 27 00 00 0c 94 00 00 d3 aa
//a5 5a 11 ca 11 01 01 09 00 16 27 00 00 0c 94 00 00 d4 aa
//a5 5a 11 ca 11 01 01 0a 00 16 27 00 00 0c 94 00 00 d5 aa
//a5 5a 11 ca 11 01 01 0b 00 16 27 00 00 0c 94 00 00 d6 aa
//a5 5a 11 ca 11 01 01 0c 00 16 27 00 00 0c 94 00 00 d7 aa
//a5 5a 11 ca 11 01 01 0d 00 16 27 00 00 0c 94 00 00 d8 aa

//Subnet Info (36 cb 11 00)
//HEAD |TYPE |     |PKG# |Project ID                |SubnetID                           |Subnet Name (char[12])                   |              |TD   |SC   |State|CRC
//a5 5a 36 cb 11 00 01 07 00 16 27 00 00 0c 94 00 00 2b 00 49 00 02 51 36 34 32 34 37 39 44 65 6d 6f 00 00 00 00 00 00 00 00 00 00 00 00 16 27 00 00 01 00 01 00 01 c3 aa
//a5 5a 36 cb 11 00 01 09 00 16 27 00 00 0c 94 00 00 2b 00 49 00 02 51 36 34 32 34 37 39 44 65 6d 6f 00 00 00 00 00 00 00 00 00 00 00 00 16 27 00 00 01 00 01 00 01 c5 aa
//a5 5a 36 cb 11 00 01 0a 00 16 27 00 00 0c 94 00 00 2b 00 49 00 02 51 36 34 32 34 37 39 44 65 6d 6f 00 00 00 00 00 00 00 00 00 00 00 00 16 27 00 00 01 00 01 00 01 c6 aa
//a5 5a 36 cb 11 00 01 0b 00 16 27 00 00 0c 94 00 00 2b 00 49 00 02 51 36 34 32 34 37 39 44 65 6d 6f 00 00 00 00 00 00 00 00 00 00 00 00 16 27 00 00 01 00 01 00 01 c7 aa
//a5 5a 36 cb 11 00 01 0c 00 16 27 00 00 0c 94 00 00 2b 00 49 00 02 51 36 34 32 34 37 39 44 65 6d 6f 00 00 00 00 00 00 00 00 00 00 00 00 16 27 00 00 01 00 01 00 01 c8 aa
//a5 5a 36 cb 11 00 01 0d 00 16 27 00 00 0c 94 00 00 2b 00 49 00 02 51 36 34 32 34 37 39 44 65 6d 6f 00 00 00 00 00 00 00 00 00 00 00 00 16 27 00 00 01 00 01 00 01 c9 aa

//Get LED List (1f ca 13 01)
//HEAD |TYPE |     |PKG# |Project ID                |SubnetID                           |HWID |CRC
//a5 5a 1f ca 13 01 01 0e 00 16 27 00 00 0c 94 00 00 2b 00 49 00 02 51 36 34 32 34 37 39 01 00 f1 aa
//a5 5a 1f ca 13 01 01 0f 00 16 27 00 00 0c 94 00 00 2b 00 49 00 02 51 36 34 32 34 37 39 01 00 f2 aa

//LED List (23 cb 13 00)
//HEAD |TYPE |     |PKG# |Project ID                |SubnetID                           |HWID |           |CRC
//a5 5a 23 cb 13 00 01 0e 00 16 27 00 00 0c 94 00 00 2b 00 49 00 02 51 36 34 32 34 37 39 01 00 00 00 01 00 f6 aa
//a5 5a 23 cb 13 00 01 0f 00 16 27 00 00 0c 94 00 00 2b 00 49 00 02 51 36 34 32 34 37 39 01 00 00 00 01 00 f7 aa

//Get SC List (1f ca 17 01)
//HEAD |TYPE |     |PKG# |Project ID                |SubnetID                           |HWID |CRC
//a5 5a 1f ca 17 01 01 10 00 16 27 00 00 0c 94 00 00 2b 00 49 00 02 51 36 34 32 34 37 39 01 00 f7 aa
//a5 5a 1f ca 17 01 01 11 00 16 27 00 00 0c 94 00 00 2b 00 49 00 02 51 36 34 32 34 37 39 01 00 f8 aa

//SC List (2d cb 17 00)
//                                                                                                               0.8.3.A     2.46.0.0
//HEAD |TYPE |     |PKG# |Project ID                |SubnetID                           |HWID |C1 C2 C3 C4 T1 T2|SW Ver     |HW Ver     |CRC
//a5 5a 2d cb 17 00 01 10 00 16 27 00 00 0c 94 00 00 2b 00 49 00 02 51 36 34 32 34 37 39 01 00 00 00 00 00 01 00 0a 03 08 00 00 00 46 02 63 aa
//a5 5a 2d cb 17 00 01 11 00 16 27 00 00 0c 94 00 00 2b 00 49 00 02 51 36 34 32 34 37 39 01 00 00 00 00 00 01 00 0a 03 08 00 00 00 46 02 64 aa

//Light CMD (22 ca 16 01)
//Intensity = AA, Mode = BB
//Intensity: DEC 0 - 100, Mode = [Static, Flash], Flash = 50
//HEAD |TYPE |     |PKG# |Project ID                |SubnetID                           |HWID |AA|  |BB|CRC
//a5 5a 22 ca 16 01 01 12 00 16 27 00 00 0c 94 00 00 2b 00 49 00 02 51 36 34 32 34 37 39 01 00 32 00 01 2e aa
//a5 5a 22 ca 16 01 01 13 00 16 27 00 00 0c 94 00 00 2b 00 49 00 02 51 36 34 32 34 37 39 01 00 32 00 01 2f aa
//a5 5a 22 ca 15 01 01 3c 00 16 27 00 00 0c 94 00 00 2b 00 49 00 02 51 36 34 32 34 37 39 01 00 32 00 01 57 aa
//a5 5a 22 ca 15 01 01 3b 00 16 27 00 00 0c 94 00 00 2b 00 49 00 02 51 36 34 32 34 37 39 01 00 00 00 00 23 aa
//a5 5a 22 ca 15 01 01 31 00 16 27 00 00 0c 94 00 00 2b 00 49 00 02 51 36 34 32 34 37 39 01 00 64 00 00 86 aa
//a5 5a 22 ca 15 01 01 39 00 16 27 00 00 0c 94 00 00 2b 00 49 00 02 51 36 34 32 34 37 39 01 00 64 00 00 21 aa
//a5 5a 22 ca 15 01 01 38 00 16 27 00 00 0c 94 00 00 2b 00 49 00 02 51 36 34 32 34 37 39 01 00 64 00 00 20 aa

//Light RES (22 cb 16 00)
//HEAD |TYPE |     |PKG# |Project ID                |SubnetID                           |HWID |AA|  |BB|CRC
//a5 5a 22 cb 16 00 01 12 00 16 27 00 00 0c 94 00 00 2b 00 49 00 02 51 36 34 32 34 37 39 01 00 32 00 01 2e aa
//a5 5a 22 cb 16 00 01 13 00 16 27 00 00 0c 94 00 00 2b 00 49 00 02 51 36 34 32 34 37 39 01 00 32 00 01 2f aa
//a5 5a 22 cb 15 00 01 3c 00 16 27 00 00 0c 94 00 00 2b 00 49 00 02 51 36 34 32 34 37 39 01 00 32 00 01 57 aa
//a5 5a 22 cb 15 00 01 3b 00 16 27 00 00 0c 94 00 00 2b 00 49 00 02 51 36 34 32 34 37 39 01 00 00 00 00 23 aa
//a5 5a 22 cb 15 00 01 3a 00 16 27 00 00 0c 94 00 00 2b 00 49 00 02 51 36 34 32 34 37 39 01 00 64 00 00 86 aa
//a5 5a 22 cb 15 00 01 39 00 16 27 00 00 0c 94 00 00 2b 00 49 00 02 51 36 34 32 34 37 39 01 00 00 00 00 21 aa
//a5 5a 22 cb 15 00 01 38 00 16 27 00 00 0c 94 00 00 2b 00 49 00 02 51 36 34 32 34 37 39 01 00 00 00 00 20 aa

//Change Name REQ (35 cb 12 00)
//HEAD |TYPE |     |PKG# |Project ID                |SubnetID                           |New Subnet Name (char[12])         |        |Project ID                |CRC
//a5 5a 35 ca 12 01 01 14 00 16 27 00 00 0c 94 00 00 2b 00 49 00 02 51 36 34 32 34 37 39 44 65 6d 6f 44 65 6d 6f 44 65 6d 6f 00 00 00 00 16 27 00 00 0c 94 00 00 77 aa

//Change Name RES (35 cb 12 00)
//HEAD |TYPE |     |PKG# |Project ID                |SubnetID                           |New Subnet Name (char[12])         |        |Project ID                |CRC
//a5 5a 35 cb 12 00 01 14 00 16 27 00 00 0c 94 00 00 2b 00 49 00 02 51 36 34 32 34 37 39 44 65 6d 6f 44 65 6d 6f 44 65 6d 6f 00 00 00 00 16 27 00 00 0c 94 00 00 77 aa


//Scan (to 255.255.255.255)
//ff 01 01 02

const FN_SUM = (buf) => {
    if (typeof buf === "string") { buf = buf.split(" ").map(d => parseInt(d, 16)); }
    //console.log(typeof buf);
    //return buf;
    let result = buf.reduce((a, c) => {
        //console.log(c);
        return parseInt(a) + parseInt(c);
    }, 0) + 1;
    //console.log(result);
    return [(result) & 0xff];
};

const FN_TID = () => {
    return [
        Math.floor(Math.random() * 0xff) & 0xff,
        Math.floor(Math.random() * 0xff) & 0xff,
    ];
};

const CONST_MAP = {
    "HEAD": [0xa5, 0x5a],
    "CMD": {
        "REQ": {
            "SUBNET_INFO": [0x11, 0xca, 0x11, 0x01],
            "LED_LIST": [0xa5, 0x5a, 0x1f, 0xca],
            "SC_LIST": [0x1f, 0xca, 0x17, 0x01],
            "SET_LIGHT": [0x22, 0xca, 0x15, 0x01], //0x22, 0xca, 0x16, 0x01],
            "CHANGE_SUBNET_NAME": [0x35, 0xca, 0x12, 0x01]
        },
        "RES": {
            "SUBNET_INFO": [0x36, 0xcb, 0x11, 0x00],
            "LED_LIST": [0x23, 0xcb, 0x13, 0x00],
            "SC_LIST": [0x2d, 0xcb, 0x17, 0x00],
            "SET_LIGHT": [0x22, 0xcb, 0x15, 0x00], // [0x22, 0xcb, 0x16, 0x00],
            "CHANGE_SUBNET_NAME": [0x35, 0xcb, 0x12, 0x00]
        }
    },
    "FN_TID": FN_TID,
    "PROJ_ID": {
        "DEV": [0x00, 0x16, 0x27, 0x00, 0x00, 0x0c, 0x94, 0x00, 0x00],
        "0622": [0x00, 0x65, 0x00, 0x00, 0x00, 0x65, 0x00, 0x00, 0x00],
        "0726": [0x00, 0x16, 0x27, 0x00, 0x00, 0x0c, 0x94, 0x00, 0x00]
    },
    "SUBNET_ID": {
        "DEV": [0x2b, 0x00, 0x49, 0x00, 0x02, 0x51, 0x36, 0x34, 0x32, 0x34, 0x37, 0x39],
        "0622": [0x27, 0x00, 0x43, 0x00, 0x02, 0x51, 0x36, 0x34, 0x32, 0x34, 0x37, 0x39],
        "0726": [0x27, 0x00, 0x43, 0x00, 0x02, 0x51, 0x36, 0x34, 0x32, 0x34, 0x37, 0x39]
    },
    "HW_ID": (addr) => {
        if (addr == "DEV") { return [0x01, 0x00]; }
        else {
            //"0622": [0..20] 

            let r = [addr & 0x00ff, (addr & 0xff00) >> 8];
            //Seems [0x00, 0x00] = Boardcast 
            //console.log(r);
            return r;
        }
    },
    "FN_SUM": FN_SUM,
    "TAIL": [0xaa],
    "SCAN": [0xff, 0x01, 0x01, 0x02]
};

const PKG_GEN = {
    "SET_LIGHT": (PROJ_ID, SUBNET_ID, HW_ID, light_intensity, light_unknown, light_mode) => {
        let result = [];
        result = result
            .concat(CONST_MAP["HEAD"])
            .concat(CONST_MAP["CMD"]["REQ"]["SET_LIGHT"])
            .concat(CONST_MAP["FN_TID"]())
            .concat(CONST_MAP["PROJ_ID"][PROJ_ID])
            .concat(CONST_MAP["SUBNET_ID"][SUBNET_ID])
            .concat(CONST_MAP["HW_ID"](HW_ID))
            .concat([light_intensity, light_unknown, light_mode])
        let sum = CONST_MAP["FN_SUM"](result);
        result = result
            .concat(sum)
            .concat(CONST_MAP["TAIL"])
        return Buffer.from(result);
    },
    "SUBNET_INFO": (PROJ_ID) => {
        let result = [];
        result = result
            .concat(CONST_MAP["HEAD"])
            .concat(CONST_MAP["CMD"]["REQ"]["SUBNET_INFO"])
            .concat(CONST_MAP["FN_TID"]())
            .concat(CONST_MAP["PROJ_ID"][PROJ_ID])
        let sum = CONST_MAP["FN_SUM"](result);
        result = result
            .concat(sum)
            .concat(CONST_MAP["TAIL"])
        return Buffer.from(result);
    },
    "SCAN": () => {
        return Buffer.from(CONST_MAP["SCAN"]);
    }
};

var exports = module.exports = { CONST_MAP, PKG_GEN };