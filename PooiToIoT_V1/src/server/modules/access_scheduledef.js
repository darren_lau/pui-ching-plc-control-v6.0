"use strict";
var oDataBase = require('./db_connection');
var logger = {};

var init = function (l) { logger = l; }

var addSchedule_def = function (req, res) {
    if (!req.body.default_id || !req.body.xml_list) {
        return res.status(400).send('Request body not complete.');
    };
    var obj_query = { default_id: req.body.default_id };
    oDataBase.Schedule_def.find(obj_query, function (err, docs) {
        if (docs.length > 0) {
            res.status(400).send('Schedule default already exists. Please use other user name.'); return;
        } else {
            var def = new Schedule_def();
            def.default_id = req.body.default_id;
            def.xml_list = req.body.xml_list;
            def.save(function (err) {
                if (err) { logger.log('error', err); res.status(500).send(err); return; }
                else { return res.status(200).send(def); }
            });
        }
    });
}

var getSchedule_def = function (req, res) {
    oDataBase.Schedule_def.find(req.query, function (err, docs) {
        if (err) { logger.log('error', err); res.status(500).send(err); return; }
        else if (docs.length == 0) { res.status(200).send("[]"); return; }
        else { res.status(200).send(docs); }
    });
}

var editSchedule_def = function (req, res) {
    if (!req.body.default_id || !req.body.xml_list) {
        return res.status(400).send('Request body not complete.');
    };
    var obj_query = { default_id: req.body.default_id };
    oDataBase.Schedule_def.find(obj_query, function (err, docs) {
        if (err) { logger.log('error', err); res.status(500).send(err); return; }
        else if (docs.length == 0) { return res.status(400).send('Request body not complete.'); }
        else {
            var def = docs[0];
            def.default_id = req.body.default_id;
            def.xml_list = req.body.xml_list;
            def.save(function (err) {
                if (err) { logger.log('error', err); res.status(500).send(err); return; }
                else { return res.status(200).send(def); }
            });
        }
    });
}

var deleteSchedule_def = function (req, res) {
    if (!req.body.username) { return res.status(400).send('Request body not complete.'); };
    var obj_query = { default_id: req.body.default_id };
    oDataBase.Schedule_def.find(obj_query, function (err, docs) {
        if (err) { logger.log('error', err); res.status(500).send(err); return; }
        else if (docs.length == 0) { return res.status(400).send('Schedule_def not found.'); }
        else {
            docs[0].remove(function (err) {
                if (err) { logger.log('error', err); res.status(500).send(err); return; }
                else { return res.status(200).send('Schedule_def deleted.'); }
            });
        }
    });
}

var exports = module.exports = {
 	addSchedule_def: addSchedule_def,
 	getSchedule_def: getSchedule_def,
 	editSchedule_def: editSchedule_def,
 	deleteSchedule_def: deleteSchedule_def,
    init: init
};
