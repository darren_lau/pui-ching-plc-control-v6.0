"use strict";
var oDataBase = require('./db_connection');

var logger = {};

var init = function (l) { logger = l; };

var getStatus = function (req, res) {
    oDataBase.Device_Status.find(req.query).populate('device_ref').exec(function (err, docs) {
        if (err) { logger.log('error', err); res.status(500).send(err); return; }
        else if (docs.length === 0) { res.status(200).send("[]"); return; }
        else { res.status(200).send(docs); }
    });
};

var exports = module.exports = {
    getStatus: getStatus,
    init: init
};
