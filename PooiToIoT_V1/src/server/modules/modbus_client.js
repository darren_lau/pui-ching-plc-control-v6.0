"use strict";
// JavaScript source code
// Note: This script no longer capable for direct access modbus. DB connection is assuemed on.
// 170915: Now it is a standalone process with new structure.
// 160706: This is focused on PLC connection - no serious modification is needed

/**
 * External libraries (npm/node)
 */
let jsmodbus = require('jsmodbus'); //Modbus Client

/**
* Self-made libraries
*/
let server_config = require('./server_config.js'); //Custom settings
let server_logger = require('./server_logger.js');
let cool_master = require('./cool_master.js'); //Cool Master Modbus stuffs

/**
* Constants
*/
let _sTarget_client_id = process.argv[2] ? process.argv[2] : process.env.ETAG_PLC_CLIENT_ID; //client_id assigned for this MQTT Client (170919) //Process arguement: client_id for taking care
let _bDebug_mode = process.argv.indexOf("debug") >= 0; //If true, spam the console with debug messages
let _bOffline_mode = process.argv.indexOf("offline") >= 0; //If true, no Modbus device will be connected. Outputs are generated instead
let _bOverride_host = process.argv.indexOf("host_mod") >= 0; //If true, ip address is overrided to 192.168.1.30
let _bOverride_port = process.argv.indexOf("port_mod") >= 0; //If true, port is overrided to secondary port

const _iPrimary_modbus_port = server_config._iPrimary_modbus_port; //Hardcoded, non-documented
const _iSecondary_modbus_port = server_config._iSecondary_modbus_port; //Hardcoded, non-documented
const _oModbus_read_range = server_config._oModbus_read_range;
const _oModbus_connection_config = server_config._oModbus_connection_config;
const _sPLC_TypeName = server_config._sPLC_TypeName;
const _sCoolMaster_TypeName = server_config._sCoolMaster_TypeName;
const _sOverrided_host = server_config._sOverrided_host;

/**
 * Internal global objects
 */
let logger = server_logger.SERVER_LOGGING(`${server_config._sLOG_DIR}/modbus_control_${_sTarget_client_id}.log`, { level: _bDebug_mode ? "debug" : "info", mute_console: false }); //Logger of this module
let oModbusclient = null; //jsmodbus client object
let iModbus_port = _iPrimary_modbus_port; //Port number for connecting Modbus device
let bModbus_connected = false; //Status flag

let set_by_parent = function (a, b, c, d, e) {
    _sTarget_client_id = a;
    _bDebug_mode = b;
    _bOffline_mode = c;
    _bOverride_host = d;
    _bOverride_port = e;
    logger = server_logger.SERVER_LOGGING(`${server_config._sLOG_DIR}/modbus_control_${_sTarget_client_id}.log`, { level: _bDebug_mode ? "debug" : "info", mute_console: false }); //Logger of this module
}

/**
 * General functions (Stage 1)
 */

let makePartialDeviceStatus = function (_oIDObj, oTarget_modbus, resp, addr, sMemType) {
    let o = Object.assign({}, _oIDObj);
    if (resp && resp.coils) {
        o.coils = resp.coils.map((d, i, a) => { return { address: addr + i, value: d }; });
    }
    if (resp && resp.register) {
        o.register = resp.register.map((d, i, a) => { return { address: addr + i, value: d }; });
    }
    o.zoneId = oTarget_modbus ? oTarget_modbus.zoneId : "";
    o.ip_addr = oTarget_modbus ? oTarget_modbus.ip_addr : "";
    o.connect_state = bModbus_connected || _bOffline_mode ? "connected" : oModbusclient ? "disconnected" : "undefined"; //
    o.last_updated = new Date();
    o.client_pid = process.pid;
    return o;
};

/**
 * Modbus definiation/ connection/ reconnection
 */

//Defination of client object with event handlers
let fModbusclient_definehandler = function (_bOverride_host, _bOverride_port, oTarget_modbus, modbus_on_connect, modbus_on_disconnect) {
    oTarget_modbus.ip_addr = _bOverride_host ? _sOverrided_host : oTarget_modbus.ip_addr;
    iModbus_port = _bOverride_port ? _iSecondary_modbus_port : _iPrimary_modbus_port;

    // Close connection if abaliable
    if (oModbusclient !== null) {
        oModbusclient.close();
        oModbusclient.connect();
        return;
    }

    // Release if defined
    oModbusclient = null;
    logger.log('info', `Connecting to Modbus device: modbus://${oTarget_modbus.ip_addr}:${iModbus_port}`);
    //oModbusclient = modbus.createTCPClient(iModbus_port, oTarget_modbus.ip_addr, cb_modbuserror);
    oModbusclient = jsmodbus.client.tcp.complete(_oModbus_connection_config({
        host: oTarget_modbus.ip_addr,
        port: iModbus_port,
        debug_mode: _bDebug_mode
    }));

    //Event handlers
    oModbusclient.on('connect', () => {
        logger.log('info', "oModbusclient.on(connect)");
        bModbus_connected = true;
        modbus_on_connect(oModbusclient);
    });

    oModbusclient.on('data', () => {
        logger.log('debug', "oModbusclient.on(data)");
    });

    oModbusclient.on('close', () => {
        logger.log('info', "oModbusclient.on(close)");
        bModbus_connected = false;
        modbus_on_disconnect();
    });

    oModbusclient.on('error', (err) => {
        //Usually "util.ETIMEOUT" or "util.ECONNREFUSED" or "util.EHOSTUNREACH"
        logger.log('error', "oModbusclient.on('error')");
        logger.log('error', err);
    });

    oModbusclient.on('end', () => {
        logger.log('info', "oModbusclient.on(end)");
        bModbus_connected = false;
        modbus_on_disconnect();
    });

    oModbusclient.connect();
};

//180615: Found that jsmodbus is handling itself.
//Callback of reconnecting Modbus client
let cb_reconnect_modbus = function () {
    logger.log('info', "reconnecting oModbusclient...");
    fModbusclient_definehandler();
};

/**
 * General functions (Stage 2)
 */

//Triggered by _bOffline_mode. Section of hard-coded logic.
let random_values = function (client_type, mem_type, d_from, d_to) {
    logger.log('debug', `random_value(${client_type}, ${mem_type})`);
    let ran_arr = [];
    let default_arr = [];

    for (let i = d_from; i <= d_to; i++) {
        default_arr.push(Math.random() >= 0.5 ? 1 : 0);
    }

    if (client_type === "PLC") {
        if (mem_type === "coils") {
            for (let i = d_from; i <= d_to; i++) {
                ran_arr.push(Math.random() >= 0.5 ? 1 : 0);
            }
        } else if (mem_type === "register") {
            for (let i = d_from; i <= d_to; i++) {
                ran_arr.push(Math.floor(Math.random() * 255));
            }
        } else {
            logger.log('error', `random_value: mem_type ${client_type}.${mem_type} is not recgonized! Binary random value will be generated instead!`);
            return default_arr;
        }
    } else if (client_type === "CoolMasterNet") {
        if (mem_type === "register") {
            for (let i = d_from; i <= d_to; i += cool_master.reg_length) {
                ran_arr.push.apply(ran_arr, cool_master.gen_random_arr());
            }
        } else {
            logger.log('error', `random_value: mem_type ${client_type}.${mem_type} is not recgonized! Binary random value will be generated instead!`);
            return default_arr;
        }
    } else {
        logger.log('error', `random_value: client_type ${client_type} is not recgonized! Binary random value will be generated instead!`);
        return default_arr;
    }

    return ran_arr;
};

/**
 * Modbus write
 */
let parse_and_write_cmd = function (d_cmd) {
    return new Promise((resolve, reject) => {
        // {"_id":"59c8af2664566bc0b8dfd658","client_id":"PLC_2a","siteId":"PuiChing_P","__v":0,"last_completed":null,"last_issued":"2017-09-25T07:24:22.424Z","modbus_fc":5,"fc_content":{"address":0,"value":[1]}}
        logger.log('debug', "Command found: " + JSON.stringify(d_cmd));
        let ModbusFn = null;
        if (_bOffline_mode) { resolve(); } //Skip if in offline mode
        if (!oModbusclient) { reject("oModbusclient is not found!"); }
        else if (!(d_cmd.modbus_fc && d_cmd.fc_content && d_cmd.fc_content.address >= 0 && d_cmd.fc_content.value && d_cmd.fc_content.value.length > 0)) {
            reject("Device_Command is invalid!");
        } else {
            //logger.log('debug', [d_cmd.fc_content.address, d_cmd.fc_content.value[0]]);
            //Resolve { resp }
            switch (d_cmd.modbus_fc) {
                case 5: oModbusclient.writeSingleCoil(d_cmd.fc_content.address, d_cmd.fc_content.value[0]).then(resolve, reject); break;
                case 6: oModbusclient.writeSingleRegister(d_cmd.fc_content.address, d_cmd.fc_content.value[0]).then(resolve, reject); break;
                case 15: oModbusclient.writeMultipleCoils(d_cmd.fc_content.address, d_cmd.fc_content.value).then(resolve, reject); break;
                case 16: oModbusclient.writeMultipleRegisters(d_cmd.fc_content.address, d_cmd.fc_content.value).then(resolve, reject); break;
                default: reject("Device_Command.modbus_fc is not supported!");
            }
        }
    });
};

/**
 * Modbus read
 */

let LocalDeviceStatus = (a, b, c, d) => { return { skip: a, resp: b, addr: c, sMemType: d }; } //Promise return 1 object only

let read_single_batch = function (in_param, oCall_Obj, oDump_arr) {
    //TODO: oCall_Obj

    return new Promise((resolve, reject) => {
        let cb_read = function (resp) {
            // resp will look like { fc: 1, byteCount: 20, coils: [ values 0 - 13 ], payload: <Buffer> }  
            // logger.log('debug', JSON.stringify(resp));
            // logger.log('debug', oDump_arr);
            if (!oDump_arr) { oDump_arr = resp; }
            else {
                //logger.log('debug', "Got response.");
                oDump_arr.byteCount += resp.byteCount;
                oDump_arr[in_param.sMemType].push.apply(oDump_arr[in_param.sMemType], resp[in_param.sMemType]);
                oDump_arr.payload = Buffer.concat([oDump_arr.payload, resp.payload]);
            }
            resolve(oDump_arr);
        };

        let err_buf = function () {
            let s = "";
            for (let i = 0; i < oReadRange.unit * 4; i++) {
                s += "f";
            }
            return Buffer.from(s, 'hex');
        };

        let err_arr = function () {
            let a = [];
            for (let i = 0; i < oReadRange.unit; i++) {
                a.push(-1);
            }
            return a;
        }

        let cb_err = function (err) {
            logger.log("error", err);
            logger.log("warn", "Dumping parameter encountered error: " + JSON.stringify({
                oCall_Obj: oCall_Obj,
                in_param: in_param
            }));
            let resp = {
                fc: 3,
                byteCount: oReadRange.unit * 2,
                register: err_arr(),
                payload: err_buf()
            };
            if (!oDump_arr) { oDump_arr = resp; }
            else {
                //logger.log('debug', "Got response.");
                oDump_arr.byteCount += resp.byteCount;
                oDump_arr[in_param.sMemType].push.apply(oDump_arr[in_param.sMemType], resp[in_param.sMemType]);
                oDump_arr.payload = Buffer.concat([oDump_arr.payload, resp.payload]);
            }
            resolve(oDump_arr);
        };

        //logger.log('debug', oCall_Obj);

        if (in_param.sMemType === "register") {
            oModbusclient.readHoldingRegisters(oCall_Obj.from, oCall_Obj.unit).then(cb_read, in_param.sClientType === "CoolMasterNet" ? cb_err : reject);
        } else if (in_param.sMemType === "coils") {
            oModbusclient.readCoils(oCall_Obj.from, oCall_Obj.unit).then(cb_read, reject);
        } else {
            reject(`Following Memory Type is not supported: ${in_param.sMemType}`);
        }
    });
};

let fDump_generalized = function (in_param) {

    return new Promise((resolve, reject) => {
        let oReadRange = in_param.oReadRange;
        //let iReadUnit = server_config._oModbus_read_unit[in_param.sMemType];

        //resolve:  return cb();
        //resolve:  return db_connection.Device_Status.findOneAndUpdate(in_param._oIDObj, { $set: makePartialDeviceStatus(oDump_arr, oReadRange.from, in_param.sMemType) }, { upsert: true, new: true }, cb);
        //logger.log("debug", [oReadRange.from, oReadRange.to]);

        if (oReadRange.to - oReadRange.from < 0) {
            logger.log('debug', `${in_param.fName}():Skipped.`);
            resolve(LocalDeviceStatus(true, null, null, null));
        } else if (_bOffline_mode) {
            logger.log('debug', `${in_param.fName}():${in_param.sMemType}#${oReadRange.from}-${in_param.sMemType}#${oReadRange.to}, Offline mode`);
            //TODO: Generate random data
            let oDump_arr = {};
            oDump_arr[in_param.sMemType] = random_values(in_param.sClientType, in_param.sMemType, oReadRange.from, oReadRange.to);
            //logger.log("debug", oDump_arr);
            resolve(LocalDeviceStatus(false, oDump_arr, oReadRange.from, in_param.sMemType));
        } else if (oModbusclient) {
            logger.log('debug', `${in_param.fName}():${in_param.sMemType}#${oReadRange.from}-${in_param.sMemType}#${oReadRange.to}, Modbus Connected: ${bModbus_connected}`);
            //Read blindly - since this client don't know where is the information
            if (bModbus_connected) {
                let fCall_arr = [];
                let oDump_arr = false;
                for (let i = oReadRange.from; i <= oReadRange.to; i += oReadRange.unit) {
                    fCall_arr.push({ from: i, unit: oReadRange.unit + i > oReadRange.to ? oReadRange.to - i : oReadRange.unit });
                }
                //logger.log('debug', JSON.stringify(fCall_arr));

                fCall_arr.reduce((p, oCall_Obj) => {
                    return p.then(() => {
                        return read_single_batch(in_param, oCall_Obj, oDump_arr);
                    }).then((new_val) => {
                        oDump_arr = new_val;
                    });
                }, Promise.resolve()).then(() => {
                    logger.log("debug", `Raw status: ${JSON.stringify(oDump_arr)}`);
                    resolve(LocalDeviceStatus(false, oDump_arr, oReadRange.from, in_param.sMemType));
                }).catch(reject);

            } else {
                logger.log('debug', "Modbus is not connected!");
                resolve(LocalDeviceStatus(false, null, null, null));
                //return cb();
            }
        } else {
            logger.log("warn", "Modbus is not defined!");
            resolve(LocalDeviceStatus(false, null, null, null));
            //return cb("Logic error: Modbus client is not found!");
        }
    });
};

let is_modbus_device = function (client_type) {
    return _oModbus_read_range[client_type];
};

let modbus_write_dump = async function (client_type, onData) {
    let oParamArr = [];
    for (let sMemType in _oModbus_read_range[client_type])
        if (_oModbus_read_range[client_type].hasOwnProperty(sMemType))
            oParamArr.push({
                oReadRange: _oModbus_read_range[client_type][sMemType],
                fName: `fDump_${client_type}_${sMemType}`,
                sMemType: sMemType,
                sClientType: client_type
            });
    //logger.log("debug", oParamArr);
    await oParamArr.reduce((p, oParam) => {
        return p.then(() => {
            //logger.log("debug", oParam);
            return fDump_generalized(oParam);
        }).then(onData);
    }, Promise.resolve());
};

let exit_modbus = new Promise((resolve, reject) => {
    if (oModbusclient)
        oModbusclient.close();
    resolve();
});

var exports = module.exports = {
    LocalDeviceStatus,
    makePartialDeviceStatus,
    modbus_write_dump,
    is_modbus_device,
    set_by_parent,
    parse_and_write_cmd,
    fModbusclient_definehandler,
    exit_modbus
};