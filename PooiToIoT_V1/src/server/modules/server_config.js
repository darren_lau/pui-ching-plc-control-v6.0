"use strict";
/**
 *  Try to put any configuration here.
 *  Then server can reflect changes quickly.
 *  For this project, most constants are stored in a XML file externally, 
 *  meanwhile grunt is called to apply changes,
 *  Here is to put the remaining configuration, e.g. library modules configuration which are seldom used
 *  Author: Lau Tsz Hei Darren
 **/

// Set up 1st stage of configurations
var exports = module.exports = {

    //Used in multiple scripts
    _sCONFIG_DIR: "@@_sCONFIG_DIR", //Config file directory. Please use absolute directory.
    _sLOG_DIR: "@@_sLOG_DIR", //Log file directory. Please use absolute directory.
    _sSiteId: "@@ET_sSITE_ID", //Site ID of the project

    _iSchedulingInterval: parseInt("@@ET_iSCHEDULINGINTERVAL"), //30 or 15 minutes

    //Used in server_logger
    _iLOG_FILE_MAXSIZE: parseInt('@@_iLOG_FILE_MAXSIZE'), //Logfile maximum size
    _iLOG_FILE_COUNT: parseInt('@@_iLOG_FILE_COUNT'), //Logfile maximum count

    //Used in db_connection
    mongoURI: "@@ET_sMONGODB_URL", //URL of MongoDB
    _iMONGOOSE_RECONNECT_TIMEOUT: parseInt("@@_iMONGOOSE_RECONNECT_TIMEOUT"), //Connect timeout for MongooseJS (in ms)

    //Used in mqtt_client
    _iMQTT_RECONNECT_UNIT: parseInt('@@_iMQTT_RECONNECT_UNIT'), //Attempt reconnect for every 4 seconds
    _iMQTT_RECONNECT_LIMIT: parseInt('@@_iMQTT_RECONNECT_LIMIT'), //Abondon to reconnect after 2 weeks

    //Used in load_xml
    //Loaded by express_host
    XML_holidayDates: 'schedule/holidayDates.xml', //File containing holiday dates for scheduling
    //XML_defaultSchedules: 'schedule/defaultSchedules.xml', //File containing scheduling rules
    //Loaded by mqtt_control
    _sConfig_XML_filename: function (_sClient_id) {
        return 'plc/mqtt-modbus_pun_' + _sClient_id + '.xml'; //Config file name.
    },

    //Used in mosca_custom
    _iMosca_read_interval: parseInt("@@_iMOSCA_READ_INTERVAL"), //Read interval of Mosca server
    ET_iMOSCA_PORT_MQTT: parseInt("@@ET_iMOSCA_PORT_MQTT"), //Port number for Mosca server in MQTT protol
    ET_iMOSCA_PORT_HTTP: parseInt("@@ET_iMOSCA_PORT_HTTP"), //Port number for Mosca server in HTTP protol
    ET_sMOSCA_HTTP_HOST_DIR: '../../../node_modules/mosca', //Or point to the mosca modbule directory
    _iDEVICE_STATUS_TIMEOUT: parseInt('@@_iDEVICE_STATUS_TIMEOUT'), //Timeout of device status (To determine if the modbus_control is hanged)

    //Used in modbus_control
    //Connect timeout for Modbus. It's a function under UNIT, LIMIT and VARIANCE (in ms)
    _iRetry_delay_unit: parseInt('@@_iMODBUS_RECONNECT_UNIT'), //5 seconds, expected value
    _iRetry_delay_limit: parseInt('@@_iMODBUS_RECONNECT_LIMIT'), //600 seconds, expected value
    _iRetry_random_range: parseInt('@@_iMODBUS_RECONNECT_VARIANCE'), //-0.5sec to +0.5sec
    //PRIMARY and SECONDARY port for Modbus 
    _iPrimary_modbus_port: parseInt('@@_iMODBUS_PORT_PRIMARY'), //Hardcoded, non-documented
    _iSecondary_modbus_port: parseInt('@@_iMODBUS_PORT_SECONDARY'), //Hardcoded, non-documented
    //Register configuration under a single PLC
    _iRead_register_from: parseInt('200'), //From document
    _iRead_register_length: parseInt('200'), //From document (399-200+1)
    //Used for Bit to Byte transformation
    _iStart_of_bulb: parseInt('@@_iMODBUS_READ_REGISTER_FROM'), //Not in document (same as the hardcode section in MQTT client)
    _iLength_of_bulb: parseInt('@@_iMODBUS_READ_REGISTER_LENGTH'), //Not in document (same as the hardcode section in MQTT client)

    //DO NOT MODIFY: Limitation under a single PLC
    //Maximum count of register/ coil can be retrieved under a singal access
    //_oModbus_read_unit: {
    //    "coils": parseInt('@@_iMODBUS_READ_COIL_UNIT'),  //ACTUAL LIMIT BY EXTERNAL
    //    "register": parseInt('@@_iMODBUS_READ_REGISTER_UNIT')  //ACTUAL LIMIT BY EXTERNAL
    //},

    _iModbus_poll_interval: parseInt('@@_iMODBUS_POLL_INTERVAL'), //Define the interval in global scope instead from PLC's config
    _oModbus_read_range: {
        "PLC": { //From PLC physical config
            "coils": { from: parseInt('@@_i_DUMP_PLC_COILS_FROM'), to: parseInt('@@_i_DUMP_PLC_COILS_TO'), unit: parseInt("@@_i_DUMP_PLC_COILS_UNIT") },
            "register": { from: parseInt('@@_i_DUMP_PLC_REGISTER_FROM'), to: parseInt('@@_i_DUMP_PLC_REGISTER_TO'), unit: parseInt("@@_i_DUMP_PLC_REGISTER_UNIT") }
        },
        "CoolMasterNet": {  //From CoolMaster's docuement
            "register": { from: parseInt('@@_i_COOLMASTER_REGISTER_FROM'), to: parseInt('@@_i_COOLMASTER_REGISTER_TO'), unit: parseInt("@@_i_COOLMASTER_REGISTER_UNIT") }
        }
    },
    _sCoolMaster_TypeName: "CoolMasterNet", //From Device.client_type in DB 
    _sPLC_TypeName: "PLC", //From Device.client_type in DB 
    _sOverrided_host: "192.168.1.30",

    //Used in delight_control
    _sDelightUdp_TypeName: "DelightController",
    _sDelightUdp_ReadUnit: parseInt("@@_i_DUMP_PLC_COILS_UNIT"),
    //PRIMARY and SECONDARY port for Delight 
    _iPrimary_delight_port: parseInt('@@_iDELIGHT_PORT_PRIMARY'), //Hardcoded, non-documented
    _iSecondary_delight_port: parseInt('@@_iDELIGHT_PORT_SECONDARY'), //Hardcoded, non-documented

    //Used in mqtt_control
    _sModbus_childprocess: '@@_sMODBUS_CHILDPROCESS', //Modbus process taking care of the defined device
    _sDevice_inf_dir: "@@_sDEVICE_INF_DIR", //Device information when offline
    _sZone_inf_dir: "@@_sZONE_INF_DIR", //Zone information when offline

    //Used in express_host
    resource_dir: "@@ET_sRESOURCE_DIR", //Resource file directory. Compiled source files are in __dirname/mode_name/src
    ET_sMOSCA_URL_MQTT: "@@ET_sMOSCA_URL_MQTT", //URL of MQTT Broker. If it is the Mosca server, it should match with its configuration
    ET_sWEBPAGE_DIR: '@@ET_sWEBPAGE_DIR',  //Directory of console webpages. Compiled source files are in __dirname/mode_name/src
    _sCOOKIE_SECRET: '@@_sCOOKIE_SECRET', //Encryption salt for cookies used in host server
    _iCOOKIE_AGE: parseInt('@@_iCOOKIE_AGE'), //Valid time period for Cookie
    _iHOST_PORT: parseInt("@@ET_iHOST_PORT"), //Port number for Express host server
    UPLOAD_LIMIT: {
        //Setting for middleware bodyparser
        JSON: { limit: '2mb' },
        RAW: { limit: '2mb' },
        TEXT: { limit: '2mb' },
        URLENCODED: { limit: '2mb', extended: true }
    },
    //See npm/mysql for more options
    MYSQL_CONNECTION_PARAM: {
        connectionLimit: 16,
        host: '@@ET_sMYSQL_HOST', //'localhost',
        port: parseInt('@@ET_sMYSQL_PORT'), //3306,
        user: '@@ET_sMYSQL_USR', //'root',
        password: '@@ET_sMYSQL_PWD', //'password',
        database: '@@ET_sMYSQL_DB' //'EtagCounterDB'
    },
};

//Set up 2nd stage of configurations. Most of them are based from above.
exports.XML_defaultSchedules = 'schedule/defaultSchedules_' + exports._iSchedulingInterval + '.xml'; //File containing scheduling rules
exports._oModbus_connection_config = function (param) {
    return {
        host: param.host,
        port: param.port,
        'autoReconnect': true,
        'reconnectTimeout': exports._iRetry_delay_limit, //exports._iRetry_delay_unit,
        'timeout': exports._iRetry_delay_unit,
        'logEnabled': param.debug_mode,
        'logLevel': param.debug_mode ? 'debug' : 'info',
        'logTimestamp': true,
        'unitId': 1 //Must greater then 0. Since It is controlled by different users, this can be constant
    };
};