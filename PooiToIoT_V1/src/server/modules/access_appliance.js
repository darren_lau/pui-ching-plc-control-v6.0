"use strict";
var oDataBase = require('./db_connection');
var logger = {};

var init = function (l) { logger = l; };

var addAppl = function (req, res) {
 //logger.log('debug', JSON.stringify(req.body));
    //Although some validation is done in UI, here will do it also
    //dI: { zone_inf, zone_ref, appl_inf, appl_ref }
    if (!(req.body.zone_ref && req.body.zone_ref.zoneId && req.body.zone_ref.client_id)) {
        logger.log('debug', "400 !zone_ref");
        res.status(400).send("Post body not complete."); return;
    } else if (!(req.body.appl_inf && req.body.appl_inf.appl_name && req.body.appl_inf.appl_type && req.body.appl_inf.appl_index >= 0)) {
        logger.log('debug', "400 !appl_inf");
        res.status(400).send("Post body not complete."); return;
    } else {
        //Note: UI check for zoneId but server check for client_id
        var query_obj = { client_id: req.body.zone_ref.client_id };
        oDataBase.Device.find(query_obj, function (err, docs) {
            if (err) { logger.log('error', err); res.status(500).send(err); return; }
            else if (docs.length === 0) {
                logger.log('debug', "400 !docs");
                res.status(400).send("Target client not found."); return;
            } else {
                var obj;
                var hit_sth = false;
                for (var i1 = 0; i1 < docs.length; i1++) {
                    if (docs[i1].zoneId === req.body.zone_ref.zoneId) {
                        obj = docs[i1];
                        for (var i2 = 0; i2 < obj.appliances.length; i2++) {
                            if (obj.appl_name === req.body.appl_inf.appl_name) {
                                hit_sth = true;
                                logger.log('debug', "400 hit_sth");
                                res.status(400).send("Target name not unique under target zone"); return;
                            }
                        }
                    }
                    for (var i0 = 0; i0 < docs[i1].appliances.length; i0++) {
                        if (docs[i1].appliances[i0].appl_index === req.body.appl_inf.appl_index) {
                            hit_sth = true;
                            logger.log('debug', "400 hit_sth");
                            res.status(400).send("Target index not unique under target device"); return;
                        }
                    }
                }
                if (!hit_sth) {
                    if (!obj) {
                        logger.log('debug', "400 !obj");
                        res.status(400).send("Target zone not found."); return;
                    } else {
                        obj.appliances.push({
                            appl_name: req.body.appl_inf.appl_name,
                            appl_type: req.body.appl_inf.appl_type,
                            appl_index: req.body.appl_inf.appl_index
                        });
                        obj.save(function (err) {
                            if (err) { logger.log('error', err); res.status(500).send(err); return; }
                            else { res.status(200).send('Appl added successfully'); }
                        });
                    }
                }
            }
        });
    }
};

var editAppl = function (req, res) {
	//logger.log('debug', JSON.stringify(req.body));
    //Although some validation is done in UI, here will do it also
    //dI: { zone_inf, zone_ref, appl_inf, appl_ref }
    if (!(req.body.zone_ref && req.body.zone_ref.zoneId && req.body.zone_ref.client_id)) {
        logger.log('debug', "400 !zone_ref");
        res.status(400).send("Post body not complete."); return;
    } else if (!(req.body.appl_ref && req.body.appl_ref.appl_name)) {
        logger.log('debug', "400 !appl_ref");
        res.status(400).send("Post body not complete."); return;
    } else if (!(req.body.appl_inf && req.body.appl_inf.appl_name && req.body.appl_inf.appl_type && req.body.appl_inf.appl_index >= 0)) {
        logger.log('debug', "400 !appl_inf");
        res.status(400).send("Post body not complete."); return;
    } else {

        //Note: UI check for zoneId but server check for client_id
        var query_obj = { client_id: req.body.zone_ref.client_id };
        oDataBase.Device.find(query_obj, function (err, docs) {
            if (err) { logger.log('error', err); res.status(500).send(err); return; }
            else if (docs.length === 0) {
                logger.log('debug', "400 !docs");
                res.status(400).send("Target client not found."); return;
            } else {
                var obj;
                var hit_sth = false;
                for (var i1 = 0; i1 < docs.length; i1++) {
                    if (docs[i1].zoneId === req.body.zone_ref.zoneId) {
                        obj = docs[i1];
                        for (var i3 = 0; i3 < obj.appliances.length; i3++) {
                            //logger.log('debug', obj.appliances[i3].appl_name + '-' + req.body.appl_ref.appl_name);
                            if (obj.appliances[i3].appl_name === req.body.appl_ref.appl_name) {
                                logger.log('debug', "Found ref - deleted");
                                obj.appliances.splice(i3, 1);
                            }
                        }
                        for (var i2 = 0; i2 < obj.appliances.length; i2++) {
                            //logger.log('debug', obj.appliances[i2].appl_name + '-' + req.body.appl_ref.appl_name);
                            if (obj.appliances[i2].appl_name === req.body.appl_inf.appl_name) {
                                hit_sth = true;
                                logger.log('debug', "400 hit_sth");
                                res.status(400).send("Target name not unique under target zone"); return;
                            }
                        }
                    }
                    //for (var i0 = 0; i0 < docs[i1].appliances.length; i0++) {
                    //    if (docs[i1].appliances[i0].appl_index == req.body.appl_inf.appl_index) {
                    //        hit_sth = true;
                    //        logger.log('debug', "400 hit_sth");
                    //        res.status(400).send("Target index not unique under target device"); return;
                    //    }
                    //}
                }
                if (!hit_sth) {
                    if (!obj) {
                        logger.log('debug', "400 !obj");
                        res.status(400).send("Target zone not found."); return;
                    } else {
                        obj.appliances.push({
                            appl_name: req.body.appl_inf.appl_name,
                            appl_type: req.body.appl_inf.appl_type,
                            appl_index: req.body.appl_inf.appl_index
                        });
                        obj.save(function (err) {
                            if (err) { logger.log('error', err); res.status(500).send(err); return; }
                            else { res.status(200).send('Appl edited successfully'); }
                        });
                    }
                }
            }
        });
    }
};

var deleteAppl = function (req, res) {
	//logger.log('debug', JSON.stringify(req.body));
    //Although some validation is done in UI, here will do it also
    //dI: { zone_inf, zone_ref, appl_inf, appl_ref }
    if (!(req.body.zone_ref && req.body.zone_ref.zoneId)) {
        logger.log('debug', "400 !zone_ref");
        res.status(400).send("Post body not complete."); return;
    } else if (!(req.body.appl_ref && req.body.appl_ref.appl_name)) {
        logger.log('debug', "400 !appl_ref");
        res.status(400).send("Post body not complete."); return;
    } else {
        //Note: Delete is zone issue only
        var query_obj = { zoneId: req.body.zone_ref.zoneId };
        //logger.log('debug', "START");
        oDataBase.Device.find(query_obj, function (err, docs) {
            if (err) { logger.log('error', err); res.status(500).send(err); return; }
            else if (docs.length === 0) {
                logger.log('debug', "400 !docs");
                res.status(400).send("Target client not found."); return;
            } else {
                //logger.log('debug', "POI");
                //Should be one since zoneId is unique
                var obj = docs[0];
                //logger.log('debug', JSON.stringify(obj));

                var hit_sth = false;
                for (var i0 = 0; i0 < obj.appliances.length; i0++) {
                    //logger.log('debug', obj.appliances[i0].appl_name + '-' + req.body.appl_ref.appl_name);
                    if (obj.appliances[i0].appl_name === req.body.appl_ref.appl_name) {
                        hit_sth = true;
                        obj.appliances.splice(i0, 1);
                        //logger.log('debug', JSON.stringify(obj.appliances));
                        //logger.log('debug', 'hit');
                        //obj.appliances[i0].remove(function (err) {
                        obj.save(function (err) {
                            if (err) { logger.log('error', err); res.status(500).send(err); return; }
                            else { res.status(200).send('Appl deleted successfully.'); return; }
                        });
                    }
                }
                if (!hit_sth) {
                    //logger.log('debug', '!hit_sth');
                    res.status(200).send('Appl not found. Considered as successful.'); return;
                }
            }
        });
    }
};

var exports = module.exports = {
	addAppl: addAppl,
	editAppl: editAppl,
	deleteAppl: deleteAppl,
    init: init
};
