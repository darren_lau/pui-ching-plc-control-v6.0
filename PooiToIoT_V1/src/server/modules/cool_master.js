"use strict";

let async = require('async');
/**
 * Ref: http://www.xdimax.net/wiki/index.php?title=Modbus_IP&oldid=270#Mode
**/
/**
 * Contains every methods related to the Cool Master Modbus:
 *      Internal bitwise function, 
 *      Generating random register values, 
 *      Generating raw array values into 3D values for IPC use
 *      Converting 3D values into field_value patirs based from XML configs
**/
/**
 * From XML
 *      <!-- Coils -->
 *      <!-- <field id="state" type="Integer" offset="0" range="1" not_revit="1"/> -->
 *      <!-- <field id="filter_sign" type="Integer" offset="1" range="1" not_revit="1"/> -->
 *      <!-- Holding Registers -->
 *	    <field id="operation_mode" type="Integer" offset="0" range="1" not_revit="1"/>
 *      <field id="fan_speed" type="Integer" offset="1" range="1" not_revit="1"/>
 *      <field id="set_temperature" type="Integer/10" offset="2" range="1" not_revit="1"/>
 *      <field id="on_off" type="Integer" offset="3" range="1" not_revit="1"/>
 *      <field id="filter" type="Integer" offset="4" range="1" not_revit="1"/>
 *      <field id="swing" type="Integer" offset="5" range="1" not_revit="1"/>
 *      <field id="room_temperature" type="Integer/10" offset="6" range="1" not_revit="1"/>
 *      <field id="failure_code" type="Integer" offset="7" range="1" not_revit="1"/>
 *      <field id="lock_bits" type="Integer" offset="8" range="1" not_revit="1"/>
**/

//Return MQTT publish topic for specific zoneId
function sMqtt_publish_topic(oZone_obj) {
    if (!(oZone_obj && oZone_obj.mqtt_tag)) {
        //logger.log('error', "mqtt_tag is missing for zoneId: " + oZone_obj.zoneId);
        return "ERROR";
    } else {
        return oZone_obj.mqtt_tag + '/PLC/Status';
    }
}

//@Param
//  device_arr: [oAppl_expanded] (see function "insert_status_from_xml")
//@Output callback
//  err: error message
//  arr: [
//      appl_type: oAppl_expanded.appl_type,
//      devices: [oAppl_expanded.appl_status]
//  ]
let rearrange_arr = function (device_arr, callback) {
    //array - map - array operation
    let map = {};
    let arr = [];
    async.each(device_arr, (device_obj, cb_in) => {
        if (!map[device_obj.appl_type]) {
            map[device_obj.appl_type] = {
                appl_type: device_obj.appl_type,
                devices: []
            };
        }
        map[device_obj.appl_type].devices.push(device_obj.appl_status);
        cb_in(null);
    }, (err) => {
        if (err) { return callback(err, null); }
        else {
            async.each(map, (appl_obj, cb_in) => {
                arr.push(appl_obj);
                cb_in(null);
            }, (err) => {
                return err ? callback(err, null) : callback(null, arr);
            });
        }
    });
};

//@param
//  oAppl_raw: {
//      appl_name: String,
//      appl_type: String,
//      appl_index: Number
//  }
//  oCoolMaster_status: 3D array [panel][AC#][field_value]
//  oConfig: From xml. Passed from "mqtt_control"
//@Output callback
//  err: error message
//  oAppl_expanded: oAppl_raw + { appl_status: { field: value } }
let insert_status_from_xml = function (logger, oAppl_raw_external, oCoolMaster_status, oConfig, callback) {
    //To make appending objects effective, initialize the appl_status first
    let oAppl_raw = Object.assign({}, oAppl_raw_external);
    oAppl_raw.appl_status = {};
    let oAppl_status = {};
    let iFound_appl_type = -1;
    let iFound_command = -1;

    //Step 2.1: Search for appl_type in config XML file
    for (let i1 = 0; i1 < oConfig.type.length; i1++) {
        /**
        logger.log('debug', "config: " + oConfig.type[i1].$.id +
            ", oModbus_inf: " + oAppl_arr[i0].appl_type + '.');
            **/
        if (oConfig.type[i1].$.id === oAppl_raw.appl_type) {
            iFound_appl_type = i1;
            //Step 2.2: Search for command "read" under appl_type
            for (let i2 = 0; i2 < oConfig.type[i1].command.length; i2++) {
                //logger.log('debug', oConfig.type[i1].command[i2].$.type);
                if (oConfig.type[i1].command[i2].$.type === "read") {
                    iFound_command = i2;
                    let err_va = [];
                    //Step 2.3: Append the appl_status object with field entries
                    for (let i3 = 0; i3 < oConfig.type[i1].command[i2].field.length; i3++) {
                        //oField_obj = {id, type, offet, range}
                        //Validation of oField_obj has been done before
                        let oField_obj = oConfig.type[i1].command[i2].field[i3].$;
                        let iIndex_target = oAppl_raw.appl_index + parseInt(oField_obj.offset);
                        let iField_raw_value;
                        let arr_map;
                        // logger.log('debug', "oField_obj=" + JSON.stringify(oField_obj));
                        try {
                            //logger.log('debug', iIndex_target);
                            //It's convinent to determine it is D (register) or Q (coil) since D >= 200 and Q < 128
                            //if (iIndex_target >= 200) { iField_raw_value = iRegister_value[iIndex_target - _iRead_register_from]; }
                            //else if (iIndex_target < 128) { iField_raw_value = iCoil_value[iIndex_target]; }
                            arr_map = va_to_arr(iIndex_target);
                            if (oCoolMaster_status[arr_map[0]][arr_map[1]] !== null) {
                                iField_raw_value = oCoolMaster_status[arr_map[0]][arr_map[1]][arr_map[2]];
                            } else {
                                err_va.push(iIndex_target); continue;
                            }
                        } catch (e) { return callback("fAnalysis_zone_status: Target index is not supported: " + iIndex_target, null); }
                        if (isNaN(parseInt(iField_raw_value))) { return callback("fAnalysis_zone_status: iField_raw_value is invalid (" + iField_raw_value + ")", null); }
                        //Convert raw value into desired field value
                        let nField_value;
                        if (oField_obj.type === "Integer") { nField_value = parseInt(iField_raw_value); }
                        if (oField_obj.type === "Integer/10") { nField_value = parseInt(iField_raw_value) / 10; }
                        if (oField_obj.type === "Integer/100") { nField_value = parseInt(iField_raw_value) / 100; }
                        //Finally append the appl_status (with appl_name for convinence)
                        oAppl_status[oField_obj.id] = nField_value;
                        oAppl_status['appl_name'] = oAppl_raw.appl_name;
                        //logger.log('info', JSON.stringify(oAppl_status));
                        //logger.log('info', "I was here");
                    }
                    if (err_va.length > 0) { logger.log('error', 'fAnalysis_zone_status: Cannot access the following address: ' + err_va); }
                }
            }
            if (iFound_command < 0) {
                return callback('fAnalysis_zone_status: Command "read" not supported for the type "' + oType_obj.appl_type + '" yet', null);
            }
            //Append appl_status to oAppl_arr
            oAppl_raw.appl_status = Object.assign({}, oAppl_status);
            //oAppl_arr.push(oAppl_obj);
            //logger.log('info', JSON.stringify(oAppl_arr[i0]));
            return callback(null, oAppl_raw);
        }
    }
    if (iFound_appl_type < 0) {
        return callback("fAnalysis_zone_status: appl_type " + oAppl_raw.appl_type + " not match", null);
    }
};

//@Param (both schema a located above)
//  oModbus_inf_obj: oModbus_inf[siteId][client_id]
//  sCoolMaster_status: 3D array [panel][AC#][field_value]
//  oConfig: From config/plc/*.xml - see "./plc_config_validation" for details
//@Output callback
//  err: error messages
//  mqtt_arr:[{
//      sTopic: String,
//      sMessage: String
//  }]
function fAnalysis_coolmaster_status(logger, oModbus_inf_obj, sCoolMaster_status, oConfig, callback) {
    //Step 1: Validation with initialization
    let oCoolMaster_status = {};
    try {
        oCoolMaster_status = sCoolMaster_status; //JSON.parse(sCoolMaster_status);
        //logger.log('debug', JSON.stringify(oCoolMaster_status));
    } catch (e) {
        return callback("fAnalysis_zone_status: message is not JSON", null);
    }

    if (!(oModbus_inf_obj && oModbus_inf_obj.client_id)) { return callback("fAnalysis_zone_status: client_id is missing!", null); }
    if (!oCoolMaster_status) { return callback(`fAnalysis_zone_status: preprocessed_oDevice_Status (${oModbus_inf_obj.client_id}) is not complete`, null); }

    let oTarget_modbus = Object.assign({}, oModbus_inf_obj);

    let oMQTT_Messages = []; // [{sTopic, sMessage}]

    //Step 2: Loop through all zones all appliances
    async.each(oModbus_inf_obj.zones, (oZone_inf, cb_one) => {
        //logger.log('debug', oZone_inf);
        let oAppliances = {};
        //var i = 0;
        let device_arr = [];
        async.each(oZone_inf.appliances, (oAppl_raw, cb_two) => {
            //logger.log('debug', oAppl_raw);
            //Step 3: Inject the values based from oConfig
            insert_status_from_xml(logger, oAppl_raw, oCoolMaster_status, oConfig, (err, oAppl_expanded) => {
                //logger.log('debug', JSON.stringify(oAppl_expanded));
                //Inject failed. Pop error message, then proceed to the next appliance. Don't stop the process.
                if (err) { return cb_two(err); }
                else if (oAppl_expanded !== null && oAppl_expanded.appl_status) {
                    //Push if the expended appl_status exists
                    //Problem encountered: oAppliances cannot operate well here. Seems assigning values into an array in async mode is bad.
                    //Current solution: Make an array out of this loop, and call another function to operate them.
                    device_arr.push(oAppl_expanded);
                    return cb_two(null);
                } else {
                    return cb_two("uncaught suituation");
                }
                //callback();
            });
        }, (err) => {
            if (err) { return cb_one(err); }
            else {
                //logger.log('debug', device_arr);
                //process.exit(0);

                //Step 3.5: Group the status array into the preferred format. 
                rearrange_arr(device_arr, (err, oAppliance_arr) => {
                    if (err) { return cb_one(err); }
                    else {
                        //logger.log('debug', JSON.stringify(oAppliances));

                        //Step 4: Make Appliance Status based from desired format.
                        let oAppl_Status = {
                            client_id: oModbus_inf_obj.client_id,
                            client_name: oModbus_inf_obj.client_name,
                            client_type: oModbus_inf_obj.client_type,
                            Appliance: oAppliance_arr
                        };
                        if (oAppl_Status.Appliance === null) {
                            return callback("fAnalysis_zone_status: Error when transforming map to array: " + JSON.stringify(oAppliances));
                        }
                        //Step 5: Make MQTT Topics and Messages
                        oMQTT_Messages.push({ sTopic: sMqtt_publish_topic(oZone_inf), sMessage: JSON.stringify(oAppl_Status) });
                        return cb_one(null);
                    }
                });
            }
        });
    }, (err) => {
        //Step 6: return messages to fire out
        return err ? callback(err, null) : callback(null, oMQTT_Messages);
    });
}

//oModbus_inf -> oModbus_cp
//oModbus_inf2 -> device's starting register
//oConfig + oField_Obj -> offset of device's starting register
//oField_value -> actual nInput_value
//Return and fire it to modbus_childprocess
//Error message is the hint of the section
function fAnalysis_set_coolmaster(logger, oConfig, oModbus_inf_obj, sTopic, bMessage, callback) {
    //logger.log('info', 'triggered. nope.'); process.exit(0);
    //Setp 0: Convert them into string since MQTT message's type is uncertain
    let sTopic_str = sTopic;
    let sMessage_str = bMessage;
    if (sTopic_str.constructor.name !== "String") { sTopic_str = sTopic_str.toString(); }
    if (sMessage_str.constructor.name !== "String") { sMessage_str = sMessage_str.toString(); }

    //Step 1: Check MQTT sTopic and bMessage,
    let sTopic_param = sTopic_str.split('/'); //sTopic_param = [siteId,zoneId,"setStatus"]; 
    let sTarget_zoneId = sTopic_param[sTopic_param.length - 2];
    let oWrite_param;
    try { oWrite_param = JSON.parse(sMessage_str); }
    catch (e) { return callback('fAnalysis_set_status: Message is not JSON: ' + sMessage_str); }
    //Step 1.5: added in 150831: for scheudler, if the input is for scheduler use,
    //Route it to another function instead of continuing.
    if (oWrite_param && oWrite_param.command === "SET_SCHEDULER"
        && oWrite_param.value !== null && oWrite_param.sch_index !== null) {
        //Since its value is supposed to be built completey by other scripts, 
        //No database access will be made. This will directly pass the values.
        return callback('fAnalysis_set_status: This client does not handle schedule request: ' + sMessage_str);
    } else if (!(oWrite_param && oWrite_param.appl_type && oWrite_param.appl_name &&
    oWrite_param.command !== null && oWrite_param.value !== null)) {
        return callback('fAnalysis_set_status: message is not complete: ' + sMessage_str);
    }

    //Step 2: Find oAppl_obj, the information of the target appliance
    if (!oModbus_inf_obj.zones[sTarget_zoneId]) {
        return callback('fAnalysis_set_status: Zone ' + sTarget_zoneId + ' not exist in oModbus_inf_obj');
    } else {
        async.filter(oModbus_inf_obj.zones[sTarget_zoneId].appliances, (oAppl_obj, callback1) => {
            return callback1(null, oAppl_obj.appl_name === oWrite_param.appl_name && oAppl_obj.appl_type === oWrite_param.appl_type);
        }, (err, results) => {
            if (err) { return callback(err); }
                //logger.log('debug', JSON.stringify(results));
                //else if (results.length < 1) {
            else if (!results) {
                logger.log('debug', 'catch 0');
                return callback("fAnalysis_set_status: Appliance not found in this PLC. It is normal when multiple PLCs are assigned to a single zone.");
            } else if (results.length < 1) {
                logger.log('debug', 'catch 1');
                return callback("fAnalysis_set_status: Appliance not found in this PLC. It is normal when multiple PLCs are assigned to a single zone.");
            } else {
                //Suppose only 1 is found. So return the first found.
                let oAppl_obj = results[0];

                //Step 3: Find oField_obj, the information of the command field
                let oField_obj;
                for (let i1 = 0; i1 < oConfig.type.length; i1++) {
                    if (oConfig.type[i1].$.id === oWrite_param.appl_type) {
                        for (let i2 = 0; i2 < oConfig.type[i1].command.length; i2++) {
                            if (oConfig.type[i1].command[i2].$.type === "write") {
                                for (let i3 = 0; i3 < oConfig.type[i1].command[i2].field.length; i3++) {
                                    if (oConfig.type[i1].command[i2].field[i3].$.id === oWrite_param.command) {
                                        oField_obj = oConfig.type[i1].command[i2].field[i3].$;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
                if (!oField_obj) {
                    return callback('fAnalysis_set_status: Command "' + oWrite_param.command +
                        '" not supported for the type "' + oWrite_param.appl_type + '" yet');
                }
                if (!oField_obj.offset) {
                    return callback('fAnalysis_set_status: oConfig.type[i1].command[i2].field[i3].$.offset is missing!');
                }
                //logger.log('info', JSON.stringify(oField_obj)); process.exit(0);

                //Step 4: Modify the input value if required
                let nInput_value;
                if (oField_obj.type === "Integer") { nInput_value = parseInt(oWrite_param.value); }
                else if (oField_obj.type === "Integer/10") { nInput_value = parseInt(oWrite_param.value) / 10; }
                else if (oField_obj.type === "Integer*10") { nInput_value = parseInt(oWrite_param.value * 10); }
                else if (oField_obj.type === "Integer/100") { nInput_value = parseInt(oWrite_param.value) / 100; }
                if (!(nInput_value >= 0)) {
                    return callback('fAnalysis_set_status: Input value not valid. type:"' + oField_obj.type +
                        '", value: "' + oWrite_param.value + '"');
                }

                //Step 5: Preparing write objects to return (Coil)
                let oWrite_obj = null;
                let oWrite_obj2 = null;
                let iRegister_base = oAppl_obj.appl_index;
                let iRegister_offset = oField_obj.offset;
                let iTarget_register = parseInt(iRegister_base) + parseInt(iRegister_offset);
                oWrite_obj = {
                    client_id: oModbus_inf_obj.client_id,
                    sCommand: 'write',
                    target_register: iTarget_register,
                    target_value: nInput_value,
                    tid: new Date().getTime()
                };
                //if (iTarget_register < 128) { oWrite_obj.option = "coil" } //Exceptional case

                /**
                //Step 5.1: Preparing write objects to return (Register)
                if (oWrite_param.appl_type == '') {
                    var iTarget_register2 = (parseInt(iRegister_base) + parseInt(iRegister_offset) + 3);
                    var oWrite_obj2 = {
                        sCommand: 'write',
                        target_register: iTarget_register2,
                        target_value: 1,
                        tid: new Date().getTime()
                    }
                }
                **/

                return callback(null, oWrite_obj, oWrite_obj2);
            }
        });
    }
}

let gen_operation_mode = function () {
    var t = Math.round(Math.random() * 11);
    return t !== 7 ? t : gen_operation_mode();
};

let gen_fan_speed = function () {
    var t = Math.round(Math.random() * 9);
    return t !== 6 ? t : gen_fan_speed();
};

let gen_set_tmp = function () {
    var t = Math.round(Math.random() * 100);
    return t + 180; //180 ~ 280
};

let gen_on_off = function () {
    return Math.random() >= 0.5 ? 1 : 0;
};

let gen_filter = function () {
    return Math.random() >= 0.5 ? 1 : 0;
};

let gen_swing = function () {
    return Math.round(Math.random() * 6);
};

let gen_room_temperature = function () {
    var t = Math.round(Math.random() * 240);
    return t + 80; //80 ~ 320
};

let gen_failure_code = function () {
    var t = Math.random();
    return t < 0.125 ? 0 : Math.round(Math.random() * 127); //Only 0 is OK - currently no error handle is required
};

let gen_lock_bits = function () {
    return Math.round(Math.random() * 255); //Only 0 is free - currently no error handle is required
};

let gen_random_arr = function () {
    //9-15 are reserved
    return [
        gen_operation_mode(),
        gen_fan_speed(),
        gen_set_tmp(),
        gen_on_off(),
        gen_filter(),
        gen_swing(),
        gen_room_temperature(),
        gen_failure_code(),
        gen_lock_bits(),
        0,
        0,
        0,
        0,
        0,
        0,
        0
    ];
};

let reg_length = 16;

let va_to_arr = function (address) {
    //0xabcd -> [a,b,c,d]
    let tmp = [
        address >> 12,
        (address >> 8) % 16,
        (address >> 4) % 16,
        address % 16
    ];
    return [tmp[0], tmp[1] * 16 + tmp[2], tmp[3]];
};

var exports = module.exports = {
    gen_random_arr: gen_random_arr,
    reg_length: reg_length,
    va_to_arr: va_to_arr,
    fAnalysis_coolmaster_status: fAnalysis_coolmaster_status,
    fAnalysis_set_coolmaster: fAnalysis_set_coolmaster
};
//console.log(va_to_arr(0x1a2b));