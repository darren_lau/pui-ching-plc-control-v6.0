"use strict";

//Standalone: [node] [delight_client.js] [brightness] [ip_address]

/**
 * External libraries (npm/node)
 */
const dgram = require('dgram');
const util = require('util');

/**
* Self-made libraries
*/
const udp_pkg_gen = require("./delight_udp_pkg");
const server_config = require('./server_config.js'); //Custom settings
const server_logger = require('./server_logger.js');

const READ_UNIT = server_config._sDelightUdp_ReadUnit; //128 - read_device_network must generate all zone_appl data under device. May change if Delight have some huge address system.

const sleep = ms => new Promise(r => setTimeout(r, ms))

/**
* Constants
*/
const _sTarget_client_id = process.argv[2] ? process.argv[2] : process.env.ETAG_PLC_CLIENT_ID; //client_id assigned for this MQTT Client (170919) //Process arguement: client_id for taking care
const _bDebug_mode = process.argv.indexOf("debug") >= 0; //If true, spam the console with debug messages
const _bOffline_mode = process.argv.indexOf("offline") >= 0; //If true, no Modbus device will be connected. Outputs are generated instead
const _bAsMain = process.argv[1].includes("delight_client"); // If true, it will act as standalone script with no connection to other scripts.
const standalone_ip = process.argv[3] ? process.argv[3] : "10.0.223.45";

/**
 * Internal global objects
 */
let logger = server_logger.SERVER_LOGGING(`${server_config._sLOG_DIR}/delight_client.log`, { level: _bDebug_mode ? "debug" : "info", mute_console: false }); //Logger of this module
let udp_client = null;
let udp_target_ip = null;
let udp_target_port = null;
let on_off = false;
let last_received = new Date();

let set_by_parent = function (a, b, c) {
    //_sTarget_client_id = a;
    //_bDebug_mode = b;
    //_bOffline_mode = c;
    logger = server_logger.SERVER_LOGGING(`${server_config._sLOG_DIR}/modbus_control_${_sTarget_client_id}.log`, { level: _bDebug_mode ? "debug" : "info", mute_console: false }); //Logger of this module
}

let emulate_device_state = {}; //Vendor will make it readable - however currently it can be "assumed" only

//0x32 0 0
const pkg_sample = (a, b) => {
    //return udp_pkg_gen["PKG_GEN"]["SET_LIGHT"](process.argv[2], process.argv[3], process.argv[4]);
    //return udp_pkg_gen["PKG_GEN"]["SET_LIGHT"]("DEV", "DEV", "DEV", Math.floor(Math.random() * 100), 0x00, 0x00);
    //on_off = !on_off;
    //return udp_pkg_gen["PKG_GEN"]["SET_LIGHT"](on_off * 100, 0x00, 0x00);
    return udp_pkg_gen["PKG_GEN"]["SET_LIGHT"]("0726", "0726", a ? a : process.argv[2], b ? b : process.argv[3], 0x00, 0x00);
};

const pkg_scan = () => {
    return udp_pkg_gen["PKG_GEN"]["SCAN"]();
}

const make_cmd_pkg = function (addr, val) {
    //return udp_pkg_gen["PKG_GEN"]["SET_LIGHT"]("0622", "0622", addr, val ? 0x64 : 0x00, 0x00, 0x00);
    return udp_pkg_gen["PKG_GEN"]["SET_LIGHT"]("0726", "0726", addr, val, 0x00, 0x00);
};

const first_packet = function () {
    return udp_pkg_gen["PKG_GEN"]["SUBNET_INFO"]("0726");
}

const p_udpsend = async (client, a, b, c) => {
    return new Promise((t, f) => {
        if (client) {
            client.send(a, b, c, (e) => {
                if (e) { f(e); }
                else { t(); }
            });
        } else {
            t();
        }
    });
};

const set_client = (oTarget_modbus, onConnect, onDisconnect) => {
    //o.zoneId = oTarget_modbus ? oTarget_modbus.zoneId : "";
    //o.ip_addr = oTarget_modbus ? oTarget_modbus.ip_addr : "";

    logger.log("debug", "set_client()");
    //logger.log("debug", { oTarget_modbus });

    udp_target_ip = oTarget_modbus ? oTarget_modbus.ip_addr : null;
    udp_target_port = (oTarget_modbus && oTarget_modbus.port) ? oTarget_modbus.port : server_config._iPrimary_delight_port;

    udp_client = dgram.createSocket('udp4');

    //client.send(message, 9988, '192.168.1.150', (err) => {
    //    client.close();
    //});

    udp_client.on('error', (err) => {
        logger.log("error", err);
        client.close();
    });

    udp_client.on('message', (msg, rinfo) => {
        let now = new Date();
        let delay = now.getTime() - last_received.getTime();
        last_received = now;
        logger.log(_bAsMain ? "info" : "debug", JSON.stringify({ msg, rinfo, delay }));
        //client.close();
    });

    udp_client.on('listening', () => {
        logger.log("debug", "udp_client.on('listening')");
        onConnect();
    });

    udp_client.on('close', () => {
        logger.log("debug", "udp_client.on('close')");
        onDisconnect();
    });

    //Not sure why but seems I must fire a message instantly
    p_udpsend(udp_client, first_packet(), udp_target_port, udp_target_ip).then(() => {
        logger.log("info", "First Packet is fired to device successfully.")
    }).catch((err) => {
        logger.log("error", err);
    });

    //logger.log("info", udp_client.address());
    //onConnect();
    //client.close();
};

let is_delight_device = (client_type) => {
    return client_type == server_config._sDelightUdp_TypeName;
};

//See mosca_custom. It has been wrapped in modbus protocol. Luckily this UDP connection is just a subset of it.
let parse_and_write_cmd = function (d_cmd) {
    return new Promise((resolve, reject) => {
        // {"_id":"59c8af2664566bc0b8dfd658","client_id":"PLC_2a","siteId":"PuiChing_P","__v":0,"last_completed":null,"last_issued":"2017-09-25T07:24:22.424Z","modbus_fc":5,"fc_content":{"address":0,"value":[1]}}
        logger.log('debug', "Command found: " + JSON.stringify(d_cmd));
        let ModbusFn = null;
        if (_bOffline_mode) { resolve(); } //Skip if in offline mode
        if (!udp_client) { reject("udp_client is not found!"); }
        else if (!(d_cmd.modbus_fc && d_cmd.fc_content && d_cmd.fc_content.address >= 0 && d_cmd.fc_content.value && d_cmd.fc_content.value.length > 0)) {
            reject("Device_Command is invalid!");
        } else {
            //logger.log('debug', [d_cmd.fc_content.address, d_cmd.fc_content.value[0]]);
            //Resolve { resp }
            switch (d_cmd.modbus_fc) {
                case 5:
                case 6:
                    //Button = coil, Slider = register, however they have no difference. yay.
                    p_udpsend(udp_client, make_cmd_pkg(d_cmd.fc_content.address, d_cmd.fc_content.value[0]), udp_target_port, udp_target_ip).then(() => {
                        logger.log("debug", "Commend Sent to Device.");
                        emulate_device_state[d_cmd.fc_content.address + ""] = d_cmd.fc_content.value[0]; resolve();
                    }).catch(reject); break;
                default: reject(`Device_Command.modbus_fc (${d_cmd.modbus_fc}) is not supported!`);
            }
        }
    });
};

const bModbus_connected = true;

let makePartialDeviceStatus = function (_oIDObj, oTarget_modbus, resp) {
    let o = Object.assign({}, _oIDObj);
    o.coils = resp;
    o.register = [];
    o.zoneId = oTarget_modbus ? oTarget_modbus.zoneId : "";
    o.ip_addr = oTarget_modbus ? oTarget_modbus.ip_addr : "";
    o.connect_state = bModbus_connected || _bOffline_mode ? "connected" : udp_client ? "disconnected" : "undefined"; //
    o.last_updated = new Date();
    o.client_pid = process.pid;
    logger.log("debug", [resp, o]);
    return o;
};

let LocalDeviceStatus = (a, b) => { return { skip: a, resp: b }; } //Promise return 1 object only

//Called by RW loop. onData must be called explictly.
let read_device_network = function (oTarget_modbus, onData) {
    return new Promise((resolve, reject) => {
        logger.log("debug", JSON.stringify(oTarget_modbus));
        let arr = [];
        for (let i = 0; i < READ_UNIT; i++) {
            arr.push({
                "address": i,
                "value": _bOffline_mode ? (Math.random() >= 0.5 ? Math.floor(Math.random() * 100) : 0) : (emulate_device_state[i + ""] ? (isNaN(parseInt(emulate_device_state[i + ""])) ? 1 : parseInt(emulate_device_state[i + ""])) : 0)
            });
        }
        //logger.log("debug", arr);
        onData(LocalDeviceStatus(false, arr));
        resolve();
    });
};

const main = async () => {
    logger.log("info", "Demo mode: " + new Date());
    let dummy_cb = function (msg) {
        return function () { logger.log("debug", msg); }
    }
    if (!udp_client) {
        set_client({ ip_addr: standalone_ip, port: 9988 }, dummy_cb("onConnect"), dummy_cb("onDisconnect"));
    }
    //let err = await p_udpsend(udp_client, pkg_scan(), 1500, '255.255.255.255');
    let err = null;

    let a = []; for (let i = 0; i < 49; i++) { a.push(i); }

    let heheXD = () => {
        return process.argv[2];//parseInt(process.argv[2]);
        //return Math.floor(Math.random()*60 + 0);
    };

    await a.reduce((p, m) => {
        return p.then(() => {
            return sleep(120);
        }).then(() => {
            return p_udpsend(udp_client, pkg_sample(m, heheXD()), udp_target_port, udp_target_ip);
        }).catch((e) => {
            logger.log("error", e);
            err = e;
        });
    }, Promise.resolve());

    if (err) {
        logger.log("error", err);
    }
    //setTimeout(() => { udp_client.close(); }, 1000);
    //if (err) console.log(err);
};

let exit_delight = function () {
    if (udp_client)
        udp_client.close();
};

if (_bAsMain) {
    setInterval(main, 10000);
    main().then(() => { logger.log("info", "Process started. Please watch for return traffic."); }).catch((err) => { logger.log("error", err); });
}
//console.log(pkg_sample());

var exports = module.exports = {
    LocalDeviceStatus,
    makePartialDeviceStatus,
    is_delight_device,
    set_client,
    set_by_parent,
    parse_and_write_cmd,
    read_device_network,
    exit_delight
};