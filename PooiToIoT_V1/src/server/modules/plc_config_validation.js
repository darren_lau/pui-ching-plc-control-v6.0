"use strict";
/**
 * Config and DB-related section
 */

//After reading the XML file and pointing the global oConfig,
//Read the details one by one with validation
function fApply_config(logger, oConfig, callback) {
    //Redundancy checking...
    logger.log('debug', "Start reading config info...");
    if (oConfig) { logger.log('debug', "object mqtt_client: " + oConfig); }
    else { return callback(false, '$'); }
    logger.log('debug', "MQTT Client information:");
    if (oConfig.$.version) { logger.log('debug', "mqtt client version: " + oConfig.$.version); }
    else { return callback(false, '$.version'); }
    if (oConfig.server && oConfig.server.length > 0) { logger.log('debug', "Mosca server hostname: " + oConfig.server[0]); }
    else { return callback(false, 'server'); }
    if (oConfig.port && oConfig.port.length > 0) { logger.log('debug', "Mosca server port: " + oConfig.port[0]); }
    else { return callback(false, 'port'); }
    if (oConfig.siteid && oConfig.siteid.length > 0) { logger.log('debug', "siteid: " + oConfig.siteid[0]); }
    else { return callback(false, 'siteid'); }
    /**
    if (oConfig.mqttclient_id && oConfig.mqttclient_id[0]) { logger.log('debug', "mqttclient_id: " + oConfig.mqttclient_id[0]); }
    else { return callback(false, 'mqttclient_id'); }
    **/
    if (oConfig.poll_period) { logger.log('debug', "poll_period: " + parseInt(oConfig.poll_period)); }
    else { return callback(false, 'poll_period'); }

    logger.log('debug', "Applicance information:");
    if (oConfig.type) {
        for (var i0 = 0; i0 < oConfig.type.length; i0++) {
            if (oConfig.type[i0].$.id)
                logger.log('debug', "\tApplicance type " + i0 + ": " + oConfig.type[i0].$.id);
            else { return callback(false, 'type[i0].$.id'); }

            logger.log('debug', "\tApplicance commands:");
            for (var i1 = 0; i1 < oConfig.type[i0].command.length; i1++) {
                if (oConfig.type[i0].command[i1].$.type)
                    logger.log('debug', "\t\tCommand type " + i1 + ": " + oConfig.type[i0].command[i1].$.type);
                else { return callback(false, 'type[i0].command[i1].$.type'); }
                if (oConfig.type[i0].command[i1].$.range)
                    logger.log('debug', "\t\tCommand range " + i1 + ": " + oConfig.type[i0].command[i1].$.range);
                else { return callback(false, 'type[i0].command[i1].$.range'); }

                logger.log('debug', "\t\tCommand fields:");
                for (var i2 = 0; i2 < oConfig.type[i0].command[i1].field.length; i2++) {
                    if (oConfig.type[i0].command[i1].field[i2].$.id)
                        logger.log('debug', "\t\t\tfield " + i2 + " id: " + oConfig.type[i0].command[i1].field[i2].$.id);
                    else { return callback(false, 'type[i0].command[i1].field[i2].$.id'); }
                    if (oConfig.type[i0].command[i1].field[i2].$.type)
                        logger.log('debug', "\t\t\tfield " + i2 + " type: " + oConfig.type[i0].command[i1].field[i2].$.type);
                    else { return callback(false, 'type[i0].command[i1].field[i2].$.type'); }
                    if (oConfig.type[i0].command[i1].field[i2].$.offset)
                        logger.log('debug', "\t\t\tfield " + i2 + " offset: " + oConfig.type[i0].command[i1].field[i2].$.offset);
                    else { return callback(false, 'type[i0].command[i1].field[i2].$.offset'); }
                    if (oConfig.type[i0].command[i1].field[i2].$.range)
                        logger.log('debug', "\t\t\tfield " + i2 + " range: " + oConfig.type[i0].command[i1].field[i2].$.range);
                    else { return callback(false, 'type[i0].command[i1].field[i2].$.range'); }

                    logger.log('debug', "\t\t\t");
                }
                logger.log('debug', "\t\t");
            }
            logger.log('debug', "\t");
        }
    }
    callback(true, null);
}

var exports = module.exports = {
    fApply_config: fApply_config
}