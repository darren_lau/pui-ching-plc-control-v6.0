"use strict";
// JavaScript source code
// Note: This script no longer capable for direct access modbus. DB connection is assuemed on.
// 170915: Now it is a standalone process with new structure.
// 160706: This is focused on PLC connection - no serious modification is needed

/**
 * Self-made libraries
 */
let server_config = require('./modules/server_config.js'); //Custom settings
let server_logger = require('./modules/server_logger.js');
let db_connection = require('./modules/db_connection.js');
let modbus_client = require('./modules/modbus_client.js');
let delight_client = require('./modules/delight_client.js');

/**
 * Constants
 */
const _sTarget_client_id = process.argv[2] ? process.argv[2] : process.env.ETAG_PLC_CLIENT_ID; //client_id assigned for this MQTT Client (170919) //Process arguement: client_id for taking care
const _bDebug_mode = process.argv.indexOf("debug") >= 0; //If true, spam the console with debug messages
const _bOffline_mode = process.argv.indexOf("offline") >= 0; //If true, no Modbus device will be connected. Outputs are generated instead
const _bOverride_host = process.argv.indexOf("host_mod") >= 0; //If true, ip address is overrided to 192.168.1.30
const _bOverride_port = process.argv.indexOf("port_mod") >= 0; //If true, port is overrided to secondary port

const _oIDObj = { siteId: server_config._sSiteId, client_id: _sTarget_client_id }; //Used for DB access + logging.
const _iModbus_poll_interval = server_config._iModbus_poll_interval;

/**
 * Objects form external source
 */
let oTarget_modbus = null; //Target modbus device from oModbus_inf, see plc_zone_appl.js. Connection parameter.

/**
 * Internal global objects
 */
let logger = server_logger.SERVER_LOGGING(`${server_config._sLOG_DIR}/modbus_control_${_sTarget_client_id}.log`, { level: _bDebug_mode ? "debug" : "info", mute_console: false }); //Logger of this module
let rwloop_interval = null; //Interval object of reading modbus
let bRWLock = false; //IO lock 

/**
 * General functions (Stage 1)
 */

let o_Err = function (msg) {
    let o = Object.assign({}, _oIDObj);
    o.err = msg;
    return o;
};

//Bind oTarget_modbus with loaded oModbus_inf
let fApply_device_inf = function (err, oLoaded_obj) {
    if (err) {
        logger.log('error', err);
        process.exit(9);
    } else if (!oLoaded_obj) {
        logger.log('error', JSON.stringify(o_Err("Device information not found")));
        process.exit(9);
    } else {
        oTarget_modbus = oLoaded_obj.toObject();
        logger.log("debug", { a: oTarget_modbus, b: oTarget_modbus.client_type, c: modbus_client.is_modbus_device(oTarget_modbus.client_type) });
        //logger.log("debug", db_connection.Device_Command);

        //Exit process when there are nothing match to target device with the given information
        if (!oTarget_modbus) {
            logger.log('error', JSON.stringify(o_Err("Device information not valid")));
            process.exit(9);
        } else {
            logger.log('info', "Client device found");
            if (!_bOffline_mode) {
                if (modbus_client.is_modbus_device(oTarget_modbus.client_type)) {
                    modbus_client.set_by_parent(_sTarget_client_id, _bDebug_mode, _bOffline_mode, _bOverride_host, _bOverride_port);
                    modbus_client.fModbusclient_definehandler(_bOverride_host, _bOverride_port, oTarget_modbus, device_on_connect, device_on_disconnect);
                } else if (delight_client.is_delight_device(oTarget_modbus.client_type)) {
                    delight_client.set_by_parent(_sTarget_client_id, _bDebug_mode, _bOffline_mode);
                    delight_client.set_client(oTarget_modbus, device_on_connect, device_on_disconnect);
                } else {
                    logger.log("error", "Uncaught device type! Exiting...");
                    process.exit(9);
                }
            } else {
                device_on_connect();
            }
            //rwloop_refresh(); //Skip connecting Modbus
        }
    }
};

/**
 * Modbus write
 */

let fufill_cmd = function (d_cmd) {
    logger.log("debug", "fufill_cmd");
    return new Promise((resolve, reject) => {
        let device_cmd = d_cmd.toObject();
        device_cmd.last_completed = new Date();
        db_connection.Device_Command.findOneAndUpdate({ _id: d_cmd._id }, { $set: device_cmd }, { upsert: true, new: true }, (err) => {
            if (err) { reject(err); } else { logger.log("debug", "Device_Command is fuilled."); resolve(); }
        });
    });
};

let read_cmd = function (db_connection) {
    logger.log("debug", "read_cmd");
    return new Promise((resolve, reject) => {
        //logger.log("debug", db_connection.Device_Command);
        if (!(db_connection && db_connection.Device_Command)) {
            resolve(`db_connection.Device_Command is undefined!`);
        } else {
            db_connection.Device_Command.findOne(_oIDObj, {}, { sort: { 'last_issued': -1 } }, (err, d_cmd) => {
                //logger.log("debug", [err, d_cmd, oTarget_modbus.client_type]);
                if (err) { reject(err); }
                else {
                    if (!d_cmd) {
                        logger.log('debug', "There is no command for this client."); resolve();
                    } else if (d_cmd.last_completed && d_cmd.last_completed.getTime() >= d_cmd.last_issued) {
                        logger.log('debug', "The newest command has been completed."); resolve();
                    } else if (modbus_client.is_modbus_device(oTarget_modbus.client_type)) {
                        modbus_client.parse_and_write_cmd(d_cmd).then(() => { return fufill_cmd(d_cmd); }).then(resolve).catch(reject);
                    } else if (delight_client.is_delight_device(oTarget_modbus.client_type)) {
                        delight_client.parse_and_write_cmd(d_cmd).then(() => { return fufill_cmd(d_cmd); }).then(resolve).catch(reject);
                    } else {
                        reject(`Unknown client type (${oTarget_modbus.client_type}) to handle!`);
                    }
                }
            });
        }
    });
};

/**
 * Modbus read
 */

let write_mdump = function (oTarget_modbus) {
    return new Promise((resolve, reject) => {
        logger.log('debug', "write_mdump()");
        //logger.log('debug', JSON.stringify(oTarget_modbus));
        if (!(oTarget_modbus && oTarget_modbus.client_type)) {
            resolve(`oTarget_modbus.client_type is undefined!`);
        } else if (modbus_client.is_modbus_device(oTarget_modbus.client_type)) {
            //logger.log('debug', "calling modbus_write_dump()");
            modbus_client.modbus_write_dump(oTarget_modbus.client_type, device_on_data).then(resolve).catch(reject);
        } else if (delight_client.is_delight_device(oTarget_modbus.client_type)) {
            delight_client.read_device_network(oTarget_modbus, device_on_data).then(resolve).catch(reject);
        } else {
            reject(`Read range of client_type (${oTarget_modbus.client_type}) is undefined!`);
        }
    });
};

/**
 * Process flow
 */

let rwloop = function () {
    let cb = (err) => {
        if (err) { logger.log('error', err); }
        else { logger.log('debug', 'rwloop return.'); }
        bRWLock = false;
    };

    logger.log('debug', `rwloop(): RW in progress: ${bRWLock}`);
    if (!bRWLock) {
        bRWLock = true;
        read_cmd(db_connection).then((err) => {
            if (err) { logger.log("warn", err); }
            //logger.log("debug", oTarget_modbus.client_type);
            return write_mdump(oTarget_modbus);
        }).then(cb).catch(cb);
    } else {
        logger.log('debug', 'Skipped: rwloop in progress.');
    }
};

let rwloop_refresh = function () {
    //logger.log('info', "rwloop_refresh()");
    if (rwloop_interval) clearInterval(rwloop_interval);
    rwloop_interval = setInterval(rwloop, _iModbus_poll_interval);
};

let device_on_data = function ({ skip, resp, addr, sMemType }) {
    //logger.log("debug", { skip, resp, addr, sMemType});
    if (!skip) {
        let update_obj = null;

        if (modbus_client.is_modbus_device(oTarget_modbus.client_type)) {
            update_obj = modbus_client.makePartialDeviceStatus(_oIDObj, oTarget_modbus, resp, addr, sMemType);
        } else if (delight_client.is_delight_device(oTarget_modbus.client_type)) {
            update_obj = delight_client.makePartialDeviceStatus(_oIDObj, oTarget_modbus, resp, addr, sMemType);
        } else {
            logger.log("debug", { skip, resp, addr, sMemType });
        }

        if (update_obj) {
            db_connection.Device_Status.findOneAndUpdate(_oIDObj, { $set: update_obj }, { upsert: true, new: true }, (err) => {
                if (err) {
                    logger.log("error", err);
                } else if (!(resp || addr || sMemType)) {
                    logger.log("info", "device_on_disconnect: Device_Status is updated.");
                } else {
                    logger.log("debug", "device_on_data: Device_Status is updated.");
                }
            });
        } else {
            logger.log("warn", "update_obj is not defined");
        }

    } else {
        logger.log("debug", "Skipped.");
    }
};

let device_on_connect = function () {
    //Insert any task you want. However the rwloop already does everything you want.
    if (_bOffline_mode) {
        logger.log('info', "The client is running in offline mode.");
    }
    if (_bDebug_mode) {
        logger.log('info', "The client is running in debug mode.");
    }
    setTimeout(rwloop_refresh, _iModbus_poll_interval);
};

let device_on_disconnect = function () {
    if (modbus_client.is_modbus_device(oTarget_modbus.client_type)) {
        return device_on_data(modbus_client.LocalDeviceStatus(false, null, null, null));
    } else {
        return device_on_data();
    }
};

//"Graceful" exiting procedure. Try to run this instead of closing abruptly.
let graceful_exit = function (code) {
    //Close the stuffs if they exists

    let exit_db = new Promise((resolve, reject) => {
        if (db_connection) {
            db_connection.onExit();
            if (db_connection.mongoose.connection) {
                db_connection.mongoose.connection.close((err) => {
                    logger.log('debug', "Mongoose disconnected from mongoDB.");
                    reject(err);
                });
            } else {
                resolve();
            }
        } else {
            resolve();
        }
    });

    let cb = (err) => {
        if (err) logger.log('error', err);
        process.exit(code !== undefined ? code : 1);
    };

    Promise.all([
        modbus_client.exit_modbus,
        delight_client.exit_delight,
        exit_db
    ]).then(cb).catch(cb);
};

//Event emitters of this process (not server nor connection but process itself). See Node API for more infomration
let set_process_handlers = function () {
    process.on('uncaughtException', (err) => {
        //Hope logging with logger is still ok (e.g. listen EADDRINUSE)
        logger.log('error', 'Caught exception (exit instantly): ' + err, function () {
            graceful_exit(1); //Redundancy exit route
        });
    });

    process.on('exit', (code) => {
        //Only sync process is allowed. Do all the closing works before reaching here
        logger.log('info', 'Process exited with code ' + code);
    });

    process.on('SIGINT', () => {
        //Triggered when user press Ctrl-C to "close" the program
        logger.log('info', "Process is shutting down by SIGINT...");

        //Real exit - or no exit
        graceful_exit(0);
    });

    process.on('SIGTERM', () => {
        //Identical to SIGINT but usually by software (shell?)
        logger.log('info', "Process is shutting down by SIGTERM...");

        //Real exit - or no exit
        graceful_exit(0);
    });
};

/**
 * Main process session
 */

let main = function () {
    logger.log('info', `modbus_control (${_sTarget_client_id}) on start with pid = ${process.pid}`);
    set_process_handlers(process);

    //RIP JSModbus original logger
    //jsmodbus.log = winston.logger.stream;

    db_connection.init(logger, (err) => {
        if (err) { return fApply_device_inf(err, null); }
        else {
            logger.log('info', "DB connection is opened by Mongoose.");
            return db_connection.Device.findOne(_oIDObj, fApply_device_inf);
        }
    });
};

main();
//End of JavaScript source code