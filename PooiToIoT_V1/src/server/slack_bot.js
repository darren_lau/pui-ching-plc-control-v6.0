"use strict";
let async = require('async');
let request = require('request');

let server_logger = require('./modules/server_logger.js');
let server_config = require('./modules/server_config.js');
let db_connection = require('./modules/db_connection.js');

const _bDebug_mode = process.argv.indexOf("debug") >= 0; //If true, spam the console with debug messages
let logger = server_logger.SERVER_LOGGING(`${server_config._sLOG_DIR}/slack_bot.log`, { level: _bDebug_mode ? "debug" : 'warn' }); //Logger of this module

let TIME_LIMIT = {
    "Device_Status": 1000 * 60 * 10
};

let error_arr = [
    "Data not found in DB",
    "Timesstamp is not found in data",
    "Data update exceed time limit",
    "The PLC is not connected."
];

let global_state = {};

//const SLACK_WEB_HOOK = "https://hooks.slack.com/services/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"; //Production
const PLC_FILTER = ["PLC_ALARM"]; //Check against client_id. 
const SLACK_WEB_HOOK = "https://hooks.slack.com/services/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"; //Development
//const SLACK_WEB_HOOK = "https://hooks.slack.com/services/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";

let desc_obj_error_str = function (op, d) {
    return JSON.stringify({
        text: {
            client_id: d.client_id,
            client_pid: d.client_pid,
            connect_state: d.connect_state,
            errno: op,
            errdesc: error_arr[op],
            server_time: (new Date()).toLocaleString(),
            data_time: d.last_updated ? d.last_updated.toLocaleString() : ""
        }
    }, null, 4);
};

let desc_obj_solved_str = function (d) {
    return JSON.stringify({
        text: {
            client_id: d.client_id,
            client_pid: d.client_pid,
            connect_state: d.connect_state,
            error_solved: true,
            server_time: (new Date()).toLocaleString(),
            data_time: d.last_updated ? d.last_updated.toLocaleString() : ""
        }
    }, null, 4);
};

let desc_obj_error_attachment = function (op, d) {
    return {
        "author_name": `slack_bot.js`,
        "title": "Error is detected in PLC network",
        "fallback": `Error in ${d.client_id}: ${op} ${error_arr[op]}`,
        "color": "danger",
        "fields": [
            {
                "title": "Client ID",
                "value": d.client_id,
                "short": true
            },
            {
                "title": "Client PID",
                "value": d.client_pid,
                "short": true
            },
            {
                "title": "Current State",
                "value": d.connect_state,
                "short": true
            },
            {
                "title": "Data Time",
                "value": d.last_updated ? d.last_updated.toLocaleString() : "",
                "short": true
            },
            {
                "title": "Error",
                "value": `${op}: ${error_arr[op]}`,
                "short": false
            }
        ],
        "footer": "DB Monitor",
        "ts": (new Date()).getTime() / 1000
    };
};

let desc_obj_solved_attachment = function (d) {
    return {
        "author_name": `slack_bot.js`,
        "title": "Error is resolved in PLC network",
        "fallback": `${d.client_id} is Back to normal`,
        "color": "good",
        "fields": [
            {
                "title": "Client ID",
                "value": d.client_id,
                "short": true
            },
            {
                "title": "Client PID",
                "value": d.client_pid,
                "short": true
            },
            {
                "title": "Current State",
                "value": d.connect_state,
                "short": true
            },
            {
                "title": "Data Time",
                "value": d.last_updated ? d.last_updated.toLocaleString() : "",
                "short": true
            }
        ],
        "footer": "DB Monitor",
        "ts": (new Date()).getTime() / 1000
    };
};

var record_error_db = function (d, code, desc, callback) {
    // Make new Object to DB
    let nobj = new db_connection.error_issue({
        client_id: d.client_id,
        raise_time: new Date(),
        //fixed_time: null,
        errno: code,
        errdesc: desc
    });
    nobj.save(callback);
};

let post_error = function (op, d, cb) {
    let post_text = desc_obj_error_str(op, d);
    let post_attachment = desc_obj_error_attachment(op, d);
    //logger.log('debug', JSON.stringify(d));
    if (PLC_FILTER.indexOf(d.client_id) >= 0) {
        request.post({
            url: SLACK_WEB_HOOK,
            form: JSON.stringify({
                //text: post_text
                attachments: [post_attachment]
            })
        }, (err, httpResponse, body) => {
            if (err) {
                return cb(err);
            } else {
                if (httpResponse.statusCode !== 200) {
                    logger.log('error', httpResponse.statusCode + " " + body);
                }
                return record_error_db(d, op, error_arr[op], cb);
            }
        });
    } else {
        db_mark_solved(d, cb);
    }
};

let check_and_send_error = function (op_arr, d, cb) {
    async.eachSeries(op_arr, (op, cb_in) => {
        let db_query = {
            client_id: d.client_id,
            errno: op,
            fixed_time: null
        };
        db_connection.error_issue.findOne(db_query).exec((err, fDS) => {
            if (err) {
                return cb_in(err);
            } else if (fDS) {
                logger.log('verbose', 'Error has been raised: ' + JSON.stringify(fDS));
                return cb_in();
            } else {
                post_error(op, d, cb_in);
                //post_error_local(sql_obj, code, cb);
            }
        });
    }, cb);
};

let send_usual_error = function (op_arr, d, cb) {
    for (let i = 0; i < op_arr.length; i++) {
        logger.log('verbose', {
            error: error_arr[op_arr[i]],
            data_dump: JSON.stringify(d)
        });
    }
    return check_and_send_error(op_arr, d, cb);
};

let db_mark_solved = function (d, callback) {
    let query_obj = {
        fixed_time: null,
        client_id: d.client_id
    };
    let update_field = {
        fixed_time: new Date()
    };
    let option_obj = { multi: true };
    db_connection.error_issue.update(query_obj, update_field, option_obj, callback);
};

let post_solved = function (d, cb) {
    let post_text = desc_obj_solved_str(d);
    let post_attachment = desc_obj_solved_attachment(d);
    //logger.log('debug', JSON.stringify(d));
    if (PLC_FILTER.indexOf(d.client_id) >= 0) {
        request.post({
            url: SLACK_WEB_HOOK,
            form: JSON.stringify({
                //text: post_text
                attachments: [post_attachment]
            })
        }, (err, httpResponse, body) => {
            if (err) {
                return cb(err);
            } else {
                if (httpResponse.statusCode !== 200) {
                    logger.log('error', httpResponse.statusCode + " " + body);
                }
                return db_mark_solved(d, cb);
            }
        });
    } else {
        db_mark_solved(d, cb);
    }
};

let flag_solved_error = function (d, cb) {
    //Aborted by Michael: Need user input for double verification.
    //This process will be done in somewhere else
    //return callback(null);

    //180427: CR made. It will be in auto.
    let db_query = {
        client_id: d.client_id,
        fixed_time: null
    };
    db_connection.error_issue.findOne(db_query).exec((err, fDS) => {
        if (err) {
            return cb(err);
        } else if (fDS) {
            //logger.log('verbose', 'Error has been raised: ' + JSON.stringify(fDS));
            //return cb();
            post_solved(d, cb);
        } else {
            logger.log('verbose', 'No error found: ' + d.client_id);
            return cb();
            //post_error(op, d, cb);
            //post_error_local(sql_obj, code, cb);
        }
    });
};

let data_check_Device_Status = function (fDS, cb) {
    try {
        if (!fDS.last_updated) {
            return send_usual_error([1], fDS, cb);
        } else {
            //console.log('info', fDS);

            let now = new Date();
            let t = new Date(fDS.last_updated);

            let t_d = now.getTime() - t.getTime();
            if (t_d < TIME_LIMIT["Device_Status"]) {
                //console.log(fDS.connect_state);
                if (fDS.connect_state !== "connected") {
                    //if (fDS.connect_state !== global_state[fDS]) {
                    return send_usual_error([3], fDS, cb);
                } else {
                    return flag_solved_error(fDS, cb);
                }
            } else {
                return send_usual_error([2], fDS, cb);
            }
        }
    } catch (e) {
        return cb(e);
    }
};

let db_check_Device_Status = function (cb) {
    db_connection.Device_Status.find({}).exec((err, fDSs) => {
        if (err) {
            return cb(err);
        } else if (fDSs && fDSs.length > 0) {
            async.each(fDSs, (fDS, cbD) => {
                return data_check_Device_Status(fDS.toObject(), cbD);
            }, cb);
        } else {
            return send_usual_error([0], { client_id: "SYSTEM" }, cb);
        }
    });
};

let init = function () {
    logger.log('info', "Start checking data.");

    async.series([
        db_check_Device_Status
    ], (err) => {
        logger.log(err ? 'error' : 'info', err ? err : "All data checked.");
    });
};

let main = function () {
    logger.log('info', 'Process start with PID ' + process.pid);

    db_connection.init(logger, (err) => {
        if (err) {
            logger.log('error', err);
            process.exit(1);
        } else {
            logger.log('info', "Database connected.");
            setTimeout(init, 5 * 1000);
            setInterval(init, 60 * 1000);
        }
    });
};

main();