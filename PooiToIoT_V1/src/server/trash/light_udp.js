"use strict";
const dgram = require('dgram');
const util = require('util');

const udp_pkg_gen = require("./udp_pkg_gen");

let udp_client = null;

let on_off = false;

let last_received = new Date();

//0x32 0 0
const pkg_sample = () => {
    //return udp_pkg_gen["PKG_GEN"]["SET_LIGHT"](process.argv[2], process.argv[3], process.argv[4]);
    //return udp_pkg_gen["PKG_GEN"]["SET_LIGHT"](Math.floor(Math.random() * 100), 0x00, 0x00);
    on_off = !on_off;
    return udp_pkg_gen["PKG_GEN"]["SET_LIGHT"](on_off * 100, 0x00, 0x00);
};

const pkg_scan = () => {
    return udp_pkg_gen["PKG_GEN"]["SCAN"]();
}

const p_udpsend = async (client, a, b, c) => {
    return new Promise((t, f) => {
        if (client) {
            client.send(a, b, c, (e) => {
                if (e) { f(e); }
                else { t(); }
            });
        } else {
            t();
        }
    });
};

const set_client = () => {
    const client = dgram.createSocket('udp4');

    //client.send(message, 9988, '192.168.1.150', (err) => {
    //    client.close();
    //});

    client.on('error', (err) => {
        console.log({ err });
        client.close();
    });

    client.on('message', (msg, rinfo) => {
        let now = new Date();
        console.log(now.getTime() - last_received.getTime())
        last_received = now;
        console.log({ msg, rinfo });
        //client.close();
    });

    //client.close();
    return client;
};

const main = async () => {
    udp_client = udp_client ? udp_client : set_client();

    //let err = await p_udpsend(udp_client, pkg_scan(), 1500, '255.255.255.255');
    let err = await p_udpsend(udp_client, pkg_sample(), 9988, '192.168.1.150');
    if (err) console.log(err);
};

setInterval(main, 200);
//main().then(console.log).catch(console.log);

//console.log(pkg_sample());