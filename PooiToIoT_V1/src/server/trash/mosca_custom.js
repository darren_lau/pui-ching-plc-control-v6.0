// JavaScript source code

/**
 * External libraries (npm/node)
 */
var mosca = require('mosca'); //Mosca server
var http = require('http'); //HTTP Bundle for Mosca
var express = require('express'); //Express module for Mosca

var server_config = require('./modules/config_adapter');
var server_logger = require('./modules/server_logger');
var oDataBase = require('./modules/db_connection');
var oDevice_Status = {};

//Mosca: for http bundle
var app = express();
var httpServ = http.createServer(app);

/**
 * Constants
 */
const
    _bDebug_mode = (process.argv.indexOf("debug") >= 0) ? true : false, //If true, spam the console with debug messages
    _sLogfile_name = "../../logs/mosca_custom.log", //Logfile for this script
    _sSiteId = server_config._sSiteId; //Site ID for global use

/**
 * Internal global objects
 */
var oMongoose_reconnect_inerval; //Timeout object
var _bSIGTERM = false; //AS what it means

/**
 * Main process session
 * Set up event handlers after the server is on
 */

//Set logger
var logger = server_logger.SERVER_LOGGING(_sLogfile_name);
if (_bDebug_mode) { logger.level = 'debug'; } //May change later

//Fire the first message with logger
logger.log('info', "mosca_custom on start with pid = " + process.pid);

//As in draft 1.02, no more direct boardcasting to db
/**
var ascoltatore = {
  type: 'mongo',
  url: 'mongodb://localhost:27017/mqtt',
  pubsubCollection: 'ascoltatori',
  mongo: {}
};

var oMosca_settings = {
  port: 1883,
  backend: ascoltatore
};
**/

//Settings for Mosca server
var oMosca_settings = {
    port: server_config.ET_iMOSCA_PORT_MQTT, //MQTT port
    http: {
        port: server_config.ET_iMOSCA_PORT_HTTP, //HTTP port
        bundle: true, //HTTP bundle
        static: server_config.ET_sMOSCA_HTTP_HOST_DIR,
        stats: false // True for boardcasting $SYS messages 
    }
}

//Create Mosca Server with settings
var oMosca_server = new mosca.Server(oMosca_settings);

//Fired when the mqtt server is ready
var oMosca_server_setup = function () {
    logger.log('info', 'Mosca server is up and running...');
    logger.log('debug', 'connecting mongodb with mongoose...');
    oDataBase.init(logger, function (err) {
        if (err) { logger.log('error', err); process.exit(1); }
        else {
            oDevice_Status = oDataBase.Device_Status;
            logger.log('info', "DB connection is opened by Mongoose.");
        }
    });
}

/**
 * Mosca server event emitters
 */
//Set the server up when ready
oMosca_server.on('ready', oMosca_server_setup);

oMosca_server.on('clientConnected', function (client) {
    logger.log('debug', 'client connected', client.id);
	require('fs').writeFile('clientConnected.txt', JSON.stringify(client), 'utf-8', function(err) {
		logger.log(err?'error':'info', err?err:"Client found!");
	});
    if (_bSIGTERM) { graceful_exit(0); }
});

oMosca_server.on('clientDisconnecting', function (client) {
    logger.log('debug', 'client disconnecting', client.id);
    if (_bSIGTERM) { graceful_exit(0); }
});

oMosca_server.on('clientDisconnected', function (client) {
    logger.log('debug', 'client disconnected', client.id);
    if (_bSIGTERM) { graceful_exit(0); }
});

oMosca_server.on('subscribed', function (sTopic, client) {
    logger.log('info', 'Topic subscribed: ' + sTopic + ' by client' + client.id);
    if (_bSIGTERM) { graceful_exit(0); }
});

oMosca_server.on('unsubscribed', function (sTopic, client) {
    logger.log('info', 'Topic unsubscribed', sTopic);
    if (_bSIGTERM) { graceful_exit(0); }
});

//Fired when a message is published
oMosca_server.on('published', function (packet, client) {
    if (_bSIGTERM) { graceful_exit(0); }
    //Decoding the packet...
    //logger.log('info', JSON.stringify(packet));
    var sCmd = packet.cmd; //string
    var bRetain = packet.retain; //boolean
    var iQos = packet.qos; //integer
    var bDup = packet.dup; //boolean
    var iLength = packet.payload? packet.payload.length : undefined; //integer
    var sTopic = packet.topic; //string
    var sMessage; //see below
    if (packet.payload.constructor.name == "Buffer") { sMessage = packet.payload.toString(); }
    else { sMessage = packet.payload; }

    if (_bDebug_mode) {
        logger.log('debug', 'Published: topic="' + sTopic + '", message="' + sMessage + '"');
    } else if (sTopic.indexOf("$SYS") < 0) { //Ignore $SYS messages
        logger.log('info', 'Published: topic="' + sTopic + '", length = ' + iLength);
    }

    var sTopic_arr = sTopic.split('/');
    if (sTopic_arr.indexOf("Status") >= 0) {
        fMongoDB_upload(sTopic, sMessage);
    }

    if ((sTopic == "END_MOSCA") && (sMessage == "END_MOSCA")) {
        logger.log('info', "Received END_MOSCA package. exiting...");
        _bSIGTERM = true;
        graceful_exit(0);
    }

});

/**
 * Mongoose-related functions
 */

//Analysis the MQTT topic and message, and then upload the schema object to DB
function fMongoDB_upload(sTopic, sMessage) {
    logger.log('debug', 'fMongoDB_upload(' + sTopic + ', sMesssage)...');
    logger.log('debug', 'fMongoDB_upload: Device_Status detected...');
    if (oDataBase.mongoose.connection.readyState != 1) {
        logger.log('error', "fMongoDB_upload: Device_Status detected but mongodb is not connected.");
        return;
    }
    //sTopic = "ETAG/zoneA/Status"
    //sMessage = JSON string
    var sTopic_arr = sTopic.split('/');
    var oMessage_obj = {};
    try {
        oMessage_obj = JSON.parse(sMessage);
    } catch (e) {
        logger.log('error', "fMongoDB_upload: Device_Status detected but it is not JSON String.");
        return;
    }

    if (oMessage_obj.ERROR) {
        //sMessage = ERROR: MODBUS_DOWN
        logger.log('error', sTopic + ', ' + sMessage);
        //May do something more
        return;
    }

    var oNewObj = new oDevice_Status({
        siteId: _sSiteId,
        zoneId: sTopic_arr[3],
        client_type: oMessage_obj.client_type,
        client_id: oMessage_obj.client_id,
        Appliance: oMessage_obj.Appliance
    });

    var oQuery_obj = {
        siteId: _sSiteId,
        zoneId: sTopic_arr[3],
        client_type: oNewObj.client_type,
        client_id: oNewObj.client_id,
    };

    //Find if there are the object is in DB already, 
    //Then either update current object or add new object
    oDevice_Status.where(oQuery_obj).count(function (err, count) {
        if (err) { logger.log('error', err); }
        logger.log('debug', 'fMongoDB_upload: count = ' + count);
        //if (count >= 0) { //Always create
        if (count == 0) { //Create when there are no current object
            oNewObj.save(function (err, oNewObj) {
                if (err) { logger.log('error', err); }
                logger.log('info', 'fMongoDB_upload: Device_Status created.');
            });
        } else {
            oDevice_Status.update(oQuery_obj, { Appliance: oMessage_obj.Appliance }, {}, function (err, raw) {
                if (err) { logger.log('error', err); }
                logger.log('info', 'fMongoDB_upload: Device_Status updated.');
            });
        }
    });
}

/**
 * Event emitters of this process (not server nor connection but process itself)
 */

process.on('uncaughtException', function (err) {
    //See Node API for more information - hope logging with logger is still ok
    //e.g. listen EADDRINUSE
    logger.log('error', 'Caught exception (exit instantly): ' + err, function () {
        graceful_exit(1); //Redundancy exit route
    });
});

process.on('exit', function (code) {
    //See Node API for more infomration - only sync process is allowed
    //Do all the closing works before reaching here
    logger.log('debug', 'Process exited with code ' + code);
});

process.on('SIGINT', function () {
    //See Node API for more infomration
    //Triggered when user press Ctrl-C to "close" the program
    logger.log('info', "Mosca server is shutting down by SIGINT...");

    //Real exit - or no exit
    graceful_exit(0);
});

process.on('SIGTERM', function () {
    //See Node API for more infomration
    //Identical to SIGINT but usually by software (shell?)
    logger.log('info', "Mosca server is shutting down by SIGTERM...");

    //Real exit - or no exit
    graceful_exit(0);
});

//"Graceful" exiting procedure. Try to run this instead of closing abruptly.
//Maybe called for tons of times since this is a busy server
function graceful_exit(code) {
    //logger.log('debug', oDataBase.mongoose);
    //Close the stuffs if they exists
    if (oDataBase.mongoose.connection) {
        oDataBase.mongoose.disconnect(function () {
            logger.log('debug', 'Mongoose disconnected.');
            if (httpServ) {
                httpServ.close(function () {
                    logger.log('debug', 'Mosca http server closed.');
                    if (oMosca_server) {
                        oMosca_server.close(function () {
                            logger.log('debug', 'Mosca server closed.');
                            process.exit(code != undefined ? code : 1);
                        });
                    } else {
                        process.exit(code != undefined ? code : 1);
                    };
                })
            } else {
                process.exit(code != undefined ? code : 1);
            };
        });
    } else {
        process.exit(code != undefined ? code : 1);
    };
};

//End of JavaScript source code