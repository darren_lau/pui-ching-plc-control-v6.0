var async = require('async'); //Async work flow
var fork = require('child_process').fork; //Child process
var fs = require('fs');

var server_config = require('./config_adapter');
var plc_zone_appl = require('./plc_zone_appl');
var cool_master = require('./cool_master');

var _sModbus_childprocess = server_config._sModbus_childprocess; //Modbus process taking care of the defined device
var _iModbus_reconnect_limit = 3;
var logger = {};
var oMqttclient = {};
var _ssiteId = "";
var _bDebug_mode = false;
var _bOffline_mode = false;
var oConfig = {};
var oModbus_inf = {};
var oModbus_objects = {}; //Key-Value pairs of status object
// Schema "oModbus_objects" in this script ( key = "client_id" ):
/**
    "client_id" : {
        client_id: String,
        oInterval: Interval object,
        bAlive: Boolean,
        oModbus_cp: Child Process Object
    }
}
**/

//Create IPC bridge between mqtt and modbus modules
//oModbus_inf can be viewed in 'src/server/modules/plc_zone_appl.js'
function boot_ipc(logger_external, oMqttclient_external, oConfig_external, oModbus_inf_external, _bOffline_mode_external, _bDebug_mode_external) {
    logger = logger_external;
    oMqttclient = oMqttclient_external;
    oConfig = oConfig_external;
    _ssiteId = oConfig.siteid[0];
    oModbus_inf = oModbus_inf_external;
    _bOffline_mode = _bOffline_mode_external;
    _bDebug_mode = _bDebug_mode_external;
    logger.log('info', "Creating Modbus clients...");
    //Only 1 object. Code kept for potential 1 proccess supports multiple PLCs. Currently process are spawn individulaly.
    async.each(oModbus_inf[_ssiteId], function (itr, callback) {
        if (!oModbus_objects[itr.client_id])
            fDefine_modbus(itr);
        callback();
    }, function (err) {
        if (err) { logger.log('error', err); process.exit(1); }
        else {
            logger.log('info', "Modbus clients created. Monitoring...");
            //process.exit(0);
            //Set check modbus oInterval objects
            oCheck_modbus_interval_obj = setInterval(fCheck_modbus, oConfig.poll_period[0] * 3);
        }
    });
}

/**
 * Modbus Client and MQTT Client-related section
 */

//Return state of modbus_childprocess with inputted oModbus_inf entry
function fCheck_modbus() {
    async.each(oModbus_objects, function (itr, callback) {
        if (!itr.oModbus_cp) {
            logger.log('error', 'fCheck_modbus: childprocess is undefined: ' + itr.client_id);
            fDefine_modbus(oModbus_inf[_ssiteId][itr.client_id]);
        } else if (!itr.bAlive) {
            logger.log('error', 'fCheck_modbus: process is not responding and being killed: ' + itr.client_id);
            itr.oModbus_cp.kill();
            itr.oModbus_cp = false;
        } else {
            //Trying to connect to the child process
            var iTid = new Date().getTime();
            itr.bAlive = false;
            itr.oModbus_cp.send({ sCommand: 'check_status', tid: iTid });
            logger.log('debug', 'fCheck_modbus (' + itr.client_id + ') with tid = ' + iTid);
        }
        callback();
    }, function (err) {
        //This function can be deleted
        if (err) { logger.log('error', err); }
        else { logger.log('info', 'Childprocess checking completed.'); }
    });
}

//150810: Now the spawning is based from client_id instead of zoneId.
//First, either update or create the oModbus_obj (information of modbus_childprocess)
//Then create modbus_childprocess and set event handlers
function fDefine_modbus(oModbus_inf_obj) {
    logger.log('debug', 'fDefine_modbus(' + oModbus_inf_obj.client_id + ')');
    //Call oModbus_cp with specified process.argv array
    var oModbus_cp;
    var oArgv_arr = [];
    oArgv_arr.push(oModbus_inf_obj.client_id);
    if (_bDebug_mode) { oArgv_arr.push("debug"); }
    if (_bOffline_mode) { oArgv_arr.push("offline"); }

    //If the child process is not exist, create it. 
    //If it is already defined, redirect it.
    oModbus_cp = ((oModbus_objects[oModbus_inf_obj.client_id]) && (oModbus_objects[oModbus_inf_obj.client_id].oModbus_cp != null)) ?
        oModbus_objects[oModbus_inf_obj.client_id].oModbus_cp : fork(_sModbus_childprocess, oArgv_arr);

    //Update internal object status
    oModbus_objects[oModbus_inf_obj.client_id] = {
        client_id: oModbus_inf_obj.client_id,
        oInterval: null,
        bAlive: true,
        oModbus_cp: oModbus_cp
    }
    logger.log('debug', 'oModbus_object for PLC ' + oModbus_inf_obj.client_id + ' is created or updated.');

    var sModbus_cp_name = "ModbusCp_" + oModbus_inf_obj.client_id + '(' + oModbus_cp.pid + ')';
    logger.log('info', sModbus_cp_name + ' is created.');

    //Set event handlers
    oModbus_cp.on('message', function (oMes) {
        //console.log(oMes);
        if (oMes.reply == 'started') {
            //Send the oModbus_inf (instead in process.argv session)
            var iTid = new Date().getTime();
            oModbus_cp.send({ oModbus_inf: oModbus_inf, tid: iTid });
            logger.log('debug', 'sent oModbus_inf to ' + sModbus_cp_name + ' with tid ' + iTid);
        } else if (oMes.reply == 'connected') {
            //logger.log('debug', JSON.stringify(oZone_map)); process.exit(0);
            logger.log('info', "Modbus client " + sModbus_cp_name + " is connected.");

            //Set up reading and publishing oInterval
            var oModbus_obj = oModbus_objects[oModbus_inf_obj.client_id];
            if (!oModbus_obj) {
                logger.log('error', "oModbus_cp (" + oModbus_inf_obj.client_id + ") is not reconized.");
                return;
            }

            //Set up polling interval if there are not
            if (oModbus_obj.oInterval) {
                clearInterval(oModbus_obj.oInterval);
                oModbus_obj.oInterval = null;
            }

            oModbus_obj.oInterval = setInterval(function () {
                if (oModbus_cp && oModbus_obj.bAlive) {
                    var client_type = oModbus_inf[_ssiteId][oModbus_inf_obj.client_id].client_type;
                    //160720: Now it client_type is useful now
                    var sReadCmd =
                        (client_type == "PLC") ? 'read' :
                        (client_type == "CoolMasterNet") ? 'read_coolmaster' :
                        false;
                    if (sReadCmd) {
                        oModbus_cp.send({ sCommand: sReadCmd, tid: new Date().getTime() });
                        //logger.log('info', "read");
                    } else {
                        logger.log('errr', "client_type (" + client_type + ") not supported in device " + oModbus_inf_obj.client_id + ". Cannot fire read command.");
                    }
                } else {
                    logger.log('error', "read event triggered but " + sModbus_cp_name + " is not exist.");
                }
            }, oConfig.poll_period[0]);

        } else if (oMes.sZone_status) {
            //process.exit(0);
            logger.log('debug', "analysis_zone_status(" + oModbus_inf_obj.client_id + "," + oMes.sZone_status + ")");
            //plc_zone_appl.helloworld();
            //Publish mqtt message after analysis, in cb section
            //#PERFORMANCE CHECK
            var iPerformance_time = new Date().getTime();
            logger.log('debug', 'start of analysis zone status: ' + iPerformance_time);
            plc_zone_appl.fAnalysis_zone_status(logger, oModbus_inf_obj, oMes.sZone_status, oConfig, function (error, oMQTT_Messages) {
                //oMQTTMessages: [{sTopic, sMessage}]

                //Publish MQTT Message with specified topic and message.
                //Log if there's error
                //(_bDebug_mode): Generate JSON file for debugging
                var iTime_amount = new Date().getTime() - iPerformance_time;
                logger.log('debug', 'end of analysis zone status: ' + iTime_amount + "ms");
                if (error) { logger.log('error', error); return; }
                else {
                    async.each(oMQTT_Messages, function (oMQTT_Message, callback) {
                        oMqttclient.publish(oMQTT_Message.sTopic, oMQTT_Message.sMessage);
                        if (_bDebug_mode) {
                            logger.log('debug', "oMqttclient.publish(" + oMQTT_Message.sTopic + ', ' + oMQTT_Message.sMessage + ")");
                            /**
                            var sMessage_pretty = JSON.stringify(JSON.parse(oMQTT_Message.sMessage), null, 4);
                            var filePath = server_config._sCONFIG_DIR + '/debug/DeviceStatus/' + oMQTT_Message.sTopic.split('/')[3] + '-' + iPerformance_time + '.json';
                            fs.writeFile(filePath, sMessage_pretty, function (err) {
                                if (err) { return callback(err); }
                                else {
                                    logger.log('debug', filePath + ' is created.');
                                    callback();
                                }
                            });
                            **/
                            callback();
                        } else {
                            logger.log('info', 'oMqttclient.publish( topic = "' + oMQTT_Message.sTopic + '", length = ' + oMQTT_Message.sMessage.length + ")");
                            callback();
                        }
                    }, function (err) {
                        if (err) { logger.log('error', err); }
                        else {
                            logger.log('info', 'All MQTT messages are fired.');
                            //process.exit(0);
                        }
                    })
                }
            });
        } else if (oMes.sCoolMaster_status) {
            //process.exit(0);
            logger.log('debug', "analysis_zone_status(" + oModbus_inf_obj.client_id + "," + oMes.sCoolMaster_status + ")");
            //plc_zone_appl.helloworld();
            //Publish mqtt message after analysis, in cb section
            //#PERFORMANCE CHECK
            var iPerformance_time = new Date().getTime();
            logger.log('debug', 'start of analysis zone status: ' + iPerformance_time);
            cool_master.fAnalysis_coolmaster_status(logger, oModbus_inf_obj, oMes.sCoolMaster_status, oConfig, function (error, oMQTT_Messages) {
                //oMQTTMessages: [{sTopic, sMessage}]

                //Publish MQTT Message with specified topic and message.
                //Log if there's error
                //(_bDebug_mode): Generate JSON file for debugging
                var iTime_amount = new Date().getTime() - iPerformance_time;
                logger.log('debug', 'end of analysis zone status: ' + iTime_amount + "ms");
                if (error) { logger.log('error', error); return; }
                else {
                    async.each(oMQTT_Messages, function (oMQTT_Message, callback) {
                        oMqttclient.publish(oMQTT_Message.sTopic, oMQTT_Message.sMessage);
                        if (_bDebug_mode) {
                            logger.log('debug', "oMqttclient.publish(" + oMQTT_Message.sTopic + ', ' + oMQTT_Message.sMessage + ")");

                            var sMessage_pretty = JSON.stringify(JSON.parse(oMQTT_Message.sMessage), null, 4);
                            var filePath = server_config._sCONFIG_DIR + '/debug/DeviceStatus/' + oMQTT_Message.sTopic.split('/')[3] + '-' + iPerformance_time + '.json';
                            fs.writeFile(filePath, sMessage_pretty, function (err) {
                                if (err) { return callback(err); }
                                else {
                                    logger.log('debug', filePath + ' is created.');
                                    callback();
                                }
                            });

                            //callback();
                        } else {
                            logger.log('info', 'oMqttclient.publish( topic = "' + oMQTT_Message.sTopic + '", length = ' + oMQTT_Message.sMessage.length + ")");
                            callback();
                        }
                    }, function (err) {
                        if (err) { logger.log('error', err); }
                        else {
                            logger.log('info', 'All MQTT messages are fired.');
                            //process.exit(0);
                        }
                    })
                }
            });
        } else if (oMes.write_result == 'success') {
            logger.log('info', sModbus_cp_name + ": setStatus successful");
        } else if (oMes.oCp_status) {
            logger.log('info', 'oCp_status of ' + sModbus_cp_name + ": " + JSON.stringify(oMes.oCp_status));
            //oModbus_objects[oModbus_inf_obj.client_id].bAlive = true;

            oModbus_objects[oModbus_inf_obj.client_id].bAlive = (((oMes.oCp_status.modbus == "connected") || (oMes.oCp_status.modbus == "disconnected")) && (oMes.oCp_status.retry_count <= _iModbus_reconnect_limit));
            //It could be "undefined" - modbus client is not defined!
            //logger.log('info', oModbus_objects[oModbus_inf_obj.client_id].bAlive);

            //If retry count is greater than 3, warn or send MQTT package
            if (!oModbus_objects[oModbus_inf_obj.client_id].bAlive) {
                logger.log('error', sModbus_cp_name + ': modbus device seems to be down.');
                oMqttclient.publish("/error/" + oModbus_inf_obj.client_id, '{"ERROR": "MODBUS_DOWN"}'); //Special topic based from PLC
            }

        } else {
            logger.log('error', sModbus_cp_name + ":" + JSON.stringify(oMes));
        }
    });

    oModbus_cp.on('close', function (code, signal) {
        //3221225786 (0xC000013A) - result of SIGINT
        if ((code != 0) && (signal == null) && (code != 3221225786)) {
            logger.log('error',
	  			sModbus_cp_name + " exit with code = " + code + ' with signal = ' + signal);
        } else {
            logger.log('info',
	  			sModbus_cp_name + " exit with code = " + code + ' with signal = ' + signal);
        }
        //Shut down status object
        clearInterval(oModbus_objects[oModbus_inf_obj.client_id].oInterval);
        oModbus_objects[oModbus_inf_obj.client_id].oModbus_cp = null;
    });
}

var sModbus_cp_name = function (client_id) {
    if (!(oModbus_objects[client_id] && oModbus_objects[client_id].oModbus_cp && oModbus_objects[client_id].oModbus_cp.pid)) {
        return "ModbusCp_" + client_id + '(undefined)';
    } else {
        return "ModbusCp_" + client_id + '(' + oModbus_objects[client_id].oModbus_cp.pid + ')';
    }
}

//Return childprocess object from client_id
var search_oModbus_object = function (client_id) {
    return oModbus_objects[client_id];
}

var exports = module.exports = {
    boot_ipc: boot_ipc,
    search_oModbus_object: search_oModbus_object,
    sModbus_cp_name: sModbus_cp_name
}