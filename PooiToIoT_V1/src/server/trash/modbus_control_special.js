// JavaScript source code
// 160706: This is focused on PLC connection - no serious modification is needed

/**
 * External libraries (npm/node)
 */
var modbus = require('jsmodbus'); //Modbus Client
var jf = require('jsonfile'); //JSON read and write
var async = require('async'); //Flow control

/**
 * Self-made libraries
 */
var server_config = require('./modules/config_adapter.js'); //Custom settings
var server_logger = require('./modules/server_logger.js');
var cool_master = require('./modules/cool_master'); //Cool Master Modbus stuffs

/**
 * Constants
 */
const
    _sTarget_client_id = process.argv[2], //Process arguement: client_id for taking care
    _bDebug_mode = process.argv.indexOf("debug") >= 0 ? true : false, //If true, spam the console with debug messages
    _bStandalone_mode = process.argv.indexOf("standalone") >= 0 ? true : false, //If true, this process is standalone process instead of child process
    _bOffline_mode = process.argv.indexOf("offline") >= 0 ? true : false, //If true, no Modbus device will be connected. Outputs are generated instead

    _sReference_file = server_config._sCONFIG_DIR + "/offlineinf/device/mqtt-modbus_inf_" + _sTarget_client_id + ".json"; //_bStandalone_mode: Same as oModbus_inf given by MQTT Client

_iRetry_delay_unit = server_config._iRetry_delay_unit; //5 seconds, expected value
_iRetry_delay_limit = server_config._iRetry_delay_limit; //600 seconds, expected value
_iRetry_random_range = server_config._iRetry_random_range; //-0.5sec to +0.5sec
_iPrimary_modbus_port = server_config._iPrimary_modbus_port; //Hardcoded, non-documented
_iSecondary_modbus_port = server_config._iSecondary_modbus_port; //Hardcoded, non-documented

_iRead_register_from = server_config.iRead_register_from, //From document
_iRead_register_length = server_config._iRead_register_length, //From document (399-200+1)
_iStart_of_bulb = server_config._iStart_of_bulb, //Not in document (same as the hardcode section in MQTT client)
_iLength_of_bulb = server_config._iLength_of_bulb, //Not in document (same as the hardcode section in MQTT client)
_iRead_register_unit = server_config._iRead_register_unit, //ACTUAL LIMIT BY EXTERNAL
_iRead_coil_unit = server_config._iRead_coil_unit, //ACTUAL LIMIT BY EXTERNAL

_iRead_register_actual_pass = Math.ceil(_iRead_register_length / _iRead_register_unit); // 4 pass 

/**
 * Objects form external source
 */
var oTarget_modbus; //Target modbus device from oModbus_inf, see plc_zone_appl.js

/**
 * Internal global objects
 */
var logger = server_logger.SERVER_LOGGING(server_config._sLOG_DIR + "/modbus_control_" + _sTarget_client_id + ".log", { mute_console: !_bStandalone_mode }); //Logger of this module
var oModbusclient; //jsmodbus client object
var iModbus_port = _iPrimary_modbus_port; //Port number for connecting Modbus device
var iModbus_retry_attempt = 0; //Reconnect counter for connecting Modbus device
var iModbus_reg_arr = []; //Multiple reading is required to update the grids
var iModbus_coil_arr = []; //Can read all in once
var icb_read_count = 0; //To count the callback count

/**
 * Modbus definiation/ connection/ reconnection
 */

//Return randomized retry delay with custom expected value and variance
function iModbus_retry_delay(iModbus_retry_attempt) {
    var iModbus_retry_time_amount = 0;
    if (iModbus_retry_attempt >= (_iRetry_delay_limit / _iRetry_delay_unit))
        iModbus_retry_time_amount = Math.floor(_iRetry_delay_unit + (Math.random() - 0.5) * _iRetry_random_range) * (_iRetry_delay_limit / _iRetry_delay_unit);
    else
        iModbus_retry_time_amount = Math.floor(_iRetry_delay_unit + (Math.random() - 0.5) * _iRetry_random_range) * iModbus_retry_attempt;
    if (iModbus_retry_time_amount <= 0) { iModbus_retry_time_amount = _iRetry_delay_unit; }
    logger.log('debug', 'iModbus_retry_delay = ' + iModbus_retry_time_amount);
    return iModbus_retry_time_amount;
}

//Defination of client object with event handlers
function fModbusclient_definehandler() {
    // Release if defined
    oModbusclient = null;

    logger.log('info', "Connecting to Modbus device: modbus://" + oTarget_modbus.ip_addr + ":" + iModbus_port);
    //oModbusclient = modbus.createTCPClient(iModbus_port, oTarget_modbus.ip_addr, cb_modbuserror);

	oModbusclient = modbus.createTCPClient(iModbus_port, process.argv[4], cb_modbuserror);

    //Event handlers
    oModbusclient.on('connect', function () {
        logger.log('info', "oModbusclient.on(connect)");


        //process.exit(0);
        if (!_bStandalone_mode) { process.send({ 'reply': 'connected' }); }
        else if (_bDebug_mode) { fRead_modbus_old(); }
        //(_bStandalone): start the task
        if ((_bStandalone_mode) && (!_bDebug_mode)) {
            //oModbusclient.writeSingleCoil(0, false, cb_write);
            //oModbusclient.readCoils(0, _iRead_coil_unit, function (resp, err) { console.log(JSON.stringify(resp));});
            //oModbusclient.readHoldingRegister(250, 64, function (resp, err) { console.log(JSON.stringify(resp)); });
            //icb_read_count = 0; fRead_modbus_reg(icb_read_count);
            //oModbusclient.readHoldingRegister(process.argv[4], process.argv[5], function (resp, err) {
            //    if (err) { logger.log('error', err); process.exit(1); }
            //    else { logger.log('info', resp); process.exit(0);}
            //});
			var a = [];
			for (var i = 16; i <= 16; i++) { a.push(i); }
			async.eachSeries(a, function (aa, cb){
				//logger.log('info', aa);
				try {
				oModbusclient.readHoldingRegister(aa, 19, function (resp, err) {
                    		 	if (!err) { logger.log('info', resp); }
					//logger.log(err? 'error' : 'info', aa); 
					cb();
				});
				} catch (err) {
					cb(err);
				}
			}, function (e) {
				logger.log(e? 'error' : 'info', e? e : "WTF.");
				process.exit(0);
			});
			/**
			logger.log('info', [parseInt(process.argv[4]), process.argv[5]]);
            oModbusclient.writeSingleRegister(parseInt(process.argv[4]), parseInt(process.argv[5]), function (resp, err) {
                if (err) { logger.log('error', err); process.exit(1); }
                else {
                    logger.log('info', resp);
                    oModbusclient.readHoldingRegister(Math.floor(process.argv[4] / 16) * 16, 16, function (resp, err) {
                            if (err) { logger.log('error', err); process.exit(1); }
                            else { logger.log('info', resp); process.exit(0);}
                    });
                }
            });
			**/
            //fRead_coolmaster();
        }
    });

    oModbusclient.on('data', function () {
        //logger.log('info', "oModbusclient.on(data)");
    });

    oModbusclient.on('close', function () {
        logger.log('info', "oModbusclient.on(close)");
        iModbus_retry_attempt++;
        logger.log('info', "reconnecting oModbusclient in " +
            iModbus_retry_delay(iModbus_retry_attempt) / 1000 +
            " seconds...");
        setTimeout(cb_reconnect_modbus, iModbus_retry_delay(iModbus_retry_attempt));
    });

    oModbusclient.on('end', function () {
        logger.log('info', "oModbusclient.on(end)");
    });
}

//Callback when there's error while creating Modbus client
cb_modbuserror = function (err) {
    //Usually "util.ETIMEOUT" or "util.ECONNREFUSED" or "util.EHOSTUNREACH"
    if (err) {
        logger.log('error', "cb_modbuserror: " + err);
        if (!_bStandalone_mode) { process.send(err); }
    }
}

//Callback of reconnecting Modbus client
cb_reconnect_modbus = function () {
    logger.log('info', "reconnecting oModbusclient...");
    fModbusclient_definehandler();
}

/**
 * Modbus write
 */

//Callback for posting result of writing register
cb_write = function (resp, err) {
    if (err) {
        //Output error when failed
        if (!_bStandalone_mode) {
            process.send({ error: "cb_write in oModbus_cp of " + oTarget_modbus.client_id });
            process.send({ error: err });
        }
        logger.log('error', err);
        return;
    } else {
        //Output result when success
        logger.log('info', resp);
        logger.log('info', "fWrite_modbus success");
        if (!_bStandalone_mode) { process.send({ 'write_result': 'success' }); }
        else { process.exit(0); } //Standalone mode end
    }
}

//Write register with given register address and value
function fWrite_modbus(oMes) {
    logger.log('info', 'fWrite_modbus: ' + JSON.stringify(oMes));
    if (_bOffline_mode) {
        if (!_bStandalone_mode) {
            process.send({ 'write_result': 'success' });
        }
    }
    else if (oModbusclient && oModbusclient.isConnected()) {
        if (oMes.option == "coil") {
            oModbusclient.writeSingleCoil(oMes.target_register, (oMes.target_value > 0) ? true : false, cb_write);
        } else {
            oModbusclient.writeSingleRegister(oMes.target_register, oMes.target_value, cb_write);
        }
    }
}

//Write registers with given start register address and values
function fWrite_modbus_sch_loop(i0, iFrom, iExit, iArr, callback) {
    if (i0 == iExit) { return callback(null); }
    logger.log('info', 'fWrite_modbus_sch_loop: D' + i0 + ' = ' + iArr[i0 - iFrom]);
    oModbusclient.writeSingleRegister(i0, iArr[i0 - iFrom], function (resp, err) {
        if (err) { return callback(err); }
        else { fWrite_modbus_sch_loop(i0 + 1, iFrom, iExit, iArr, callback) }
    });
}

function fWrite_modbus_sch(oMes) {
    logger.log('info', 'fWrite_modbus: ' + JSON.stringify(oMes));
    if (_bOffline_mode) {
        if (!_bStandalone_mode) {
            process.send({ 'write_result': 'success' });
        }
    }
    else if (oModbusclient && oModbusclient.isConnected()) {
        var i0 = oMes.target_register;
        var iFrom = oMes.target_register;
        var iExit = oMes.target_register + oMes.target_value.length;
        var iArr = oMes.target_value;
        fWrite_modbus_sch_loop(i0, iFrom, iExit, iArr, function (err) {
            if (err) { cb_write(null, err); }
            else { cb_write("fWrite_modbus_sch: write success", null); }
        });
    }
};

/**
 * Modbus read
 */

//Added in 150804
//Since the state of coils is put into the register with scale 8 : 1 instead of 1 : 1,
//The registers should be extracted to minimize the impact of the system.
fExtract_bulbs = function (iOriginalArr) {
    var iArr = iOriginalArr.slice(_iStart_of_bulb - _iRead_register_from, _iStart_of_bulb - _iRead_register_from + _iLength_of_bulb);
    var iOut = [];
    for (var i0 = 0; i0 < iArr.length / 8; i0++) {
        for (var i1 = 0; i1 < 8; i1++) {
            iOut.push(iArr[i0] % 2);
            iArr[i0] = iArr[i0] >> 1;
        }
    }
    for (var i2 = 0; i2 < iArr.length; i2++) {
        iOriginalArr[_iStart_of_bulb - _iRead_register_from + i2] = iOut[i2];
    }
    //logger.log('info', iOriginalArr.slice(250-200, 300-200));
    return iOriginalArr;
}


//Callback for posting result of reading coil
cb_read_coil = function (resp, err) {
    if (err) {
        //Output error when failed
        if (!_bStandalone_mode) {
            process.send({ error: "cb_read_coil in oModbus_cp of " + oTarget_modbus.client_id });
            process.send({ error: err });
        }
        logger.log('error', err);
        //Reset global array
        iModbus_coil_arr.length = 0;
        iModbus_reg_arr.length = 0;
        return;
        //} else if (icb_read_count == 1) {
    } else {
        //Since Boolean may cause confusion for further analysis, they will be converted into Number
        //iModbus_coil_arr = resp.coils;
        for (var i0 = 0; i0 < resp.coils.length; i0++) {
            iModbus_coil_arr.push(resp.coils[i0] ? 1 : 0);
        }
        fRead_modbus_reg(icb_read_count);
    }
};

//Callback for posting result of reading register
cb_read_reg = function (resp, err) {
    if (err) {
        //Output error when failed
        if (!_bStandalone_mode) {
            process.send({ error: "cb_read_reg in oModbus_cp of " + oTarget_modbus.client_id });
            process.send({ error: err });
        }
        logger.log('error', err);
        //Reset global array
        iModbus_reg_arr.length = 0;
        iModbus_coil_arr.length = 0;
        return;
        //} else if (icb_read_count == 1) {
    } else if (icb_read_count == _iRead_register_actual_pass) {
        //Ouput final data
        //Emulating output package (form jsmodbus)
        resp = {
            fc: 3,
            byteCount: parseInt(_iRead_register_length * 2 + iModbus_coil_arr.length / 8),
            register: iModbus_reg_arr,
            coils: iModbus_coil_arr
            //register: fExtract_bulbs(iModbus_reg_arr)
        };
        //Output result when success
        logger.log('info', JSON.stringify(resp));
        //logger.log('info', "Length of resp.register:" + resp.register.length);
        //logger.log('info', iModbus_reg_arr[300-200+1] );
        if (!_bStandalone_mode) { process.send({ 'sZone_status': JSON.stringify(resp) }); }
        else { process.exit(0); } //Standalone mode end
        //Reset global array
        iModbus_coil_arr.length = 0;
        iModbus_reg_arr.length = 0;
    } else {
        var iStart = icb_read_count * _iRead_register_unit;
        var iEnd = iStart + _iRead_register_unit - 1;
        if (iEnd >= _iRead_register_length) { iEnd = _iRead_register_length - 1; }

        //Expend the array with 0
        //var iReg_arr = [];
        for (i0 = iStart; i0 <= iEnd; i0++) {
            var i1 = i0 - icb_read_count * _iRead_register_unit;
            iModbus_reg_arr[i0] = resp.register[i1];
        }

        icb_read_count++;
        fRead_modbus_reg(icb_read_count);
    }
}

//(_bOffline_mode): Generate register values instead of reading them
function cb_read_offline() {
    var iReg_arr = [];
    var iCoil_arr = [];
    for (var i0 = 0; i0 < _iRead_register_length; i0++) {
        //iReg_arr[i0] = parseInt(i0 + _iRead_register_from); //Value = address
        iReg_arr.push(Math.floor(Math.random() * 255)); //Value = random
    }
    for (var i1 = 0; i1 < _iRead_coil_unit; i1++) {
        var i = Math.random() >= 0.5 ? 1 : 0;
        iCoil_arr.push(i);
    }
    //Emulating output package (form jsmodbus)
    var resp = {
        fc: 3,
        byteCount: parseInt(iReg_arr.length * 2 + iCoil_arr.length / 8),
        register: iReg_arr,
        coils: iCoil_arr
    };
    //Output package
    logger.log('info', JSON.stringify(resp));
    if (!_bStandalone_mode) { process.send({ 'sZone_status': JSON.stringify(resp) }); }
}

var merge_appliances = function () {
    //All required data is global variables.
    var arr = [];
    var p = oTarget_modbus.zones;
    for (var key in p) {
        if (p.hasOwnProperty(key)) {
            var p2 = p[key].appliances;
            for (var key2 in p2) {
                if (p2.hasOwnProperty(key2)) {
                    arr.push(p2[key2]);
                    //logger.log('info', p2[key2].appl_name);
                }
            }
        }
    }
    return arr;
}

function cb_read_coolmaster_offline() {
    //Return a 3D array: [panel][AC#][field_value]
    //logger.log('info', JSON.stringify(oTarget_modbus.zones['K15-1']));
    var appl_arr = merge_appliances();
    if (appl_arr.length == 0) {
        if (!_bStandalone_mode) {
            process.send({ error: "cb_read_reg in oModbus_cp of " + oTarget_modbus.client_id });
            process.send({ error: 'Appliance information not found.' });
        }
        logger.log('error', 'Appliance information not found.'); return;
    }
    var oACReg = [];
    async.eachSeries(appl_arr, function (appl_obj, callback) {
        if (!appl_obj.appl_index) {
            return callback('appl_index not found under the specific appliance.');
        } else {
            var base = appl_obj.appl_index;
            var len = cool_master.reg_length;
            var temp_arr = cool_master.gen_random_arr();
            var arr_map = cool_master.va_to_arr(base);
            //logger.log('info', arr_map);
            if (!oACReg[arr_map[0]]) { oACReg[arr_map[0]] = []; }
            if (!oACReg[arr_map[0]][arr_map[1]]) { oACReg[arr_map[0]][arr_map[1]] = []; }
            for (var i = 0; i < len; i++) {
                oACReg[arr_map[0]][arr_map[1]][arr_map[2] + i] = temp_arr[i];
                //logger.log('info', 'BAM' + temp_arr[i]);
            }
            return callback();
        }
    }, function (err) {
        if (err) { logger.log('error', err); }
        else {
            //Output package
            logger.log('info', { 'sCoolMaster_status': JSON.stringify(oACReg) });
            if (!_bStandalone_mode) { process.send({ 'sCoolMaster_status': JSON.stringify(oACReg) }); }
        }
    });
}

/**
 * 150812: Reading route: Read Coil will be called before Read Register in serial.
 */

//Call oModbusclient to read register
function fRead_modbus_coil() {
    logger.log('info', "fRead_modbus_coil():Q0-Q" + (_iRead_coil_unit - 1));
    //Read blindly - since this client don't know where is the information
    oModbusclient.readCoils(0, _iRead_coil_unit, cb_read_coil);
}

//Call oModbusclient to read register
function fRead_modbus_reg(iPass) {
    var iStart = _iRead_register_from + iPass * _iRead_register_unit;
    var iEnd = iStart + _iRead_register_unit - 1;
    logger.log('info', "fRead_modbus_reg(" + iPass + "):D" + iStart + "-D" + iEnd);
    //Read blindly - since this client don't know where is the information
    oModbusclient.readHoldingRegister(iStart, _iRead_register_unit, cb_read_reg);
}

//(_bDebug_mode): Call oModbusclient to read register but in old ranges
function fRead_modbus_old() {
    //As in 22/06/15 - before changing register ranges
    logger.log('info', "fRead_modbus_old()");
    oModbusclient.readHoldingRegister(_iRead_register_from, _iRead_register_unit, cb_read_reg);
}

//(_bOffline_mode): Redirect to callback function
function fRead_modbus_offline() {
    //Not actually reading - generate output instead 
    logger.log('info', "fRead_modbus_offline()");
    cb_read_offline();
}

function fRead_coolmaster_offline() {
    //Not actually reading - generate output instead 
    logger.log('info', "fRead_coolmaster_offline()");
    cb_read_coolmaster_offline();
}

function fRead_coolmaster() {
    var appl_arr = merge_appliances();
    if (appl_arr.length == 0) {
        if (!_bStandalone_mode) {
            process.send({ error: "cb_read_reg in oModbus_cp of " + oTarget_modbus.client_id });
            process.send({ error: 'Appliance information not found.' });
        }
        logger.log('error', 'Appliance information not found.'); return;
    }
    var oACReg = [];
    //Async for loop
    async.eachSeries(appl_arr, function (appl_obj, callback) {
        logger.log('debug', 'Reading holding register #' + appl_obj.appl_index + "...");
        oModbusclient.readHoldingRegister(appl_obj.appl_index, cool_master.reg_length, function (resp, err) {
            //resp = {
            //    fc: 3,
            //    byteCount: parseInt(_iRead_register_length * 2 + iModbus_coil_arr.length / 8),
            //    register: iModbus_reg_arr,
            //    coils: iModbus_coil_arr
            //    register: fExtract_bulbs(iModbus_reg_arr)
            //};
            if (err) { return callback(err); }
            else {
                var base = appl_obj.appl_index;
                var len = cool_master.reg_length;
                var temp_arr = resp.register;
                var arr_map = cool_master.va_to_arr(base);
                //logger.log('info', arr_map);
                if (!oACReg[arr_map[0]]) { oACReg[arr_map[0]] = []; }
                if (!oACReg[arr_map[0]][arr_map[1]]) { oACReg[arr_map[0]][arr_map[1]] = []; }
                for (var i = 0; i < len; i++) {
                    oACReg[arr_map[0]][arr_map[1]][i] = temp_arr[i];
                    //logger.log('info', 'BAM' + temp_arr[i]);
                }
                return callback();
            }
        });
    }, function (err) {
        if (err) {
            //Output error when failed
            if (!_bStandalone_mode) {
                process.send({ error: "cb_read_reg in oModbus_cp of " + oTarget_modbus.client_id });
                process.send({ error: err });
            }
            logger.log('error', err);
        } else {
            logger.log('info', { 'sCoolMaster_status': JSON.stringify(oACReg) });
            if (!_bStandalone_mode) { process.send({ 'sCoolMaster_status': JSON.stringify(oACReg) }); }
            else { process.exit(0); } //Standalone mode end
        }
    });
}

/**
 * Modbus check - called by MQTT Client mqtt-modbus.js
 */

//Return status with IPC package
function fCheck_status() {
    var oResponse = {};
    oResponse.cp_pid = process.pid;
    if (oModbusclient) {
        if (oModbusclient.isConnected())
            oResponse.modbus = 'connected';
        else {
            oResponse.modbus = 'disconnected';
            oResponse.retry_count = iModbus_retry_attempt;
        }
    } else {
        oResponse.modbus = 'undefined';
    }
    logger.log('info', "process.send: " + JSON.stringify({ oCp_status: oResponse }));
    process.send({ oCp_status: oResponse });
}

//Toggle conneciton port from 502 to 505
function fSwitch_port() {
    if (iModbus_port == _iPrimary_modbus_port) { iModbus_port = _iSecondary_modbus_port; }
    else { iModbus_port = _iPrimary_modbus_port; }
}

//Bind oTarget_modbus with loaded oModbus_inf
function fApply_modbus_inf(oLoaded_obj) {
    oTarget_modbus = oLoaded_obj[server_config._sSiteId][_sTarget_client_id];
    logger.log('info', "Client device found");
    if (_bOffline_mode) {
        if (!_bStandalone_mode) {
            process.send({ 'reply': "connected" });
        } else {
            //fRead_modbus_offline();
            fRead_coolmaster_offline();
        }
    }
    else { fModbusclient_definehandler(); }

    //Exit process when there are nothing match to target device with the given information
    if (!oTarget_modbus) {
        logger.log('error', "Client device not found");
        if (!_bStandalone_mode) { process.send({ 'error': "Client device not found" }); }
        process.exit(9);
    }
}

/**
 * Main process session
 */

//Override jsmodbus's original logger
modbus.setLogger(function (msg) {
    logger.log('debug', "oModbusclient: " + msg);
});

if (_bDebug_mode) { logger.level = 'silly'; }

//Startup session
if (_bStandalone_mode) {
    //Add console output and load JSON file (generated from parent process "mqtt-modbus debug")
    logger.log('info', "modbus_childprocess (" + _sTarget_client_id + ") on start with pid = " + process.pid);
    jf.readFile(_sReference_file, function (err, oLoaded_obj) {
        if (err) { logger.log('error', err); process.exit(9); }
        else { fApply_modbus_inf(oLoaded_obj); }
    });
} else {
    logger.log('info', "modbus_childprocess (" + _sTarget_client_id + ") on start...");
    process.send({ reply: "started" });
    //Wait for oModbus_inf
}

//Event handlers for child process mode
if (!_bStandalone_mode) {
    //Mute the console log since there will be tons of them running in the same console

    process.on('message', function (oMes) {
        logger.log('info', "process.on(message): " + JSON.stringify(oMes));
        if (!oMes.tid) { logger.log('error', 'tid is missing!'); }
        if (oMes.sCommand == 'read') {
            if (_bOffline_mode) { fRead_modbus_offline(); }
            else if (oModbusclient && oModbusclient.isConnected()) {
                if (_bDebug_mode) { fRead_modbus_old(); }
                else {
                    icb_read_count = 0;
                    fRead_modbus_coil();
                    //fRead_modbus_reg(icb_read_count);
                }
            }
        } else if (oMes.sCommand == 'read_coolmaster') {
            //New set of functions
            if (_bOffline_mode) { fRead_coolmaster_offline(); }
            else if (oModbusclient && oModbusclient.isConnected()) {
                icb_read_count = 0;
                //fRead_coolmaster_reg(icb_read_count);
                fRead_coolmaster();
            }
        } else if (oMes.sCommand == 'write') {
            fWrite_modbus(oMes);
        } else if (oMes.sCommand == 'write_sch') {
            fWrite_modbus_sch(oMes);
        } else if (oMes.sCommand == 'check_status') {
            fCheck_status();
        }
            /**
            else if (oMes.sCommand == 'switch_port') {
                fSwitch_port();
            } 
            **/
        else if (oMes.oModbus_inf) {
            //logger.log('info', JSON.stringify(oMes.oModbus_inf));
            fApply_modbus_inf(oMes.oModbus_inf);
        } else {
            process.send("error: sCommand not support: " + oMes.sCommand);
            logger.log('error', 'sCommand not support: ' + oMes.sCommand);
        }
    });

    process.on('exit', function (code, signal) {
        process.send({ code: code, signal: signal });
    });
}

/**
 * Event emitters of this process (not server nor connection but process itself)
 */

if (_bStandalone_mode) {

    process.on('uncaughtException', function (err) {
        //See Node API for more information - hope logging with logger is still ok
        //e.g. listen EADDRINUSE
        logger.log('error', 'Caught exception (exit instantly): ' + err, function () {
            graceful_exit(1); //Redundancy exit route
        });
    });

    process.on('exit', function (code) {
        //See Node API for more infomration - only sync process is allowed
        //Do all the closing works before reaching here
        logger.log('debug', 'Process exited with code ' + code);
    });

    process.on('SIGINT', function () {
        //See Node API for more infomration
        //Triggered when user press Ctrl-C to "close" the program
        logger.log('debug', "Mosca server is shutting down by SIGINT...");

        //Real exit - or no exit
        graceful_exit(0);
    });

    process.on('SIGTERM', function () {
        //See Node API for more infomration
        //Identical to SIGINT but usually by software (shell?)
        logger.log('debug', "Mosca server is shutting down by SIGTERM...");

        //Real exit - or no exit
        graceful_exit(0);
    });
}

//"Graceful" exiting procedure. Try to run this instead of closing abruptly.
function graceful_exit(code) {
    //Close the stuffs if they exists
    if (oModbusclient) {
        oModbusclient.close();
    }
    process.exit(code != undefined ? code : 1);
}

//End of JavaScript source code