"use strict";
// JavaScript source code
// Note: This script no longer capable for direct access modbus. DB connection is assuemed on.
// 170915: Now it is a standalone process with new structure.
// 160706: This is focused on PLC connection - no serious modification is needed

/**
 * External libraries (npm/node)
 */
let jsmodbus = require('jsmodbus'); //Modbus Client
let async = require('async'); //Flow control
let fs = require('fs');

/**
 * Self-made libraries
 */
let server_config = require('./modules/server_config.js'); //Custom settings
let server_logger = require('./modules/server_logger.js');
let db_connection = require('./modules/db_connection.js');
let cool_master = require('./modules/cool_master.js'); //Cool Master Modbus stuffs

/**
 * Constants
 */
const _sTarget_client_id = process.argv[2] ? process.argv[2] : process.env.ETAG_PLC_CLIENT_ID; //client_id assigned for this MQTT Client (170919) //Process arguement: client_id for taking care
const _bDebug_mode = process.argv.indexOf("debug") >= 0; //If true, spam the console with debug messages
const _bOffline_mode = process.argv.indexOf("offline") >= 0; //If true, no Modbus device will be connected. Outputs are generated instead
const _bOverride_host = process.argv.indexOf("host_mod") >= 0; //If true, ip address is overrided to 192.168.1.30
const _bOverride_port = process.argv.indexOf("port_mod") >= 0; //If true, port is overrided to secondary port

const _iPrimary_modbus_port = server_config._iPrimary_modbus_port; //Hardcoded, non-documented
const _iSecondary_modbus_port = server_config._iSecondary_modbus_port; //Hardcoded, non-documented
const _iModbus_poll_interval = server_config._iModbus_poll_interval;
const _oModbus_read_range = server_config._oModbus_read_range;
const _oIDObj = { siteId: server_config._sSiteId, client_id: _sTarget_client_id }; //Used for DB access + logging.
const _oModbus_connection_config = server_config._oModbus_connection_config;
const _sPLC_TypeName = server_config._sPLC_TypeName;
const _sCoolMaster_TypeName = server_config._sCoolMaster_TypeName;
const _sOverrided_host = server_config._sOverrided_host;

/**
 * Objects form external source
 */
var oTarget_modbus = null; //Target modbus device from oModbus_inf, see plc_zone_appl.js

/**
 * Internal global objects
 */
let logger = server_logger.SERVER_LOGGING(`${server_config._sLOG_DIR}/modbus_control_${_sTarget_client_id}.log`, { level: _bDebug_mode ? "debug" : "info", mute_console: false }); //Logger of this module
let oModbusclient = null; //jsmodbus client object
let iModbus_port = _iPrimary_modbus_port; //Port number for connecting Modbus device
let rwloop_interval = null; //Interval object of reading modbus
let bModbus_connected = false; //Status flag
let bRWLock = false; //IO lock 

/**
 * General functions (Stage 1)
 */

let o_Err = function (msg) {
    let o = Object.assign({}, _oIDObj);
    o.err = msg;
    return o;
};

let makePartialDeviceStatus = function (resp, addr) {
    let o = Object.assign({}, _oIDObj);
    if (resp && resp.coils) {
        o.coils = resp.coils.map((d, i, a) => { return { address: addr + i, value: d }; });
    }
    if (resp && resp.register) {
        o.register = resp.register.map((d, i, a) => { return { address: addr + i, value: d }; });
    }
    o.zoneId = oTarget_modbus ? oTarget_modbus.zoneId : "";
    o.ip_addr = oTarget_modbus ? oTarget_modbus.ip_addr : "";
    o.connect_state = bModbus_connected || _bOffline_mode ? "connected" : oModbusclient ? "disconnected" : "undefined"; //
    o.last_updated = new Date();
    o.client_pid = process.pid;
    return o;
};

/**
 * Modbus definiation/ connection/ reconnection
 */

//Defination of client object with event handlers
let fModbusclient_definehandler = function () {
    // Close connection if abaliable
    if (oModbusclient !== null) {
        oModbusclient.close();
        oModbusclient.connect();
        return;
    }

    // Release if defined
    oModbusclient = null;
    logger.log('info', `Connecting to Modbus device: modbus://${oTarget_modbus.ip_addr}:${iModbus_port}`);
    //oModbusclient = modbus.createTCPClient(iModbus_port, oTarget_modbus.ip_addr, cb_modbuserror);
    oModbusclient = jsmodbus.client.tcp.complete(_oModbus_connection_config({
        host: oTarget_modbus.ip_addr,
        port: iModbus_port,
        debug_mode: _bDebug_mode
    }));

    //Event handlers
    oModbusclient.on('connect', () => {
        logger.log('info', "oModbusclient.on(connect)");
        bModbus_connected = true;
        modbus_on_connect(oModbusclient);
    });

    oModbusclient.on('data', () => {
        logger.log('debug', "oModbusclient.on(data)");
    });

    oModbusclient.on('close', () => {
        logger.log('info', "oModbusclient.on(close)");
        bModbus_connected = false;

        db_connection.Device_Status.findOneAndUpdate(_oIDObj, { $set: makePartialDeviceStatus(null, null, null) }, { upsert: true, new: true }, (err) => {
            logger.log(err ? "error" : "info", err ? err : "Device_Status is  updated. oModbusclient is still alive.");
        });
    });

    oModbusclient.on('error', (err) => {
        //Usually "util.ETIMEOUT" or "util.ECONNREFUSED" or "util.EHOSTUNREACH"
        logger.log('error', "oModbusclient.on('error')");
        logger.log('error', err);
    });

    oModbusclient.on('end', () => {
        logger.log('info', "oModbusclient.on(end)");
        bModbus_connected = false;

        db_connection.Device_Status.findOneAndUpdate(_oIDObj, { $set: makePartialDeviceStatus(null, null, null) }, { upsert: true, new: true }, (err) => {
            logger.log(err ? "error" : "info", err ? err : "Device_Status is  updated. oModbusclient is still alive.");
        });
    });

    oModbusclient.connect();
};

//Callback of reconnecting Modbus client
let cb_reconnect_modbus = function () {
    logger.log('info', "reconnecting oModbusclient...");
    fModbusclient_definehandler();
};

//Bind oTarget_modbus with loaded oModbus_inf
let fApply_modbus_inf = function (err, oLoaded_obj) {
    if (err) {
        logger.log('error', err);
        process.exit(9);
    } else if (!oLoaded_obj) {
        logger.log('error', JSON.stringify(o_Err("Device information not found")));
        process.exit(9);
    } else {
        oTarget_modbus = oLoaded_obj.toObject();
        oTarget_modbus.ip_addr = _bOverride_host ? _sOverrided_host : oTarget_modbus.ip_addr;
        iModbus_port = _bOverride_port ? _iSecondary_modbus_port : _iPrimary_modbus_port;

        //Exit process when there are nothing match to target device with the given information
        if (!oTarget_modbus) {
            logger.log('error', JSON.stringify(o_Err("Device information not valid")));
            process.exit(9);
        } else {
            logger.log('info', "Client device found");
            if (!_bOffline_mode) {
                fModbusclient_definehandler();
                rwloop_refresh();
            } else {
                rwloop_refresh(); //Skip connecting Modbus
            }
        }
    }
};

/**
 * General functions (Stage 2)
 */

//Triggered by _bOffline_mode. Section of hard-coded logic.
let random_values = function (client_type, mem_type, from, to) {
    logger.log('debug', `random_value(${client_type}, ${mem_type})`);
    let ran_arr = [];
    let default_arr = [];

    for (let i = from; i <= to; i++) {
        default_arr.push(Math.random() >= 0.5 ? 1 : 0);
    }

    if (client_type === "PLC") {
        if (mem_type === "coils") {
            for (let i = from; i <= to; i++) {
                ran_arr.push(Math.random() >= 0.5 ? 1 : 0);
            }
        } else if (mem_type === "register") {
            for (let i = from; i <= to; i++) {
                ran_arr.push(Math.floor(Math.random() * 255));
            }
        } else {
            logger.log('error', `random_value: mem_type ${client_type}.${mem_type} is not recgonized! Binary random value will be generated instead!`);
            return default_arr;
        }
    } else if (client_type === "CoolMasterNet") {
        if (mem_type === "register") {
            for (let i = from; i <= to; i += cool_master.reg_length) {
                ran_arr.push.apply(ran_arr, cool_master.gen_random_arr());
            }
        } else {
            logger.log('error', `random_value: mem_type ${client_type}.${mem_type} is not recgonized! Binary random value will be generated instead!`);
            return default_arr;
        }
    } else {
        logger.log('error', `random_value: client_type ${client_type} is not recgonized! Binary random value will be generated instead!`);
        return default_arr;
    }

    return ran_arr;
};

/**
 * Modbus write
 */

let fufill_cmd = function (d_cmd, cb) {
    let device_cmd = d_cmd.toObject();
    device_cmd.last_completed = new Date();
    return db_connection.Device_Command.findOneAndUpdate({ _id: d_cmd._id }, { $set: device_cmd }, { upsert: true, new: true }, cb);
};

let parse_and_write_cmd = function (d_cmd, cb) {
    // {"_id":"59c8af2664566bc0b8dfd658","client_id":"PLC_2a","siteId":"PuiChing_P","__v":0,"last_completed":null,"last_issued":"2017-09-25T07:24:22.424Z","modbus_fc":5,"fc_content":{"address":0,"value":[1]}}
    logger.log('debug', "Command found: " + JSON.stringify(d_cmd));
    let ModbusFn = null;
    if (_bOffline_mode) { return fufill_cmd(d_cmd, cb); } //Skip if in offline mode
    if (!oModbusclient) { logger.log("warn", "oModbusclient is not found!"); return cb(); }
    else if (!(d_cmd.modbus_fc && d_cmd.fc_content && d_cmd.fc_content.address >= 0 && d_cmd.fc_content.value && d_cmd.fc_content.value.length > 0)) {
        return cb("Device_Command is invalid!");
    } else {
        //logger.log('debug', [d_cmd.fc_content.address, d_cmd.fc_content.value[0]]);
        switch (d_cmd.modbus_fc) {
            case 5: oModbusclient.writeSingleCoil(d_cmd.fc_content.address, d_cmd.fc_content.value[0]).then((resp) => {
                logger.log('debug', JSON.stringify(resp));
                return fufill_cmd(d_cmd, cb);
            }, cb); break;
            case 6: oModbusclient.writeSingleRegister(d_cmd.fc_content.address, d_cmd.fc_content.value[0]).then((resp) => {
                logger.log('debug', JSON.stringify(resp));
                return fufill_cmd(d_cmd, cb);
            }, cb); break;
            case 15: oModbusclient.writeMultipleCoils(d_cmd.fc_content.address, d_cmd.fc_content.value).then((resp) => {
                logger.log('debug', JSON.stringify(resp));
                return fufill_cmd(d_cmd, cb);
            }, cb); break;
            case 16: oModbusclient.writeMultipleRegisters(d_cmd.fc_content.address, d_cmd.fc_content.value).then((resp) => {
                logger.log('debug', JSON.stringify(resp));
                return fufill_cmd(d_cmd, cb);
            }, cb); break;
            default: return cb("Device_Command.modbus_fc is not supported!");
        }
    }
};

let read_cmd = function (cb) {
    db_connection.Device_Command.findOne(_oIDObj, {}, { sort: { 'last_issued': -1 } }, (err, d_cmd) => {
        if (err) { return cb(err); }
        else {
            if (!d_cmd) {
                logger.log('debug', "There is no command for this client."); return cb();
            } else if (d_cmd.last_completed && d_cmd.last_completed.getTime() >= d_cmd.last_issued) {
                logger.log('debug', "The newest command has been completed."); return cb();
            } else {
                parse_and_write_cmd(d_cmd, cb);
            }
        }
    });
};

/**
 * Modbus read
 */

let fDump_generalized = function (in_param, cb) {
    let oReadRange = in_param.oReadRange;
    //let iReadUnit = server_config._oModbus_read_unit[in_param.sMemType];

    if (oReadRange.to - oReadRange.from < 0) {
        logger.log('debug', `${in_param.fName}():Skipped.`);
        return cb();
    } else if (_bOffline_mode) {
        logger.log('debug', `${in_param.fName}():${in_param.sMemType}#${oReadRange.from}-${in_param.sMemType}#${oReadRange.to}, Offline mode`);
        //TODO: Generate random data
        let oDump_arr = {};
        oDump_arr[in_param.sMemType] = random_values(in_param.sClientType, in_param.sMemType, oReadRange.from, oReadRange.to);
        return db_connection.Device_Status.findOneAndUpdate(_oIDObj, { $set: makePartialDeviceStatus(oDump_arr, oReadRange.from, in_param.sMemType) }, { upsert: true, new: true }, cb);
    } else if (oModbusclient) {
        logger.log('debug', `${in_param.fName}():${in_param.sMemType}#${oReadRange.from}-${in_param.sMemType}#${oReadRange.to}, Modbus Connected: ${bModbus_connected}`);
        //Read blindly - since this client don't know where is the information
        if (bModbus_connected) {
            let fCall_arr = [];
            let oDump_arr = false;
            for (let i = oReadRange.from; i <= oReadRange.to; i += oReadRange.unit) {
                fCall_arr.push({ from: i, unit: oReadRange.unit + i > oReadRange.to ? oReadRange.to - i : oReadRange.unit });
            }
            logger.log('debug', JSON.stringify(fCall_arr));
            async.eachSeries(fCall_arr, (oCall_Obj, cb_in) => {
                let cb_read = function (resp) {
                    // resp will look like { fc: 1, byteCount: 20, coils: [ values 0 - 13 ], payload: <Buffer> }  
                    // console.log(resp);
                    //logger.log('debug', JSON.stringify(resp));
                    //logger.log('debug', oDump_arr);
                    if (!oDump_arr) { oDump_arr = resp; }
                    else {
                        //logger.log('debug', "Got response.");
                        oDump_arr.byteCount += resp.byteCount;
                        oDump_arr[in_param.sMemType].push.apply(oDump_arr[in_param.sMemType], resp[in_param.sMemType]);
                        oDump_arr.payload = Buffer.concat([oDump_arr.payload, resp.payload]);
                    }
                    return cb_in();
                };

                let err_buf = function () {
                    let s = "";
                    for (let i = 0; i < oReadRange.unit * 4; i++) {
                        s += "f";
                    }
                    return Buffer.from(s, 'hex');
                };

                let err_arr = function () {
                    let a = [];
                    for (let i = 0; i < oReadRange.unit; i++) {
                        a.push(-1);
                    }
                    return a;
                }

                let cb_err = function (err) {
                    logger.log("error", err);
                    logger.log("warn", "Dumping parameter encountered error: " + JSON.stringify({
                        oCall_Obj: oCall_Obj,
                        in_param: in_param
                    }));
                    let resp = {
                        fc: 3,
                        byteCount: oReadRange.unit * 2,
                        register: err_arr(),
                        payload: err_buf()
                    };
                    if (!oDump_arr) { oDump_arr = resp; }
                    else {
                        //logger.log('debug', "Got response.");
                        oDump_arr.byteCount += resp.byteCount;
                        oDump_arr[in_param.sMemType].push.apply(oDump_arr[in_param.sMemType], resp[in_param.sMemType]);
                        oDump_arr.payload = Buffer.concat([oDump_arr.payload, resp.payload]);
                    }
                    return cb_in();
                };

                if (in_param.sMemType === "register") {
                    oModbusclient.readHoldingRegisters(oCall_Obj.from, oCall_Obj.unit).then(cb_read, in_param.sClientType === "CoolMasterNet" ? cb_err : cb_in);
                } else if (in_param.sMemType === "coils") {
                    oModbusclient.readCoils(oCall_Obj.from, oCall_Obj.unit).then(cb_read, cb_in);
                } else {
                    return cb_in(`Following Memory Type is not supported: ${in_param.sMemType}`);
                }
            }, (err) => {
                if (err) { return cb(err); }
                else {
                    let oPartialDeviceStatus = makePartialDeviceStatus(oDump_arr, oReadRange.from, in_param.sMemType);
                    if (_bDebug_mode) {
                        logger.log('debug', "Read completed.");
                        //logger.log('debug', JSON.stringify(oPartialDeviceStatus));
                    }
                    return db_connection.Device_Status.findOneAndUpdate(_oIDObj, { $set: oPartialDeviceStatus }, { upsert: true, new: true }, cb);
                }
            });
        } else {
            logger.log('debug', "Modbus is not connected!");
            return db_connection.Device_Status.findOneAndUpdate(_oIDObj, { $set: makePartialDeviceStatus(null, null, null) }, { upsert: true, new: true }, cb);
            //return cb();
        }
    } else {
        logger.log("warn", "Modbus is not defined!");
        return db_connection.Device_Status.findOneAndUpdate(_oIDObj, { $set: makePartialDeviceStatus(null, null, null) }, { upsert: true, new: true }, cb);
        //return cb("Logic error: Modbus client is not found!");
    }
};

let write_mdump = function (cb) {
    logger.log('debug', "write_mdump()");
    //logger.log('debug', JSON.stringify(oTarget_modbus));
    if (!oTarget_modbus.client_type) {
        logger.log('error', `DB.Device.client_type is undefined!`); return cb();
    } else if (!_oModbus_read_range[oTarget_modbus.client_type]) {
        logger.log('error', `Read range of client_type (${oTarget_modbus.client_type}) is undefined!`); return cb();
    } else {
        let oParamArr = [];
        for (let sMemType in _oModbus_read_range[oTarget_modbus.client_type])
            if (_oModbus_read_range[oTarget_modbus.client_type].hasOwnProperty(sMemType))
                oParamArr.push({
                    oReadRange: _oModbus_read_range[oTarget_modbus.client_type][sMemType],
                    fName: `fDump_${oTarget_modbus.client_type}_${sMemType}`,
                    sMemType: sMemType,
                    sClientType: oTarget_modbus.client_type
                });
        return async.eachSeries(oParamArr, fDump_generalized, cb);
    }
};

/**
 * Process flow
 */

let rwloop = function () {
    logger.log('debug', `rwloop(): RW in progress: ${bRWLock}`);
    if (!bRWLock) {
        bRWLock = true;
        async.series([read_cmd, write_mdump], (err) => {
            if (err) { logger.log('error', err); }
            else { logger.log('debug', 'rwloop return.'); }
            bRWLock = false;
        });
    } else {
        logger.log('debug', 'Skipped: rwloop in progress.');
    }
};

let rwloop_refresh = function () {
    //logger.log('info', "rwloop_refresh()");
    if (rwloop_interval) clearInterval(rwloop_interval);
    rwloop_interval = setInterval(rwloop, _iModbus_poll_interval);
};

let modbus_on_connect = function (oModbusclient) {
    //Insert any task you want. However the rwloop already does everything you want.
    if (_bOffline_mode) {
        logger.log('info', "The client is running in offline mode.");
    }
    if (_bDebug_mode) {
        logger.log('info', "The client is running in debug mode.");
    }
    rwloop_refresh();
};


//"Graceful" exiting procedure. Try to run this instead of closing abruptly.
let graceful_exit = function (code) {
    //Close the stuffs if they exists
    let exit_modbus = function (cb) {
        if (oModbusclient)
            oModbusclient.close();
        return cb();
    };

    let exit_db = function (cb) {
        if (db_connection) {
            db_connection.onExit();
            if (db_connection.mongoose.connection) {
                db_connection.mongoose.connection.close((err) => {
                    logger.log('debug', "Mongoose disconnected from mongoDB.");
                    return cb(err);
                });
            } else {
                return cb();
            }
        }
    };

    async.parallel([exit_modbus, exit_db], (err) => {
        if (err) logger.log('error', err);
        process.exit(code !== undefined ? code : 1);
    });
};

//Event emitters of this process (not server nor connection but process itself). See Node API for more infomration
let set_process_handlers = function () {
    process.on('uncaughtException', (err) => {
        //Hope logging with logger is still ok (e.g. listen EADDRINUSE)
        logger.log('error', 'Caught exception (exit instantly): ' + err, function () {
            graceful_exit(1); //Redundancy exit route
        });
    });

    process.on('exit', (code) => {
        //Only sync process is allowed. Do all the closing works before reaching here
        logger.log('info', 'Process exited with code ' + code);
    });

    process.on('SIGINT', () => {
        //Triggered when user press Ctrl-C to "close" the program
        logger.log('info', "Process is shutting down by SIGINT...");

        //Real exit - or no exit
        graceful_exit(0);
    });

    process.on('SIGTERM', () => {
        //Identical to SIGINT but usually by software (shell?)
        logger.log('info', "Process is shutting down by SIGTERM...");

        //Real exit - or no exit
        graceful_exit(0);
    });
};

/**
 * Main process session
 */

let main = function () {
    logger.log('info', `modbus_control (${_sTarget_client_id}) on start with pid = ${process.pid}`);
    set_process_handlers(process);

    //RIP JSModbus original logger
    //jsmodbus.log = winston.logger.stream;

    db_connection.init(logger, (err) => {
        if (err) { return fApply_modbus_inf(err, null); }
        else {
            logger.log('info', "DB connection is opened by Mongoose.");
            return db_connection.Device.findOne(_oIDObj, fApply_modbus_inf);
        }
    });
};

main();
//End of JavaScript source code