// JavaScript source code

/**
 * External libraries (npm/node)
 */
var fs = require('fs'); //File stream

/**
 * Internal libraries
 */
var server_config = require('./modules/config_adapter');
var server_logger = require('./modules/server_logger');
var oDataBase = require('./modules/db_connection');
var mqtt_client = require('./modules/mqtt_client');
var load_xml = require('./modules/load_xml');
var plc_zone_appl = require('./modules/plc_zone_appl');
var mqtt_modbus_ipc = require('./modules/mqtt_modbus_ipc');
var cool_master = require('./modules/cool_master');

/**
 * Objects form external source
 */
var oConfig; //From mqtt-client.xml - HUGE object
var oModbus_inf = []; //Array of Device from DB. (Client side) 
var oZone_inf = {}; //Array of Device from DB. (Zone side)
//var oModbus_inf2 = []; //Hardcoded modbus device informtion from document

/**
 * Constants
 */
const
    _sClient_id = process.argv[2] ? process.argv[2] : process.env.ETAG_PLC_CLIENT_ID, //client_id assigned for this MQTT Client (150824)

    _bDebug_mode = (process.argv.indexOf("debug") >= 0) ? true : false, //If true, spam the console with debug messages
    _bOffline_mode = (process.argv.indexOf("offline") >= 0) ? true : false, //For modbus_childprocess

    _sLogfile_name = server_config._sLOG_DIR + '/mqtt_control.log', //Logfile for this script
    _sConfig_dir = server_config._sCONFIG_DIR, //Config file's directory
    _sModbus_inf_filename = 'offlineinf/device/mqtt-modbus_inf_' + _sClient_id + '.json', //Offline version of oModbus_inf when DB is not accessable
    _sZone_inf_filename = 'offlineinf/zoneinf/mqtt-modbus_zoneinf_' + _sClient_id + '.json'; //Offline version of oZone_inf when DB is not accessable

/**
 * Internal global objects
 */
var logger = server_logger.SERVER_LOGGING(_sLogfile_name, { level: _bDebug_mode ? "debug" : "info" }); //Set logger
var oDevice = {}; //DB Schema Objects
var oMqttclient = {}; //MQTT Client object - 1 per script

var oZone_map = {}; //Mapping for zoneId under client_id ( e.g. oZone_map["PLC_1"] = ["zone1", "zone2"] )

var oCheck_modbus_interval_obj; //Interval object

var iPerformance_time = 0; //For performance check

//Connect the DB, send query for oConfiguration (mapping) and then oDevice (target),
//And point them as global oModbus_inf
//(_bDebug_mode): Generate JSON file for debugging
var fRequest_modbus_inf = function (siteId, mqttclient_id, callback) {
    logger.log('debug', "fRequest_modbus_inf(" + siteId + ',' + mqttclient_id + ")");
    oDataBase.init(logger, function (err) {
        //return err ? callback(err) : fDB_on_connected(siteId, mqttclient_id);
        //fDB_on_connected(siteId, mqttclient_id);
        //return err ? callback(err) : callback(null);
        if (err) { return callback(err); }
        else {
            oDevice = oDataBase.Device; //DB Schema Objects
            fDB_on_connected(siteId, mqttclient_id, function (err) {
                return err ? callback(err) : callback(null);
            });
        }
    });
}

var fDB_on_connected = function (siteId, mqttclient_id, callback) {
    logger.log('debug', "siteId = " + siteId + ", mqttclient_id = " + mqttclient_id);
    //logger.log('debug', oDataBase.Device);
    //logger.log('debug', oDataBase.Device_Status);
    //return callback(null);

    //Step 1: Get Device doc, convert it to desired format (MQTT > Client > Zone = Appl)
    var oDevice_db_query = {
        siteId: oConfig.siteid[0],
        client_id: _sClient_id
    };

    oDevice.find(oDevice_db_query, function (err, oDevice_docs) {
        if (err) { return callback("Error when accessing config_db: " + err); }
        else if (oDevice_docs.length == 0) {
            return callback("fRequest_modbus_inf: No matching Device found in DB by query: " + JSON.stringify(oDevice_db_query));
        } else {
            plc_zone_appl.build_plc_based_inf(logger, oDevice_docs, function (err, converted_data) {
                if (err) { return callback(err); }
                else {
                    oModbus_inf = converted_data;
                    //_bDebug_mode: Create JSON file for reference and "modbus_childprocess standalone"
                    if (_bDebug_mode) {
                        fs.writeFile(_sConfig_dir + '/' + _sModbus_inf_filename,
                            JSON.stringify(oModbus_inf, null, 4),
                            function (err) {
                                if (err) { logger.log('error', err); process.exit(1); }
                                logger.log('debug', "Offline information file " + _sModbus_inf_filename + " is created.");
                            });
                    }

                    return callback();
                }
            });
        }
    });

}

//(_bOffline_mode): Offline version of fRequest_modbus_inf
//Instead of generating files in _bDebug_mode, it reads predefined files (Copied from DB).
//Make sure it is already filtered in DB because of predefined DB structure
function fRequest_modbus_inf_offline(siteId, client_id, callback) {
    logger.log('info', "Offline mode: Reading " + _sModbus_inf_filename);
    fs.readFile(_sConfig_dir + '/' + _sModbus_inf_filename, function (err, data) {
        if (err) { return callback(err); }
        else {
            try {
                oModbus_inf = JSON.parse(data);
            } catch (e) {
                return callback("fRequest_modbus_inf_offline: content in file " + _sModbus_inf_filename + " is not JSON String.");
            }
            return callback();
        }
    });
};

/**
 * Main process session
 */

//Fire the first message with logger
logger.log('info', "MQTT-Client on start with pid = " + process.pid);

//Check process arguement
if (!_sClient_id) {
    logger.log('error', "Process arguement (client_id) is missing; process cannot be started.");
    process.exit(9);
} else {
    logger.log('info', "client_id assigned for MQTT-Client = " + _sClient_id);
}

load_xml.loadForMQTTControl(logger, _sClient_id, function (err, config_from_xml) {
    if (err) { logger.log('error', err); process.exit(1); }
    else {
        oConfig = config_from_xml;
        logger.log('debug', "Booting MQTT Client...");
        var MQTTonConnected = function () {
            //Define modbus child process
            mqtt_modbus_ipc.boot_ipc(logger, oMqttclient, oConfig, oModbus_inf, _bOffline_mode, _bDebug_mode);
            mqtt_client.subscribe_for_plc(logger, oModbus_inf[oConfig.siteid[0]][_sClient_id]);
        };

        var MQTTonMessage = function (sTopic, bMessage) {
            //#PERFORMANCE CHECK
            iPerformance_time = new Date().getTime();
            var client_type = oModbus_inf[oConfig.siteid[0]][_sClient_id].client_type;
            logger.log('debug', 'start of analysis zone status: ' + iPerformance_time);
            if (client_type == "PLC") {
                plc_zone_appl.fAnalysis_set_status(logger, oConfig, oModbus_inf[oConfig.siteid[0]][_sClient_id], sTopic, bMessage, function (err, oWrite_obj1, oWrite_obj2) {
                    //#Performance check
                    var iTime_amount = new Date().getTime() - iPerformance_time;
                    logger.log('debug', 'end of analysis zone set status: ' + iTime_amount + "ms");
                    if (err) { logger.log('error', err); logger.log('error', "sTopic = " + sTopic + ", bMessage = " + bMessage); }
                    else {
                        //Step 3: Find oModbus_cp with the client_id
                        var oModbus_ojbect = mqtt_modbus_ipc.search_oModbus_object(_sClient_id);
                        if (!oModbus_ojbect) {
                            return logger.log('error', 'fAnalysis_set_status: oModbus_object not found with client_id: ' + _sClient_id);
                        } else if (!oModbus_ojbect.oModbus_cp) {
                            return logger.log('error', 'fAnalysis_set_status: oModbus_cp not found in oModbus_ojbect with client_id: ' + _sClient_id);
                        }
                        if (oWrite_obj1) {
                            //Step 6: All checking passed. Found all target. Now fire it.
                            logger.log('info', "IPC to ModbusCp(" + oModbus_ojbect.oModbus_cp.pid
                                + "), oWrite_obj: " + JSON.stringify(oWrite_obj1));
                            oModbus_ojbect.oModbus_cp.send(oWrite_obj1);
                        }
                        if (oWrite_obj2) {
                            //Step 6.1: Exceptional case: 'fire event' for Thermo
                            logger.log('info', "IPC to ModbusCp(" + oModbus_ojbect.oModbus_cp.pid
                                + "), oWrite_obj: " + JSON.stringify(oWrite_obj2));
                            oModbus_ojbect.oModbus_cp.send(oWrite_obj2);
                        }
                        logger.log('debug', "setStatus sent.");
                    }
                });
            } else if (client_type == "CoolMasterNet") {
                cool_master.fAnalysis_set_coolmaster(logger, oConfig, oModbus_inf[oConfig.siteid[0]][_sClient_id], sTopic, bMessage, function (err, oWrite_obj1, oWrite_obj2) {
                    //#Performance check
                    var iTime_amount = new Date().getTime() - iPerformance_time;
                    logger.log('debug', 'end of analysis zone set status: ' + iTime_amount + "ms");
                    if (err) { logger.log('error', err); logger.log('error', "sTopic = " + sTopic + ", bMessage = " + bMessage); }
                    else {
                        //Step 3: Find oModbus_cp with the client_id
                        var oModbus_ojbect = mqtt_modbus_ipc.search_oModbus_object(_sClient_id);
                        if (!oModbus_ojbect) {
                            return logger.log('error', 'fAnalysis_set_status: oModbus_object not found with client_id: ' + _sClient_id);
                        } else if (!oModbus_ojbect.oModbus_cp) {
                            return logger.log('error', 'fAnalysis_set_status: oModbus_cp not found in oModbus_ojbect with client_id: ' + _sClient_id);
                        }
                        if (oWrite_obj1) {
                            //Step 6: All checking passed. Found all target. Now fire it.
                            logger.log('info', "IPC to ModbusCp(" + oModbus_ojbect.oModbus_cp.pid
                                + "), oWrite_obj: " + JSON.stringify(oWrite_obj1));
                            oModbus_ojbect.oModbus_cp.send(oWrite_obj1);
                        }
                        if (oWrite_obj2) {
                            //Step 6.1: Exceptional case: 'fire event' for Thermo
                            logger.log('info', "IPC to ModbusCp(" + oModbus_ojbect.oModbus_cp.pid
                                + "), oWrite_obj: " + JSON.stringify(oWrite_obj2));
                            oModbus_ojbect.oModbus_cp.send(oWrite_obj2);
                        }
                        logger.log('debug', "setStatus sent.");
                    }
                });
            } else {
                logger.log('error', 'client_type (' + client_type + ')not supported under receiving MQTT message. Ignored.');
            }
        };

        var MQTT_URL = 'mqtt://' + oConfig.server[0] + ':' + oConfig.port[0] + '/server';

        var MQTTonDisconnected = function () {
            if (oCheck_modbus_interval_obj) { clearInterval(oCheck_modbus_interval_obj); }
        }

        var MQTTBoot = function (err) {
            if (err) { logger.log('error', err); process.exit(9); }
            else { oMqttclient = mqtt_client.boot_mqtt_basic(logger, MQTT_URL, MQTTonConnected, MQTTonMessage, MQTTonDisconnected); }
        }

        //If no error occured, oConfig and oModbus_inf will be defined before callback
        //If error occured, exit process directly.
        if (_bDebug_mode) {
            fRequest_modbus_inf(oConfig.siteid[0], _sClient_id, MQTTBoot);
        } else if (_bOffline_mode) {
            fRequest_modbus_inf_offline(oConfig.siteid[0], _sClient_id, MQTTBoot);
        } else {
            fRequest_modbus_inf(oConfig.siteid[0], _sClient_id, MQTTBoot);
        }
    }
})

/**
 * Event emitters of this process (not server nor connection but process itself)
 */
/**
process.on('uncaughtException', function (err) {
    //See Node API for more information - hope logging with logger is still ok
    //e.g. listen EADDRINUSE
    logger.log('error', 'Caught exception (exit instantly): ' + err);

    graceful_exit(1); //Redundancy exit route
});
**/
process.on('exit', function (code) {
    //See Node API for more infomration - only sync process is allowed
    //Do all the closing works before reaching here
    logger.log('debug', 'Process exited with code ' + code);
});

process.on('SIGINT', function () {
    //See Node API for more infomration
    //Triggered when user press Ctrl-C to "close" the program
    logger.log('debug', "MQTT client is shutting down by SIGINT...");

    //Real exit - or no exit
    graceful_exit(0);
});

process.on('SIGTERM', function () {
    //See Node API for more infomration
    //Identical to SIGINT but usually by software (shell?)
    logger.log('debug', "MQTT client is shutting down by SIGTERM...");

    //Real exit - or no exit
    graceful_exit(0);
});

//"Graceful" exiting procedure. Try to run this instead of closing abruptly.
function graceful_exit(code) {
    //Close the stuffs if they exists
    if (oDataBase.mongoose.connection) {
        oDataBase.mongoose.disconnect(function () {
            logger.log('debug', 'Mongoose disconnected.');
            if (oMqttclient) {

                //mqtt.Client#end([force], [cb]) <- See MQTT API description
                oMqttclient.end(false, function () {
                    logger.log('debug', 'MQTT Client closed.'); //Never log for some reason
                    process.exit(code != undefined ? code : 1); //But will run this
                });

            } else {
                process.exit(code != undefined ? code : 1);
            }
        });
    } else {
        process.exit(code != undefined ? code : 1);
    };
};
//End of JavaScript source code