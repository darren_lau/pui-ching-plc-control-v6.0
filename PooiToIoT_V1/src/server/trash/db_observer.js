"use strict";
// JavaScript source code
// 170920: Now the script will read for all devices within the given site. Binding logic will be groupped once again.

/**
 * External libraries (npm/node)
 */
let async = require('async'); //Flow control
let fs = require('fs'); //File stream

/**
 * Internal libraries
 */
let server_config = require('./modules/server_config.js');
let server_logger = require('./modules/server_logger.js');
let db_connection = require('./modules/db_connection.js');

let load_xml = require('./modules/load_xml.js');
let mapping_logic = require('./modules/mapping_logic.js');

/**
 * Objects form external source
 */
let oDeviceINF_XML = {}; //Device config in XML format { client_id, XML docuement content bounded with client_id }
let oDeviceINF_DB = {}; //Array of Device from DB. { client_id, DB.Device bounded with client_id }
let oDeviceINF_PARSED = {}; //Parsed oDeviceINF_DB for mapping_logic

/**
 * Constants
 */
const _bDebug_mode = process.argv.indexOf("debug") >= 0 ? true : false; //If true, spam the console with debug messages
const _sConfig_dir = server_config._sCONFIG_DIR; //Config file's directory
const _oDeviceQuery = { siteId: server_config._sSiteId };
const _iModbus_poll_interval = server_config._iModbus_poll_interval;

/**
 * Internal global objects
 */

let logger = server_logger.SERVER_LOGGING(`${server_config._sLOG_DIR}/db_observer.log`, { level: _bDebug_mode ? "debug" : "info", mute_console: false }); //Logger of this module
let rwloop_interval = null; //Interval object of reading modbus
let bRWLock = false; //IO lock 

/**
 * General functions (Stage 1)
 */

let o_Err = function (msg) {
    let o = Object.assign({}, _oIDObj);
    o.err = msg;
    return o;
};

/**
 * Process flow
 * May not be optimized since the process flow may expand
 */

let rwloop = function () {
    logger.log('debug', `rwloop(): RW in progress: ${bRWLock}`);
    if (!bRWLock) {
        bRWLock = true;
        db_connection.Device_Status.find(_oDeviceQuery, (err, oDeviceStatus) => {
            if (err) { logger.log('error', err); bRWLock = false;  }
            else {
                mapping_logic.parse_device_status(logger, oDeviceStatus, oDeviceINF_XML, oDeviceINF_DB, oDeviceINF_PARSED, (err, mqtt_msg_arr) => {
                    if (_bDebug_mode) {
                        let filepath = `${server_config._sLOG_DIR}/mqtt_msg_arr.json`;
                        fs.writeFile(filepath, JSON.stringify(mqtt_msg_arr, null, 4), "utf-8", (err) => {
                            logger.log(err ? "error" : "debug", err ? err : `File dumped to ${filepath}`);
                        });
                    }
                    try {
                        mqtt_msg_arr.forEach((mqtt_msg_device) => {
                            mqtt_msg_device.forEach((mqtt_msg_device_zone) => {
                                let mqtt_topic = mqtt_msg_device_zone.sTopic;
                                logger.log('info', mqtt_msg_device_zone.sTopic);
                                let mqtt_msg = mqtt_msg_device_zone.sMessage;
                                let parsed_msg = JSON.parse(mqtt_msg_device_zone.sMessage);
                                parsed_msg.Appliance.forEach((mqtt_msg_appl_type) => {
                                    mqtt_msg_appl_type.devices.forEach((mqtt_msg_appl) => {
                                        logger.log('info', JSON.stringify(mqtt_msg_appl));
                                    });
                                });
                            });
                        });
                    } catch (e) {
                        logger.log('error', e);
                    }
                    //more_progress(mqtt_msg_arr);
                    logger.log('debug', 'rwloop return.');
                    bRWLock = false;
                });
            }
        });
    } else {
        logger.log('debug', 'Skipped: rwloop in progress.');
    }
};

let rwloop_refresh = function () {
    if (rwloop_interval) clearInterval(rwloop_interval);
    rwloop_interval = setInterval(rwloop, _iModbus_poll_interval);
};

let fOnExternalDataLoaded = function () {
    //Insert any task you want. However the rwloop already does everything you want.
    if (_bDebug_mode) {
        logger.log('info', "The client is running in debug mode.");
    }
    rwloop_refresh();
};

//Bind oDeviceINF_DB with loaded oDevice_inf
let fApply_device_inf = function (err, oLoaded_obj) {
    if (err) {
        logger.log('error', err);
        process.exit(9);
    } else {
        //Exit process when there are nothing match to target device with the given information
        if (!oLoaded_obj) {
            logger.log('error', JSON.stringify(o_Err("Site information not found")));
            process.exit(9);
        } else {
            oDeviceINF_DB = oLoaded_obj;

            let client_arr = oDeviceINF_DB.map((d, i, a) => { return d.client_id; });
            logger.log('info', "[db_connection] Site information found.");
            logger.log('debug', "Site information found: " + JSON.stringify(client_arr));
            //process.exit(0);
            async.each(client_arr, (sClient_id, cb_in) => {
                load_xml.loadForMQTTControl(logger, sClient_id, (err, config_from_xml) => {
                    if (err) { logger.log('error', err); }
                    else { oDeviceINF_XML[sClient_id] = config_from_xml; }
                    return cb_in();
                });
            }, (err) => {
                if (err) { logger.log('error', err); } //Should not happen. Config file error should be skipped.
                else {
                    logger.log('info', "[load_xml] Device configs are loaded.");
                    if (_bDebug_mode) {
                        let filepath = `${server_config._sLOG_DIR}/client_config_${_oDeviceQuery.siteId}.json`;
                        fs.writeFile(filepath, JSON.stringify(oDeviceINF_XML, null, 4), "utf-8", (err) => {
                            logger.log(err ? "error" : "debug", err ? err : `File dumped to ${filepath}`);
                           
                        });
                    } 
                    mapping_logic.build_plc_based_inf(logger, oDeviceINF_DB, (err, wrapped_oDevice) => {
                        if (err) {
                            logger.log('error', err);
                            process.exit(9);
                        } else {
                            logger.log('info', "[plc_zone_appl] Device info are parsed.");
                            oDeviceINF_PARSED = wrapped_oDevice;
                            fOnExternalDataLoaded();
                        }
                    });
                }
            });
        }
    }
};

//"Graceful" exiting procedure. Try to run this instead of closing abruptly.
let graceful_exit = function (code) {
    //Close the stuffs if they exists
    let exit_db = function (cb) {
        if (db_connection) {
            db_connection.onExit();
            if (db_connection.mongoose.connection) {
                db_connection.mongoose.connection.close((err) => {
                    logger.log('debug', "Mongoose disconnected from mongoDB.");
                    return cb(err);
                });
            } else {
                return cb();
            }
        }
    };

    async.parallel([exit_db], (err) => {
        if (err) logger.log('error', err);
        process.exit(code !== undefined ? code : 1);
    });
};

//Event emitters of this process (not server nor connection but process itself). See Node API for more infomration
let set_process_handlers = function () {
    process.on('uncaughtException', (err) => {
        //Hope logging with logger is still ok (e.g. listen EADDRINUSE)
        logger.log('error', 'Caught exception (exit instantly): ' + err, function () {
            graceful_exit(1); //Redundancy exit route
        });
    });

    process.on('exit', (code) => {
        //Only sync process is allowed. Do all the closing works before reaching here
        logger.log('info', 'Process exited with code ' + code);
    });

    process.on('SIGINT', () => {
        //Triggered when user press Ctrl-C to "close" the program
        logger.log('info', "Process is shutting down by SIGINT...");

        //Real exit - or no exit
        graceful_exit(0);
    });

    process.on('SIGTERM', () => {
        //Identical to SIGINT but usually by software (shell?)
        logger.log('info', "Process is shutting down by SIGTERM...");

        //Real exit - or no exit
        graceful_exit(0);
    });
};

/**
 * Main process session
 */
let main = function () {
    //Fire the first message with logger
    logger.log('info', "DB observer on start with pid = " + process.pid);
    set_process_handlers(process);

    db_connection.init(logger, (err) => {
        if (err) { return fApply_device_inf(err, null); }
        else {
            logger.log('info', "DB connection is opened by Mongoose.");
            return db_connection.Device.find(_oDeviceQuery, fApply_device_inf);
        }
    });
};

main();
//End of JavaScript source code