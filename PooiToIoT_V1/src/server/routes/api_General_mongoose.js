// Warning: Highly general.
// Naming will be general to make it easy to copy
var express = require('express');
var ServerPaging = require('../modules/kGcmd2MongooseQuery');
//var Filtering = require('../server_modules/kGcmd2Sequelizequery');
//var result = f.resolveFilter(req.query.filter);

// Global Object from external
/**
var schema = {};
//var logger = {};
var query_obj = {};
var update_field = {};
**/

module.exports = {
    init: function (l, s, q, u) {
        var logger = l;
        var schema = s;
        var query_obj = q;
        var update_field = u;

        var router = express.Router();

        /* GET home page. */
        router.get('/', function (req, res) {
            res.status(200).send('Hello World!');
        });

        router.get('/read', function (req, res) {
            //logger.log('debug', req.baseUrl);
            var cb_after_read = function (err, doc) {
                if (err) {
                    logger.log('error', err); return res.status(500).send(err);
                } else {
                    return res.status(200).send(doc);
                }
            };

            if (req.baseUrl.toLowerCase().indexOf("device_status") >= 0) {
                //logger.log('debug', 'HI');
                schema.find(req.query).populate('device_ref').exec(cb_after_read);
            } else {
                schema.find(req.query).exec(cb_after_read);
            }
        });

        router.post('/read', function (req, res) {
            //Warning: Complex algo. Need to place to somewhere else.
            //Decoder of Server paging, filtering and sorting.
            //logger.log('debug', '[api_General] BODY = ' + JSON.stringify(req.body, null, 4));
            var q = {};

            try {
                //logger.log('info', req.body);
                q = ServerPaging.resolve(req.body);
                //logger.log('info', JSON.stringify(q));
            } catch (e) {
                return res.status(500).send(e);
            }

            //Bad practice. Not suitable for serious development.
            schema.count(q.query_obj, function (err, count) {
                //logger.log('debug', "Number of users:", count);
                //logger.log('debug', req.baseUrl.toLowerCase().indexOf("device_status"));

                var cb_after_read = function (err, doc) {
                    if (err) {
                        logger.log('error', err); return res.status(500).send(err);
                    } else {
                        return res.status(200).send({ itemCount: count, items: doc });
                    }
                };

                if (req.baseUrl.toLowerCase().indexOf("device_status") >= 0) {
                    schema.find(q.query_obj, q.criteria, q.options).populate('device_ref').exec(cb_after_read);
                } else {
                    schema.find(q.query_obj, q.criteria, q.options).exec(cb_after_read);
                }
            });
        });

        router.post('/update', function (req, res) {
            //logger.log('debug', '[api_General] BODY = ' + JSON.stringify(req.body));
            schema.findOneAndUpdate(query_obj(req.body), { $set: req.body }, { new: true }, function (err, doc) {
                if (err) {
                    logger.log('error', err);
                    res.status(500).send(err.toString());
                } else {
                    res.status(200).send(doc);
                }
            });
        });

        router.post('/create', function (req, res) {
            // Checking for duplicate
            //logger.log('debug', JSON.stringify(query_obJ(req.body)));
            schema.findOne(query_obj(req.body)).exec(function (err, doc) {
                if (err) {
                    logger.log('error', err);
                    res.status(500).send(err.toString());
                    return;
                } else if (doc) {
                    res.status(400).send("Data already created with query " + JSON.stringify(req.body));
                    return;
                } else {
                    // Make new Object to DB
                    var nobj = new schema(req.body);
                    nobj.save(function (err, doc) {
                        if (err) {
                            logger.log('error', err);
                            res.status(500).send(err.toString());
                        } else {
                            res.status(200).send(nobj);
                        }
                    });
                }
            });
        });

        router.post('/destroy', function (req, res) {
            schema.findOneAndRemove(query_obj(req.body), {}, function (err, doc) {
                if (err) {
                    logger.log('error', err);
                    res.status(500).send(err.toString());
                } else {
                    res.status(200).send(doc);
                }
            });
        });

        return router;
    }
};