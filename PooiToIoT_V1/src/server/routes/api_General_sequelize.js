// Warning: Highly general.
// Naming will be general to make it easy to copy
var express = require('express');
var Filtering = require('../modules/kGcmd2Sequelizequery');
//var result = f.resolveFilter(req.query.filter);

// Global Object from external
/**
var schema = {};
//var logger = {};
var query_obj = {};
var update_field = {};
**/

//Copied from api_root. Will block most traffic when it is not logged in.
var ensureAuthenticated = function (req, res, next) {
    if (req.isAuthenticated()) { res.cookie('username', req.user.username); return next(); }
    res.redirect('/login');
};

module.exports = {
    init: function (l, s, q, u, s_include) {
        var logger = l;
        var schema = s;
        var query_obj = q;
        var update_field = u;

        var router = express.Router();

        /* GET home page. */
        router.get('/', function (req, res) {
            res.status(200).send('Hello World!');
        });

        router.get('/read', ensureAuthenticated, function (req, res) {
            schema.findAll({
                where: req.query,
                include: s_include
            }).then(function (result) {
                /**
                if (req.baseUrl.toLowerCase().indexOf("device_status") >= 0) {
                    //Native Javascript won't work
                    result.forEach(function (d) {
                        d.setDataValue("device_ref", d.getDataValue("Device"));
                        d.setDataValue("Device", undefined);
                    });
                }
                **/
                return res.status(200).send(result);
            }).catch(function (err) {
                logger.log('error', err); return res.status(500).send(err);
            });
        });

        router.post('/read', ensureAuthenticated, function (req, res) {
            //Warning: Complex algo. Need to place to somewhere else.
            //Decoder of Server paging, filtering and sorting.
            logger.log('debug', '[api_General] BODY = ' + JSON.stringify(req.body));
            var where = null;
            var order = null;
            try {
                req.body.take = parseInt(req.body.take);
                req.body.skip = parseInt(req.body.skip);
                req.body.page = parseInt(req.body.page);
                req.body.pageSize = parseInt(req.body.pageSize);
                req.body.take = isNaN(req.body.take) ? null : req.body.take;
                req.body.skip = isNaN(req.body.skip) ? null : req.body.skip;
                req.body.page = isNaN(req.body.page) ? null : req.body.page;
                req.body.pageSize = isNaN(req.body.pageSize) ? null : req.body.pageSize;
                var f = new Filtering();
                if (req.body.filter)
                    where = f.resolveFilter(req.body.filter);
                if (req.body.sort)
                    order = f.resolveSort(req.body.sort);
                logger.log('debug', '[api_General] WHERE = ' + JSON.stringify(where));
                logger.log('debug', '[api_General] SORT = ' + JSON.stringify(order));
            } catch (err) {
                logger.log('error', err);
                where = null;
                sort = null;
            }
            schema.findAndCountAll({
                where: where, order: order,
                limit: req.body.pageSize ? req.body.pageSize : undefined,
                offset: req.body.skip ? req.body.skip : undefined,
                attributes: req.body.attributes,
                include: s_include
            }).then(function (result) {
                /**
                if (req.baseUrl.toLowerCase().indexOf("device_status") >= 0) {
                    //Native Javascript won't work
                    result.rows.forEach(function (d) {
                        d.setDataValue("device_ref", d.getDataValue("Device"));
                        d.setDataValue("Device", undefined);
                    });
                }
                **/
                var out = {
                    items: result.rows,
                    itemCount: result.count
                };
                //logger.log('debug', out.itemCount);
                //logger.log('debug', out.items.length);
                return res.status(200).send(out);
            }).catch(function (err) {
                logger.log('error', err); return res.status(500).send({ error: err });
            });
        });

        router.post('/update', ensureAuthenticated, function (req, res) {
            //logger.log('debug', '[api_General] BODY = ' + JSON.stringify(req.body));
            schema.update(update_field(req.body), { where: query_obj(req.body) }).then(function (result) {
                return res.status(200).send(result);
            }).catch(function (err) {
                logger.log('error', err); return res.status(500).send(err);
            });
        });

        router.post('/create', ensureAuthenticated, function (req, res) {
            /**
            schema
                .findOrCreate({ where: query_obj(req.body), defaults: update_field(req.body) })
                .spread(function (result, created) {
                    logger.log('debug', { result: result, created: created });
                    return created ? res.status(200).send(result) : res.status(400).send(result);
                }, function (err) {
                    logger.log('error', err); return res.status(500).send(err);
                });
            **/
            //logger.log('debug', req.body);
            //logger.log('debug', update_field(req.body));
            schema.create(req.body).then(function (result) {
                return result ? res.status(200).send(result) : res.status(400).send(result);
            }).catch(function (err) {
                logger.log('error', err); return res.status(500).send(err);
            });
        });

        router.post('/destroy', ensureAuthenticated, function (req, res) {
            schema.destroy({ where: query_obj(req.body) }).then(function (result) {
                return res.status(200).send({ count: result });
            }).catch(function (err) {
                logger.log('error', err); return res.status(500).send(err);
            });
        });

        return router;
    }
};