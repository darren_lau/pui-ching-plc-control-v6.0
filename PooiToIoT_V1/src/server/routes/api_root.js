"use strict";
var express = require('express');
var router = express.Router();
var passport = require('passport');

// Server config
var server_config = require('../modules/server_config.js'); //Custom settings
var server_logger = require('../modules/server_logger.js');
var mqtt_client = require('../modules/mqtt_client.js');

var logger = server_logger.SERVER_LOGGING(server_config._sLOG_DIR + '/api_root.log', { level: 'debug' }); //using Winston instead of Morgan

var ensureAuthenticated = function (req, res, next) {
    if (req.isAuthenticated()) { res.cookie('username', req.user.username); return next(); }
    res.redirect('/login');
};

/* GET home page. */
router.get('/', function (req, res) {
    res.redirect("/login");
    //res.send("HELLO WORLD!");
});

/**
 * Login/ Logout 
**/
router.get('/login', function (req, res) {
    res.set({ 'charset': 'utf-8' });
    res.render('login.html', { user: req.user, failed: false });
});

router.get('/login_failed', function (req, res) {
    res.set({ 'charset': 'utf-8' });
    res.render('login.html', { user: req.user, failed: true });
});

router.post('/login',
    passport.authenticate('local', { failureRedirect: '/login_failed' }),
    function (req, res) {
        res.set({ 'charset': 'utf-8' });
        res.render('console.html', { user: req.user });
    });

router.post('/login_failed',
    passport.authenticate('local', { failureRedirect: '/login_failed' }),
    function (req, res) {
        res.set({ 'charset': 'utf-8' });
        res.render('console.html', { user: req.user });
    });

router.get('/isloggedin', function (req, res) {
    if (!req.user) { res.send("false"); }
    else { res.send("true"); }
});

router.get('/usertype', function (req, res) {
    if (!req.user) { res.send("null"); }
    else { res.send(req.user.usertype); }
});

router.get('/logout', function (req, res) {
    req.logout();
    res.redirect('/login');
});

/**
 * Schedule
 */
var access_schedule = require('../modules/access_schedule.js');
access_schedule.init(logger);
router.get('/getAllSchedules', access_schedule.getAllSchedule);
router.get('/getTimeSlots', ensureAuthenticated, access_schedule.getTimeSlots);
router.get('/getSchedule_console', access_schedule.getSchedule_console);
router.get('/getSchedule_scheduler', ensureAuthenticated, access_schedule.getSchedule_scheduler); //1 day
router.get('/getSchedule_scheduler_month', ensureAuthenticated, access_schedule.getSchedule_scheduler_month); //1 month
router.post('/editOneDaySchedule', ensureAuthenticated, access_schedule.editOneDaySchedule);

/**
 * MQTT/ Push to PLC
 */
mqtt_client.boot_mqtt_basic(logger, server_config.ET_sMOSCA_URL_MQTT); //(logger_in, URL, onConnect, onMessage)
router.post('/fireMqttMessage', ensureAuthenticated, mqtt_client.fireMqttMessage);
router.post('/refresh_plc', access_schedule.refresh_plc); //Used by refresh_plc_pun, PUSH TOMORROW
router.post('/refresh_plc_console', ensureAuthenticated, access_schedule.refresh_plc_console); //Used by web console, PUSH TODAY
//next_date = new Date( (new Date()).getTime() + 1000 * 60 * 60 *20);
//date_str = moment(next_date).toISOString().substring(0, 10);

/**
 * Device/ Device_Status/ Device_Command
 */
//Wrote by Darren. HTTP query will be put into Mongoose query directly.
//Convinence will be maximum with minimum lines. Add restriction if needed.
//Keep in mind that it is defined as Mongoose Documents.
//Use angular.copy(doc.toObject) if need to modify the format for indirect usage.
var access_plc_device = require('../modules/access_plc_device.js');
access_plc_device.init(logger);
router.get('/getDevices', ensureAuthenticated, access_plc_device.getDevices);

var access_device_status = require('../modules/access_device_status.js');
access_device_status.init(logger);
router.get('/getStatus', ensureAuthenticated, access_device_status.getStatus);

var access_device_command = require('../modules/access_device_command.js');
access_device_command.init(logger);
router.get('/getDeviceCommand', ensureAuthenticated, access_device_command.getCommand);

/**
 * Appliances
 */
//Wrote by Darren. APIs specially for UI. Not documented.
var access_appliance = require('../modules/access_appliance.js');
access_appliance.init(logger);
router.post('/addAppl', ensureAuthenticated, access_appliance.addAppl);
router.post('/editAppl', ensureAuthenticated, access_appliance.editAppl);
router.post('/deleteAppl', ensureAuthenticated, access_appliance.deleteAppl);

/**
 * Zone
 */
var access_zone = require('../modules/access_zone.js');
access_zone.init(logger);
router.post('/addZone', ensureAuthenticated, access_zone.addZone);
router.post('/editZone', ensureAuthenticated, access_zone.editZone);
router.post('/deleteZone', ensureAuthenticated, access_zone.deleteZone);

/**
 * User
 */
//Added by Darren
//Warning: No verification is implemented
//User's CRUD
var access_user = require('../modules/access_user.js');
access_user.init(logger);
router.post('/addUser', ensureAuthenticated, access_user.addUser);
router.get('/getUser', ensureAuthenticated, access_user.getUser);
router.post('/editUser', ensureAuthenticated, access_user.editUser);
router.post('/deleteUser', ensureAuthenticated, access_user.deleteUser);

/**
 * Schedule_Default
 */
//Added by Darren
//Warning: No verification is implemented
//Schedule_def's CRUD
var access_scheduledef = require('../modules/access_scheduledef.js');
access_scheduledef.init(logger);
router.post('/addSchedule_def', ensureAuthenticated, access_scheduledef.addSchedule_def);
router.get('/getSchedule_def', ensureAuthenticated, access_scheduledef.getSchedule_def);
router.post('/editSchedule_def', ensureAuthenticated, access_scheduledef.editSchedule_def);
router.post('/deleteSchedule_def', ensureAuthenticated, access_scheduledef.deleteSchedule_def);

module.exports = router;