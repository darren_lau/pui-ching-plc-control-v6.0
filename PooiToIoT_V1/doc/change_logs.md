# Change Logs (`PooiToIoT_V1` v1_180924) #

- 180924 (v6.0):
	- Added delight control through UDP
	- Frontend modulation as web templates
	- Added chart and time filter support (Currently disabled)
- 180521 (v5.4):
	- Emulator for sensor and UI fix
- 180430 (v5.3):
	- Another Slack Bot for alarm reporting.
- 180218 (v5.2):
	- Attempt to encounter the "Hanging PLC" problem
- 171027 (v5.1):
	- Tons of bugfix. 
	- Modulation optimized. 
	- Log option on more modules.
- 170925 (v5.0):
	- Communication model is modified (Sockets > Observers + Polling)
	- Slack bot to detect disconnection
	- Frontend coding rebuild
- 160830 (v4.2):
	- Bug fix for loading schedule default, parsing MQTT messages
	- Minor UI update
- 160725 (v4.1):
	- Added support for VRV conrol.
- 160715 (v4.0):
    > NOTE: Do not change schema!
    > Codes are rebuilt with a better structure.
    > Build procedure is unchanged.
    > But for changing intervals betweem 15 and 30 minutes, please follow the following step:
    > - Backup everything first.
    > - Change `/config/server/server_config.xml` > `<mode>` - `<ET_iSCHEDULINGINTERVAL>` into `15`/`30`
    > - Drop DB collection `Schedule_dev` (Additional script need to be implemented if you want to preserve events information)
    > - `grunt build --mode=dev` (Rebuild all the codes)
    > - `cd mode/src/task/`, `node set_punDB_ver2` (Mainly Upsert)
    > - `cd mode/src/server/`, `node "servers"` (Start the servers)
- 160309 (v1.5):
    > Note: 
    > The appointed PLC ID is still defined in environment variable.
    > Also, a new account is expected to be created for each new PLCs 
    > (e.g. `PLC_13a`, `PLC_13b`, `PLC_13c`)
    > To run the processes, after typing `grunt build --mode=xxx`, go to folder `xxx` and start each process
