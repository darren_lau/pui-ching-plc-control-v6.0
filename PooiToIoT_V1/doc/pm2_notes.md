# Notes on using PM2 (`PooiToIoT_V1` v1_180924) #

* Official website: http://pm2.keymetrics.io/
* npm page: https://www.npmjs.com/package/pm2
- - - - - - - - -
### Setting pm2 up ###
* Install if it was not installed `sudo npm install -g pm2`
* Start monitoring process by `pm2 start xxx.js --name process_name -e path/err/log -o path/ouput/log -- process_argv more_argv etc`
* View all monitored process: `pm2 list`
* Restart process: `pm2 restart id/ process_name/ all`
* Stop process: `pm2 stop id/ process_name/ all`
* Update pm2 (If notified): `pm2 update` (Stop and start) or `pm2 deepUpdate` (Reinstall and update)
- - - - - - - - -
### Boot pm2 on startup ###
* `pm2 startup` and then pm2 still tell you a longer command. Apply it.
* Start all processes.
* `pm2 save` to save the process list.
- - - - - - - - -
### Manage PM2 logs ###
* *PIPE `stdout` AND `stderr` TO LOG FILE BY DEFAULT*.
* Read http://pm2.keymetrics.io/docs/usage/log-management/ and choose a method
* Currently using `pm2-logrotate` (`pm2 install pm2-logrotate`) from https://github.com/pm2-hive/pm2-logrotate
- - - - - - - - - -
- Process that should be monitored by `pm2`:
	* `node ./PooiToIoT_V1/prod/src/server/express_host.js` (Web console with telnet control page)
	* `node ./PooiToIoT_V1/prod/src/server/device_control.js [client_id]` (Device controller)
	* `node ./PooiToIoT_V1/prod/src/server/mosca_custom.js` (MQTT broker with embedded logic)
	* `node ./PooiToIoT_V1/prod/src/server/mqtt_slack.js` (Alarm monitor)
	* `node ./PooiToIoT_V1/prod/src/server/mqtt_spambot.js` (Sensor emulator)
	* `node ./PooiToIoT_V1/prod/src/server/slack_bot.js` (Watch for abnormal state)