# Command List (`PooiToIoT_V1` v1_180924) #

### General Node Commands ###

`node modbus_control.js PLC_2a debug offline host_mod`

### PM2 Commands ###
```
pm2 start modbus_control.js --name modbus_PLC_2a -- PLC_2a debug offline host_mod
pm2 flush # Clear all the logs
sudo pm2 logrotate -u user
pm2 install pm2-logrotate
```

### Scheduled Tasks ###
- Do it via `crontab -e`.
```
30 23 * * * pm2 restart all
30 23 * * * pm2 flush
0 3 * * * /home/safeuser/plc_server/daily_launcher.sh >> /home/safeuser/plc_server/refresh_plc_pun.log
```

### Standalone mode ###
```
node delight_client.js [brightness]
```